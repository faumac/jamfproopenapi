# EraseDeviceCommandReturnToService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Bool** |  | [optional] 
**mdmProfileData** | **String** | Base64 encoded mdm profile | [optional] 
**wifiProfileData** | **String** | Base64 encoded wifi profile | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


