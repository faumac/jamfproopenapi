# ManagedSoftwareUpdatePlanGroupPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | [**PlanGroupPost**](PlanGroupPost.md) |  | 
**config** | [**PlanConfigurationPost**](PlanConfigurationPost.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


