# ManagedSoftwareUpdateStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**osUpdatesStatusId** | **String** |  | [optional] 
**device** | [**ManagedSoftwareUpdateStatusDevice**](ManagedSoftwareUpdateStatusDevice.md) |  | [optional] 
**downloadPercentComplete** | **Double** |  | [optional] 
**downloaded** | **Bool** |  | [optional] 
**productKey** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**deferralsRemaining** | **Int** | not applicable to all managed software update statuses | [optional] 
**maxDeferrals** | **Int** | not applicable to all managed software update statuses | [optional] 
**nextScheduledInstall** | **Date** | not applicable to all managed software update statuses | [optional] 
**pastNotifications** | **[Date]** | not applicable to all managed software update statuses | [optional] 
**created** | **Date** |  | [optional] 
**updated** | **Date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


