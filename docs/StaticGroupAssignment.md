# StaticGroupAssignment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] [readonly] 
**groupName** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**assignments** | [Assignment] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


