# VolumePurchasingSubscriptionsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1VolumePurchasingSubscriptionsGet**](VolumePurchasingSubscriptionsAPI.md#v1volumepurchasingsubscriptionsget) | **GET** /v1/volume-purchasing-subscriptions | Retrieve Volume Purchasing Subscriptions
[**v1VolumePurchasingSubscriptionsIdDelete**](VolumePurchasingSubscriptionsAPI.md#v1volumepurchasingsubscriptionsiddelete) | **DELETE** /v1/volume-purchasing-subscriptions/{id} | Delete a Volume Purchasing Subscription with the supplied id
[**v1VolumePurchasingSubscriptionsIdGet**](VolumePurchasingSubscriptionsAPI.md#v1volumepurchasingsubscriptionsidget) | **GET** /v1/volume-purchasing-subscriptions/{id} | Retrieve a Volume Purchasing Subscription with the supplied id
[**v1VolumePurchasingSubscriptionsIdPut**](VolumePurchasingSubscriptionsAPI.md#v1volumepurchasingsubscriptionsidput) | **PUT** /v1/volume-purchasing-subscriptions/{id} | Update a Volume Purchasing Subscription
[**v1VolumePurchasingSubscriptionsPost**](VolumePurchasingSubscriptionsAPI.md#v1volumepurchasingsubscriptionspost) | **POST** /v1/volume-purchasing-subscriptions | Create a Volume Purchasing Subscription


# **v1VolumePurchasingSubscriptionsGet**
```swift
    open class func v1VolumePurchasingSubscriptionsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: VolumePurchasingSubscriptions?, _ error: Error?) -> Void)
```

Retrieve Volume Purchasing Subscriptions

Retrieves Volume Purchasing Subscriptions

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Allowable properties are id, name, and enabled. (optional)

// Retrieve Volume Purchasing Subscriptions
VolumePurchasingSubscriptionsAPI.v1VolumePurchasingSubscriptionsGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Allowable properties are id, name, and enabled. | [optional] 

### Return type

[**VolumePurchasingSubscriptions**](VolumePurchasingSubscriptions.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingSubscriptionsIdDelete**
```swift
    open class func v1VolumePurchasingSubscriptionsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Volume Purchasing Subscription with the supplied id

Deletes a Volume Purchasing Subscription with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Subscription identifier

// Delete a Volume Purchasing Subscription with the supplied id
VolumePurchasingSubscriptionsAPI.v1VolumePurchasingSubscriptionsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Subscription identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingSubscriptionsIdGet**
```swift
    open class func v1VolumePurchasingSubscriptionsIdGet(id: String, completion: @escaping (_ data: VolumePurchasingSubscription?, _ error: Error?) -> Void)
```

Retrieve a Volume Purchasing Subscription with the supplied id

Retrieves a Volume Purchasing Subscription with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Subscription identifier

// Retrieve a Volume Purchasing Subscription with the supplied id
VolumePurchasingSubscriptionsAPI.v1VolumePurchasingSubscriptionsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Subscription identifier | 

### Return type

[**VolumePurchasingSubscription**](VolumePurchasingSubscription.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingSubscriptionsIdPut**
```swift
    open class func v1VolumePurchasingSubscriptionsIdPut(id: String, volumePurchasingSubscriptionBase: VolumePurchasingSubscriptionBase, completion: @escaping (_ data: VolumePurchasingSubscription?, _ error: Error?) -> Void)
```

Update a Volume Purchasing Subscription

Updates a Volume Purchasing Subscription

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Subscription identifier
let volumePurchasingSubscriptionBase = VolumePurchasingSubscriptionBase(name: "name_example", enabled: true, triggers: ["triggers_example"], locationIds: ["locationIds_example"], internalRecipients: [InternalRecipient(accountId: "accountId_example", frequency: "frequency_example")], externalRecipients: [ExternalRecipient(name: "name_example", email: "email_example")], siteId: "siteId_example") // VolumePurchasingSubscriptionBase | Volume Purchasing Subscription to update

// Update a Volume Purchasing Subscription
VolumePurchasingSubscriptionsAPI.v1VolumePurchasingSubscriptionsIdPut(id: id, volumePurchasingSubscriptionBase: volumePurchasingSubscriptionBase) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Subscription identifier | 
 **volumePurchasingSubscriptionBase** | [**VolumePurchasingSubscriptionBase**](VolumePurchasingSubscriptionBase.md) | Volume Purchasing Subscription to update | 

### Return type

[**VolumePurchasingSubscription**](VolumePurchasingSubscription.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingSubscriptionsPost**
```swift
    open class func v1VolumePurchasingSubscriptionsPost(volumePurchasingSubscriptionBase: VolumePurchasingSubscriptionBase, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Volume Purchasing Subscription

Creates a Volume Purchasing Subscription

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let volumePurchasingSubscriptionBase = VolumePurchasingSubscriptionBase(name: "name_example", enabled: true, triggers: ["triggers_example"], locationIds: ["locationIds_example"], internalRecipients: [InternalRecipient(accountId: "accountId_example", frequency: "frequency_example")], externalRecipients: [ExternalRecipient(name: "name_example", email: "email_example")], siteId: "siteId_example") // VolumePurchasingSubscriptionBase | Volume Purchasing Subscription to create

// Create a Volume Purchasing Subscription
VolumePurchasingSubscriptionsAPI.v1VolumePurchasingSubscriptionsPost(volumePurchasingSubscriptionBase: volumePurchasingSubscriptionBase) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volumePurchasingSubscriptionBase** | [**VolumePurchasingSubscriptionBase**](VolumePurchasingSubscriptionBase.md) | Volume Purchasing Subscription to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

