# SsoCertificateAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2SsoCertDelete**](SsoCertificateAPI.md#v2ssocertdelete) | **DELETE** /v2/sso/cert | Delete the currently configured certificate used by SSO 
[**v2SsoCertDownloadGet**](SsoCertificateAPI.md#v2ssocertdownloadget) | **GET** /v2/sso/cert/download | Download the certificate currently configured for use with Jamf Pro&#39;s SSO configuration 
[**v2SsoCertGet**](SsoCertificateAPI.md#v2ssocertget) | **GET** /v2/sso/cert | Retrieve the certificate currently configured for use with SSO 
[**v2SsoCertParsePost**](SsoCertificateAPI.md#v2ssocertparsepost) | **POST** /v2/sso/cert/parse | Parse the certificate to get details about certificate type and keys needed to upload certificate file 
[**v2SsoCertPost**](SsoCertificateAPI.md#v2ssocertpost) | **POST** /v2/sso/cert | Jamf Pro will generate a new certificate and use it to sign SSO 
[**v2SsoCertPut**](SsoCertificateAPI.md#v2ssocertput) | **PUT** /v2/sso/cert | Update the certificate used by Jamf Pro to sign SSO requests to the identify provider 


# **v2SsoCertDelete**
```swift
    open class func v2SsoCertDelete(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the currently configured certificate used by SSO 

Deletes the currently configured certificate used by SSO.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete the currently configured certificate used by SSO 
SsoCertificateAPI.v2SsoCertDelete() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2SsoCertDownloadGet**
```swift
    open class func v2SsoCertDownloadGet(completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download the certificate currently configured for use with Jamf Pro's SSO configuration 

Downloads the certificate currently configured for use with Jamf Pro's SSO configuration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Download the certificate currently configured for use with Jamf Pro's SSO configuration 
SsoCertificateAPI.v2SsoCertDownloadGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2SsoCertGet**
```swift
    open class func v2SsoCertGet(completion: @escaping (_ data: SsoKeystoreResponseWithDetails?, _ error: Error?) -> Void)
```

Retrieve the certificate currently configured for use with SSO 

Retrieves the certificate currently configured for use with SSO.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the certificate currently configured for use with SSO 
SsoCertificateAPI.v2SsoCertGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoKeystoreResponseWithDetails**](SsoKeystoreResponseWithDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2SsoCertParsePost**
```swift
    open class func v2SsoCertParsePost(ssoKeystoreParse: SsoKeystoreParse, completion: @escaping (_ data: SsoKeystoreCertParseResponse?, _ error: Error?) -> Void)
```

Parse the certificate to get details about certificate type and keys needed to upload certificate file 

Parse the certificate to get details about certificate type and keys needed to upload certificate file.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ssoKeystoreParse = SsoKeystoreParse(keystorePassword: "keystorePassword_example", keystoreFile: 123, keystoreFileName: "keystoreFileName_example") // SsoKeystoreParse | 

// Parse the certificate to get details about certificate type and keys needed to upload certificate file 
SsoCertificateAPI.v2SsoCertParsePost(ssoKeystoreParse: ssoKeystoreParse) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssoKeystoreParse** | [**SsoKeystoreParse**](SsoKeystoreParse.md) |  | 

### Return type

[**SsoKeystoreCertParseResponse**](SsoKeystoreCertParseResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2SsoCertPost**
```swift
    open class func v2SsoCertPost(completion: @escaping (_ data: SsoKeystoreResponseWithDetails?, _ error: Error?) -> Void)
```

Jamf Pro will generate a new certificate and use it to sign SSO 

Jamf Pro will generate a new certificate and use it to sign SSO requests to the identity provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Jamf Pro will generate a new certificate and use it to sign SSO 
SsoCertificateAPI.v2SsoCertPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoKeystoreResponseWithDetails**](SsoKeystoreResponseWithDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2SsoCertPut**
```swift
    open class func v2SsoCertPut(ssoKeystore: SsoKeystore, completion: @escaping (_ data: SsoKeystoreResponseWithDetails?, _ error: Error?) -> Void)
```

Update the certificate used by Jamf Pro to sign SSO requests to the identify provider 

Update the certificate used by Jamf Pro to sign SSO requests to the identify provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ssoKeystore = SsoKeystore(keys: [CertificateKey(id: "id_example", valid: true)], key: "key_example", password: "password_example", type: "type_example", keystoreSetupType: "keystoreSetupType_example") // SsoKeystore | 

// Update the certificate used by Jamf Pro to sign SSO requests to the identify provider 
SsoCertificateAPI.v2SsoCertPut(ssoKeystore: ssoKeystore) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssoKeystore** | [**SsoKeystore**](SsoKeystore.md) |  | 

### Return type

[**SsoKeystoreResponseWithDetails**](SsoKeystoreResponseWithDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

