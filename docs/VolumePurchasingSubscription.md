# VolumePurchasingSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**enabled** | **Bool** |  | [optional] [default to true]
**triggers** | **[String]** |  | [optional] 
**locationIds** | **[String]** |  | [optional] 
**internalRecipients** | [InternalRecipient] |  | [optional] 
**externalRecipients** | [ExternalRecipient] |  | [optional] 
**siteId** | **String** |  | [optional] [default to "-1"]
**id** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


