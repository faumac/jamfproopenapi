# ApiError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**httpStatus** | **Int** | HTTP status of the response | [optional] 
**errors** | [ApiErrorCause] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


