# LapsHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdDate** | **Date** |  | [optional] 
**dateLastSeen** | **Date** |  | [optional] 
**expirationTime** | **Date** |  | [optional] 
**rotationStatus** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


