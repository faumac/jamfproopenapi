# VolumePurchasingContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] [readonly] 
**licenseCountTotal** | **Int** |  | [optional] [readonly] 
**licenseCountInUse** | **Int** |  | [optional] [readonly] 
**licenseCountReported** | **Int** |  | [optional] [readonly] 
**iconUrl** | **String** |  | [optional] [readonly] 
**deviceTypes** | **[String]** |  | [optional] [readonly] 
**contentType** | **String** |  | [optional] [readonly] 
**pricingParam** | **String** |  | [optional] [readonly] 
**adamId** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


