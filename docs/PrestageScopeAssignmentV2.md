# PrestageScopeAssignmentV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serialNumber** | **String** |  | [optional] 
**assignmentDate** | **Date** |  | [optional] 
**userAssigned** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


