# RemoteAdministrationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**previewRemoteAdministrationConfigurationsGet**](RemoteAdministrationAPI.md#previewremoteadministrationconfigurationsget) | **GET** /preview/remote-administration-configurations | Get information about all remote administration configurations.


# **previewRemoteAdministrationConfigurationsGet**
```swift
    open class func previewRemoteAdministrationConfigurationsGet(page: Int? = nil, pageSize: Int? = nil, completion: @escaping (_ data: RemoteAdministrationSearchResults?, _ error: Error?) -> Void)
```

Get information about all remote administration configurations.

Remote administration feature creates a secure screen-sharing experience between Jamf Pro administrators and their end-users.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)

// Get information about all remote administration configurations.
RemoteAdministrationAPI.previewRemoteAdministrationConfigurationsGet(page: page, pageSize: pageSize) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]

### Return type

[**RemoteAdministrationSearchResults**](RemoteAdministrationSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

