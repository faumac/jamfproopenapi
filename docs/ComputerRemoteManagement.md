# ComputerRemoteManagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**managed** | **Bool** |  | [optional] 
**managementUsername** | **String** |  | [optional] 
**managementPassword** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


