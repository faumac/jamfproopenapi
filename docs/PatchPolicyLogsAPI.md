# PatchPolicyLogsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2PatchPoliciesIdLogsDeviceIdDetailsGet**](PatchPolicyLogsAPI.md#v2patchpoliciesidlogsdeviceiddetailsget) | **GET** /v2/patch-policies/{id}/logs/{deviceId}/details | Return attempt details for a specific log
[**v2PatchPoliciesIdLogsDeviceIdGet**](PatchPolicyLogsAPI.md#v2patchpoliciesidlogsdeviceidget) | **GET** /v2/patch-policies/{id}/logs/{deviceId} | Retrieves a single Patch Policy Log 
[**v2PatchPoliciesIdLogsEligibleRetryCountGet**](PatchPolicyLogsAPI.md#v2patchpoliciesidlogseligibleretrycountget) | **GET** /v2/patch-policies/{id}/logs/eligible-retry-count | Return the count of the Patch Policy Logs for the patch policy id that are eligible for a retry attempt 
[**v2PatchPoliciesIdLogsGet**](PatchPolicyLogsAPI.md#v2patchpoliciesidlogsget) | **GET** /v2/patch-policies/{id}/logs | Retrieve Patch Policy Logs 
[**v2PatchPoliciesIdLogsRetryAllPost**](PatchPolicyLogsAPI.md#v2patchpoliciesidlogsretryallpost) | **POST** /v2/patch-policies/{id}/logs/retry-all | Send retry attempts for all devices
[**v2PatchPoliciesIdLogsRetryPost**](PatchPolicyLogsAPI.md#v2patchpoliciesidlogsretrypost) | **POST** /v2/patch-policies/{id}/logs/retry | Send retry attempts for specific devices


# **v2PatchPoliciesIdLogsDeviceIdDetailsGet**
```swift
    open class func v2PatchPoliciesIdLogsDeviceIdDetailsGet(id: String, deviceId: String, completion: @escaping (_ data: [PatchPolicyLogDetail]?, _ error: Error?) -> Void)
```

Return attempt details for a specific log

Return attempt details for a specific log

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id
let deviceId = "deviceId_example" // String | device id

// Return attempt details for a specific log
PatchPolicyLogsAPI.v2PatchPoliciesIdLogsDeviceIdDetailsGet(id: id, deviceId: deviceId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 
 **deviceId** | **String** | device id | 

### Return type

[**[PatchPolicyLogDetail]**](PatchPolicyLogDetail.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdLogsDeviceIdGet**
```swift
    open class func v2PatchPoliciesIdLogsDeviceIdGet(id: String, deviceId: String, completion: @escaping (_ data: PatchPolicyLogV2?, _ error: Error?) -> Void)
```

Retrieves a single Patch Policy Log 

Retrieves a single Patch Policy Log

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id
let deviceId = "deviceId_example" // String | device id

// Retrieves a single Patch Policy Log 
PatchPolicyLogsAPI.v2PatchPoliciesIdLogsDeviceIdGet(id: id, deviceId: deviceId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 
 **deviceId** | **String** | device id | 

### Return type

[**PatchPolicyLogV2**](PatchPolicyLogV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdLogsEligibleRetryCountGet**
```swift
    open class func v2PatchPoliciesIdLogsEligibleRetryCountGet(id: String, completion: @escaping (_ data: PatchPolicyLogEligibleRetryCount?, _ error: Error?) -> Void)
```

Return the count of the Patch Policy Logs for the patch policy id that are eligible for a retry attempt 

return the count of the patch policy logs for the patch policy id that  are eligible for a retry attempt

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id

// Return the count of the Patch Policy Logs for the patch policy id that are eligible for a retry attempt 
PatchPolicyLogsAPI.v2PatchPoliciesIdLogsEligibleRetryCountGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 

### Return type

[**PatchPolicyLogEligibleRetryCount**](PatchPolicyLogEligibleRetryCount.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdLogsGet**
```swift
    open class func v2PatchPoliciesIdLogsGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: PatchPolicyLogs?, _ error: Error?) -> Void)
```

Retrieve Patch Policy Logs 

Retrieves Patch Policy Logs

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is deviceName:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Patch Policy Logs collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: deviceId, deviceName, statusCode, statusDate, attemptNumber, ignoredForPatchPolicyId. This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve Patch Policy Logs 
PatchPolicyLogsAPI.v2PatchPoliciesIdLogsGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is deviceName:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Patch Policy Logs collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: deviceId, deviceName, statusCode, statusDate, attemptNumber, ignoredForPatchPolicyId. This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**PatchPolicyLogs**](PatchPolicyLogs.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdLogsRetryAllPost**
```swift
    open class func v2PatchPoliciesIdLogsRetryAllPost(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Send retry attempts for all devices

Send retry attempts for all devices

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id

// Send retry attempts for all devices
PatchPolicyLogsAPI.v2PatchPoliciesIdLogsRetryAllPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdLogsRetryPost**
```swift
    open class func v2PatchPoliciesIdLogsRetryPost(id: String, patchPolicyLogRetry: PatchPolicyLogRetry, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Send retry attempts for specific devices

Send retry attempts for specific devices

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id
let patchPolicyLogRetry = PatchPolicyLogRetry(deviceIds: ["deviceIds_example"]) // PatchPolicyLogRetry | 

// Send retry attempts for specific devices
PatchPolicyLogsAPI.v2PatchPoliciesIdLogsRetryPost(id: id, patchPolicyLogRetry: patchPolicyLogRetry) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 
 **patchPolicyLogRetry** | [**PatchPolicyLogRetry**](PatchPolicyLogRetry.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

