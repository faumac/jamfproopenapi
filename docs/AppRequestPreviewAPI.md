# AppRequestPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1AppRequestFormInputFieldsGet**](AppRequestPreviewAPI.md#v1apprequestforminputfieldsget) | **GET** /v1/app-request/form-input-fields | Search for Form Input Fields 
[**v1AppRequestFormInputFieldsIdDelete**](AppRequestPreviewAPI.md#v1apprequestforminputfieldsiddelete) | **DELETE** /v1/app-request/form-input-fields/{id} | Remove specified Form Input Field record 
[**v1AppRequestFormInputFieldsIdGet**](AppRequestPreviewAPI.md#v1apprequestforminputfieldsidget) | **GET** /v1/app-request/form-input-fields/{id} | Get specified Form Input Field object 
[**v1AppRequestFormInputFieldsIdPut**](AppRequestPreviewAPI.md#v1apprequestforminputfieldsidput) | **PUT** /v1/app-request/form-input-fields/{id} | Update specified Form Input Field object 
[**v1AppRequestFormInputFieldsPost**](AppRequestPreviewAPI.md#v1apprequestforminputfieldspost) | **POST** /v1/app-request/form-input-fields | Create Form Input Field record 
[**v1AppRequestFormInputFieldsPut**](AppRequestPreviewAPI.md#v1apprequestforminputfieldsput) | **PUT** /v1/app-request/form-input-fields | Replace all Form Input Fields 
[**v1AppRequestSettingsGet**](AppRequestPreviewAPI.md#v1apprequestsettingsget) | **GET** /v1/app-request/settings | Get Applicastion Request Settings 
[**v1AppRequestSettingsPut**](AppRequestPreviewAPI.md#v1apprequestsettingsput) | **PUT** /v1/app-request/settings | Update Application Request Settings 


# **v1AppRequestFormInputFieldsGet**
```swift
    open class func v1AppRequestFormInputFieldsGet(completion: @escaping (_ data: AppRequestFormInputFieldSearchResults?, _ error: Error?) -> Void)
```

Search for Form Input Fields 

Search for form input fields

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Search for Form Input Fields 
AppRequestPreviewAPI.v1AppRequestFormInputFieldsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AppRequestFormInputFieldSearchResults**](AppRequestFormInputFieldSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestFormInputFieldsIdDelete**
```swift
    open class func v1AppRequestFormInputFieldsIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified Form Input Field record 

Removes specified form input field record 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Instance id of form input field record

// Remove specified Form Input Field record 
AppRequestPreviewAPI.v1AppRequestFormInputFieldsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Instance id of form input field record | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestFormInputFieldsIdGet**
```swift
    open class func v1AppRequestFormInputFieldsIdGet(id: Int, completion: @escaping (_ data: AppRequestFormInputField?, _ error: Error?) -> Void)
```

Get specified Form Input Field object 

Gets specified form input field object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Instance id of form input field record

// Get specified Form Input Field object 
AppRequestPreviewAPI.v1AppRequestFormInputFieldsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Instance id of form input field record | 

### Return type

[**AppRequestFormInputField**](AppRequestFormInputField.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestFormInputFieldsIdPut**
```swift
    open class func v1AppRequestFormInputFieldsIdPut(id: Int, appRequestFormInputField: AppRequestFormInputField, completion: @escaping (_ data: AppRequestFormInputField?, _ error: Error?) -> Void)
```

Update specified Form Input Field object 

Update specified form input field object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Instance id of form input field record
let appRequestFormInputField = AppRequestFormInputField(id: 123, title: "title_example", description: "description_example", priority: 123) // AppRequestFormInputField | form input field object to create. ids defined in this body will be ignored

// Update specified Form Input Field object 
AppRequestPreviewAPI.v1AppRequestFormInputFieldsIdPut(id: id, appRequestFormInputField: appRequestFormInputField) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Instance id of form input field record | 
 **appRequestFormInputField** | [**AppRequestFormInputField**](AppRequestFormInputField.md) | form input field object to create. ids defined in this body will be ignored | 

### Return type

[**AppRequestFormInputField**](AppRequestFormInputField.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestFormInputFieldsPost**
```swift
    open class func v1AppRequestFormInputFieldsPost(appRequestFormInputField: AppRequestFormInputField, completion: @escaping (_ data: AppRequestFormInputField?, _ error: Error?) -> Void)
```

Create Form Input Field record 

Create form input field record 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let appRequestFormInputField = AppRequestFormInputField(id: 123, title: "title_example", description: "description_example", priority: 123) // AppRequestFormInputField | form input field object to create. ids defined in this body will be ignored

// Create Form Input Field record 
AppRequestPreviewAPI.v1AppRequestFormInputFieldsPost(appRequestFormInputField: appRequestFormInputField) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appRequestFormInputField** | [**AppRequestFormInputField**](AppRequestFormInputField.md) | form input field object to create. ids defined in this body will be ignored | 

### Return type

[**AppRequestFormInputField**](AppRequestFormInputField.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestFormInputFieldsPut**
```swift
    open class func v1AppRequestFormInputFieldsPut(appRequestFormInputField: [AppRequestFormInputField], completion: @escaping (_ data: [AppRequestFormInputField]?, _ error: Error?) -> Void)
```

Replace all Form Input Fields 

Replace all form input fields. Will delete, update, and create all input fields accordingly. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let appRequestFormInputField = [AppRequestFormInputField(id: 123, title: "title_example", description: "description_example", priority: 123)] // [AppRequestFormInputField] | list of form input fields to replace all existing fields. Will delete, update, and create all input fields accordingly.

// Replace all Form Input Fields 
AppRequestPreviewAPI.v1AppRequestFormInputFieldsPut(appRequestFormInputField: appRequestFormInputField) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appRequestFormInputField** | [**[AppRequestFormInputField]**](AppRequestFormInputField.md) | list of form input fields to replace all existing fields. Will delete, update, and create all input fields accordingly. | 

### Return type

[**[AppRequestFormInputField]**](AppRequestFormInputField.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestSettingsGet**
```swift
    open class func v1AppRequestSettingsGet(completion: @escaping (_ data: AppRequestSettings?, _ error: Error?) -> Void)
```

Get Applicastion Request Settings 

Get app request settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Applicastion Request Settings 
AppRequestPreviewAPI.v1AppRequestSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AppRequestSettings**](AppRequestSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AppRequestSettingsPut**
```swift
    open class func v1AppRequestSettingsPut(appRequestSettings: AppRequestSettings, completion: @escaping (_ data: AppRequestSettings?, _ error: Error?) -> Void)
```

Update Application Request Settings 

Update app request settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let appRequestSettings = AppRequestSettings(isEnabled: true, appStoreLocale: "appStoreLocale_example", requesterUserGroupId: 123, approverEmails: ["approverEmails_example"]) // AppRequestSettings | App request settings object

// Update Application Request Settings 
AppRequestPreviewAPI.v1AppRequestSettingsPut(appRequestSettings: appRequestSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appRequestSettings** | [**AppRequestSettings**](AppRequestSettings.md) | App request settings object | 

### Return type

[**AppRequestSettings**](AppRequestSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

