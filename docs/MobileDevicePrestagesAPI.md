# MobileDevicePrestagesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MobileDevicePrestagesGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesget) | **GET** /v1/mobile-device-prestages | Search for sorted and paged Mobile Device Prestages 
[**v1MobileDevicePrestagesIdAttachmentsDelete**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidattachmentsdelete) | **DELETE** /v1/mobile-device-prestages/{id}/attachments | Remove an attachment for a Mobile Device Prestage 
[**v1MobileDevicePrestagesIdAttachmentsGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidattachmentsget) | **GET** /v1/mobile-device-prestages/{id}/attachments | Get attachments for a Mobile Device Prestage 
[**v1MobileDevicePrestagesIdAttachmentsPost**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidattachmentspost) | **POST** /v1/mobile-device-prestages/{id}/attachments | Add an attachment to a Mobile Device Prestage 
[**v1MobileDevicePrestagesIdDelete**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesiddelete) | **DELETE** /v1/mobile-device-prestages/{id} | Delete a Mobile Device Prestage with the supplied id 
[**v1MobileDevicePrestagesIdGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidget) | **GET** /v1/mobile-device-prestages/{id} | Retrieve a Mobile Device Prestage with the supplied id 
[**v1MobileDevicePrestagesIdHistoryGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidhistoryget) | **GET** /v1/mobile-device-prestages/{id}/history | Get sorted and paged Mobile Device Prestage history objects 
[**v1MobileDevicePrestagesIdHistoryPost**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidhistorypost) | **POST** /v1/mobile-device-prestages/{id}/history | Add Mobile Device Prestage history object notes 
[**v1MobileDevicePrestagesIdPut**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidput) | **PUT** /v1/mobile-device-prestages/{id} | Update a Mobile Device Prestage 
[**v1MobileDevicePrestagesIdScopeDelete**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidscopedelete) | **DELETE** /v1/mobile-device-prestages/{id}/scope | Remove Device Scope for a specific Mobile Device Prestage 
[**v1MobileDevicePrestagesIdScopeGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidscopeget) | **GET** /v1/mobile-device-prestages/{id}/scope | Get Device Scope for a specific Mobile Device Prestage 
[**v1MobileDevicePrestagesIdScopePost**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidscopepost) | **POST** /v1/mobile-device-prestages/{id}/scope | Add Device Scope for a specific Mobile Device Prestage 
[**v1MobileDevicePrestagesIdScopePut**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesidscopeput) | **PUT** /v1/mobile-device-prestages/{id}/scope | Replace Device Scope for a specific Mobile Device Prestage 
[**v1MobileDevicePrestagesPost**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagespost) | **POST** /v1/mobile-device-prestages | Create a Mobile Device Prestage 
[**v1MobileDevicePrestagesScopeGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagesscopeget) | **GET** /v1/mobile-device-prestages/scope | Get all Device Scope for all Mobile Device Prestages 
[**v1MobileDevicePrestagesSyncGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagessyncget) | **GET** /v1/mobile-device-prestages/sync | Get all Prestage sync States for all prestages 
[**v1MobileDevicePrestagesSyncIdGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagessyncidget) | **GET** /v1/mobile-device-prestages/sync/{id} | Get all prestage sync states for a single prestage 
[**v1MobileDevicePrestagesSyncIdLatestGet**](MobileDevicePrestagesAPI.md#v1mobiledeviceprestagessyncidlatestget) | **GET** /v1/mobile-device-prestages/sync/{id}/latest | Get the latest Sync State for a single Prestage 
[**v2MobileDevicePrestagesGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesget) | **GET** /v2/mobile-device-prestages | Get sorted and paged Mobile Device Prestages 
[**v2MobileDevicePrestagesIdAttachmentsDeleteMultiplePost**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidattachmentsdeletemultiplepost) | **POST** /v2/mobile-device-prestages/{id}/attachments/delete-multiple | Remove an attachment for a Mobile Device Prestage 
[**v2MobileDevicePrestagesIdAttachmentsGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidattachmentsget) | **GET** /v2/mobile-device-prestages/{id}/attachments | Get attachments for a Mobile Device Prestage 
[**v2MobileDevicePrestagesIdAttachmentsPost**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidattachmentspost) | **POST** /v2/mobile-device-prestages/{id}/attachments | Add an attachment to a Mobile Device Prestage 
[**v2MobileDevicePrestagesIdDelete**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesiddelete) | **DELETE** /v2/mobile-device-prestages/{id} | Delete a Mobile Device Prestage with the supplied id 
[**v2MobileDevicePrestagesIdGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidget) | **GET** /v2/mobile-device-prestages/{id} | Retrieve a Mobile Device Prestage with the supplied id 
[**v2MobileDevicePrestagesIdHistoryGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidhistoryget) | **GET** /v2/mobile-device-prestages/{id}/history | Get sorted and paged Mobile Device Prestage history objects 
[**v2MobileDevicePrestagesIdHistoryPost**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidhistorypost) | **POST** /v2/mobile-device-prestages/{id}/history | Add Mobile Device Prestage history object notes 
[**v2MobileDevicePrestagesIdPut**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidput) | **PUT** /v2/mobile-device-prestages/{id} | Update a Mobile Device Prestage 
[**v2MobileDevicePrestagesIdScopeDeleteMultiplePost**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidscopedeletemultiplepost) | **POST** /v2/mobile-device-prestages/{id}/scope/delete-multiple | Remove Device Scope for a specific Mobile Device Prestage 
[**v2MobileDevicePrestagesIdScopeGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidscopeget) | **GET** /v2/mobile-device-prestages/{id}/scope | Get Device Scope for a specific Mobile Device Prestage 
[**v2MobileDevicePrestagesIdScopePost**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidscopepost) | **POST** /v2/mobile-device-prestages/{id}/scope | Add Device Scope for a specific Mobile Device Prestage 
[**v2MobileDevicePrestagesIdScopePut**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidscopeput) | **PUT** /v2/mobile-device-prestages/{id}/scope | Replace Device Scope for a specific Mobile Device Prestage 
[**v2MobileDevicePrestagesIdSyncsGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidsyncsget) | **GET** /v2/mobile-device-prestages/{id}/syncs | Get all prestage sync states for a single prestage 
[**v2MobileDevicePrestagesIdSyncsLatestGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesidsyncslatestget) | **GET** /v2/mobile-device-prestages/{id}/syncs/latest | Get the latest Sync State for a single Prestage 
[**v2MobileDevicePrestagesPost**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagespost) | **POST** /v2/mobile-device-prestages | Create a Mobile Device Prestage 
[**v2MobileDevicePrestagesScopeGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagesscopeget) | **GET** /v2/mobile-device-prestages/scope | Get all Device Scope for all Mobile Device Prestages 
[**v2MobileDevicePrestagesSyncsGet**](MobileDevicePrestagesAPI.md#v2mobiledeviceprestagessyncsget) | **GET** /v2/mobile-device-prestages/syncs | Get all Prestage sync States for all prestages 


# **v1MobileDevicePrestagesGet**
```swift
    open class func v1MobileDevicePrestagesGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: MobileDevicePrestageSearchResults?, _ error: Error?) -> Void)
```

Search for sorted and paged Mobile Device Prestages 

Search for sorted and paged mobile device prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "id:asc")

// Search for sorted and paged Mobile Device Prestages 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;id:asc&quot;]

### Return type

[**MobileDevicePrestageSearchResults**](MobileDevicePrestageSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdAttachmentsDelete**
```swift
    open class func v1MobileDevicePrestagesIdAttachmentsDelete(id: Int, fileAttachmentDelete: FileAttachmentDelete, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove an attachment for a Mobile Device Prestage 

Remove an attachment for a Mobile Device Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let fileAttachmentDelete = FileAttachmentDelete(ids: [123]) // FileAttachmentDelete | 

// Remove an attachment for a Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdAttachmentsDelete(id: id, fileAttachmentDelete: fileAttachmentDelete) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **fileAttachmentDelete** | [**FileAttachmentDelete**](FileAttachmentDelete.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdAttachmentsGet**
```swift
    open class func v1MobileDevicePrestagesIdAttachmentsGet(id: Int, completion: @escaping (_ data: [FileAttachment]?, _ error: Error?) -> Void)
```

Get attachments for a Mobile Device Prestage 

Get attachments for a Mobile Device Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier

// Get attachments for a Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdAttachmentsGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 

### Return type

[**[FileAttachment]**](FileAttachment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdAttachmentsPost**
```swift
    open class func v1MobileDevicePrestagesIdAttachmentsPost(id: Int, file: URL, completion: @escaping (_ data: PrestageFileAttachment?, _ error: Error?) -> Void)
```

Add an attachment to a Mobile Device Prestage 

Add an attachment to a Mobile Device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Identifier of the Mobile Device Prestage the attachment should be assigned to
let file = URL(string: "https://example.com")! // URL | The file to upload

// Add an attachment to a Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdAttachmentsPost(id: id, file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Identifier of the Mobile Device Prestage the attachment should be assigned to | 
 **file** | **URL** | The file to upload | 

### Return type

[**PrestageFileAttachment**](PrestageFileAttachment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdDelete**
```swift
    open class func v1MobileDevicePrestagesIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Mobile Device Prestage with the supplied id 

Deletes a Mobile Device Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier

// Delete a Mobile Device Prestage with the supplied id 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdGet**
```swift
    open class func v1MobileDevicePrestagesIdGet(id: Int, completion: @escaping (_ data: GetMobileDevicePrestage?, _ error: Error?) -> Void)
```

Retrieve a Mobile Device Prestage with the supplied id 

Retrieves a Mobile Device Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier

// Retrieve a Mobile Device Prestage with the supplied id 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 

### Return type

[**GetMobileDevicePrestage**](GetMobileDevicePrestage.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdHistoryGet**
```swift
    open class func v1MobileDevicePrestagesIdHistoryGet(id: Int, page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get sorted and paged Mobile Device Prestage history objects 

Gets sorted and paged mobile device prestage history objects

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is duplicated for each sort criterion, e.g., ...&sort=name%2Casc&sort=date%2Cdesc (optional)

// Get sorted and paged Mobile Device Prestage history objects 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdHistoryGet(id: id, page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name%2Casc&amp;sort&#x3D;date%2Cdesc | [optional] 

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdHistoryPost**
```swift
    open class func v1MobileDevicePrestagesIdHistoryPost(id: Int, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Mobile Device Prestage history object notes 

Adds mobile device prestage history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Mobile Device Prestage history object notes 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdPut**
```swift
    open class func v1MobileDevicePrestagesIdPut(id: Int, putMobileDevicePrestage: PutMobileDevicePrestage, completion: @escaping (_ data: GetMobileDevicePrestage?, _ error: Error?) -> Void)
```

Update a Mobile Device Prestage 

Updates a Mobile Device Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let putMobileDevicePrestage = PutMobileDevicePrestage(displayName: "displayName_example", isMandatory: false, isMdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", isDefaultPrestage: false, enrollmentSiteId: 123, isKeepExistingSiteMembership: true, isKeepExistingLocationInformation: true, isRequireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", isPreventActivationLock: true, isEnableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: 123, skipSetupItems: "TODO", locationInformation: LocationInformation(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: 123, buildingId: 123, id: 123, versionLock: 123), purchasingInformation: PrestagePurchasingInformation(id: 123, isLeased: true, isPurchased: true, appleCareID: "appleCareID_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: 123, isAllowPairing: true, isMultiUser: true, isSupervised: true, maximumSharedAccounts: 123, isAutoAdvanceSetup: true, isConfigureDeviceBeforeSetupAssistant: true, language: "language_example", region: "region_example", names: MobileDevicePrestageNames(assignNamesUsing: "assignNamesUsing_example", prestageDeviceNames: [MobileDevicePrestageName(id: 123, deviceName: "deviceName_example", isUsed: false)], deviceNamePrefix: "deviceNamePrefix_example", deviceNameSuffix: "deviceNameSuffix_example", singleDeviceName: "singleDeviceName_example", isManageNames: true, isDeviceNamingConfigured: true), versionLock: 123) // PutMobileDevicePrestage | Mobile Device Prestage to update

// Update a Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdPut(id: id, putMobileDevicePrestage: putMobileDevicePrestage) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **putMobileDevicePrestage** | [**PutMobileDevicePrestage**](PutMobileDevicePrestage.md) | Mobile Device Prestage to update | 

### Return type

[**GetMobileDevicePrestage**](GetMobileDevicePrestage.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdScopeDelete**
```swift
    open class func v1MobileDevicePrestagesIdScopeDelete(id: Int, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Remove Device Scope for a specific Mobile Device Prestage 

Remove device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to remove from scope

// Remove Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdScopeDelete(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to remove from scope | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdScopeGet**
```swift
    open class func v1MobileDevicePrestagesIdScopeGet(id: Int, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Get Device Scope for a specific Mobile Device Prestage 

Get device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier

// Get Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdScopeGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdScopePost**
```swift
    open class func v1MobileDevicePrestagesIdScopePost(id: Int, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Add Device Scope for a specific Mobile Device Prestage 

Add device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Add Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdScopePost(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesIdScopePut**
```swift
    open class func v1MobileDevicePrestagesIdScopePut(id: Int, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Replace Device Scope for a specific Mobile Device Prestage 

Replace device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Replace Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesIdScopePut(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesPost**
```swift
    open class func v1MobileDevicePrestagesPost(mobileDevicePrestage: MobileDevicePrestage, completion: @escaping (_ data: GetMobileDevicePrestage?, _ error: Error?) -> Void)
```

Create a Mobile Device Prestage 

Create a mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let mobileDevicePrestage = MobileDevicePrestage(displayName: "displayName_example", isMandatory: false, isMdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", isDefaultPrestage: false, enrollmentSiteId: 123, isKeepExistingSiteMembership: true, isKeepExistingLocationInformation: true, isRequireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", isPreventActivationLock: true, isEnableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: 123, skipSetupItems: "TODO", locationInformation: LocationInformation(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: 123, buildingId: 123, id: 123, versionLock: 123), purchasingInformation: PrestagePurchasingInformation(id: 123, isLeased: true, isPurchased: true, appleCareID: "appleCareID_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: 123, isAllowPairing: true, isMultiUser: true, isSupervised: true, maximumSharedAccounts: 123, isAutoAdvanceSetup: true, isConfigureDeviceBeforeSetupAssistant: true, language: "language_example", region: "region_example", names: MobileDevicePrestageNames(assignNamesUsing: "assignNamesUsing_example", prestageDeviceNames: [MobileDevicePrestageName(id: 123, deviceName: "deviceName_example", isUsed: false)], deviceNamePrefix: "deviceNamePrefix_example", deviceNameSuffix: "deviceNameSuffix_example", singleDeviceName: "singleDeviceName_example", isManageNames: true, isDeviceNamingConfigured: true)) // MobileDevicePrestage | Mobile Device Prestage to create. ids defined in this body will be ignored

// Create a Mobile Device Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesPost(mobileDevicePrestage: mobileDevicePrestage) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mobileDevicePrestage** | [**MobileDevicePrestage**](MobileDevicePrestage.md) | Mobile Device Prestage to create. ids defined in this body will be ignored | 

### Return type

[**GetMobileDevicePrestage**](GetMobileDevicePrestage.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesScopeGet**
```swift
    open class func v1MobileDevicePrestagesScopeGet(completion: @escaping (_ data: PrestageScope?, _ error: Error?) -> Void)
```

Get all Device Scope for all Mobile Device Prestages 

Get all device scope for all mobile device prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all Device Scope for all Mobile Device Prestages 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesScopeGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PrestageScope**](PrestageScope.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesSyncGet**
```swift
    open class func v1MobileDevicePrestagesSyncGet(completion: @escaping (_ data: [PrestageSyncStatus]?, _ error: Error?) -> Void)
```

Get all Prestage sync States for all prestages 

Get all prestage sync states for all prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all Prestage sync States for all prestages 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesSyncGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[PrestageSyncStatus]**](PrestageSyncStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesSyncIdGet**
```swift
    open class func v1MobileDevicePrestagesSyncIdGet(id: Int, completion: @escaping (_ data: [PrestageSyncStatus]?, _ error: Error?) -> Void)
```

Get all prestage sync states for a single prestage 

Get all prestage sync states for a single prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier

// Get all prestage sync states for a single prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesSyncIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 

### Return type

[**[PrestageSyncStatus]**](PrestageSyncStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicePrestagesSyncIdLatestGet**
```swift
    open class func v1MobileDevicePrestagesSyncIdLatestGet(id: Int, completion: @escaping (_ data: PrestageSyncStatus?, _ error: Error?) -> Void)
```

Get the latest Sync State for a single Prestage 

Get the latest sync state for a single prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Mobile Device Prestage identifier

// Get the latest Sync State for a single Prestage 
MobileDevicePrestagesAPI.v1MobileDevicePrestagesSyncIdLatestGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Mobile Device Prestage identifier | 

### Return type

[**PrestageSyncStatus**](PrestageSyncStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesGet**
```swift
    open class func v2MobileDevicePrestagesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: MobileDevicePrestageSearchResultsV2?, _ error: Error?) -> Void)
```

Get sorted and paged Mobile Device Prestages 

Gets sorted and paged mobile device prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Get sorted and paged Mobile Device Prestages 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**MobileDevicePrestageSearchResultsV2**](MobileDevicePrestageSearchResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdAttachmentsDeleteMultiplePost**
```swift
    open class func v2MobileDevicePrestagesIdAttachmentsDeleteMultiplePost(id: String, ids: Ids, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove an attachment for a Mobile Device Prestage 

Remove an attachment for a Mobile Device Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let ids = Ids(ids: ["ids_example"]) // Ids | 

// Remove an attachment for a Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdAttachmentsDeleteMultiplePost(id: id, ids: ids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **ids** | [**Ids**](Ids.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdAttachmentsGet**
```swift
    open class func v2MobileDevicePrestagesIdAttachmentsGet(id: String, completion: @escaping (_ data: [FileAttachmentV2]?, _ error: Error?) -> Void)
```

Get attachments for a Mobile Device Prestage 

Get attachments for a Mobile Device Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier

// Get attachments for a Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdAttachmentsGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 

### Return type

[**[FileAttachmentV2]**](FileAttachmentV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdAttachmentsPost**
```swift
    open class func v2MobileDevicePrestagesIdAttachmentsPost(id: String, file: URL, completion: @escaping (_ data: PrestageFileAttachmentV2?, _ error: Error?) -> Void)
```

Add an attachment to a Mobile Device Prestage 

Add an attachment to a Mobile Device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Identifier of the Mobile Device Prestage the attachment should be assigned to
let file = URL(string: "https://example.com")! // URL | The file to upload

// Add an attachment to a Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdAttachmentsPost(id: id, file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Identifier of the Mobile Device Prestage the attachment should be assigned to | 
 **file** | **URL** | The file to upload | 

### Return type

[**PrestageFileAttachmentV2**](PrestageFileAttachmentV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdDelete**
```swift
    open class func v2MobileDevicePrestagesIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Mobile Device Prestage with the supplied id 

Deletes a Mobile Device Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier

// Delete a Mobile Device Prestage with the supplied id 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdGet**
```swift
    open class func v2MobileDevicePrestagesIdGet(id: String, completion: @escaping (_ data: GetMobileDevicePrestageV2?, _ error: Error?) -> Void)
```

Retrieve a Mobile Device Prestage with the supplied id 

Retrieves a Mobile Device Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier

// Retrieve a Mobile Device Prestage with the supplied id 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 

### Return type

[**GetMobileDevicePrestageV2**](GetMobileDevicePrestageV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdHistoryGet**
```swift
    open class func v2MobileDevicePrestagesIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get sorted and paged Mobile Device Prestage history objects 

Gets sorted and paged mobile device prestage history objects

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is duplicated for each sort criterion, e.g., ...&sort=name%2Casc&sort=date%2Cdesc (optional)

// Get sorted and paged Mobile Device Prestage history objects 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name%2Casc&amp;sort&#x3D;date%2Cdesc | [optional] 

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdHistoryPost**
```swift
    open class func v2MobileDevicePrestagesIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Mobile Device Prestage history object notes 

Adds mobile device prestage history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Mobile Device Prestage history object notes 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdPut**
```swift
    open class func v2MobileDevicePrestagesIdPut(id: String, putMobileDevicePrestageV2: PutMobileDevicePrestageV2, completion: @escaping (_ data: GetMobileDevicePrestageV2?, _ error: Error?) -> Void)
```

Update a Mobile Device Prestage 

Updates a Mobile Device Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let putMobileDevicePrestageV2 = PutMobileDevicePrestageV2(displayName: "displayName_example", mandatory: false, mdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", defaultPrestage: false, enrollmentSiteId: "enrollmentSiteId_example", keepExistingSiteMembership: true, keepExistingLocationInformation: true, requireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", preventActivationLock: true, enableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: "deviceEnrollmentProgramInstanceId_example", skipSetupItems: "TODO", locationInformation: LocationInformationV2(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: "departmentId_example", buildingId: "buildingId_example", id: "id_example", versionLock: 123), purchasingInformation: PrestagePurchasingInformationV2(id: "id_example", leased: true, purchased: true, appleCareId: "appleCareId_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: "enrollmentCustomizationId_example", language: "language_example", region: "region_example", autoAdvanceSetup: true, allowPairing: true, multiUser: true, supervised: true, maximumSharedAccounts: 123, configureDeviceBeforeSetupAssistant: true, names: MobileDevicePrestageNamesV2(assignNamesUsing: "assignNamesUsing_example", prestageDeviceNames: [MobileDevicePrestageNameV2(id: "id_example", deviceName: "deviceName_example", used: false)], deviceNamePrefix: "deviceNamePrefix_example", deviceNameSuffix: "deviceNameSuffix_example", singleDeviceName: "singleDeviceName_example", manageNames: true, deviceNamingConfigured: true), sendTimezone: true, timezone: "timezone_example", storageQuotaSizeMegabytes: 123, useStorageQuotaSize: true, temporarySessionOnly: false, enforceTemporarySessionTimeout: false, temporarySessionTimeout: 123, enforceUserSessionTimeout: false, userSessionTimeout: 123, versionLock: 123) // PutMobileDevicePrestageV2 | Mobile Device Prestage to update

// Update a Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdPut(id: id, putMobileDevicePrestageV2: putMobileDevicePrestageV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **putMobileDevicePrestageV2** | [**PutMobileDevicePrestageV2**](PutMobileDevicePrestageV2.md) | Mobile Device Prestage to update | 

### Return type

[**GetMobileDevicePrestageV2**](GetMobileDevicePrestageV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdScopeDeleteMultiplePost**
```swift
    open class func v2MobileDevicePrestagesIdScopeDeleteMultiplePost(id: String, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Remove Device Scope for a specific Mobile Device Prestage 

Remove device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to remove from scope

// Remove Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdScopeDeleteMultiplePost(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to remove from scope | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdScopeGet**
```swift
    open class func v2MobileDevicePrestagesIdScopeGet(id: String, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Get Device Scope for a specific Mobile Device Prestage 

Get device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier

// Get Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdScopeGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdScopePost**
```swift
    open class func v2MobileDevicePrestagesIdScopePost(id: String, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Add Device Scope for a specific Mobile Device Prestage 

Add device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Add Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdScopePost(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdScopePut**
```swift
    open class func v2MobileDevicePrestagesIdScopePut(id: String, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Replace Device Scope for a specific Mobile Device Prestage 

Replace device scope for a specific mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Replace Device Scope for a specific Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdScopePut(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdSyncsGet**
```swift
    open class func v2MobileDevicePrestagesIdSyncsGet(id: String, completion: @escaping (_ data: [PrestageSyncStatusV2]?, _ error: Error?) -> Void)
```

Get all prestage sync states for a single prestage 

Get all prestage sync states for a single prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier

// Get all prestage sync states for a single prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdSyncsGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 

### Return type

[**[PrestageSyncStatusV2]**](PrestageSyncStatusV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesIdSyncsLatestGet**
```swift
    open class func v2MobileDevicePrestagesIdSyncsLatestGet(id: String, completion: @escaping (_ data: PrestageSyncStatusV2?, _ error: Error?) -> Void)
```

Get the latest Sync State for a single Prestage 

Get the latest sync state for a single prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Prestage identifier

// Get the latest Sync State for a single Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesIdSyncsLatestGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Prestage identifier | 

### Return type

[**PrestageSyncStatusV2**](PrestageSyncStatusV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesPost**
```swift
    open class func v2MobileDevicePrestagesPost(mobileDevicePrestageV2: MobileDevicePrestageV2, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Mobile Device Prestage 

Create a mobile device prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let mobileDevicePrestageV2 = MobileDevicePrestageV2(displayName: "displayName_example", mandatory: false, mdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", defaultPrestage: false, enrollmentSiteId: "enrollmentSiteId_example", keepExistingSiteMembership: true, keepExistingLocationInformation: true, requireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", preventActivationLock: true, enableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: "deviceEnrollmentProgramInstanceId_example", skipSetupItems: "TODO", locationInformation: LocationInformationV2(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: "departmentId_example", buildingId: "buildingId_example", id: "id_example", versionLock: 123), purchasingInformation: PrestagePurchasingInformationV2(id: "id_example", leased: true, purchased: true, appleCareId: "appleCareId_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: "enrollmentCustomizationId_example", language: "language_example", region: "region_example", autoAdvanceSetup: true, allowPairing: true, multiUser: true, supervised: true, maximumSharedAccounts: 123, configureDeviceBeforeSetupAssistant: true, names: MobileDevicePrestageNamesV2(assignNamesUsing: "assignNamesUsing_example", prestageDeviceNames: [MobileDevicePrestageNameV2(id: "id_example", deviceName: "deviceName_example", used: false)], deviceNamePrefix: "deviceNamePrefix_example", deviceNameSuffix: "deviceNameSuffix_example", singleDeviceName: "singleDeviceName_example", manageNames: true, deviceNamingConfigured: true), sendTimezone: true, timezone: "timezone_example", storageQuotaSizeMegabytes: 123, useStorageQuotaSize: true, temporarySessionOnly: false, enforceTemporarySessionTimeout: false, temporarySessionTimeout: 123, enforceUserSessionTimeout: false, userSessionTimeout: 123) // MobileDevicePrestageV2 | Mobile Device Prestage to create. ids defined in this body will be ignored

// Create a Mobile Device Prestage 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesPost(mobileDevicePrestageV2: mobileDevicePrestageV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mobileDevicePrestageV2** | [**MobileDevicePrestageV2**](MobileDevicePrestageV2.md) | Mobile Device Prestage to create. ids defined in this body will be ignored | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesScopeGet**
```swift
    open class func v2MobileDevicePrestagesScopeGet(completion: @escaping (_ data: PrestageScopeV2?, _ error: Error?) -> Void)
```

Get all Device Scope for all Mobile Device Prestages 

Get all device scope for all mobile device prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all Device Scope for all Mobile Device Prestages 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesScopeGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PrestageScopeV2**](PrestageScopeV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicePrestagesSyncsGet**
```swift
    open class func v2MobileDevicePrestagesSyncsGet(completion: @escaping (_ data: [PrestageSyncStatusV2]?, _ error: Error?) -> Void)
```

Get all Prestage sync States for all prestages 

Get all prestage sync states for all prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all Prestage sync States for all prestages 
MobileDevicePrestagesAPI.v2MobileDevicePrestagesSyncsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[PrestageSyncStatusV2]**](PrestageSyncStatusV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

