# BrandingAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1BrandingImagesDownloadIdGet**](BrandingAPI.md#v1brandingimagesdownloadidget) | **GET** /v1/branding-images/download/{id} | Download a self service branding image 


# **v1BrandingImagesDownloadIdGet**
```swift
    open class func v1BrandingImagesDownloadIdGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download a self service branding image 

Download a self service branding image

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of the self service branding image

// Download a self service branding image 
BrandingAPI.v1BrandingImagesDownloadIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of the self service branding image | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

