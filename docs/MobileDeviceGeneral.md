# MobileDeviceGeneral

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**udid** | **String** |  | [optional] 
**displayName** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**lastInventoryUpdateDate** | **Date** |  | [optional] 
**osVersion** | **String** |  | [optional] 
**osRapidSecurityResponse** | **String** |  | [optional] 
**osBuild** | **String** |  | [optional] 
**osSupplementalBuildVersion** | **String** |  | [optional] 
**softwareUpdateDeviceId** | **String** |  | [optional] 
**ipAddress** | **String** |  | [optional] 
**managed** | **Bool** |  | [optional] 
**supervised** | **Bool** |  | [optional] 
**deviceOwnershipType** | **String** |  | [optional] 
**enrollmentMethodPrestage** | [**EnrollmentMethodPrestage**](EnrollmentMethodPrestage.md) |  | [optional] 
**enrollmentSessionTokenValid** | **Bool** |  | [optional] 
**lastEnrolledDate** | **Date** |  | [optional] 
**mdmProfileExpirationDate** | **Date** |  | [optional] 
**timeZone** | **String** | IANA time zone database name | [optional] 
**declarativeDeviceManagementEnabled** | **Bool** |  | [optional] 
**extensionAttributes** | [MobileDeviceExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


