# MobileDevice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**wifiMacAddress** | **String** |  | [optional] 
**udid** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**softwareUpdateDeviceId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


