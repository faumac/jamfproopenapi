# InventoryInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**managedComputers** | **Int** | Number of managed computers in inventory. | [optional] [readonly] 
**unmanagedComputers** | **Int** | Number of unmanaged computers in inventory. | [optional] [readonly] 
**managedDevices** | **Int** | Number of managed devices in inventory. | [optional] [readonly] 
**unmanagedDevices** | **Int** | Number of unmanaged devices in inventory. | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


