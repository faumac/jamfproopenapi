# DashboardSetupTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **Bool** |  | [optional] [default to false]
**error** | [**DashboardApiError**](DashboardApiError.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


