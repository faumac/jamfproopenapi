# LdapGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**ldapServerId** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**distinguishedName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


