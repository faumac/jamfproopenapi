# Assignment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mobileDeviceId** | **String** |  | [optional] 
**selected** | **Bool** | If true the device should be added to the group, if false should be removed from the group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


