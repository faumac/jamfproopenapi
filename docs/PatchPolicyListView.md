# PatchPolicyListView

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**policyName** | **String** |  | [optional] 
**policyEnabled** | **Bool** |  | [optional] 
**policyTargetVersion** | **String** |  | [optional] 
**policyDeploymentMethod** | **String** |  | [optional] 
**softwareTitle** | **String** |  | [optional] 
**softwareTitleConfigurationId** | **String** |  | [optional] 
**pending** | **Int** |  | [optional] 
**completed** | **Int** |  | [optional] 
**deferred** | **Int** |  | [optional] 
**failed** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


