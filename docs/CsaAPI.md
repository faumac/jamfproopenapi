# CsaAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1CsaTokenDelete**](CsaAPI.md#v1csatokendelete) | **DELETE** /v1/csa/token | Delete the CSA token exchange - This will disable Jamf Pro&#39;s ability to authenticate with cloud-hosted services 
[**v1CsaTokenGet**](CsaAPI.md#v1csatokenget) | **GET** /v1/csa/token | Get details regarding the CSA token exchange 
[**v1CsaTokenPost**](CsaAPI.md#v1csatokenpost) | **POST** /v1/csa/token | Initialize the CSA token exchange 
[**v1CsaTokenPut**](CsaAPI.md#v1csatokenput) | **PUT** /v1/csa/token | Re-initialize the CSA token exchange with new credentials 


# **v1CsaTokenDelete**
```swift
    open class func v1CsaTokenDelete(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the CSA token exchange - This will disable Jamf Pro's ability to authenticate with cloud-hosted services 

Delete the CSA token exchange - This will disable Jamf Pro's ability to authenticate with cloud-hosted services 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete the CSA token exchange - This will disable Jamf Pro's ability to authenticate with cloud-hosted services 
CsaAPI.v1CsaTokenDelete() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CsaTokenGet**
```swift
    open class func v1CsaTokenGet(completion: @escaping (_ data: CsaToken?, _ error: Error?) -> Void)
```

Get details regarding the CSA token exchange 

Get details regarding the CSA token exchange 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get details regarding the CSA token exchange 
CsaAPI.v1CsaTokenGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CsaToken**](CsaToken.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CsaTokenPost**
```swift
    open class func v1CsaTokenPost(jamfNationCredentials: JamfNationCredentials? = nil, completion: @escaping (_ data: CsaToken?, _ error: Error?) -> Void)
```

Initialize the CSA token exchange 

Initializes the CSA token exchange - This will allow Jamf Pro to authenticate with cloud-hosted services 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let jamfNationCredentials = JamfNationCredentials(emailAddress: "emailAddress_example", password: "password_example") // JamfNationCredentials | Jamf Nation username and password  (optional)

// Initialize the CSA token exchange 
CsaAPI.v1CsaTokenPost(jamfNationCredentials: jamfNationCredentials) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jamfNationCredentials** | [**JamfNationCredentials**](JamfNationCredentials.md) | Jamf Nation username and password  | [optional] 

### Return type

[**CsaToken**](CsaToken.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CsaTokenPut**
```swift
    open class func v1CsaTokenPut(jamfNationCredentials: JamfNationCredentials? = nil, completion: @escaping (_ data: CsaToken?, _ error: Error?) -> Void)
```

Re-initialize the CSA token exchange with new credentials 

Re-initialize the CSA token exchange with new credentials 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let jamfNationCredentials = JamfNationCredentials(emailAddress: "emailAddress_example", password: "password_example") // JamfNationCredentials | Jamf Nation username and password  (optional)

// Re-initialize the CSA token exchange with new credentials 
CsaAPI.v1CsaTokenPut(jamfNationCredentials: jamfNationCredentials) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jamfNationCredentials** | [**JamfNationCredentials**](JamfNationCredentials.md) | Jamf Nation username and password  | [optional] 

### Return type

[**CsaToken**](CsaToken.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

