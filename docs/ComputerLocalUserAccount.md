# ComputerLocalUserAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **String** |  | [optional] 
**userGuid** | **String** |  | [optional] [readonly] 
**username** | **String** |  | [optional] 
**fullName** | **String** |  | [optional] 
**admin** | **Bool** |  | [optional] 
**homeDirectory** | **String** |  | [optional] 
**homeDirectorySizeMb** | **Int64** | Home directory size in MB. | [optional] [readonly] 
**fileVault2Enabled** | **Bool** |  | [optional] 
**userAccountType** | **String** |  | [optional] 
**passwordMinLength** | **Int** |  | [optional] 
**passwordMaxAge** | **Int** |  | [optional] 
**passwordMinComplexCharacters** | **Int** |  | [optional] 
**passwordHistoryDepth** | **Int** |  | [optional] 
**passwordRequireAlphanumeric** | **Bool** |  | [optional] 
**computerAzureActiveDirectoryId** | **String** |  | [optional] 
**userAzureActiveDirectoryId** | **String** |  | [optional] 
**azureActiveDirectoryId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


