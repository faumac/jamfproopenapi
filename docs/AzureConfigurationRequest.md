# AzureConfigurationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cloudIdPCommon** | [**CloudIdPCommonRequest**](CloudIdPCommonRequest.md) |  | 
**server** | [**AzureServerConfigurationRequest**](AzureServerConfigurationRequest.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


