# CloudLdapAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1LdapKeystoreVerifyPost**](CloudLdapAPI.md#v1ldapkeystoreverifypost) | **POST** /v1/ldap-keystore/verify | Validate keystore for Cloud Identity Provider secure connection
[**v2CloudLdapsDefaultsProviderMappingsGet**](CloudLdapAPI.md#v2cloudldapsdefaultsprovidermappingsget) | **GET** /v2/cloud-ldaps/defaults/{provider}/mappings | Get default mappings
[**v2CloudLdapsDefaultsProviderServerConfigurationGet**](CloudLdapAPI.md#v2cloudldapsdefaultsproviderserverconfigurationget) | **GET** /v2/cloud-ldaps/defaults/{provider}/server-configuration | Get default server configuration
[**v2CloudLdapsIdConnectionBindGet**](CloudLdapAPI.md#v2cloudldapsidconnectionbindget) | **GET** /v2/cloud-ldaps/{id}/connection/bind | Get bind connection pool statistics
[**v2CloudLdapsIdConnectionSearchGet**](CloudLdapAPI.md#v2cloudldapsidconnectionsearchget) | **GET** /v2/cloud-ldaps/{id}/connection/search | Get search connection pool statistics
[**v2CloudLdapsIdConnectionStatusGet**](CloudLdapAPI.md#v2cloudldapsidconnectionstatusget) | **GET** /v2/cloud-ldaps/{id}/connection/status | Tests the communication with the specified cloud connection 
[**v2CloudLdapsIdDelete**](CloudLdapAPI.md#v2cloudldapsiddelete) | **DELETE** /v2/cloud-ldaps/{id} | Delete Cloud Identity Provider configuration.
[**v2CloudLdapsIdGet**](CloudLdapAPI.md#v2cloudldapsidget) | **GET** /v2/cloud-ldaps/{id} | Get Cloud Identity Provider configuration with given id.
[**v2CloudLdapsIdMappingsGet**](CloudLdapAPI.md#v2cloudldapsidmappingsget) | **GET** /v2/cloud-ldaps/{id}/mappings | Get mappings configurations for Cloud Identity Providers server configuration.
[**v2CloudLdapsIdMappingsPut**](CloudLdapAPI.md#v2cloudldapsidmappingsput) | **PUT** /v2/cloud-ldaps/{id}/mappings | Update Cloud Identity Provider mappings configuration.
[**v2CloudLdapsIdPut**](CloudLdapAPI.md#v2cloudldapsidput) | **PUT** /v2/cloud-ldaps/{id} | Update Cloud Identity Provider configuration
[**v2CloudLdapsPost**](CloudLdapAPI.md#v2cloudldapspost) | **POST** /v2/cloud-ldaps | Create Cloud Identity Provider configuration


# **v1LdapKeystoreVerifyPost**
```swift
    open class func v1LdapKeystoreVerifyPost(cloudLdapKeystoreFile: CloudLdapKeystoreFile, completion: @escaping (_ data: CloudLdapKeystore?, _ error: Error?) -> Void)
```

Validate keystore for Cloud Identity Provider secure connection

Validate keystore for Cloud Identity Provider secure connection

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let cloudLdapKeystoreFile = CloudLdapKeystoreFile(password: "password_example", fileBytes: 123, fileName: "fileName_example") // CloudLdapKeystoreFile | 

// Validate keystore for Cloud Identity Provider secure connection
CloudLdapAPI.v1LdapKeystoreVerifyPost(cloudLdapKeystoreFile: cloudLdapKeystoreFile) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cloudLdapKeystoreFile** | [**CloudLdapKeystoreFile**](CloudLdapKeystoreFile.md) |  | 

### Return type

[**CloudLdapKeystore**](CloudLdapKeystore.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsDefaultsProviderMappingsGet**
```swift
    open class func v2CloudLdapsDefaultsProviderMappingsGet(provider: String, completion: @escaping (_ data: CloudLdapMappingsResponse?, _ error: Error?) -> Void)
```

Get default mappings

Get default mappings for Cloud Identity Provider Provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let provider = "provider_example" // String | Cloud Identity Provider name

// Get default mappings
CloudLdapAPI.v2CloudLdapsDefaultsProviderMappingsGet(provider: provider) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider** | **String** | Cloud Identity Provider name | 

### Return type

[**CloudLdapMappingsResponse**](CloudLdapMappingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsDefaultsProviderServerConfigurationGet**
```swift
    open class func v2CloudLdapsDefaultsProviderServerConfigurationGet(provider: String, completion: @escaping (_ data: CloudLdapServerResponse?, _ error: Error?) -> Void)
```

Get default server configuration

Get default server configuration for Cloud Identity Provider Identity Provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let provider = "provider_example" // String | Cloud Identity Provider name

// Get default server configuration
CloudLdapAPI.v2CloudLdapsDefaultsProviderServerConfigurationGet(provider: provider) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider** | **String** | Cloud Identity Provider name | 

### Return type

[**CloudLdapServerResponse**](CloudLdapServerResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdConnectionBindGet**
```swift
    open class func v2CloudLdapsIdConnectionBindGet(id: String, completion: @escaping (_ data: CloudLdapConnectionPoolStatistics?, _ error: Error?) -> Void)
```

Get bind connection pool statistics

Get all search connection pool for chosen Cloud Identity Provider. numConnectionsClosedDefunct - The number of connections that have been closed as defunct. numConnectionsClosedExpired - The number of connections that have been closed because they were expired. numConnectionsClosedUnneeded - The number of connections that have been closed because they were no longer needed. numFailedCheckouts - The number of failed attempts to check out a connection from the pool. numFailedConnectionAttempts - The number of failed attempts to create a connection for use in the pool. numReleasedValid - The number of valid connections released back to the pool. numSuccessfulCheckouts - The number of successful attempts to check out a connection from the pool. numSuccessfulCheckoutsNewConnection - The number of successful checkout attempts that had to create a new connection because none were available. numSuccessfulConnectionAttempts - The number successful attempts to create a connection for use in the pool. maximumAvailableConnections - The maximum number of connections that may be available in the pool at any time. numSuccessfulCheckoutsWithoutWait - The number of successful checkout attempts that were able to take an existing connection without waiting. numSuccessfulCheckoutsAfterWait - The number of successful checkout attempts that retrieved a connection from the pool after waiting for it to become available. numAvailableConnections - The number of connections currently available for use in the pool. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Get bind connection pool statistics
CloudLdapAPI.v2CloudLdapsIdConnectionBindGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**CloudLdapConnectionPoolStatistics**](CloudLdapConnectionPoolStatistics.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdConnectionSearchGet**
```swift
    open class func v2CloudLdapsIdConnectionSearchGet(id: String, completion: @escaping (_ data: CloudLdapConnectionPoolStatistics?, _ error: Error?) -> Void)
```

Get search connection pool statistics

Get all search connection pool for chosen Cloud Identity Provider. numConnectionsClosedDefunct - The number of connections that have been closed as defunct. numConnectionsClosedExpired - The number of connections that have been closed because they were expired. numConnectionsClosedUnneeded - The number of connections that have been closed because they were no longer needed. numFailedCheckouts - The number of failed attempts to check out a connection from the pool. numFailedConnectionAttempts - The number of failed attempts to create a connection for use in the pool. numReleasedValid - The number of valid connections released back to the pool. numSuccessfulCheckouts - The number of successful attempts to check out a connection from the pool. numSuccessfulCheckoutsNewConnection - The number of successful checkout attempts that had to create a new connection because none were available. numSuccessfulConnectionAttempts - The number successful attempts to create a connection for use in the pool. maximumAvailableConnections - The maximum number of connections that may be available in the pool at any time. numSuccessfulCheckoutsWithoutWait - The number of successful checkout attempts that were able to take an existing connection without waiting. numSuccessfulCheckoutsAfterWait - The number of successful checkout attempts that retrieved a connection from the pool after waiting for it to become available. numAvailableConnections - The number of connections currently available for use in the pool. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Get search connection pool statistics
CloudLdapAPI.v2CloudLdapsIdConnectionSearchGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**CloudLdapConnectionPoolStatistics**](CloudLdapConnectionPoolStatistics.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdConnectionStatusGet**
```swift
    open class func v2CloudLdapsIdConnectionStatusGet(id: String, completion: @escaping (_ data: CloudLdapConnectionStatus?, _ error: Error?) -> Void)
```

Tests the communication with the specified cloud connection 

Tests the communication with the specified cloud connection 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Tests the communication with the specified cloud connection 
CloudLdapAPI.v2CloudLdapsIdConnectionStatusGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**CloudLdapConnectionStatus**](CloudLdapConnectionStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdDelete**
```swift
    open class func v2CloudLdapsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Cloud Identity Provider configuration.

Delete Cloud Identity Provider configuration.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Delete Cloud Identity Provider configuration.
CloudLdapAPI.v2CloudLdapsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdGet**
```swift
    open class func v2CloudLdapsIdGet(id: String, completion: @escaping (_ data: LdapConfigurationResponse?, _ error: Error?) -> Void)
```

Get Cloud Identity Provider configuration with given id.

Get Cloud Identity Provider configuration with given id.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Get Cloud Identity Provider configuration with given id.
CloudLdapAPI.v2CloudLdapsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**LdapConfigurationResponse**](LdapConfigurationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdMappingsGet**
```swift
    open class func v2CloudLdapsIdMappingsGet(id: String, completion: @escaping (_ data: CloudLdapMappingsResponse?, _ error: Error?) -> Void)
```

Get mappings configurations for Cloud Identity Providers server configuration.

Get all mappings configurations for Cloud Identity Providers server configuration.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Get mappings configurations for Cloud Identity Providers server configuration.
CloudLdapAPI.v2CloudLdapsIdMappingsGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**CloudLdapMappingsResponse**](CloudLdapMappingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdMappingsPut**
```swift
    open class func v2CloudLdapsIdMappingsPut(id: String, cloudLdapMappingsRequest: CloudLdapMappingsRequest, completion: @escaping (_ data: CloudLdapMappingsResponse?, _ error: Error?) -> Void)
```

Update Cloud Identity Provider mappings configuration.

Update Cloud Identity Provider mappings configuration. Cannot be used for partial updates, all content body must be sent.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let cloudLdapMappingsRequest = CloudLdapMappingsRequest(userMappings: UserMappings(objectClassLimitation: "objectClassLimitation_example", objectClasses: "objectClasses_example", searchBase: "searchBase_example", searchScope: "searchScope_example", additionalSearchBase: "additionalSearchBase_example", userID: "userID_example", username: "username_example", realName: "realName_example", emailAddress: "emailAddress_example", department: "department_example", building: "building_example", room: "room_example", phone: "phone_example", position: "position_example", userUuid: "userUuid_example"), groupMappings: GroupMappings(objectClassLimitation: "objectClassLimitation_example", objectClasses: "objectClasses_example", searchBase: "searchBase_example", searchScope: "searchScope_example", groupID: "groupID_example", groupName: "groupName_example", groupUuid: "groupUuid_example"), membershipMappings: MembershipMappings(groupMembershipMapping: "groupMembershipMapping_example")) // CloudLdapMappingsRequest | Cloud Identity Provider mappings to update.

// Update Cloud Identity Provider mappings configuration.
CloudLdapAPI.v2CloudLdapsIdMappingsPut(id: id, cloudLdapMappingsRequest: cloudLdapMappingsRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **cloudLdapMappingsRequest** | [**CloudLdapMappingsRequest**](CloudLdapMappingsRequest.md) | Cloud Identity Provider mappings to update. | 

### Return type

[**CloudLdapMappingsResponse**](CloudLdapMappingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsIdPut**
```swift
    open class func v2CloudLdapsIdPut(id: String, ldapConfigurationUpdate: LdapConfigurationUpdate, completion: @escaping (_ data: LdapConfigurationResponse?, _ error: Error?) -> Void)
```

Update Cloud Identity Provider configuration

Update Cloud Identity Provider configuration. Cannot be used for partial updates, all content body must be sent.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let ldapConfigurationUpdate = LdapConfigurationUpdate(cloudIdPCommon: CloudIdPCommon(id: "id_example", displayName: "displayName_example", providerName: "providerName_example"), server: CloudLdapServerUpdate(serverUrl: "serverUrl_example", enabled: true, domainName: "domainName_example", port: 123, keystore: CloudLdapKeystoreFile(password: "password_example", fileBytes: 123, fileName: "fileName_example"), connectionTimeout: 123, searchTimeout: 123, useWildcards: true, connectionType: "connectionType_example", membershipCalculationOptimizationEnabled: true), mappings: CloudLdapMappingsRequest(userMappings: UserMappings(objectClassLimitation: "objectClassLimitation_example", objectClasses: "objectClasses_example", searchBase: "searchBase_example", searchScope: "searchScope_example", additionalSearchBase: "additionalSearchBase_example", userID: "userID_example", username: "username_example", realName: "realName_example", emailAddress: "emailAddress_example", department: "department_example", building: "building_example", room: "room_example", phone: "phone_example", position: "position_example", userUuid: "userUuid_example"), groupMappings: GroupMappings(objectClassLimitation: "objectClassLimitation_example", objectClasses: "objectClasses_example", searchBase: "searchBase_example", searchScope: "searchScope_example", groupID: "groupID_example", groupName: "groupName_example", groupUuid: "groupUuid_example"), membershipMappings: MembershipMappings(groupMembershipMapping: "groupMembershipMapping_example"))) // LdapConfigurationUpdate | Cloud Identity Provider configuration to update

// Update Cloud Identity Provider configuration
CloudLdapAPI.v2CloudLdapsIdPut(id: id, ldapConfigurationUpdate: ldapConfigurationUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **ldapConfigurationUpdate** | [**LdapConfigurationUpdate**](LdapConfigurationUpdate.md) | Cloud Identity Provider configuration to update | 

### Return type

[**LdapConfigurationResponse**](LdapConfigurationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CloudLdapsPost**
```swift
    open class func v2CloudLdapsPost(ldapConfigurationRequest: LdapConfigurationRequest, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Cloud Identity Provider configuration

Create new Cloud Identity Provider configuration with unique display name. If mappings not provided, then defaults will be generated instead.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ldapConfigurationRequest = LdapConfigurationRequest(cloudIdPCommon: CloudIdPCommonRequest(displayName: "displayName_example", providerName: "providerName_example"), server: CloudLdapServerRequest(serverUrl: "serverUrl_example", enabled: true, domainName: "domainName_example", port: 123, keystore: CloudLdapKeystoreFile(password: "password_example", fileBytes: 123, fileName: "fileName_example"), connectionTimeout: 123, searchTimeout: 123, useWildcards: true, connectionType: "connectionType_example", membershipCalculationOptimizationEnabled: true), mappings: CloudLdapMappingsRequest(userMappings: UserMappings(objectClassLimitation: "objectClassLimitation_example", objectClasses: "objectClasses_example", searchBase: "searchBase_example", searchScope: "searchScope_example", additionalSearchBase: "additionalSearchBase_example", userID: "userID_example", username: "username_example", realName: "realName_example", emailAddress: "emailAddress_example", department: "department_example", building: "building_example", room: "room_example", phone: "phone_example", position: "position_example", userUuid: "userUuid_example"), groupMappings: GroupMappings(objectClassLimitation: "objectClassLimitation_example", objectClasses: "objectClasses_example", searchBase: "searchBase_example", searchScope: "searchScope_example", groupID: "groupID_example", groupName: "groupName_example", groupUuid: "groupUuid_example"), membershipMappings: MembershipMappings(groupMembershipMapping: "groupMembershipMapping_example"))) // LdapConfigurationRequest | Cloud Identity Provider configuration to create

// Create Cloud Identity Provider configuration
CloudLdapAPI.v2CloudLdapsPost(ldapConfigurationRequest: ldapConfigurationRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ldapConfigurationRequest** | [**LdapConfigurationRequest**](LdapConfigurationRequest.md) | Cloud Identity Provider configuration to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

