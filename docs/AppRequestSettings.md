# AppRequestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isEnabled** | **Bool** |  | [optional] 
**appStoreLocale** | **String** | Can be any of the country codes from /v1/app-store-country-codes or \&quot;deviceLocale\&quot; to use each individual device&#39;s locale | [optional] 
**requesterUserGroupId** | **Int** |  | [optional] 
**approverEmails** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


