# IconAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1IconDownloadIdGet**](IconAPI.md#v1icondownloadidget) | **GET** /v1/icon/download/{id} | Download a self service icon 
[**v1IconIdGet**](IconAPI.md#v1iconidget) | **GET** /v1/icon/{id} | Get an icon 
[**v1IconPost**](IconAPI.md#v1iconpost) | **POST** /v1/icon | Upload an icon 


# **v1IconDownloadIdGet**
```swift
    open class func v1IconDownloadIdGet(id: String, res: String? = nil, scale: String? = nil, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download a self service icon 

Download a self service icon

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of the self service icon
let res = "res_example" // String | request a specific resolution of original, 300, or 512; invalid options will result in original resolution (optional) (default to "original")
let scale = "scale_example" // String | request a scale; 0 results in original image, non-0 results in scaled to 300 (optional) (default to "0")

// Download a self service icon 
IconAPI.v1IconDownloadIdGet(id: id, res: res, scale: scale) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of the self service icon | 
 **res** | **String** | request a specific resolution of original, 300, or 512; invalid options will result in original resolution | [optional] [default to &quot;original&quot;]
 **scale** | **String** | request a scale; 0 results in original image, non-0 results in scaled to 300 | [optional] [default to &quot;0&quot;]

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1IconIdGet**
```swift
    open class func v1IconIdGet(id: String, completion: @escaping (_ data: IconResponse?, _ error: Error?) -> Void)
```

Get an icon 

Get an icon

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of the icon

// Get an icon 
IconAPI.v1IconIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of the icon | 

### Return type

[**IconResponse**](IconResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1IconPost**
```swift
    open class func v1IconPost(file: URL, completion: @escaping (_ data: IconResponse?, _ error: Error?) -> Void)
```

Upload an icon 

Uploads an icon

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let file = URL(string: "https://example.com")! // URL | The file to upload

// Upload an icon 
IconAPI.v1IconPost(file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **URL** | The file to upload | 

### Return type

[**IconResponse**](IconResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

