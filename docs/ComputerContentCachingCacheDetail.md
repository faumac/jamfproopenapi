# ComputerContentCachingCacheDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**computerContentCachingCacheDetailsId** | **String** |  | [optional] [readonly] 
**categoryName** | **String** |  | [optional] [readonly] 
**diskSpaceBytesUsed** | **Int64** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


