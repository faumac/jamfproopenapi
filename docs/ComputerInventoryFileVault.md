# ComputerInventoryFileVault

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**computerId** | **String** |  | [optional] [readonly] 
**name** | **String** |  | [optional] 
**personalRecoveryKey** | **String** |  | [optional] 
**bootPartitionEncryptionDetails** | [**ComputerPartitionEncryption**](ComputerPartitionEncryption.md) |  | [optional] 
**individualRecoveryKeyValidityStatus** | **String** |  | [optional] 
**institutionalRecoveryKeyPresent** | **Bool** |  | [optional] 
**diskEncryptionConfigurationName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


