# Script

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | 
**info** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**priority** | **String** |  | [optional] 
**categoryId** | **String** |  | [optional] 
**categoryName** | **String** |  | [optional] 
**parameter4** | **String** |  | [optional] 
**parameter5** | **String** |  | [optional] 
**parameter6** | **String** |  | [optional] 
**parameter7** | **String** |  | [optional] 
**parameter8** | **String** |  | [optional] 
**parameter9** | **String** |  | [optional] 
**parameter10** | **String** |  | [optional] 
**parameter11** | **String** |  | [optional] 
**osRequirements** | **String** |  | [optional] 
**scriptContents** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


