# JamfProInitializationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SystemInitializeDatabaseConnectionPost**](JamfProInitializationAPI.md#v1systeminitializedatabaseconnectionpost) | **POST** /v1/system/initialize-database-connection | Provide Database Password during startup 
[**v1SystemInitializePost**](JamfProInitializationAPI.md#v1systeminitializepost) | **POST** /v1/system/initialize | Set up fresh installed Jamf Pro Server 


# **v1SystemInitializeDatabaseConnectionPost**
```swift
    open class func v1SystemInitializeDatabaseConnectionPost(databasePassword: DatabasePassword, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Provide Database Password during startup 

Provide database password during startup. Endpoint is accessible when database password was not configured and Jamf Pro server has not been initialized yet.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let databasePassword = DatabasePassword(password: "password_example") // DatabasePassword | 

// Provide Database Password during startup 
JamfProInitializationAPI.v1SystemInitializeDatabaseConnectionPost(databasePassword: databasePassword) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **databasePassword** | [**DatabasePassword**](DatabasePassword.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SystemInitializePost**
```swift
    open class func v1SystemInitializePost(initializeV1: InitializeV1, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Set up fresh installed Jamf Pro Server 

Set up fresh installed Jamf Pro Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let initializeV1 = InitializeV1(activationCode: "activationCode_example", institutionName: "institutionName_example", eulaAccepted: false, username: "username_example", password: "password_example", email: "email_example", jssUrl: "jssUrl_example") // InitializeV1 | 

// Set up fresh installed Jamf Pro Server 
JamfProInitializationAPI.v1SystemInitializePost(initializeV1: initializeV1) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **initializeV1** | [**InitializeV1**](InitializeV1.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

