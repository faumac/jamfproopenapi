# SelfServiceAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SelfServiceSettingsGet**](SelfServiceAPI.md#v1selfservicesettingsget) | **GET** /v1/self-service/settings | Get an object representation of Self Service settings 
[**v1SelfServiceSettingsPut**](SelfServiceAPI.md#v1selfservicesettingsput) | **PUT** /v1/self-service/settings | Put an object representation of Self Service settings 


# **v1SelfServiceSettingsGet**
```swift
    open class func v1SelfServiceSettingsGet(completion: @escaping (_ data: SelfServiceSettings?, _ error: Error?) -> Void)
```

Get an object representation of Self Service settings 

gets an object representation of Self Service settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get an object representation of Self Service settings 
SelfServiceAPI.v1SelfServiceSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SelfServiceSettings**](SelfServiceSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceSettingsPut**
```swift
    open class func v1SelfServiceSettingsPut(selfServiceSettings: SelfServiceSettings, completion: @escaping (_ data: SelfServiceSettings?, _ error: Error?) -> Void)
```

Put an object representation of Self Service settings 

puts an object representation of Self Service settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let selfServiceSettings = SelfServiceSettings(installSettings: SelfServiceInstallSettings(installAutomatically: false, installLocation: "installLocation_example"), loginSettings: SelfServiceLoginSettings(userLoginLevel: "userLoginLevel_example", allowRememberMe: false, authType: "authType_example"), configurationSettings: SelfServiceInteractionSettings(notificationsEnabled: false, alertUserApprovedMdm: false, defaultLandingPage: "defaultLandingPage_example", defaultHomeCategoryId: 123, bookmarksName: "bookmarksName_example")) // SelfServiceSettings | object that contains all editable global fields to alter Self Service settings 

// Put an object representation of Self Service settings 
SelfServiceAPI.v1SelfServiceSettingsPut(selfServiceSettings: selfServiceSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selfServiceSettings** | [**SelfServiceSettings**](SelfServiceSettings.md) | object that contains all editable global fields to alter Self Service settings  | 

### Return type

[**SelfServiceSettings**](SelfServiceSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

