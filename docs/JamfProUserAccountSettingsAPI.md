# JamfProUserAccountSettingsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1UserPreferencesKeyIdDelete**](JamfProUserAccountSettingsAPI.md#v1userpreferenceskeyiddelete) | **DELETE** /v1/user/preferences/{keyId} | Remove specified setting for authenticated user 
[**v1UserPreferencesKeyIdGet**](JamfProUserAccountSettingsAPI.md#v1userpreferenceskeyidget) | **GET** /v1/user/preferences/{keyId} | Get the user setting for the authenticated user and key 
[**v1UserPreferencesKeyIdPut**](JamfProUserAccountSettingsAPI.md#v1userpreferenceskeyidput) | **PUT** /v1/user/preferences/{keyId} | Persist the user setting 
[**v1UserPreferencesSettingsKeyIdGet**](JamfProUserAccountSettingsAPI.md#v1userpreferencessettingskeyidget) | **GET** /v1/user/preferences/settings/{keyId} | Get the user preferences for the authenticated user and key. 


# **v1UserPreferencesKeyIdDelete**
```swift
    open class func v1UserPreferencesKeyIdDelete(keyId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified setting for authenticated user 

Remove specified setting for authenticated user 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let keyId = "keyId_example" // String | unique key of user setting to be persisted

// Remove specified setting for authenticated user 
JamfProUserAccountSettingsAPI.v1UserPreferencesKeyIdDelete(keyId: keyId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyId** | **String** | unique key of user setting to be persisted | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UserPreferencesKeyIdGet**
```swift
    open class func v1UserPreferencesKeyIdGet(keyId: String, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Get the user setting for the authenticated user and key 

Gets the user setting for the authenticated user and key. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let keyId = "keyId_example" // String | user setting to be retrieved

// Get the user setting for the authenticated user and key 
JamfProUserAccountSettingsAPI.v1UserPreferencesKeyIdGet(keyId: keyId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyId** | **String** | user setting to be retrieved | 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UserPreferencesKeyIdPut**
```swift
    open class func v1UserPreferencesKeyIdPut(keyId: String, body: AnyCodable? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Persist the user setting 

Persists the user setting 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let keyId = "keyId_example" // String | unique key of user setting to be persisted
let body = "TODO" // AnyCodable | user setting value to be persisted (optional)

// Persist the user setting 
JamfProUserAccountSettingsAPI.v1UserPreferencesKeyIdPut(keyId: keyId, body: body) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyId** | **String** | unique key of user setting to be persisted | 
 **body** | **AnyCodable** | user setting value to be persisted | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UserPreferencesSettingsKeyIdGet**
```swift
    open class func v1UserPreferencesSettingsKeyIdGet(keyId: String, completion: @escaping (_ data: UserPreferencesSettings?, _ error: Error?) -> Void)
```

Get the user preferences for the authenticated user and key. 

Gets the user preferences for the authenticated user and key. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let keyId = "keyId_example" // String | user setting to be retrieved

// Get the user preferences for the authenticated user and key. 
JamfProUserAccountSettingsAPI.v1UserPreferencesSettingsKeyIdGet(keyId: keyId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyId** | **String** | user setting to be retrieved | 

### Return type

[**UserPreferencesSettings**](UserPreferencesSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

