# MobileDeviceSearchParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pageNumber** | **Int** |  | [optional] 
**pageSize** | **Int** |  | [optional] 
**isLoadToEnd** | **Bool** |  | [optional] 
**orderBy** | [OrderBy] |  | [optional] 
**udid** | **String** |  | [optional] 
**macAddress** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**osType** | **String** |  | [optional] 
**isManaged** | **Bool** |  | [optional] 
**excludedIds** | **[Int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


