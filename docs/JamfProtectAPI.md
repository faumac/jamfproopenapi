# JamfProtectAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfProtectDelete**](JamfProtectAPI.md#v1jamfprotectdelete) | **DELETE** /v1/jamf-protect | Delete Jamf Protect API registration.
[**v1JamfProtectDeploymentsIdTasksGet**](JamfProtectAPI.md#v1jamfprotectdeploymentsidtasksget) | **GET** /v1/jamf-protect/deployments/{id}/tasks | Search for deployment tasks for a config profile linked to Jamf Protect 
[**v1JamfProtectDeploymentsIdTasksRetryPost**](JamfProtectAPI.md#v1jamfprotectdeploymentsidtasksretrypost) | **POST** /v1/jamf-protect/deployments/{id}/tasks/retry | Request a retry of Protect install tasks 
[**v1JamfProtectGet**](JamfProtectAPI.md#v1jamfprotectget) | **GET** /v1/jamf-protect | Jamf Protect integration settings
[**v1JamfProtectHistoryGet**](JamfProtectAPI.md#v1jamfprotecthistoryget) | **GET** /v1/jamf-protect/history | Get Jamf Protect history 
[**v1JamfProtectHistoryPost**](JamfProtectAPI.md#v1jamfprotecthistorypost) | **POST** /v1/jamf-protect/history | Add Jamf Protect history notes 
[**v1JamfProtectPlansGet**](JamfProtectAPI.md#v1jamfprotectplansget) | **GET** /v1/jamf-protect/plans | Get all of the previously synced Jamf Protect Plans with information about their associated configuration profile
[**v1JamfProtectPlansSyncPost**](JamfProtectAPI.md#v1jamfprotectplanssyncpost) | **POST** /v1/jamf-protect/plans/sync | Sync Plans with Jamf Protect
[**v1JamfProtectPut**](JamfProtectAPI.md#v1jamfprotectput) | **PUT** /v1/jamf-protect | Jamf Protect integration settings
[**v1JamfProtectRegisterPost**](JamfProtectAPI.md#v1jamfprotectregisterpost) | **POST** /v1/jamf-protect/register | Register a Jamf Protect API configuration with Jamf Pro


# **v1JamfProtectDelete**
```swift
    open class func v1JamfProtectDelete(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Jamf Protect API registration.

Deletes an existing Jamf Protect API registration if present. Jamf Protect API integration will be disabled.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete Jamf Protect API registration.
JamfProtectAPI.v1JamfProtectDelete() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectDeploymentsIdTasksGet**
```swift
    open class func v1JamfProtectDeploymentsIdTasksGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: DeploymentTaskSearchResults?, _ error: Error?) -> Void)
```

Search for deployment tasks for a config profile linked to Jamf Protect 

Search for config profiles linked to Jamf Protect

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | the UUID of the Jamf Protect deployment
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Search for deployment tasks for a config profile linked to Jamf Protect 
JamfProtectAPI.v1JamfProtectDeploymentsIdTasksGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | the UUID of the Jamf Protect deployment | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**DeploymentTaskSearchResults**](DeploymentTaskSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectDeploymentsIdTasksRetryPost**
```swift
    open class func v1JamfProtectDeploymentsIdTasksRetryPost(id: String, ids: Ids, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Request a retry of Protect install tasks 

Request a retry of Protect install tasks 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | the UUID of the deployment associated with the retry
let ids = Ids(ids: ["ids_example"]) // Ids | task IDs to retry

// Request a retry of Protect install tasks 
JamfProtectAPI.v1JamfProtectDeploymentsIdTasksRetryPost(id: id, ids: ids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | the UUID of the deployment associated with the retry | 
 **ids** | [**Ids**](Ids.md) | task IDs to retry | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectGet**
```swift
    open class func v1JamfProtectGet(completion: @escaping (_ data: ProtectSettingsResponse?, _ error: Error?) -> Void)
```

Jamf Protect integration settings

Jamf Protect integration settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Jamf Protect integration settings
JamfProtectAPI.v1JamfProtectGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ProtectSettingsResponse**](ProtectSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectHistoryGet**
```swift
    open class func v1JamfProtectHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Jamf Protect history 

Get Jamf Protect history 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Jamf Protect history 
JamfProtectAPI.v1JamfProtectHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectHistoryPost**
```swift
    open class func v1JamfProtectHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Jamf Protect history notes 

Add Jamf Protect history notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Jamf Protect history notes 
JamfProtectAPI.v1JamfProtectHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectPlansGet**
```swift
    open class func v1JamfProtectPlansGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: PlanSearchResults?, _ error: Error?) -> Void)
```

Get all of the previously synced Jamf Protect Plans with information about their associated configuration profile

Get all of the previously synced Jamf Protect Plans with information about their associated configuration profile

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get all of the previously synced Jamf Protect Plans with information about their associated configuration profile
JamfProtectAPI.v1JamfProtectPlansGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**PlanSearchResults**](PlanSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectPlansSyncPost**
```swift
    open class func v1JamfProtectPlansSyncPost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Sync Plans with Jamf Protect

Sync Plans with Jamf Protect. Configuration profiles associated with new plans will be imported to Jamf Pro.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Sync Plans with Jamf Protect
JamfProtectAPI.v1JamfProtectPlansSyncPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectPut**
```swift
    open class func v1JamfProtectPut(protectUpdatableSettingsRequest: ProtectUpdatableSettingsRequest, completion: @escaping (_ data: ProtectSettingsResponse?, _ error: Error?) -> Void)
```

Jamf Protect integration settings

Jamf Protect integration settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let protectUpdatableSettingsRequest = ProtectUpdatableSettingsRequest(autoInstall: true) // ProtectUpdatableSettingsRequest | Updatable Jamf Protect Settings

// Jamf Protect integration settings
JamfProtectAPI.v1JamfProtectPut(protectUpdatableSettingsRequest: protectUpdatableSettingsRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protectUpdatableSettingsRequest** | [**ProtectUpdatableSettingsRequest**](ProtectUpdatableSettingsRequest.md) | Updatable Jamf Protect Settings | 

### Return type

[**ProtectSettingsResponse**](ProtectSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProtectRegisterPost**
```swift
    open class func v1JamfProtectRegisterPost(protectRegistrationRequest: ProtectRegistrationRequest, completion: @escaping (_ data: ProtectSettingsResponse?, _ error: Error?) -> Void)
```

Register a Jamf Protect API configuration with Jamf Pro

Register a Jamf Protect API configuration with Jamf Pro

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let protectRegistrationRequest = ProtectRegistrationRequest(protectUrl: "protectUrl_example", clientId: "clientId_example", password: "password_example") // ProtectRegistrationRequest | Jamf Protect API connection information

// Register a Jamf Protect API configuration with Jamf Pro
JamfProtectAPI.v1JamfProtectRegisterPost(protectRegistrationRequest: protectRegistrationRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protectRegistrationRequest** | [**ProtectRegistrationRequest**](ProtectRegistrationRequest.md) | Jamf Protect API connection information | 

### Return type

[**ProtectSettingsResponse**](ProtectSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

