# AuthorizationV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**AuthAccountV1**](AuthAccountV1.md) |  | [optional] 
**accountGroups** | [AccountGroup] |  | [optional] 
**sites** | [V1Site] |  | [optional] 
**authenticationType** | [**AuthenticationType**](AuthenticationType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


