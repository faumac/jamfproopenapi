# MobileDeviceUserProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**identifier** | **String** |  | [optional] 
**removable** | **Bool** |  | [optional] 
**lastInstalled** | **Date** |  | [optional] 
**username** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


