# EbooksAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1EbooksGet**](EbooksAPI.md#v1ebooksget) | **GET** /v1/ebooks | Get Ebook object 
[**v1EbooksIdGet**](EbooksAPI.md#v1ebooksidget) | **GET** /v1/ebooks/{id} | Get specified Ebook object 
[**v1EbooksIdScopeGet**](EbooksAPI.md#v1ebooksidscopeget) | **GET** /v1/ebooks/{id}/scope | Get specified scope of Ebook object 


# **v1EbooksGet**
```swift
    open class func v1EbooksGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: EbookSearchResults?, _ error: Error?) -> Void)
```

Get Ebook object 

Gets ebook object

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Get Ebook object 
EbooksAPI.v1EbooksGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**EbookSearchResults**](EbookSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EbooksIdGet**
```swift
    open class func v1EbooksIdGet(id: String, completion: @escaping (_ data: Ebook?, _ error: Error?) -> Void)
```

Get specified Ebook object 

Gets specified Ebook object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of ebook record

// Get specified Ebook object 
EbooksAPI.v1EbooksIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of ebook record | 

### Return type

[**Ebook**](Ebook.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EbooksIdScopeGet**
```swift
    open class func v1EbooksIdScopeGet(id: String, completion: @escaping (_ data: EbookScope?, _ error: Error?) -> Void)
```

Get specified scope of Ebook object 

Gets specified scope of Ebook object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of ebook record

// Get specified scope of Ebook object 
EbooksAPI.v1EbooksIdScopeGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of ebook record | 

### Return type

[**EbookScope**](EbookScope.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

