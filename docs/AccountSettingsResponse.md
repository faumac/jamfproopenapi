# AccountSettingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id of Account Settings | [optional] 
**payloadConfigured** | **Bool** |  | [optional] [default to false]
**localAdminAccountEnabled** | **Bool** |  | [optional] [default to false]
**adminUsername** | **String** |  | [optional] [default to ""]
**hiddenAdminAccount** | **Bool** |  | [optional] [default to false]
**localUserManaged** | **Bool** |  | [optional] [default to false]
**userAccountType** | **String** |  | [optional] [default to .standard]
**versionLock** | **Int** |  | [optional] [default to 0]
**prefillPrimaryAccountInfoFeatureEnabled** | **Bool** |  | [optional] [default to false]
**prefillType** | **String** | Values accepted are only CUSTOM and DEVICE_OWNER | [optional] [default to "CUSTOM"]
**prefillAccountFullName** | **String** |  | [optional] [default to ""]
**prefillAccountUserName** | **String** |  | [optional] [default to ""]
**preventPrefillInfoFromModification** | **Bool** |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


