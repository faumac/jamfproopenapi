# ComputerInventory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**udid** | **String** |  | [optional] 
**general** | [**ComputerGeneral**](ComputerGeneral.md) |  | [optional] 
**diskEncryption** | [**ComputerDiskEncryption**](ComputerDiskEncryption.md) |  | [optional] 
**purchasing** | [**ComputerPurchase**](ComputerPurchase.md) |  | [optional] 
**applications** | [ComputerApplication] |  | [optional] 
**storage** | [**ComputerStorage**](ComputerStorage.md) |  | [optional] 
**userAndLocation** | [**ComputerUserAndLocation**](ComputerUserAndLocation.md) |  | [optional] 
**configurationProfiles** | [ComputerConfigurationProfile] |  | [optional] 
**printers** | [ComputerPrinter] |  | [optional] 
**services** | [ComputerService] |  | [optional] 
**hardware** | [**ComputerHardware**](ComputerHardware.md) |  | [optional] 
**localUserAccounts** | [ComputerLocalUserAccount] |  | [optional] 
**certificates** | [ComputerCertificate] |  | [optional] 
**attachments** | [ComputerAttachment] |  | [optional] 
**plugins** | [ComputerPlugin] |  | [optional] 
**packageReceipts** | [**ComputerPackageReceipts**](ComputerPackageReceipts.md) |  | [optional] 
**fonts** | [ComputerFont] |  | [optional] 
**security** | [**ComputerSecurity**](ComputerSecurity.md) |  | [optional] 
**operatingSystem** | [**ComputerOperatingSystem**](ComputerOperatingSystem.md) |  | [optional] 
**licensedSoftware** | [ComputerLicensedSoftware] |  | [optional] 
**ibeacons** | [ComputerIbeacon] |  | [optional] 
**softwareUpdates** | [ComputerSoftwareUpdate] |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 
**contentCaching** | [**ComputerContentCaching**](ComputerContentCaching.md) |  | [optional] 
**groupMemberships** | [GroupMembership] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


