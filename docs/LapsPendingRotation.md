# LapsPendingRotation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lapsUser** | [**LapsUserV2**](LapsUserV2.md) |  | [optional] 
**createdDate** | **Date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


