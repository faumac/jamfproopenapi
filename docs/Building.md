# Building

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | 
**streetAddress1** | **String** |  | [optional] 
**streetAddress2** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**stateProvince** | **String** |  | [optional] 
**zipPostalCode** | **String** |  | [optional] 
**country** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


