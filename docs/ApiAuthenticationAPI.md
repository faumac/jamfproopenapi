# ApiAuthenticationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authCurrentPost**](ApiAuthenticationAPI.md#authcurrentpost) | **POST** /auth/current | Get the authorization details associated with the current API token 
[**authGet**](ApiAuthenticationAPI.md#authget) | **GET** /auth | Get all the Authorization details associated with the current api 
[**authInvalidateTokenPost**](ApiAuthenticationAPI.md#authinvalidatetokenpost) | **POST** /auth/invalidateToken | Invalidate current token 
[**authKeepAlivePost**](ApiAuthenticationAPI.md#authkeepalivepost) | **POST** /auth/keepAlive | Invalidate existing token and generates new token 
[**authTokensPost**](ApiAuthenticationAPI.md#authtokenspost) | **POST** /auth/tokens | Create a token based on other authentication details (basic, etc.) 
[**v1AuthGet**](ApiAuthenticationAPI.md#v1authget) | **GET** /v1/auth | Get all the Authorization details associated with the current api 
[**v1AuthInvalidateTokenPost**](ApiAuthenticationAPI.md#v1authinvalidatetokenpost) | **POST** /v1/auth/invalidate-token | Invalidate current token 
[**v1AuthKeepAlivePost**](ApiAuthenticationAPI.md#v1authkeepalivepost) | **POST** /v1/auth/keep-alive | Invalidate existing token and generates new token 
[**v1AuthTokenPost**](ApiAuthenticationAPI.md#v1authtokenpost) | **POST** /v1/auth/token | Create a token based on other authentication details (basic, etc.) 


# **authCurrentPost**
```swift
    open class func authCurrentPost(completion: @escaping (_ data: CurrentAuthorization?, _ error: Error?) -> Void)
```

Get the authorization details associated with the current API token 

Get the authorization details associated with the current API token for the users current site

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the authorization details associated with the current API token 
ApiAuthenticationAPI.authCurrentPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CurrentAuthorization**](CurrentAuthorization.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authGet**
```swift
    open class func authGet(completion: @escaping (_ data: Authorization?, _ error: Error?) -> Void)
```

Get all the Authorization details associated with the current api 

Get all the authorization details associated with the current api token

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all the Authorization details associated with the current api 
ApiAuthenticationAPI.authGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Authorization**](Authorization.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authInvalidateTokenPost**
```swift
    open class func authInvalidateTokenPost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Invalidate current token 

Invalidates current token

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Invalidate current token 
ApiAuthenticationAPI.authInvalidateTokenPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authKeepAlivePost**
```swift
    open class func authKeepAlivePost(completion: @escaping (_ data: AuthToken?, _ error: Error?) -> Void)
```

Invalidate existing token and generates new token 

Invalidates existing token and generates new token with extended expiration based on existing token credentials.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Invalidate existing token and generates new token 
ApiAuthenticationAPI.authKeepAlivePost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthToken**](AuthToken.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authTokensPost**
```swift
    open class func authTokensPost(completion: @escaping (_ data: AuthToken?, _ error: Error?) -> Void)
```

Create a token based on other authentication details (basic, etc.) 

Create a token based on other authentication details (basic, etc.) 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Create a token based on other authentication details (basic, etc.) 
ApiAuthenticationAPI.authTokensPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthToken**](AuthToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AuthGet**
```swift
    open class func v1AuthGet(completion: @escaping (_ data: AuthorizationV1?, _ error: Error?) -> Void)
```

Get all the Authorization details associated with the current api 

Get all the authorization details associated with the current api token

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all the Authorization details associated with the current api 
ApiAuthenticationAPI.v1AuthGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthorizationV1**](AuthorizationV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AuthInvalidateTokenPost**
```swift
    open class func v1AuthInvalidateTokenPost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Invalidate current token 

Invalidates current token

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Invalidate current token 
ApiAuthenticationAPI.v1AuthInvalidateTokenPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AuthKeepAlivePost**
```swift
    open class func v1AuthKeepAlivePost(completion: @escaping (_ data: AuthTokenV1?, _ error: Error?) -> Void)
```

Invalidate existing token and generates new token 

Invalidates existing token and generates new token with extended expiration based on existing token credentials.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Invalidate existing token and generates new token 
ApiAuthenticationAPI.v1AuthKeepAlivePost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthTokenV1**](AuthTokenV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AuthTokenPost**
```swift
    open class func v1AuthTokenPost(completion: @escaping (_ data: AuthTokenV1?, _ error: Error?) -> Void)
```

Create a token based on other authentication details (basic, etc.) 

Create a token based on other authentication details (basic, etc.) 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Create a token based on other authentication details (basic, etc.) 
ApiAuthenticationAPI.v1AuthTokenPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthTokenV1**](AuthTokenV1.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

