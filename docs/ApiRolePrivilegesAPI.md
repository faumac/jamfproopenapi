# ApiRolePrivilegesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ApiRolePrivilegesGet**](ApiRolePrivilegesAPI.md#v1apiroleprivilegesget) | **GET** /v1/api-role-privileges | Get the current Jamf API Role Privileges
[**v1ApiRolePrivilegesSearchGet**](ApiRolePrivilegesAPI.md#v1apiroleprivilegessearchget) | **GET** /v1/api-role-privileges/search | Search the current Jamf API Role Privileges


# **v1ApiRolePrivilegesGet**
```swift
    open class func v1ApiRolePrivilegesGet(completion: @escaping (_ data: ApiRolePrivileges?, _ error: Error?) -> Void)
```

Get the current Jamf API Role Privileges

Get role privileges

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the current Jamf API Role Privileges
ApiRolePrivilegesAPI.v1ApiRolePrivilegesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiRolePrivileges**](ApiRolePrivileges.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ApiRolePrivilegesSearchGet**
```swift
    open class func v1ApiRolePrivilegesSearchGet(name: String, limit: String? = nil, completion: @escaping (_ data: ApiRolePrivileges?, _ error: Error?) -> Void)
```

Search the current Jamf API Role Privileges

Search role privileges

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let name = "name_example" // String | The partial or complete privilege name we are searching for
let limit = "limit_example" // String | Limit the query results, defaults to 15 (optional) (default to "15")

// Search the current Jamf API Role Privileges
ApiRolePrivilegesAPI.v1ApiRolePrivilegesSearchGet(name: name, limit: limit) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String** | The partial or complete privilege name we are searching for | 
 **limit** | **String** | Limit the query results, defaults to 15 | [optional] [default to &quot;15&quot;]

### Return type

[**ApiRolePrivileges**](ApiRolePrivileges.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

