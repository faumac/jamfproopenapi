# MobileDevicePrestageNamesV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignNamesUsing** | **String** |  | [optional] 
**prestageDeviceNames** | [MobileDevicePrestageNameV2] |  | [optional] 
**deviceNamePrefix** | **String** |  | [optional] 
**deviceNameSuffix** | **String** |  | [optional] 
**singleDeviceName** | **String** |  | [optional] 
**manageNames** | **Bool** |  | [optional] 
**deviceNamingConfigured** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


