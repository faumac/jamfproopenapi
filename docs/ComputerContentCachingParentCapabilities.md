# ComputerContentCachingParentCapabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentCachingParentCapabilitiesId** | **String** |  | [optional] [readonly] 
**imports** | **Bool** |  | [optional] [readonly] 
**namespaces** | **Bool** |  | [optional] [readonly] 
**personalContent** | **Bool** |  | [optional] [readonly] 
**queryParameters** | **Bool** |  | [optional] [readonly] 
**sharedContent** | **Bool** |  | [optional] [readonly] 
**prioritization** | **Bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


