# VolumePurchasingLocationPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | If no value is provided when creating a VolumePurchasingLocation object, the &#39;name&#39; will default to the &#39;locationName&#39; value | [optional] 
**automaticallyPopulatePurchasedContent** | **Bool** |  | [optional] [default to false]
**sendNotificationWhenNoLongerAssigned** | **Bool** |  | [optional] [default to false]
**autoRegisterManagedUsers** | **Bool** |  | [optional] [default to false]
**siteId** | **String** |  | [optional] [default to "-1"]
**serviceToken** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


