# MobileDeviceCertificateV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commonName** | **String** |  | [optional] 
**isIdentity** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


