# MobileDeviceServiceSubscriptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrierSettingsVersion** | **String** |  | [optional] 
**currentCarrierNetwork** | **String** |  | [optional] 
**currentMobileCountryCode** | **String** |  | [optional] 
**currentMobileNetworkCode** | **String** |  | [optional] 
**subscriberCarrierNetwork** | **String** |  | [optional] 
**eid** | **String** |  | [optional] 
**iccid** | **String** |  | [optional] 
**imei** | **String** |  | [optional] 
**dataPreferred** | **Bool** |  | [optional] 
**roaming** | **Bool** |  | [optional] 
**voicePreferred** | **Bool** |  | [optional] 
**label** | **String** |  | [optional] 
**labelId** | **String** | The unique identifier for this subscription. | [optional] 
**meid** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**slot** | **String** | The description of the slot that contains the SIM representing this subscription. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


