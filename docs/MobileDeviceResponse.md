# MobileDeviceResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mobileDeviceId** | **String** |  | [optional] [readonly] 
**deviceType** | **String** | Based on the value of this type either ios or appleTv objects will be populated. | 
**hardware** | [**MobileDeviceHardware**](MobileDeviceHardware.md) |  | [optional] 
**userAndLocation** | [**MobileDeviceUserAndLocation**](MobileDeviceUserAndLocation.md) |  | [optional] 
**purchasing** | [**MobileDevicePurchasing**](MobileDevicePurchasing.md) |  | [optional] 
**applications** | [MobileDeviceApplicationInventoryDetail] |  | [optional] 
**certificates** | [MobileDeviceCertificate] |  | [optional] 
**profiles** | [MobileDeviceProfile] |  | [optional] 
**userProfiles** | [MobileDeviceUserProfile] |  | [optional] 
**extensionAttributes** | [MobileDeviceExtensionAttribute] |  | [optional] 
**general** | [**MobileDeviceIosGeneral**](MobileDeviceIosGeneral.md) |  | [optional] 
**security** | [**MobileDeviceSecurity**](MobileDeviceSecurity.md) |  | [optional] 
**ebooks** | [MobileDeviceEbookInventoryDetail] |  | [optional] 
**network** | [**MobileDeviceNetwork**](MobileDeviceNetwork.md) |  | [optional] 
**serviceSubscriptions** | [MobileDeviceServiceSubscriptions] |  | [optional] 
**provisioningProfiles** | [MobileDeviceProvisioningProfiles] |  | [optional] 
**sharedUsers** | [MobileDeviceSharedUser] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


