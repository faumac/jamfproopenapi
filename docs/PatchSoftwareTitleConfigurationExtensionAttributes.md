# PatchSoftwareTitleConfigurationExtensionAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accepted** | **Bool** | Once an extension attribute is accepted, it cannot be reverted. | [optional] [default to false]
**eaId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


