# ComputerOperatingSystem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] [readonly] 
**version** | **String** |  | [optional] [readonly] 
**build** | **String** |  | [optional] [readonly] 
**supplementalBuildVersion** | **String** | Collected for macOS 13.0 or later | [optional] [readonly] 
**rapidSecurityResponse** | **String** | Collected for macOS 13.0 or later | [optional] [readonly] 
**activeDirectoryStatus** | **String** |  | [optional] [readonly] 
**fileVault2Status** | **String** |  | [optional] 
**softwareUpdateDeviceId** | **String** |  | [optional] [readonly] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


