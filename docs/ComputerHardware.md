# ComputerHardware

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**make** | **String** |  | [optional] [readonly] 
**model** | **String** |  | [optional] [readonly] 
**modelIdentifier** | **String** |  | [optional] [readonly] 
**serialNumber** | **String** |  | [optional] [readonly] 
**processorSpeedMhz** | **Int64** | Processor Speed in MHz. | [optional] [readonly] 
**processorCount** | **Int** |  | [optional] [readonly] 
**coreCount** | **Int** |  | [optional] [readonly] 
**processorType** | **String** |  | [optional] [readonly] 
**processorArchitecture** | **String** |  | [optional] [readonly] 
**busSpeedMhz** | **Int64** |  | [optional] [readonly] 
**cacheSizeKilobytes** | **Int64** | Cache Size in KB. | [optional] [readonly] 
**networkAdapterType** | **String** |  | [optional] 
**macAddress** | **String** |  | [optional] 
**altNetworkAdapterType** | **String** |  | [optional] 
**altMacAddress** | **String** |  | [optional] 
**totalRamMegabytes** | **Int64** | Total RAM Size in MB. | [optional] [readonly] 
**openRamSlots** | **Int** | Available RAM slots. | [optional] [readonly] 
**batteryCapacityPercent** | **Int** | Remaining percentage of battery power. | [optional] [readonly] 
**smcVersion** | **String** |  | [optional] [readonly] 
**nicSpeed** | **String** |  | [optional] [readonly] 
**opticalDrive** | **String** |  | [optional] [readonly] 
**bootRom** | **String** |  | [optional] [readonly] 
**bleCapable** | **Bool** |  | [optional] [readonly] 
**supportsIosAppInstalls** | **Bool** |  | [optional] [readonly] 
**appleSilicon** | **Bool** |  | [optional] [readonly] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


