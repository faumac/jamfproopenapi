# SsoSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ssoForEnrollmentEnabled** | **Bool** |  | [default to false]
**ssoBypassAllowed** | **Bool** |  | [default to false]
**ssoEnabled** | **Bool** |  | [default to false]
**ssoForMacOsSelfServiceEnabled** | **Bool** |  | [default to false]
**tokenExpirationDisabled** | **Bool** |  | [default to false]
**userAttributeEnabled** | **Bool** |  | [default to false]
**userAttributeName** | **String** |  | [optional] [default to " "]
**userMapping** | **String** |  | 
**enrollmentSsoForAdueEnabled** | **Bool** |  | [default to false]
**enrollmentSsoConfig** | [**EnrollmentSsoConfig**](EnrollmentSsoConfig.md) |  | [optional] 
**groupEnrollmentAccessEnabled** | **Bool** |  | [default to false]
**groupAttributeName** | **String** |  | [default to "http://schemas.xmlsoap.org/claims/Group"]
**groupRdnKey** | **String** |  | [default to " "]
**groupEnrollmentAccessName** | **String** |  | [optional] [default to " "]
**idpProviderType** | **String** |  | 
**idpUrl** | **String** |  | [optional] 
**entityId** | **String** |  | 
**metadataFileName** | **String** |  | [optional] 
**otherProviderTypeName** | **String** |  | [optional] [default to " "]
**federationMetadataFile** | **Data** |  | [optional] 
**metadataSource** | **String** |  | 
**sessionTimeout** | **Int** |  | [optional] [default to 480]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


