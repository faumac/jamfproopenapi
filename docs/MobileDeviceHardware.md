# MobileDeviceHardware

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capacityMb** | **Int** |  | [optional] 
**availableSpaceMb** | **Int** |  | [optional] 
**usedSpacePercentage** | **Int** |  | [optional] 
**batteryLevel** | **Int** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**wifiMacAddress** | **String** |  | [optional] 
**bluetoothMacAddress** | **String** |  | [optional] 
**modemFirmwareVersion** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**modelNumber** | **String** |  | [optional] 
**bluetoothLowEnergyCapable** | **Bool** |  | [optional] 
**deviceId** | **String** |  | [optional] 
**extensionAttributes** | [MobileDeviceExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


