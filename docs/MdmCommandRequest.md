# MdmCommandRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientData** | [MdmCommandClientRequest] |  | [optional] 
**commandData** | [**MdmCommandRequestCommandData**](MdmCommandRequestCommandData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


