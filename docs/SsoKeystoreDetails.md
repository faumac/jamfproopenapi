# SsoKeystoreDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keys** | **[String]** |  | [optional] 
**serialNumber** | **Int** |  | [optional] 
**subject** | **String** |  | [optional] 
**issuer** | **String** |  | [optional] 
**expiration** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


