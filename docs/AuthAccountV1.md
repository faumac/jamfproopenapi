# AuthAccountV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**realName** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**preferences** | [**AccountPreferencesV1**](AccountPreferencesV1.md) |  | [optional] 
**multiSiteAdmin** | **Bool** |  | [optional] 
**accessLevel** | **String** |  | [optional] 
**privilegeSet** | **String** |  | [optional] 
**privilegesBySite** | [String: [String]] |  | [optional] 
**groupIds** | **[String]** |  | [optional] 
**currentSiteId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


