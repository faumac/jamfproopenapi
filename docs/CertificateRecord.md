# CertificateRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subjectX500Principal** | **String** |  | [optional] 
**issuerX500Principal** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**version** | **Int** |  | [optional] 
**notAfter** | **Int** |  | [optional] 
**notBefore** | **Int** |  | [optional] 
**signature** | [**Signature**](Signature.md) |  | [optional] 
**keyUsage** | **[String]** |  | [optional] 
**keyUsageExtended** | **[String]** |  | [optional] 
**sha1Fingerprint** | **String** |  | [optional] 
**sha256Fingerprint** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


