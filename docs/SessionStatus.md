# SessionStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionState** | **String** | Session state | [optional] 
**online** | **Bool** | Defines if the end user is online | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


