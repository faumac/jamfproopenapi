# SsoFailoverData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**failoverUrl** | **String** |  | [optional] 
**generationTime** | **Int64** | Generation time of failover key | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


