# DeviceEnrollmentInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | 
**supervisionIdentityId** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**serverName** | **String** |  | [optional] [readonly] 
**serverUuid** | **String** |  | [optional] [readonly] 
**adminId** | **String** |  | [optional] [readonly] 
**orgName** | **String** |  | [optional] [readonly] 
**orgEmail** | **String** |  | [optional] [readonly] 
**orgPhone** | **String** |  | [optional] [readonly] 
**orgAddress** | **String** |  | [optional] [readonly] 
**tokenExpirationDate** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


