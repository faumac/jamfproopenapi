# InventoryPreloadRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] [readonly] 
**serialNumber** | **String** |  | 
**deviceType** | **String** |  | 
**username** | **String** |  | [optional] 
**fullName** | **String** |  | [optional] 
**emailAddress** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**department** | **String** |  | [optional] 
**building** | **String** |  | [optional] 
**room** | **String** |  | [optional] 
**poNumber** | **String** |  | [optional] 
**poDate** | **String** |  | [optional] 
**warrantyExpiration** | **String** |  | [optional] 
**appleCareId** | **String** |  | [optional] 
**lifeExpectancy** | **String** |  | [optional] 
**purchasePrice** | **String** |  | [optional] 
**purchasingContact** | **String** |  | [optional] 
**purchasingAccount** | **String** |  | [optional] 
**leaseExpiration** | **String** |  | [optional] 
**barCode1** | **String** |  | [optional] 
**barCode2** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**vendor** | **String** |  | [optional] 
**extensionAttributes** | [InventoryPreloadExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


