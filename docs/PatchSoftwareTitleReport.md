# PatchSoftwareTitleReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**computerName** | **String** |  | [optional] 
**deviceId** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**operatingSystemVersion** | **String** |  | [optional] 
**lastContactTime** | **Date** |  | [optional] 
**buildingName** | **String** |  | [optional] 
**departmentName** | **String** |  | [optional] 
**siteName** | **String** |  | [optional] 
**version** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


