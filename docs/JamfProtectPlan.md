# JamfProtectPlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** |  | [optional] [readonly] 
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | [optional] [readonly] 
**description** | **String** |  | [optional] [readonly] 
**profileId** | **Int** |  | [optional] [readonly] 
**profileName** | **String** |  | [optional] [readonly] 
**scopeDescription** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


