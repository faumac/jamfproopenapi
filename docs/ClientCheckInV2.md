# ClientCheckInV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkInFrequency** | **Int** | Suggested values are 5, 15, 30, or 60. Web interface will not display correctly if not one of those. Minimim is 5, maximum is 60. | [optional] [default to 15]
**createHooks** | **Bool** |  | [optional] [default to false]
**hookLog** | **Bool** |  | [optional] [default to false]
**hookPolicies** | **Bool** |  | [optional] [default to false]
**hookHideRestore** | **Bool** |  | [optional] [default to false]
**hookMcx** | **Bool** |  | [optional] [default to false]
**backgroundHooks** | **Bool** |  | [optional] [default to false]
**hookDisplayStatus** | **Bool** |  | [optional] [default to false]
**createStartupScript** | **Bool** |  | [optional] [default to false]
**startupLog** | **Bool** |  | [optional] [default to false]
**startupPolicies** | **Bool** |  | [optional] [default to false]
**startupSsh** | **Bool** |  | [optional] [default to false]
**startupMcx** | **Bool** |  | [optional] [default to false]
**enableLocalConfigurationProfiles** | **Bool** |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


