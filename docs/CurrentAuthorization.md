# CurrentAuthorization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**CurrentAccount**](CurrentAccount.md) |  | [optional] 
**sites** | [Site] |  | [optional] 
**authenticationType** | [**AuthenticationType**](AuthenticationType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


