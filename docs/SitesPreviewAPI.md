# SitesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**settingsSitesGet**](SitesPreviewAPI.md#settingssitesget) | **GET** /settings/sites | Find all sites 


# **settingsSitesGet**
```swift
    open class func settingsSitesGet(completion: @escaping (_ data: [Site]?, _ error: Error?) -> Void)
```

Find all sites 

Found all sites. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Find all sites 
SitesPreviewAPI.settingsSitesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Site]**](Site.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

