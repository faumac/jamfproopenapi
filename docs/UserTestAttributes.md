# UserTestAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fullName** | **String** |  | [optional] 
**emailAddress** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**room** | **String** |  | [optional] 
**buildingName** | **String** |  | [optional] 
**departmentName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


