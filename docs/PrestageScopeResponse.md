# PrestageScopeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prestageId** | **Int** |  | [optional] 
**assignments** | [PrestageScopeAssignment] |  | [optional] 
**versionLock** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


