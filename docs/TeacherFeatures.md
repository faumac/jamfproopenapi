# TeacherFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isAllowAppLock** | **Bool** |  | [optional] 
**isAllowWebLock** | **Bool** |  | [optional] 
**isAllowRestrictions** | **Bool** |  | [optional] 
**isAllowAttentionScreen** | **Bool** |  | [optional] 
**isAllowClearPasscode** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


