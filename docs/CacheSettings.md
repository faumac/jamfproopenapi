# CacheSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] [default to "0"]
**name** | **String** |  | [optional] [default to "cache configuration"]
**cacheType** | **String** |  | 
**timeToLiveSeconds** | **Int** |  | 
**timeToIdleSeconds** | **Int** |  | [optional] 
**directoryTimeToLiveSeconds** | **Int** |  | [optional] 
**ehcacheMaxBytesLocalHeap** | **String** |  | [optional] [default to "null"]
**cacheUniqueId** | **String** | The default is for Jamf Pro to generate a UUID, so we can only give an example instead. | 
**elasticache** | **Bool** |  | [optional] [default to false]
**memcachedEndpoints** | [MemcachedEndpoints] |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


