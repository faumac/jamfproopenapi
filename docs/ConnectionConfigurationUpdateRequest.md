# ConnectionConfigurationUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** | Name for Team Viewer Connection Configuration | [optional] 
**enabled** | **Bool** | Defines the intent to enable or disable Team Viewer connection | [optional] 
**sessionTimeout** | **Int** | Number of minutes before the session expires | [optional] 
**token** | **String** | Script token for Team Viewer Connection Configuration | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


