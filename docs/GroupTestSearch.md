# GroupTestSearch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distinguishedName** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**serverId** | **String** |  | [optional] 
**name** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


