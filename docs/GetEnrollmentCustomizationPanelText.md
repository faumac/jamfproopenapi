# GetEnrollmentCustomizationPanelText

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**rank** | **Int** |  | 
**body** | **String** |  | 
**subtext** | **String** |  | [optional] 
**title** | **String** |  | 
**backButtonText** | **String** |  | 
**continueButtonText** | **String** |  | 
**id** | **Int** |  | [optional] 
**type** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


