# InventoryInformationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1InventoryInformationGet**](InventoryInformationAPI.md#v1inventoryinformationget) | **GET** /v1/inventory-information | Get statistics about managed/unmanaged devices and computers in the inventory 


# **v1InventoryInformationGet**
```swift
    open class func v1InventoryInformationGet(completion: @escaping (_ data: InventoryInformation?, _ error: Error?) -> Void)
```

Get statistics about managed/unmanaged devices and computers in the inventory 

Gets statistics about managed/unmanaged devices and computers in the inventory. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get statistics about managed/unmanaged devices and computers in the inventory 
InventoryInformationAPI.v1InventoryInformationGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InventoryInformation**](InventoryInformation.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

