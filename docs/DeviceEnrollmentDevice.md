# DeviceEnrollmentDevice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**deviceEnrollmentProgramInstanceId** | **String** |  | [optional] 
**prestageId** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**color** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**profileStatus** | **String** |  | [optional] 
**syncState** | [**AssignRemoveProfileResponseSyncState**](AssignRemoveProfileResponseSyncState.md) |  | [optional] 
**profileAssignTime** | **String** |  | [optional] 
**profilePushTime** | **String** |  | [optional] 
**deviceAssignedDate** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


