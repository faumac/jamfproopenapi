# PrestageScopeResponseV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prestageId** | **String** |  | [optional] 
**assignments** | [PrestageScopeAssignmentV2] |  | [optional] 
**versionLock** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


