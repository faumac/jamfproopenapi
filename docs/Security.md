# Security

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isDataProtected** | **Bool** |  | [optional] 
**isBlockLevelEncryptionCapable** | **Bool** |  | [optional] 
**isFileLevelEncryptionCapable** | **Bool** |  | [optional] 
**isPasscodePresent** | **Bool** |  | [optional] 
**isPasscodeCompliant** | **Bool** |  | [optional] 
**isPasscodeCompliantWithProfile** | **Bool** |  | [optional] 
**hardwareEncryption** | **Int** |  | [optional] 
**isActivationLockEnabled** | **Bool** |  | [optional] 
**isJailBreakDetected** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


