# AndroidDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**osName** | **String** |  | [optional] 
**manufacturer** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**internalCapacityMb** | **Int** |  | [optional] 
**internalAvailableMb** | **Int** |  | [optional] 
**internalPercentUsed** | **Int** |  | [optional] 
**externalCapacityMb** | **Int** |  | [optional] 
**externalAvailableMb** | **Int** |  | [optional] 
**externalPercentUsed** | **Int** |  | [optional] 
**batteryLevel** | **Int** |  | [optional] 
**lastBackupTimestamp** | **Date** |  | [optional] 
**apiVersion** | **Int** |  | [optional] 
**computer** | [**IdAndName**](IdAndName.md) |  | [optional] 
**security** | [**Security**](Security.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


