# SmartUserGroupsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SmartUserGroupsIdRecalculatePost**](SmartUserGroupsPreviewAPI.md#v1smartusergroupsidrecalculatepost) | **POST** /v1/smart-user-groups/{id}/recalculate | Recalculate the smart group for the given id and then return the ids for the users in the smart group 
[**v1UsersIdRecalculateSmartGroupsPost**](SmartUserGroupsPreviewAPI.md#v1usersidrecalculatesmartgroupspost) | **POST** /v1/users/{id}/recalculate-smart-groups | Recalculate a smart group for the given user id and then return the count of smart groups the user falls into 


# **v1SmartUserGroupsIdRecalculatePost**
```swift
    open class func v1SmartUserGroupsIdRecalculatePost(id: Int, completion: @escaping (_ data: RecalculationResults?, _ error: Error?) -> Void)
```

Recalculate the smart group for the given id and then return the ids for the users in the smart group 

Recalculates the smart group for the given id and then returns the ids for the users in the smart group 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance id of smart group

// Recalculate the smart group for the given id and then return the ids for the users in the smart group 
SmartUserGroupsPreviewAPI.v1SmartUserGroupsIdRecalculatePost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance id of smart group | 

### Return type

[**RecalculationResults**](RecalculationResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UsersIdRecalculateSmartGroupsPost**
```swift
    open class func v1UsersIdRecalculateSmartGroupsPost(id: Int, completion: @escaping (_ data: RecalculationResults?, _ error: Error?) -> Void)
```

Recalculate a smart group for the given user id and then return the count of smart groups the user falls into 

Recalculates a smart group for the given user id and then returns the count of smart groups the user falls into 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | id of user

// Recalculate a smart group for the given user id and then return the count of smart groups the user falls into 
SmartUserGroupsPreviewAPI.v1UsersIdRecalculateSmartGroupsPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | id of user | 

### Return type

[**RecalculationResults**](RecalculationResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

