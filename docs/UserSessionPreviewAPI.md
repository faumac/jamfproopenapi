# UserSessionPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userGet**](UserSessionPreviewAPI.md#userget) | **GET** /user | Return all Jamf Pro user acounts 
[**userUpdateSessionPost**](UserSessionPreviewAPI.md#userupdatesessionpost) | **POST** /user/updateSession | Update values in the User&#39;s current session 


# **userGet**
```swift
    open class func userGet(completion: @escaping (_ data: [Account]?, _ error: Error?) -> Void)
```

Return all Jamf Pro user acounts 

Return all Jamf Pro user acounts. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return all Jamf Pro user acounts 
UserSessionPreviewAPI.userGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Account]**](Account.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userUpdateSessionPost**
```swift
    open class func userUpdateSessionPost(session: Session? = nil, completion: @escaping (_ data: Session?, _ error: Error?) -> Void)
```

Update values in the User's current session 

Updates values in the user's current session. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let session = Session(currentSiteId: 123) // Session | Values to update in user's current session. (optional)

// Update values in the User's current session 
UserSessionPreviewAPI.userUpdateSessionPost(session: session) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session** | [**Session**](Session.md) | Values to update in user&#39;s current session. | [optional] 

### Return type

[**Session**](Session.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

