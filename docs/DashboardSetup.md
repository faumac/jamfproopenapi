# DashboardSetup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setupTaskOptions** | [**DashboardSetupSetupTaskOptions**](DashboardSetupSetupTaskOptions.md) |  | [optional] 
**featureOptions** | [**DashboardSetupFeatureOptions**](DashboardSetupFeatureOptions.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


