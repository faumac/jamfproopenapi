# Attributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vpnUuid** | **String** |  | [optional] 
**associatedDomains** | **[String]** |  | [optional] 
**removable** | **Bool** |  | [optional] 
**enableDirectDownloads** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


