# LapsSettingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**autoDeployEnabled** | **Bool** | When enabled, all appropriate computers will have the SetAutoAdminPassword command sent to them automatically. | [optional] 
**passwordRotationTime** | **Int** | The amount of time in seconds that the local admin password will be rotated after viewing. | [optional] 
**autoExpirationTime** | **Int** | The amount of time in seconds that the local admin password will be rotated automatically if it is never viewed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


