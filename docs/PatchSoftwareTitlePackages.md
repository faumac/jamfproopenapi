# PatchSoftwareTitlePackages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**packageId** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**displayName** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


