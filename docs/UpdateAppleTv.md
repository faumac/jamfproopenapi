# UpdateAppleTv

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**airplayPassword** | **String** |  | [optional] 
**purchasing** | [**Purchasing**](Purchasing.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


