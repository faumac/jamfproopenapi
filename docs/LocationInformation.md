# LocationInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**realname** | **String** |  | 
**phone** | **String** |  | 
**email** | **String** |  | 
**room** | **String** |  | 
**position** | **String** |  | 
**departmentId** | **Int** |  | 
**buildingId** | **Int** |  | 
**id** | **Int** |  | 
**versionLock** | **Int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


