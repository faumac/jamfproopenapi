# AvailableOsUpdatesAvailableUpdates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**macOS** | **[String]** |  | [optional] 
**iOS** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


