# VppSubscriptionsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vppSubscriptionsGet**](VppSubscriptionsPreviewAPI.md#vppsubscriptionsget) | **GET** /vpp/subscriptions | Found all VPP - subscriptions 
[**vppSubscriptionsIdGet**](VppSubscriptionsPreviewAPI.md#vppsubscriptionsidget) | **GET** /vpp/subscriptions/{id} | Found VPP subscription by id 


# **vppSubscriptionsGet**
```swift
    open class func vppSubscriptionsGet(completion: @escaping (_ data: [VppTokenSubscription]?, _ error: Error?) -> Void)
```

Found all VPP - subscriptions 

Found all vpp - subscriptions. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Found all VPP - subscriptions 
VppSubscriptionsPreviewAPI.vppSubscriptionsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[VppTokenSubscription]**](VppTokenSubscription.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vppSubscriptionsIdGet**
```swift
    open class func vppSubscriptionsIdGet(id: Int, completion: @escaping (_ data: VppTokenSubscription?, _ error: Error?) -> Void)
```

Found VPP subscription by id 

Found vpp subscription by id. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | id of vpp subscription to be retrieved

// Found VPP subscription by id 
VppSubscriptionsPreviewAPI.vppSubscriptionsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | id of vpp subscription to be retrieved | 

### Return type

[**VppTokenSubscription**](VppTokenSubscription.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

