# SettingsCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bootstrapTokenAllowed** | **Bool** |  | [optional] 
**bluetooth** | **Bool** |  | [optional] 
**appAnalytics** | [**AppAnalyticsSetting**](AppAnalyticsSetting.md) |  | [optional] 
**diagnosticSubmission** | [**DiagnosticSubmissionSetting**](DiagnosticSubmissionSetting.md) |  | [optional] 
**dataRoaming** | [**DataRoamingSetting**](DataRoamingSetting.md) |  | [optional] 
**voiceRoaming** | [**VoiceRoamingSetting**](VoiceRoamingSetting.md) |  | [optional] 
**personalHotspot** | [**PersonalHotspotSetting**](PersonalHotspotSetting.md) |  | [optional] 
**maximumResidentUsers** | **Int** |  | [optional] 
**deviceName** | **String** |  | [optional] 
**applicationAttributes** | [**ApplicationAttributes**](ApplicationAttributes.md) |  | [optional] 
**sharedDeviceConfiguration** | [**SharedDeviceConfiguration**](SharedDeviceConfiguration.md) |  | [optional] 
**applicationConfiguration** | [**ApplicationConfiguration**](ApplicationConfiguration.md) |  | [optional] 
**timeZone** | **String** |  | [optional] 
**softwareUpdateSettings** | [**SoftwareUpdateSettings**](SoftwareUpdateSettings.md) |  | [optional] 
**passcodeLockGracePeriod** | **Int** | The number of seconds before a locked screen requires the user to enter the device passcode to unlock it. (Shared iPad Only) | [optional] 
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


