# LapsPasswordAndAudits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** |  | [optional] [readonly] 
**dateLastSeen** | **Date** |  | [optional] 
**expirationTime** | **Date** |  | [optional] 
**audits** | [LapsAudit] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


