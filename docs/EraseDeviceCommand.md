# EraseDeviceCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preserveDataPlan** | **Bool** | If true, preserve the data plan on an iPhone or iPad with eSIM functionality, if one exists. This value is available in iOS 11 and later. | [optional] [default to false]
**disallowProximitySetup** | **Bool** | If true, disable Proximity Setup on the next reboot and skip the pane in Setup Assistant. This value is available in iOS 11 and later. Prior to iOS 14, don’t use this option with any other option. | [optional] [default to false]
**pin** | **String** | The six-character PIN for Find My. This value is available in macOS 10.8 and later. | [optional] 
**obliterationBehavior** | **String** | This key defines the fallback behavior for erasing a device. | [optional] 
**returnToService** | [**EraseDeviceCommandReturnToService**](EraseDeviceCommandReturnToService.md) |  | [optional] 
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


