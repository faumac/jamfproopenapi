# PolicyProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isPoliciesRequireNetworkStateChange** | **Bool** | This field always returns false. | [optional] [default to false]
**isAllowNetworkStateChangeTriggers** | **Bool** |  | [optional] [default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


