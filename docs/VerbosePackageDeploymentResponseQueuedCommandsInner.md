# VerbosePackageDeploymentResponseQueuedCommandsInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device** | **Int** |  | [optional] 
**commandUuid** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


