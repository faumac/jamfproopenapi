# ApiRolesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteApiRole**](ApiRolesAPI.md#deleteapirole) | **DELETE** /v1/api-roles/{id} | Delete API Integrations Role
[**getAllApiRoles**](ApiRolesAPI.md#getallapiroles) | **GET** /v1/api-roles | Get the current Jamf API Roles
[**getOneApiRole**](ApiRolesAPI.md#getoneapirole) | **GET** /v1/api-roles/{id} | Get the specific Jamf API Role
[**postCreateApiRole**](ApiRolesAPI.md#postcreateapirole) | **POST** /v1/api-roles | Create a new API role
[**putUpdateApiRole**](ApiRolesAPI.md#putupdateapirole) | **PUT** /v1/api-roles/{id} | Update API Integrations Role


# **deleteApiRole**
```swift
    open class func deleteApiRole(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete API Integrations Role

Delete specific Role

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of API role

// Delete API Integrations Role
ApiRolesAPI.deleteApiRole(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of API role | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllApiRoles**
```swift
    open class func getAllApiRoles(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: ApiRoleResult?, _ error: Error?) -> Void)
```

Get the current Jamf API Roles

Get roles with Search Criteria

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Fields allowed in the query: id, displayName. Example: sort=displayName:desc (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter app titles collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, displayName. Example: displayName==\"*myRole*\" (optional) (default to "")

// Get the current Jamf API Roles
ApiRolesAPI.getAllApiRoles(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Fields allowed in the query: id, displayName. Example: sort&#x3D;displayName:desc | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter app titles collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, displayName. Example: displayName&#x3D;&#x3D;\&quot;*myRole*\&quot; | [optional] [default to &quot;&quot;]

### Return type

[**ApiRoleResult**](ApiRoleResult.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneApiRole**
```swift
    open class func getOneApiRole(id: String, completion: @escaping (_ data: ApiRole?, _ error: Error?) -> Void)
```

Get the specific Jamf API Role

Get specific Role

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of API role

// Get the specific Jamf API Role
ApiRolesAPI.getOneApiRole(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of API role | 

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postCreateApiRole**
```swift
    open class func postCreateApiRole(apiRoleRequest: ApiRoleRequest, completion: @escaping (_ data: ApiRole?, _ error: Error?) -> Void)
```

Create a new API role

Post to create new Role

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let apiRoleRequest = ApiRoleRequest(displayName: "displayName_example", privileges: ["privileges_example"]) // ApiRoleRequest | API Integrations Role to create

// Create a new API role
ApiRolesAPI.postCreateApiRole(apiRoleRequest: apiRoleRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiRoleRequest** | [**ApiRoleRequest**](ApiRoleRequest.md) | API Integrations Role to create | 

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **putUpdateApiRole**
```swift
    open class func putUpdateApiRole(id: String, apiRoleRequest: ApiRoleRequest, completion: @escaping (_ data: ApiRole?, _ error: Error?) -> Void)
```

Update API Integrations Role

Update specific Role

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of API role
let apiRoleRequest = ApiRoleRequest(displayName: "displayName_example", privileges: ["privileges_example"]) // ApiRoleRequest | API Integrations Role to update

// Update API Integrations Role
ApiRolesAPI.putUpdateApiRole(id: id, apiRoleRequest: apiRoleRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of API role | 
 **apiRoleRequest** | [**ApiRoleRequest**](ApiRoleRequest.md) | API Integrations Role to update | 

### Return type

[**ApiRole**](ApiRole.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

