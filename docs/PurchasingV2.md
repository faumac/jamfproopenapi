# PurchasingV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchased** | **Bool** |  | [optional] 
**leased** | **Bool** |  | [optional] 
**poNumber** | **String** |  | [optional] 
**vendor** | **String** |  | [optional] 
**appleCareId** | **String** |  | [optional] 
**purchasePrice** | **String** |  | [optional] 
**purchasingAccount** | **String** |  | [optional] 
**poDate** | **Date** |  | [optional] 
**warrantyExpiresDate** | **Date** |  | [optional] 
**leaseExpiresDate** | **Date** |  | [optional] 
**lifeExpectancy** | **Int** |  | [optional] 
**purchasingContact** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


