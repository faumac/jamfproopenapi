# GetEnrollmentCustomizationPanelSsoAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**rank** | **Int** |  | 
**isUseJamfConnect** | **Bool** |  | 
**longNameAttribute** | **String** |  | 
**shortNameAttribute** | **String** |  | 
**isGroupEnrollmentAccessEnabled** | **Bool** |  | 
**groupEnrollmentAccessName** | **String** |  | [default to ""]
**id** | **Int** |  | [optional] 
**type** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


