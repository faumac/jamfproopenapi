# EnrollmentAccessGroupV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Group ID. | [optional] [readonly] 
**ldapServerId** | **String** |  | [optional] [readonly] 
**name** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**enterpriseEnrollmentEnabled** | **Bool** |  | [optional] 
**personalEnrollmentEnabled** | **Bool** |  | [optional] 
**accountDrivenUserEnrollmentEnabled** | **Bool** |  | [optional] 
**requireEula** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


