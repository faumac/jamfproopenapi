# PrestageSyncStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**syncState** | **String** |  | [optional] 
**prestageId** | **Int** |  | [optional] 
**timestamp** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


