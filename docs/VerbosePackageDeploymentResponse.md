# VerbosePackageDeploymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queuedCommands** | [VerbosePackageDeploymentResponseQueuedCommandsInner] |  | [optional] 
**errors** | [VerbosePackageDeploymentResponseErrorsInner] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


