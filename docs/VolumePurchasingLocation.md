# VolumePurchasingLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**totalPurchasedLicenses** | **Int** |  | [optional] [readonly] 
**totalUsedLicenses** | **Int** |  | [optional] [readonly] 
**id** | **String** |  | [optional] [readonly] 
**appleId** | **String** |  | [optional] [readonly] 
**organizationName** | **String** |  | [optional] [readonly] 
**tokenExpiration** | **String** |  | [optional] [readonly] 
**countryCode** | **String** | The two-letter ISO 3166-1 code that designates the country where the Volume Purchasing account is located. | [optional] [readonly] 
**locationName** | **String** |  | [optional] [readonly] 
**clientContextMismatch** | **Bool** | If this is \&quot;true\&quot;, the clientContext used by this server does not match the clientContext returned by the Volume Purchasing API. | [optional] [readonly] 
**automaticallyPopulatePurchasedContent** | **Bool** |  | [optional] 
**sendNotificationWhenNoLongerAssigned** | **Bool** |  | [optional] 
**autoRegisterManagedUsers** | **Bool** |  | [optional] 
**siteId** | **String** |  | [optional] 
**lastSyncTime** | **String** |  | [optional] [readonly] 
**content** | [VolumePurchasingContent] |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


