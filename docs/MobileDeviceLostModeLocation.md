# MobileDeviceLostModeLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastLocationUpdate** | **Date** |  | [optional] 
**lostModeLocationHorizontalAccuracyMeters** | **Double** |  | [optional] 
**lostModeLocationVerticalAccuracyMeters** | **Double** |  | [optional] 
**lostModeLocationAltitudeMeters** | **Double** |  | [optional] 
**lostModeLocationSpeedMetersPerSecond** | **Double** |  | [optional] 
**lostModeLocationCourseDegrees** | **Double** |  | [optional] 
**lostModeLocationTimestamp** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


