# PatchSummaryVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**absoluteOrderId** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**onVersion** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


