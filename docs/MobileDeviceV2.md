# MobileDeviceV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**wifiMacAddress** | **String** |  | [optional] 
**udid** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**managementId** | **String** |  | [optional] [readonly] 
**softwareUpdateDeviceId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


