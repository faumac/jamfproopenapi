# PatchSoftwareTitleConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**categoryId** | **String** |  | [optional] [default to "-1"]
**siteId** | **String** |  | [optional] [default to "-1"]
**uiNotifications** | **Bool** |  | [optional] [default to false]
**emailNotifications** | **Bool** |  | [optional] [default to false]
**softwareTitleId** | **String** |  | 
**jamfOfficial** | **Bool** |  | [optional] [readonly] 
**extensionAttributes** | [PatchSoftwareTitleConfigurationExtensionAttributes] |  | [optional] 
**softwareTitleName** | **String** |  | [optional] [readonly] 
**softwareTitleNameId** | **String** |  | [optional] [readonly] 
**softwareTitlePublisher** | **String** |  | [optional] [readonly] 
**patchSourceName** | **String** |  | [optional] [readonly] 
**patchSourceEnabled** | **Bool** |  | [optional] [readonly] 
**id** | **String** |  | [optional] 
**packages** | [PatchSoftwareTitlePackages] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


