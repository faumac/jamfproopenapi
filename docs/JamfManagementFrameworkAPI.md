# JamfManagementFrameworkAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfManagementFrameworkRedeployIdPost**](JamfManagementFrameworkAPI.md#v1jamfmanagementframeworkredeployidpost) | **POST** /v1/jamf-management-framework/redeploy/{id} | Redeploy Jamf Management Framework 


# **v1JamfManagementFrameworkRedeployIdPost**
```swift
    open class func v1JamfManagementFrameworkRedeployIdPost(id: String, completion: @escaping (_ data: RedeployJamfManagementFrameworkResponse?, _ error: Error?) -> Void)
```

Redeploy Jamf Management Framework 

Redeploys the Jamf Management Framework for enrolled device 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer

// Redeploy Jamf Management Framework 
JamfManagementFrameworkAPI.v1JamfManagementFrameworkRedeployIdPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer | 

### Return type

[**RedeployJamfManagementFrameworkResponse**](RedeployJamfManagementFrameworkResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

