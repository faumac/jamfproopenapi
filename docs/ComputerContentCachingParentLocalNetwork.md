# ComputerContentCachingParentLocalNetwork

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentCachingParentLocalNetworkId** | **String** |  | [optional] [readonly] 
**speed** | **Int64** |  | [optional] [readonly] 
**wired** | **Bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


