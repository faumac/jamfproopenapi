# StaticUserGroupsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1StaticUserGroupsGet**](StaticUserGroupsPreviewAPI.md#v1staticusergroupsget) | **GET** /v1/static-user-groups | Return a list of all Static User Groups 
[**v1StaticUserGroupsIdGet**](StaticUserGroupsPreviewAPI.md#v1staticusergroupsidget) | **GET** /v1/static-user-groups/{id} | Return a specific Static User Group by id 


# **v1StaticUserGroupsGet**
```swift
    open class func v1StaticUserGroupsGet(completion: @escaping (_ data: [StaticUserGroup]?, _ error: Error?) -> Void)
```

Return a list of all Static User Groups 

Returns a list of all static user groups. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return a list of all Static User Groups 
StaticUserGroupsPreviewAPI.v1StaticUserGroupsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[StaticUserGroup]**](StaticUserGroup.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1StaticUserGroupsIdGet**
```swift
    open class func v1StaticUserGroupsIdGet(id: Int, completion: @escaping (_ data: StaticUserGroup?, _ error: Error?) -> Void)
```

Return a specific Static User Group by id 

Returns a specific static user group by id. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Instance id of static user group record

// Return a specific Static User Group by id 
StaticUserGroupsPreviewAPI.v1StaticUserGroupsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Instance id of static user group record | 

### Return type

[**StaticUserGroup**](StaticUserGroup.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

