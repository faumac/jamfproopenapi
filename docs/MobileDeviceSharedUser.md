# MobileDeviceSharedUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**managedAppleId** | **String** |  | [optional] 
**loggedIn** | **Bool** |  | [optional] 
**dataToSync** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


