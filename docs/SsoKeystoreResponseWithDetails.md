# SsoKeystoreResponseWithDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keystore** | [**SsoKeystoreResponse**](SsoKeystoreResponse.md) |  | [optional] 
**keystoreDetails** | [**SsoKeystoreDetails**](SsoKeystoreDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


