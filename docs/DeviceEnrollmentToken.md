# DeviceEnrollmentToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokenFileName** | **String** | Optional name of the token to be saved, if no name is provided one will be auto-generated | [optional] 
**encodedToken** | **Data** | The base 64 encoded token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


