# SelfServiceSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**installSettings** | [**SelfServiceInstallSettings**](SelfServiceInstallSettings.md) |  | [optional] 
**loginSettings** | [**SelfServiceLoginSettings**](SelfServiceLoginSettings.md) |  | [optional] 
**configurationSettings** | [**SelfServiceInteractionSettings**](SelfServiceInteractionSettings.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


