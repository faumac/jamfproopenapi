# ComputerContentCachingDataMigrationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Int64** |  | [optional] [readonly] 
**domain** | **String** |  | [optional] [readonly] 
**userInfo** | [ComputerContentCachingDataMigrationErrorUserInfo] |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


