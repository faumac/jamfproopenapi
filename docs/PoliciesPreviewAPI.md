# PoliciesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**settingsObjPolicyPropertiesGet**](PoliciesPreviewAPI.md#settingsobjpolicypropertiesget) | **GET** /settings/obj/policyProperties | Get Policy Properties object 
[**settingsObjPolicyPropertiesPut**](PoliciesPreviewAPI.md#settingsobjpolicypropertiesput) | **PUT** /settings/obj/policyProperties | Update Policy Properties object 
[**v1PolicyPropertiesGet**](PoliciesPreviewAPI.md#v1policypropertiesget) | **GET** /v1/policy-properties | Get Policy Properties object 
[**v1PolicyPropertiesPut**](PoliciesPreviewAPI.md#v1policypropertiesput) | **PUT** /v1/policy-properties | Update Policy Properties object 


# **settingsObjPolicyPropertiesGet**
```swift
    open class func settingsObjPolicyPropertiesGet(completion: @escaping (_ data: PolicyProperties?, _ error: Error?) -> Void)
```

Get Policy Properties object 

Gets `Policy Properties` object. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Policy Properties object 
PoliciesPreviewAPI.settingsObjPolicyPropertiesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PolicyProperties**](PolicyProperties.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **settingsObjPolicyPropertiesPut**
```swift
    open class func settingsObjPolicyPropertiesPut(policyProperties: PolicyProperties, completion: @escaping (_ data: PolicyProperties?, _ error: Error?) -> Void)
```

Update Policy Properties object 

Update Policy Properties object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let policyProperties = PolicyProperties(isPoliciesRequireNetworkStateChange: false, isAllowNetworkStateChangeTriggers: false) // PolicyProperties | Policy Properties object to update

// Update Policy Properties object 
PoliciesPreviewAPI.settingsObjPolicyPropertiesPut(policyProperties: policyProperties) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policyProperties** | [**PolicyProperties**](PolicyProperties.md) | Policy Properties object to update | 

### Return type

[**PolicyProperties**](PolicyProperties.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PolicyPropertiesGet**
```swift
    open class func v1PolicyPropertiesGet(completion: @escaping (_ data: PolicyPropertiesV1?, _ error: Error?) -> Void)
```

Get Policy Properties object 

Gets `Policy Properties` object. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Policy Properties object 
PoliciesPreviewAPI.v1PolicyPropertiesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PolicyPropertiesV1**](PolicyPropertiesV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PolicyPropertiesPut**
```swift
    open class func v1PolicyPropertiesPut(policyPropertiesV1: PolicyPropertiesV1, completion: @escaping (_ data: PolicyPropertiesV1?, _ error: Error?) -> Void)
```

Update Policy Properties object 

Update Policy Properties object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let policyPropertiesV1 = PolicyPropertiesV1(policiesRequireNetworkStateChange: false, allowNetworkStateChangeTriggers: false) // PolicyPropertiesV1 | Policy Properties object to update

// Update Policy Properties object 
PoliciesPreviewAPI.v1PolicyPropertiesPut(policyPropertiesV1: policyPropertiesV1) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **policyPropertiesV1** | [**PolicyPropertiesV1**](PolicyPropertiesV1.md) | Policy Properties object to update | 

### Return type

[**PolicyPropertiesV1**](PolicyPropertiesV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

