# VenafiPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1PkiVenafiIdConnectionStatusGet**](VenafiPreviewAPI.md#v1pkivenafiidconnectionstatusget) | **GET** /v1/pki/venafi/{id}/connection-status | Tests the communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
[**v1PkiVenafiIdDelete**](VenafiPreviewAPI.md#v1pkivenafiiddelete) | **DELETE** /v1/pki/venafi/{id} | Delete a Venafi PKI configuration from Jamf Pro 
[**v1PkiVenafiIdDependentProfilesGet**](VenafiPreviewAPI.md#v1pkivenafiiddependentprofilesget) | **GET** /v1/pki/venafi/{id}/dependent-profiles | Get configuration profile data using specified Venafi CA object 
[**v1PkiVenafiIdGet**](VenafiPreviewAPI.md#v1pkivenafiidget) | **GET** /v1/pki/venafi/{id} | Retrieve a Venafi PKI configuration from Jamf Pro 
[**v1PkiVenafiIdHistoryGet**](VenafiPreviewAPI.md#v1pkivenafiidhistoryget) | **GET** /v1/pki/venafi/{id}/history | Get specified Venafi CA history object 
[**v1PkiVenafiIdHistoryPost**](VenafiPreviewAPI.md#v1pkivenafiidhistorypost) | **POST** /v1/pki/venafi/{id}/history | Add specified Venafi CA Object Note 
[**v1PkiVenafiIdJamfPublicKeyGet**](VenafiPreviewAPI.md#v1pkivenafiidjamfpublickeyget) | **GET** /v1/pki/venafi/{id}/jamf-public-key | Downloads a certificate used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
[**v1PkiVenafiIdJamfPublicKeyRegeneratePost**](VenafiPreviewAPI.md#v1pkivenafiidjamfpublickeyregeneratepost) | **POST** /v1/pki/venafi/{id}/jamf-public-key/regenerate | Regenerates a certificate used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
[**v1PkiVenafiIdPatch**](VenafiPreviewAPI.md#v1pkivenafiidpatch) | **PATCH** /v1/pki/venafi/{id} | Update a Venafi PKI configuration in Jamf Pro 
[**v1PkiVenafiIdProxyTrustStoreDelete**](VenafiPreviewAPI.md#v1pkivenafiidproxytruststoredelete) | **DELETE** /v1/pki/venafi/{id}/proxy-trust-store | Removes the PKI Proxy Server public key used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
[**v1PkiVenafiIdProxyTrustStoreGet**](VenafiPreviewAPI.md#v1pkivenafiidproxytruststoreget) | **GET** /v1/pki/venafi/{id}/proxy-trust-store | Downloads the PKI Proxy Server public key to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
[**v1PkiVenafiIdProxyTrustStorePost**](VenafiPreviewAPI.md#v1pkivenafiidproxytruststorepost) | **POST** /v1/pki/venafi/{id}/proxy-trust-store | Uploads the PKI Proxy Server public key to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
[**v1PkiVenafiPost**](VenafiPreviewAPI.md#v1pkivenafipost) | **POST** /v1/pki/venafi | Create a PKI configuration in Jamf Pro for Venafi 


# **v1PkiVenafiIdConnectionStatusGet**
```swift
    open class func v1PkiVenafiIdConnectionStatusGet(id: String, completion: @escaping (_ data: VenafiServiceStatus?, _ error: Error?) -> Void)
```

Tests the communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

Tests the communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Tests the communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
VenafiPreviewAPI.v1PkiVenafiIdConnectionStatusGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

[**VenafiServiceStatus**](VenafiServiceStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdDelete**
```swift
    open class func v1PkiVenafiIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Venafi PKI configuration from Jamf Pro 

Delete a Venafi PKI configuration from Jamf Pro 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Delete a Venafi PKI configuration from Jamf Pro 
VenafiPreviewAPI.v1PkiVenafiIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdDependentProfilesGet**
```swift
    open class func v1PkiVenafiIdDependentProfilesGet(id: String, completion: @escaping (_ data: VenafiPkiPayloadRecordSearchResults?, _ error: Error?) -> Void)
```

Get configuration profile data using specified Venafi CA object 

Get configuration profile data using specified Venafi CA object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Get configuration profile data using specified Venafi CA object 
VenafiPreviewAPI.v1PkiVenafiIdDependentProfilesGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

[**VenafiPkiPayloadRecordSearchResults**](VenafiPkiPayloadRecordSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdGet**
```swift
    open class func v1PkiVenafiIdGet(id: String, completion: @escaping (_ data: VenafiCaRecord?, _ error: Error?) -> Void)
```

Retrieve a Venafi PKI configuration from Jamf Pro 

Retrieve a Venafi PKI configuration from Jamf Pro 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Retrieve a Venafi PKI configuration from Jamf Pro 
VenafiPreviewAPI.v1PkiVenafiIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

[**VenafiCaRecord**](VenafiCaRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdHistoryGet**
```swift
    open class func v1PkiVenafiIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get specified Venafi CA history object 

Get specified Venafi CA history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get specified Venafi CA history object 
VenafiPreviewAPI.v1PkiVenafiIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdHistoryPost**
```swift
    open class func v1PkiVenafiIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add specified Venafi CA Object Note 

Adds specified Venafi CA Object Note 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of Venafi CA history record
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | venafi ca history notes to create

// Add specified Venafi CA Object Note 
VenafiPreviewAPI.v1PkiVenafiIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of Venafi CA history record | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | venafi ca history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdJamfPublicKeyGet**
```swift
    open class func v1PkiVenafiIdJamfPublicKeyGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Downloads a certificate used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

Downloads a certificate for an existing Venafi configuration that can be used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Downloads a certificate used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
VenafiPreviewAPI.v1PkiVenafiIdJamfPublicKeyGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pem-certificate-chain, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdJamfPublicKeyRegeneratePost**
```swift
    open class func v1PkiVenafiIdJamfPublicKeyRegeneratePost(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Regenerates a certificate used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

Regenerates a certificate for an existing Venafi configuration that can be used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Regenerates a certificate used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
VenafiPreviewAPI.v1PkiVenafiIdJamfPublicKeyRegeneratePost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdPatch**
```swift
    open class func v1PkiVenafiIdPatch(id: String, venafiCaRecord: VenafiCaRecord, completion: @escaping (_ data: VenafiCaRecord?, _ error: Error?) -> Void)
```

Update a Venafi PKI configuration in Jamf Pro 

Update a Venafi PKI configuration in Jamf Pro 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration
let venafiCaRecord = VenafiCaRecord(id: 123, name: "name_example", username: "username_example", password: "password_example", passwordConfigured: true, proxyAddress: "proxyAddress_example", revocationEnabled: true, clientId: "clientId_example", refreshToken: "refreshToken_example", refreshTokenConfigured: true) // VenafiCaRecord | 

// Update a Venafi PKI configuration in Jamf Pro 
VenafiPreviewAPI.v1PkiVenafiIdPatch(id: id, venafiCaRecord: venafiCaRecord) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 
 **venafiCaRecord** | [**VenafiCaRecord**](VenafiCaRecord.md) |  | 

### Return type

[**VenafiCaRecord**](VenafiCaRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdProxyTrustStoreDelete**
```swift
    open class func v1PkiVenafiIdProxyTrustStoreDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Removes the PKI Proxy Server public key used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

Removes the uploaded PKI Proxy Server public key to do basic TLS certificate validation between Jamf Pro and a Jamf Pro PKI Proxy Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Removes the PKI Proxy Server public key used to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
VenafiPreviewAPI.v1PkiVenafiIdProxyTrustStoreDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdProxyTrustStoreGet**
```swift
    open class func v1PkiVenafiIdProxyTrustStoreGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Downloads the PKI Proxy Server public key to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

Downloads the uploaded PKI Proxy Server public key to do basic TLS certificate validation between Jamf Pro and a Jamf Pro PKI Proxy Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration

// Downloads the PKI Proxy Server public key to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
VenafiPreviewAPI.v1PkiVenafiIdProxyTrustStoreGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pem-certificate-chain, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiIdProxyTrustStorePost**
```swift
    open class func v1PkiVenafiIdProxyTrustStorePost(id: String, body: URL, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Uploads the PKI Proxy Server public key to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 

Uploads the PKI Proxy Server public key to do basic TLS certificate validation between Jamf Pro and a Jamf Pro PKI Proxy Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Venafi configuration
let body = URL(string: "https://example.com")! // URL | 

// Uploads the PKI Proxy Server public key to secure communication between Jamf Pro and a Jamf Pro PKI Proxy Server 
VenafiPreviewAPI.v1PkiVenafiIdProxyTrustStorePost(id: id, body: body) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Venafi configuration | 
 **body** | **URL** |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/pem-certificate-chain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiVenafiPost**
```swift
    open class func v1PkiVenafiPost(venafiCaRecord: VenafiCaRecord, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a PKI configuration in Jamf Pro for Venafi 

Creates a Venafi PKI configuration in Jamf Pro, which can be used to issue certificates 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let venafiCaRecord = VenafiCaRecord(id: 123, name: "name_example", username: "username_example", password: "password_example", passwordConfigured: true, proxyAddress: "proxyAddress_example", revocationEnabled: true, clientId: "clientId_example", refreshToken: "refreshToken_example", refreshTokenConfigured: true) // VenafiCaRecord | 

// Create a PKI configuration in Jamf Pro for Venafi 
VenafiPreviewAPI.v1PkiVenafiPost(venafiCaRecord: venafiCaRecord) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **venafiCaRecord** | [**VenafiCaRecord**](VenafiCaRecord.md) |  | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

