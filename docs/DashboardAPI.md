# DashboardAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1DashboardGet**](DashboardAPI.md#v1dashboardget) | **GET** /v1/dashboard | Get all the dashboard setup information 


# **v1DashboardGet**
```swift
    open class func v1DashboardGet(completion: @escaping (_ data: DashboardSetup?, _ error: Error?) -> Void)
```

Get all the dashboard setup information 

Get all the dashboard information for widgets and setup tasks 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all the dashboard setup information 
DashboardAPI.v1DashboardGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DashboardSetup**](DashboardSetup.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

