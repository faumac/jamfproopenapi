# PatchPolicyLogRetry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceIds** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


