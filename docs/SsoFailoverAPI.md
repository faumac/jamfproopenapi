# SsoFailoverAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SsoFailoverGeneratePost**](SsoFailoverAPI.md#v1ssofailovergeneratepost) | **POST** /v1/sso/failover/generate | Regenerates failover url
[**v1SsoFailoverGet**](SsoFailoverAPI.md#v1ssofailoverget) | **GET** /v1/sso/failover | Retrieve the current failover settings


# **v1SsoFailoverGeneratePost**
```swift
    open class func v1SsoFailoverGeneratePost(completion: @escaping (_ data: SsoFailoverData?, _ error: Error?) -> Void)
```

Regenerates failover url

Regenerates failover url, by changing failover key to new one, and returns new failover settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Regenerates failover url
SsoFailoverAPI.v1SsoFailoverGeneratePost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoFailoverData**](SsoFailoverData.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoFailoverGet**
```swift
    open class func v1SsoFailoverGet(completion: @escaping (_ data: SsoFailoverData?, _ error: Error?) -> Void)
```

Retrieve the current failover settings

Retrieve the current failover settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the current failover settings
SsoFailoverAPI.v1SsoFailoverGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoFailoverData**](SsoFailoverData.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

