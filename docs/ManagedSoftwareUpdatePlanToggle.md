# ManagedSoftwareUpdatePlanToggle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**toggle** | **Bool** |  | 
**forceInstallLocalDateEnabled** | **Bool** |  | [optional] [readonly] 
**dssEnabled** | **Bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


