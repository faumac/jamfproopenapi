# VenafiCaRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] [readonly] 
**name** | **String** |  | 
**username** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**passwordConfigured** | **Bool** |  | [optional] [readonly] 
**proxyAddress** | **String** |  | [optional] 
**revocationEnabled** | **Bool** |  | [optional] 
**clientId** | **String** |  | [optional] 
**refreshToken** | **String** |  | [optional] 
**refreshTokenConfigured** | **Bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


