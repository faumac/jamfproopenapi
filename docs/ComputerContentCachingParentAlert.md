# ComputerContentCachingParentAlert

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentCachingParentAlertId** | **String** |  | [optional] [readonly] 
**addresses** | **[String]** |  | [optional] [readonly] 
**className** | **String** |  | [optional] [readonly] 
**postDate** | **Date** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


