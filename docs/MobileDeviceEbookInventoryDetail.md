# MobileDeviceEbookInventoryDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**kind** | **String** |  | [optional] 
**managementState** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


