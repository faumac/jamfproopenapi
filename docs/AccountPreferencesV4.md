# AccountPreferencesV4

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** | Language codes supported by Jamf Pro | [default to .en]
**dateFormat** | **String** |  | 
**timezone** | **String** |  | 
**disableRelativeDates** | **Bool** |  | 
**disablePageLeaveCheck** | **Bool** |  | 
**disableTablePagination** | **Bool** |  | 
**disableShortcutsTooltips** | **Bool** |  | 
**configProfilesSortingMethod** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


