# SelfServiceInstallSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**installAutomatically** | **Bool** | true if Self Service is insalled automatically, false if not  | [optional] [default to false]
**installLocation** | **String** | path at which Self Service is installed  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


