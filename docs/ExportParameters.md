# ExportParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **Int** |  | [optional] [default to 0]
**pageSize** | **Int** |  | [optional] [default to 100]
**sort** | **[String]** | Sorting criteria in the format: [&lt;property&gt;[:asc/desc]. Default direction when not stated is ascending. | [optional] 
**filter** | **String** |  | [optional] 
**fields** | [ExportField] | Used to change default order or ignore some of the fields. When null or empty array, all fields will be exported. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


