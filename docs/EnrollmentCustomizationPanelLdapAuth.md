# EnrollmentCustomizationPanelLdapAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**rank** | **Int** |  | 
**usernameLabel** | **String** |  | 
**passwordLabel** | **String** |  | 
**title** | **String** |  | 
**backButtonText** | **String** |  | 
**continueButtonText** | **String** |  | 
**ldapGroupAccess** | [EnrollmentCustomizationLdapGroupAccess] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


