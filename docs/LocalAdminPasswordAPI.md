# LocalAdminPasswordAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1LocalAdminPasswordClientManagementIdAccountUsernameAuditGet**](LocalAdminPasswordAPI.md#v1localadminpasswordclientmanagementidaccountusernameauditget) | **GET** /v1/local-admin-password/{clientManagementId}/account/{username}/audit | Get LAPS password viewed history.
[**v1LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet**](LocalAdminPasswordAPI.md#v1localadminpasswordclientmanagementidaccountusernamepasswordget) | **GET** /v1/local-admin-password/{clientManagementId}/account/{username}/password | Get current LAPS password for specified username on a client.
[**v1LocalAdminPasswordClientManagementIdAccountsGet**](LocalAdminPasswordAPI.md#v1localadminpasswordclientmanagementidaccountsget) | **GET** /v1/local-admin-password/{clientManagementId}/accounts | Get the LAPS capable admin accounts for a device.
[**v1LocalAdminPasswordClientManagementIdSetPasswordPut**](LocalAdminPasswordAPI.md#v1localadminpasswordclientmanagementidsetpasswordput) | **PUT** /v1/local-admin-password/{clientManagementId}/set-password | Set the LAPS password for a device.
[**v1LocalAdminPasswordSettingsGet**](LocalAdminPasswordAPI.md#v1localadminpasswordsettingsget) | **GET** /v1/local-admin-password/settings | Get the current LAPS settings.
[**v1LocalAdminPasswordSettingsPut**](LocalAdminPasswordAPI.md#v1localadminpasswordsettingsput) | **PUT** /v1/local-admin-password/settings | Update settings for LAPS.
[**v2LocalAdminPasswordClientManagementIdAccountUsernameAuditGet**](LocalAdminPasswordAPI.md#v2localadminpasswordclientmanagementidaccountusernameauditget) | **GET** /v2/local-admin-password/{clientManagementId}/account/{username}/audit | Get LAPS password viewed history.
[**v2LocalAdminPasswordClientManagementIdAccountUsernameHistoryGet**](LocalAdminPasswordAPI.md#v2localadminpasswordclientmanagementidaccountusernamehistoryget) | **GET** /v2/local-admin-password/{clientManagementId}/account/{username}/history | Get LAPS historical records for target device and username.
[**v2LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet**](LocalAdminPasswordAPI.md#v2localadminpasswordclientmanagementidaccountusernamepasswordget) | **GET** /v2/local-admin-password/{clientManagementId}/account/{username}/password | Get current LAPS password for specified username on a client.
[**v2LocalAdminPasswordClientManagementIdAccountsGet**](LocalAdminPasswordAPI.md#v2localadminpasswordclientmanagementidaccountsget) | **GET** /v2/local-admin-password/{clientManagementId}/accounts | Get the LAPS capable admin accounts for a device.
[**v2LocalAdminPasswordClientManagementIdSetPasswordPut**](LocalAdminPasswordAPI.md#v2localadminpasswordclientmanagementidsetpasswordput) | **PUT** /v2/local-admin-password/{clientManagementId}/set-password | Set the LAPS password for a device.
[**v2LocalAdminPasswordPendingRotationsGet**](LocalAdminPasswordAPI.md#v2localadminpasswordpendingrotationsget) | **GET** /v2/local-admin-password/pending-rotations | Get a list of the current devices and usernames with pending LAPS rotations
[**v2LocalAdminPasswordSettingsGet**](LocalAdminPasswordAPI.md#v2localadminpasswordsettingsget) | **GET** /v2/local-admin-password/settings | Get the current LAPS settings.
[**v2LocalAdminPasswordSettingsPut**](LocalAdminPasswordAPI.md#v2localadminpasswordsettingsput) | **PUT** /v2/local-admin-password/settings | Update settings for LAPS.


# **v1LocalAdminPasswordClientManagementIdAccountUsernameAuditGet**
```swift
    open class func v1LocalAdminPasswordClientManagementIdAccountUsernameAuditGet(clientManagementId: String, username: String, completion: @escaping (_ data: LapsPasswordAuditsResults?, _ error: Error?) -> Void)
```

Get LAPS password viewed history.

Get the full history of all local admin passwords for a specific username on a target device. History will include password, who viewed the password and when it was viewed. Get audit history by using the client management id and username as the path parameters.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let username = "username_example" // String | user name to view audit information for

// Get LAPS password viewed history.
LocalAdminPasswordAPI.v1LocalAdminPasswordClientManagementIdAccountUsernameAuditGet(clientManagementId: clientManagementId, username: username) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **username** | **String** | user name to view audit information for | 

### Return type

[**LapsPasswordAuditsResults**](LapsPasswordAuditsResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet**
```swift
    open class func v1LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet(clientManagementId: String, username: String, completion: @escaping (_ data: LapsPasswordResponse?, _ error: Error?) -> Void)
```

Get current LAPS password for specified username on a client.

Get current LAPS password for specified client by using the client management id and username as the path parameters. Once the password is viewed it will be rotated out with a new password based on the rotation time settings.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let username = "username_example" // String | user name for the account

// Get current LAPS password for specified username on a client.
LocalAdminPasswordAPI.v1LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet(clientManagementId: clientManagementId, username: username) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **username** | **String** | user name for the account | 

### Return type

[**LapsPasswordResponse**](LapsPasswordResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LocalAdminPasswordClientManagementIdAccountsGet**
```swift
    open class func v1LocalAdminPasswordClientManagementIdAccountsGet(clientManagementId: String, completion: @escaping (_ data: LapsUserResults?, _ error: Error?) -> Void)
```

Get the LAPS capable admin accounts for a device.

Get a full list of admin accounts that are LAPS capable. Capable accounts are returned in the AutoSetupAdminAccounts from QueryResponses.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.

// Get the LAPS capable admin accounts for a device.
LocalAdminPasswordAPI.v1LocalAdminPasswordClientManagementIdAccountsGet(clientManagementId: clientManagementId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 

### Return type

[**LapsUserResults**](LapsUserResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LocalAdminPasswordClientManagementIdSetPasswordPut**
```swift
    open class func v1LocalAdminPasswordClientManagementIdSetPasswordPut(clientManagementId: String, lapsUserPasswordRequest: LapsUserPasswordRequest, completion: @escaping (_ data: LapsUserPasswordResponse?, _ error: Error?) -> Void)
```

Set the LAPS password for a device.

Set the LAPS password for a device. This will set the password for all LAPS capable accounts.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let lapsUserPasswordRequest = LapsUserPasswordRequest(lapsUserPasswordList: [LapsUserPassword(username: "username_example", password: "password_example")]) // LapsUserPasswordRequest | LAPS password to set

// Set the LAPS password for a device.
LocalAdminPasswordAPI.v1LocalAdminPasswordClientManagementIdSetPasswordPut(clientManagementId: clientManagementId, lapsUserPasswordRequest: lapsUserPasswordRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **lapsUserPasswordRequest** | [**LapsUserPasswordRequest**](LapsUserPasswordRequest.md) | LAPS password to set | 

### Return type

[**LapsUserPasswordResponse**](LapsUserPasswordResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LocalAdminPasswordSettingsGet**
```swift
    open class func v1LocalAdminPasswordSettingsGet(completion: @escaping (_ data: LapsSettingsResponse?, _ error: Error?) -> Void)
```

Get the current LAPS settings.

Return information about the current LAPS settings.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the current LAPS settings.
LocalAdminPasswordAPI.v1LocalAdminPasswordSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LapsSettingsResponse**](LapsSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LocalAdminPasswordSettingsPut**
```swift
    open class func v1LocalAdminPasswordSettingsPut(lapsSettingsRequest: LapsSettingsRequest, completion: @escaping (_ data: LapsSettingsResponse?, _ error: Error?) -> Void)
```

Update settings for LAPS.

Update settings for LAPS.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let lapsSettingsRequest = LapsSettingsRequest(autoDeployEnabled: false, passwordRotationTime: 123, autoExpirationTime: 123) // LapsSettingsRequest | LAPS settings to update

// Update settings for LAPS.
LocalAdminPasswordAPI.v1LocalAdminPasswordSettingsPut(lapsSettingsRequest: lapsSettingsRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lapsSettingsRequest** | [**LapsSettingsRequest**](LapsSettingsRequest.md) | LAPS settings to update | 

### Return type

[**LapsSettingsResponse**](LapsSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordClientManagementIdAccountUsernameAuditGet**
```swift
    open class func v2LocalAdminPasswordClientManagementIdAccountUsernameAuditGet(clientManagementId: String, username: String, completion: @escaping (_ data: LapsPasswordAuditsResultsV2?, _ error: Error?) -> Void)
```

Get LAPS password viewed history.

Get the full history of all local admin passwords for a specific username on a target device. History will include password, who viewed the password and when it was viewed. Get audit history by using the client management id and username as the path parameters.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let username = "username_example" // String | user name to view audit information for

// Get LAPS password viewed history.
LocalAdminPasswordAPI.v2LocalAdminPasswordClientManagementIdAccountUsernameAuditGet(clientManagementId: clientManagementId, username: username) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **username** | **String** | user name to view audit information for | 

### Return type

[**LapsPasswordAuditsResultsV2**](LapsPasswordAuditsResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordClientManagementIdAccountUsernameHistoryGet**
```swift
    open class func v2LocalAdminPasswordClientManagementIdAccountUsernameHistoryGet(clientManagementId: String, username: String, completion: @escaping (_ data: LapsHistoryResponse?, _ error: Error?) -> Void)
```

Get LAPS historical records for target device and username.

Get the full history of all for a specific username on a target device. History will include date created, date last seen, expiration time, and rotational status. Get audit history by using the client management id and username as the path parameters.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let username = "username_example" // String | user name to view history for

// Get LAPS historical records for target device and username.
LocalAdminPasswordAPI.v2LocalAdminPasswordClientManagementIdAccountUsernameHistoryGet(clientManagementId: clientManagementId, username: username) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **username** | **String** | user name to view history for | 

### Return type

[**LapsHistoryResponse**](LapsHistoryResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet**
```swift
    open class func v2LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet(clientManagementId: String, username: String, completion: @escaping (_ data: LapsPasswordResponseV2?, _ error: Error?) -> Void)
```

Get current LAPS password for specified username on a client.

Get current LAPS password for specified client by using the client management id and username as the path parameters. Once the password is viewed it will be rotated out with a new password based on the rotation time settings.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let username = "username_example" // String | user name for the account

// Get current LAPS password for specified username on a client.
LocalAdminPasswordAPI.v2LocalAdminPasswordClientManagementIdAccountUsernamePasswordGet(clientManagementId: clientManagementId, username: username) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **username** | **String** | user name for the account | 

### Return type

[**LapsPasswordResponseV2**](LapsPasswordResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordClientManagementIdAccountsGet**
```swift
    open class func v2LocalAdminPasswordClientManagementIdAccountsGet(clientManagementId: String, completion: @escaping (_ data: LapsUserResultsV2?, _ error: Error?) -> Void)
```

Get the LAPS capable admin accounts for a device.

Get a full list of admin accounts that are LAPS capable. Capable accounts are returned in the AutoSetupAdminAccounts from QueryResponses.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.

// Get the LAPS capable admin accounts for a device.
LocalAdminPasswordAPI.v2LocalAdminPasswordClientManagementIdAccountsGet(clientManagementId: clientManagementId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 

### Return type

[**LapsUserResultsV2**](LapsUserResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordClientManagementIdSetPasswordPut**
```swift
    open class func v2LocalAdminPasswordClientManagementIdSetPasswordPut(clientManagementId: String, lapsUserPasswordRequestV2: LapsUserPasswordRequestV2, completion: @escaping (_ data: LapsUserPasswordResponseV2?, _ error: Error?) -> Void)
```

Set the LAPS password for a device.

Set the LAPS password for a device. This will set the password for all LAPS capable accounts.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientManagementId = "clientManagementId_example" // String | client management id of target device.
let lapsUserPasswordRequestV2 = LapsUserPasswordRequestV2(lapsUserPasswordList: [LapsUserPasswordV2(username: "username_example", password: "password_example")]) // LapsUserPasswordRequestV2 | LAPS password to set

// Set the LAPS password for a device.
LocalAdminPasswordAPI.v2LocalAdminPasswordClientManagementIdSetPasswordPut(clientManagementId: clientManagementId, lapsUserPasswordRequestV2: lapsUserPasswordRequestV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientManagementId** | **String** | client management id of target device. | 
 **lapsUserPasswordRequestV2** | [**LapsUserPasswordRequestV2**](LapsUserPasswordRequestV2.md) | LAPS password to set | 

### Return type

[**LapsUserPasswordResponseV2**](LapsUserPasswordResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordPendingRotationsGet**
```swift
    open class func v2LocalAdminPasswordPendingRotationsGet(completion: @escaping (_ data: LapsPendingRotationResponse?, _ error: Error?) -> Void)
```

Get a list of the current devices and usernames with pending LAPS rotations

Return information about all devices and usernames currently in the state of a pending LAPS rotation

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get a list of the current devices and usernames with pending LAPS rotations
LocalAdminPasswordAPI.v2LocalAdminPasswordPendingRotationsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LapsPendingRotationResponse**](LapsPendingRotationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordSettingsGet**
```swift
    open class func v2LocalAdminPasswordSettingsGet(completion: @escaping (_ data: LapsSettingsResponseV2?, _ error: Error?) -> Void)
```

Get the current LAPS settings.

Return information about the current LAPS settings.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the current LAPS settings.
LocalAdminPasswordAPI.v2LocalAdminPasswordSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LapsSettingsResponseV2**](LapsSettingsResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2LocalAdminPasswordSettingsPut**
```swift
    open class func v2LocalAdminPasswordSettingsPut(lapsSettingsRequestV2: LapsSettingsRequestV2, completion: @escaping (_ data: LapsSettingsResponseV2?, _ error: Error?) -> Void)
```

Update settings for LAPS.

Update settings for LAPS.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let lapsSettingsRequestV2 = LapsSettingsRequestV2(autoDeployEnabled: false, passwordRotationTime: 123, autoRotateEnabled: false, autoRotateExpirationTime: 123) // LapsSettingsRequestV2 | LAPS settings to update

// Update settings for LAPS.
LocalAdminPasswordAPI.v2LocalAdminPasswordSettingsPut(lapsSettingsRequestV2: lapsSettingsRequestV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lapsSettingsRequestV2** | [**LapsSettingsRequestV2**](LapsSettingsRequestV2.md) | LAPS settings to update | 

### Return type

[**LapsSettingsResponseV2**](LapsSettingsResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

