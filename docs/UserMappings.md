# UserMappings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**objectClassLimitation** | **String** |  | 
**objectClasses** | **String** |  | 
**searchBase** | **String** |  | 
**searchScope** | **String** |  | 
**additionalSearchBase** | **String** |  | [optional] [default to ""]
**userID** | **String** |  | 
**username** | **String** |  | 
**realName** | **String** |  | 
**emailAddress** | **String** |  | 
**department** | **String** |  | 
**building** | **String** |  | [default to ""]
**room** | **String** |  | [default to ""]
**phone** | **String** |  | [default to ""]
**position** | **String** |  | 
**userUuid** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


