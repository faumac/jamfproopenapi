# CertificateIdentityV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  | [optional] [default to "null"]
**keystorePassword** | **String** |  | [optional] [default to ""]
**identityKeystore** | **Data** | The base 64 encoded certificate. | [optional] 
**md5Sum** | **String** | The md5 checksum of the certificate file. Intended to be used in verifification the cert being used to sign QuickAdd packages. | [optional] [readonly] [default to ""]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


