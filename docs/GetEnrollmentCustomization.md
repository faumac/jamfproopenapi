# GetEnrollmentCustomization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **Int** |  | 
**displayName** | **String** |  | 
**description** | **String** |  | 
**enrollmentCustomizationBrandingSettings** | [**EnrollmentCustomizationBrandingSettings**](EnrollmentCustomizationBrandingSettings.md) |  | 
**id** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


