# ConditionalAccessAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ConditionalAccessDeviceComplianceInformationComputerDeviceIdGet**](ConditionalAccessAPI.md#v1conditionalaccessdevicecomplianceinformationcomputerdeviceidget) | **GET** /v1/conditional-access/device-compliance-information/computer/{deviceId} | Get compliance information for a single computer device
[**v1ConditionalAccessDeviceComplianceInformationMobileDeviceIdGet**](ConditionalAccessAPI.md#v1conditionalaccessdevicecomplianceinformationmobiledeviceidget) | **GET** /v1/conditional-access/device-compliance-information/mobile/{deviceId} | Get compliance information for a single mobile device


# **v1ConditionalAccessDeviceComplianceInformationComputerDeviceIdGet**
```swift
    open class func v1ConditionalAccessDeviceComplianceInformationComputerDeviceIdGet(deviceId: String, completion: @escaping (_ data: [DeviceComplianceInformation]?, _ error: Error?) -> Void)
```

Get compliance information for a single computer device

Return basic compliance information for the given computer device

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let deviceId = "deviceId_example" // String | ID of the device the query pertains

// Get compliance information for a single computer device
ConditionalAccessAPI.v1ConditionalAccessDeviceComplianceInformationComputerDeviceIdGet(deviceId: deviceId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String** | ID of the device the query pertains | 

### Return type

[**[DeviceComplianceInformation]**](DeviceComplianceInformation.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ConditionalAccessDeviceComplianceInformationMobileDeviceIdGet**
```swift
    open class func v1ConditionalAccessDeviceComplianceInformationMobileDeviceIdGet(deviceId: String, completion: @escaping (_ data: [DeviceComplianceInformation]?, _ error: Error?) -> Void)
```

Get compliance information for a single mobile device

Return basic compliance information for the given mobile device

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let deviceId = "deviceId_example" // String | ID of the device the query pertains

// Get compliance information for a single mobile device
ConditionalAccessAPI.v1ConditionalAccessDeviceComplianceInformationMobileDeviceIdGet(deviceId: deviceId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **String** | ID of the device the query pertains | 

### Return type

[**[DeviceComplianceInformation]**](DeviceComplianceInformation.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

