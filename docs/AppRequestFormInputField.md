# AppRequestFormInputField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] [readonly] 
**title** | **String** |  | 
**description** | **String** |  | [optional] 
**priority** | **Int** | Highest priority is 1, lowest is 255 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


