# MacOsBrandingConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**applicationName** | **String** |  | [optional] 
**brandingName** | **String** |  | [optional] 
**brandingNameSecondary** | **String** |  | [optional] 
**iconId** | **Int** |  | [optional] 
**brandingHeaderImageId** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


