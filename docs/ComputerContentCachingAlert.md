# ComputerContentCachingAlert

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cacheBytesLimit** | **Int64** |  | [optional] [readonly] 
**className** | **String** |  | [optional] [readonly] 
**pathPreventingAccess** | **String** |  | [optional] [readonly] 
**postDate** | **Date** |  | [optional] [readonly] 
**reservedVolumeBytes** | **Int64** |  | [optional] [readonly] 
**resource** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


