# UpdateMobileDevice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**siteId** | **Int** |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**updatedExtensionAttributes** | [ExtensionAttribute] |  | [optional] 
**ios** | [**UpdateIos**](UpdateIos.md) |  | [optional] 
**appleTv** | [**UpdateAppleTv**](UpdateAppleTv.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


