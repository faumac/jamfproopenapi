# ComputerSecurity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sipStatus** | **String** |  | [optional] 
**gatekeeperStatus** | **String** |  | [optional] 
**xprotectVersion** | **String** |  | [optional] 
**autoLoginDisabled** | **Bool** |  | [optional] 
**remoteDesktopEnabled** | **Bool** | Collected for macOS 10.14.4 or later | [optional] 
**activationLockEnabled** | **Bool** | Collected for macOS 10.15.0 or later | [optional] 
**recoveryLockEnabled** | **Bool** |  | [optional] 
**firewallEnabled** | **Bool** |  | [optional] 
**secureBootLevel** | **String** | Collected for macOS 10.15.0 or later | [optional] 
**externalBootLevel** | **String** | Collected for macOS 10.15.0 or later | [optional] 
**bootstrapTokenAllowed** | **Bool** | Collected for macOS 11 or later | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


