# ComputerInventoryAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ComputersInventoryDetailIdGet**](ComputerInventoryAPI.md#v1computersinventorydetailidget) | **GET** /v1/computers-inventory-detail/{id} | Return all sections of a computer
[**v1ComputersInventoryDetailIdPatch**](ComputerInventoryAPI.md#v1computersinventorydetailidpatch) | **PATCH** /v1/computers-inventory-detail/{id} | Update specific fields on a computer
[**v1ComputersInventoryFilevaultGet**](ComputerInventoryAPI.md#v1computersinventoryfilevaultget) | **GET** /v1/computers-inventory/filevault | Return paginated FileVault information for all computers
[**v1ComputersInventoryGet**](ComputerInventoryAPI.md#v1computersinventoryget) | **GET** /v1/computers-inventory | Return paginated Computer Inventory records
[**v1ComputersInventoryIdAttachmentsAttachmentIdDelete**](ComputerInventoryAPI.md#v1computersinventoryidattachmentsattachmentiddelete) | **DELETE** /v1/computers-inventory/{id}/attachments/{attachmentId} | Remove attachment
[**v1ComputersInventoryIdAttachmentsAttachmentIdGet**](ComputerInventoryAPI.md#v1computersinventoryidattachmentsattachmentidget) | **GET** /v1/computers-inventory/{id}/attachments/{attachmentId} | Download attachment file
[**v1ComputersInventoryIdAttachmentsPost**](ComputerInventoryAPI.md#v1computersinventoryidattachmentspost) | **POST** /v1/computers-inventory/{id}/attachments | Upload attachment and assign to computer
[**v1ComputersInventoryIdDelete**](ComputerInventoryAPI.md#v1computersinventoryiddelete) | **DELETE** /v1/computers-inventory/{id} | Remove specified Computer record
[**v1ComputersInventoryIdFilevaultGet**](ComputerInventoryAPI.md#v1computersinventoryidfilevaultget) | **GET** /v1/computers-inventory/{id}/filevault | Return FileVault information for a specific computer
[**v1ComputersInventoryIdGet**](ComputerInventoryAPI.md#v1computersinventoryidget) | **GET** /v1/computers-inventory/{id} | Return General section of a Computer
[**v1ComputersInventoryIdViewRecoveryLockPasswordGet**](ComputerInventoryAPI.md#v1computersinventoryidviewrecoverylockpasswordget) | **GET** /v1/computers-inventory/{id}/view-recovery-lock-password | Return a Computers Recovery Lock Password


# **v1ComputersInventoryDetailIdGet**
```swift
    open class func v1ComputersInventoryDetailIdGet(id: String, completion: @escaping (_ data: ComputerInventory?, _ error: Error?) -> Void)
```

Return all sections of a computer

Return all sections of a computer

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record

// Return all sections of a computer
ComputerInventoryAPI.v1ComputersInventoryDetailIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 

### Return type

[**ComputerInventory**](ComputerInventory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryDetailIdPatch**
```swift
    open class func v1ComputersInventoryDetailIdPatch(id: String, computerInventoryUpdateRequest: ComputerInventoryUpdateRequest, completion: @escaping (_ data: ComputerInventory?, _ error: Error?) -> Void)
```

Update specific fields on a computer

Update specific fields on a computer, then return the updated computer object.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record
let computerInventoryUpdateRequest = ComputerInventoryUpdateRequest(udid: "udid_example", general: ComputerGeneralUpdate(name: "name_example", lastIpAddress: "lastIpAddress_example", barcode1: "barcode1_example", barcode2: "barcode2_example", assetTag: "assetTag_example", extensionAttributes: [ComputerExtensionAttribute(definitionId: "definitionId_example", name: "name_example", description: "description_example", enabled: true, multiValue: true, values: ["values_example"], dataType: "dataType_example", options: ["options_example"], inputType: "inputType_example")]), purchasing: ComputerPurchase(leased: true, purchased: true, poNumber: "poNumber_example", poDate: Date(), vendor: "vendor_example", warrantyDate: Date(), appleCareId: "appleCareId_example", leaseDate: Date(), purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", extensionAttributes: [nil]), userAndLocation: ComputerUserAndLocation(username: "username_example", realname: "realname_example", email: "email_example", position: "position_example", phone: "phone_example", departmentId: "departmentId_example", buildingId: "buildingId_example", room: "room_example", extensionAttributes: [nil]), hardware: ComputerHardwareUpdate(networkAdapterType: "networkAdapterType_example", macAddress: "macAddress_example", altNetworkAdapterType: "altNetworkAdapterType_example", altMacAddress: "altMacAddress_example", extensionAttributes: [nil]), operatingSystem: ComputerOperatingSystemUpdate(extensionAttributes: [nil]), extensionAttributes: [nil]) // ComputerInventoryUpdateRequest | 

// Update specific fields on a computer
ComputerInventoryAPI.v1ComputersInventoryDetailIdPatch(id: id, computerInventoryUpdateRequest: computerInventoryUpdateRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 
 **computerInventoryUpdateRequest** | [**ComputerInventoryUpdateRequest**](ComputerInventoryUpdateRequest.md) |  | 

### Return type

[**ComputerInventory**](ComputerInventory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryFilevaultGet**
```swift
    open class func v1ComputersInventoryFilevaultGet(page: Int? = nil, pageSize: Int? = nil, completion: @escaping (_ data: ComputerInventoryFileVaultSearchResults?, _ error: Error?) -> Void)
```

Return paginated FileVault information for all computers

Return paginated FileVault information for all computers

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)

// Return paginated FileVault information for all computers
ComputerInventoryAPI.v1ComputersInventoryFilevaultGet(page: page, pageSize: pageSize) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]

### Return type

[**ComputerInventoryFileVaultSearchResults**](ComputerInventoryFileVaultSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryGet**
```swift
    open class func v1ComputersInventoryGet(section: [ComputerSection]? = nil, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: ComputerInventorySearchResults?, _ error: Error?) -> Void)
```

Return paginated Computer Inventory records

Return paginated Computer Inventory records

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let section = [ComputerSection()] // [ComputerSection] | section of computer details, if not specified, General section data is returned. Multiple section parameters are supported, e.g. section=GENERAL&section=HARDWARE (optional)
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `general.name:asc`. Multiple sort criteria are supported and must be separated with a comma.  Fields allowed in the sort: `general.name`, `udid`, `id`, `general.assetTag`, `general.jamfBinaryVersion`, `general.lastContactTime`, `general.lastEnrolledDate`, `general.lastCloudBackupDate`, `general.reportDate`, `general.remoteManagement.managementUsername`, `general.mdmCertificateExpiration`, `general.platform`, `hardware.make`, `hardware.model`, `operatingSystem.build`, `operatingSystem.supplementalBuildVersion`, `operatingSystem.rapidSecurityResponse`, `operatingSystem.name`, `operatingSystem.version`, `userAndLocation.realname`, `purchasing.lifeExpectancy`, `purchasing.warrantyDate`  Example: `sort=udid:desc,general.name:asc`.  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter computer inventory collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: `general.name`, `udid`, `id`, `general.assetTag`, `general.barcode1`, `general.barcode2`, `general.enrolledViaAutomatedDeviceEnrollment`, `general.lastIpAddress`, `general.itunesStoreAccountActive`, `general.jamfBinaryVersion`, `general.lastContactTime`, `general.lastEnrolledDate`, `general.lastCloudBackupDate`, `general.reportDate`, `general.lastReportedIp`, `general.remoteManagement.managed`, `general.remoteManagement.managementUsername`, `general.mdmCapable.capable`, `general.mdmCertificateExpiration`, `general.platform`, `general.supervised`, `general.userApprovedMdm`, `general.declarativeDeviceManagementEnabled`,  `hardware.bleCapable`, `hardware.macAddress`, `hardware.make`, `hardware.model`, `hardware.modelIdentifier`, `hardware.serialNumber`, `hardware.supportsIosAppInstalls`,`hardware.appleSilicon`, `operatingSystem.activeDirectoryStatus`, `operatingSystem.fileVault2Status`, `operatingSystem.build`, `operatingSystem.supplementalBuildVersion`, `operatingSystem.rapidSecurityResponse`, `operatingSystem.name`, `operatingSystem.version`, `security.activationLockEnabled`, `security.recoveryLockEnabled`,`security.firewallEnabled`,`userAndLocation.buildingId`, `userAndLocation.departmentId`, `userAndLocation.email`, `userAndLocation.realname`, `userAndLocation.phone`, `userAndLocation.position`,`userAndLocation.room`, `userAndLocation.username`, `purchasing.appleCareId`, `purchasing.lifeExpectancy`, `purchasing.purchased`, `purchasing.leased`, `purchasing.vendor`, `purchasing.warrantyDate`,  This param can be combined with paging and sorting. Example: `filter=general.name==\"Orchard\"`  (optional) (default to "")

// Return paginated Computer Inventory records
ComputerInventoryAPI.v1ComputersInventoryGet(section: section, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **section** | [**[ComputerSection]**](ComputerSection.md) | section of computer details, if not specified, General section data is returned. Multiple section parameters are supported, e.g. section&#x3D;GENERAL&amp;section&#x3D;HARDWARE | [optional] 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;general.name:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma.  Fields allowed in the sort: &#x60;general.name&#x60;, &#x60;udid&#x60;, &#x60;id&#x60;, &#x60;general.assetTag&#x60;, &#x60;general.jamfBinaryVersion&#x60;, &#x60;general.lastContactTime&#x60;, &#x60;general.lastEnrolledDate&#x60;, &#x60;general.lastCloudBackupDate&#x60;, &#x60;general.reportDate&#x60;, &#x60;general.remoteManagement.managementUsername&#x60;, &#x60;general.mdmCertificateExpiration&#x60;, &#x60;general.platform&#x60;, &#x60;hardware.make&#x60;, &#x60;hardware.model&#x60;, &#x60;operatingSystem.build&#x60;, &#x60;operatingSystem.supplementalBuildVersion&#x60;, &#x60;operatingSystem.rapidSecurityResponse&#x60;, &#x60;operatingSystem.name&#x60;, &#x60;operatingSystem.version&#x60;, &#x60;userAndLocation.realname&#x60;, &#x60;purchasing.lifeExpectancy&#x60;, &#x60;purchasing.warrantyDate&#x60;  Example: &#x60;sort&#x3D;udid:desc,general.name:asc&#x60;.  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter computer inventory collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: &#x60;general.name&#x60;, &#x60;udid&#x60;, &#x60;id&#x60;, &#x60;general.assetTag&#x60;, &#x60;general.barcode1&#x60;, &#x60;general.barcode2&#x60;, &#x60;general.enrolledViaAutomatedDeviceEnrollment&#x60;, &#x60;general.lastIpAddress&#x60;, &#x60;general.itunesStoreAccountActive&#x60;, &#x60;general.jamfBinaryVersion&#x60;, &#x60;general.lastContactTime&#x60;, &#x60;general.lastEnrolledDate&#x60;, &#x60;general.lastCloudBackupDate&#x60;, &#x60;general.reportDate&#x60;, &#x60;general.lastReportedIp&#x60;, &#x60;general.remoteManagement.managed&#x60;, &#x60;general.remoteManagement.managementUsername&#x60;, &#x60;general.mdmCapable.capable&#x60;, &#x60;general.mdmCertificateExpiration&#x60;, &#x60;general.platform&#x60;, &#x60;general.supervised&#x60;, &#x60;general.userApprovedMdm&#x60;, &#x60;general.declarativeDeviceManagementEnabled&#x60;,  &#x60;hardware.bleCapable&#x60;, &#x60;hardware.macAddress&#x60;, &#x60;hardware.make&#x60;, &#x60;hardware.model&#x60;, &#x60;hardware.modelIdentifier&#x60;, &#x60;hardware.serialNumber&#x60;, &#x60;hardware.supportsIosAppInstalls&#x60;,&#x60;hardware.appleSilicon&#x60;, &#x60;operatingSystem.activeDirectoryStatus&#x60;, &#x60;operatingSystem.fileVault2Status&#x60;, &#x60;operatingSystem.build&#x60;, &#x60;operatingSystem.supplementalBuildVersion&#x60;, &#x60;operatingSystem.rapidSecurityResponse&#x60;, &#x60;operatingSystem.name&#x60;, &#x60;operatingSystem.version&#x60;, &#x60;security.activationLockEnabled&#x60;, &#x60;security.recoveryLockEnabled&#x60;,&#x60;security.firewallEnabled&#x60;,&#x60;userAndLocation.buildingId&#x60;, &#x60;userAndLocation.departmentId&#x60;, &#x60;userAndLocation.email&#x60;, &#x60;userAndLocation.realname&#x60;, &#x60;userAndLocation.phone&#x60;, &#x60;userAndLocation.position&#x60;,&#x60;userAndLocation.room&#x60;, &#x60;userAndLocation.username&#x60;, &#x60;purchasing.appleCareId&#x60;, &#x60;purchasing.lifeExpectancy&#x60;, &#x60;purchasing.purchased&#x60;, &#x60;purchasing.leased&#x60;, &#x60;purchasing.vendor&#x60;, &#x60;purchasing.warrantyDate&#x60;,  This param can be combined with paging and sorting. Example: &#x60;filter&#x3D;general.name&#x3D;&#x3D;\&quot;Orchard\&quot;&#x60;  | [optional] [default to &quot;&quot;]

### Return type

[**ComputerInventorySearchResults**](ComputerInventorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdAttachmentsAttachmentIdDelete**
```swift
    open class func v1ComputersInventoryIdAttachmentsAttachmentIdDelete(id: String, attachmentId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove attachment

Remove attachment

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record
let attachmentId = "attachmentId_example" // String | instance id of attachment object

// Remove attachment
ComputerInventoryAPI.v1ComputersInventoryIdAttachmentsAttachmentIdDelete(id: id, attachmentId: attachmentId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 
 **attachmentId** | **String** | instance id of attachment object | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdAttachmentsAttachmentIdGet**
```swift
    open class func v1ComputersInventoryIdAttachmentsAttachmentIdGet(id: String, attachmentId: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download attachment file

Download attachment file

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record
let attachmentId = "attachmentId_example" // String | instance id of attachment object

// Download attachment file
ComputerInventoryAPI.v1ComputersInventoryIdAttachmentsAttachmentIdGet(id: id, attachmentId: attachmentId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 
 **attachmentId** | **String** | instance id of attachment object | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdAttachmentsPost**
```swift
    open class func v1ComputersInventoryIdAttachmentsPost(id: String, file: URL, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Upload attachment and assign to computer

Upload attachment and assign to computer

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record
let file = URL(string: "https://example.com")! // URL | The file to upload

// Upload attachment and assign to computer
ComputerInventoryAPI.v1ComputersInventoryIdAttachmentsPost(id: id, file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 
 **file** | **URL** | The file to upload | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdDelete**
```swift
    open class func v1ComputersInventoryIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified Computer record

Remove specified Computer record

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record

// Remove specified Computer record
ComputerInventoryAPI.v1ComputersInventoryIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdFilevaultGet**
```swift
    open class func v1ComputersInventoryIdFilevaultGet(id: String, completion: @escaping (_ data: ComputerInventoryFileVault?, _ error: Error?) -> Void)
```

Return FileVault information for a specific computer

Return FileVault information for a specific computer

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record

// Return FileVault information for a specific computer
ComputerInventoryAPI.v1ComputersInventoryIdFilevaultGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 

### Return type

[**ComputerInventoryFileVault**](ComputerInventoryFileVault.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdGet**
```swift
    open class func v1ComputersInventoryIdGet(id: String, section: [ComputerSection]? = nil, completion: @escaping (_ data: ComputerInventory?, _ error: Error?) -> Void)
```

Return General section of a Computer

Return General section of a Computer

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record
let section = [ComputerSection()] // [ComputerSection] | section of computer details, if not specified, General section data is returned. Multiple section parameters are supported, e.g. section=general&section=hardware (optional)

// Return General section of a Computer
ComputerInventoryAPI.v1ComputersInventoryIdGet(id: id, section: section) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 
 **section** | [**[ComputerSection]**](ComputerSection.md) | section of computer details, if not specified, General section data is returned. Multiple section parameters are supported, e.g. section&#x3D;general&amp;section&#x3D;hardware | [optional] 

### Return type

[**ComputerInventory**](ComputerInventory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputersInventoryIdViewRecoveryLockPasswordGet**
```swift
    open class func v1ComputersInventoryIdViewRecoveryLockPasswordGet(id: String, completion: @escaping (_ data: ComputerInventoryRecoveryLockPasswordResponse?, _ error: Error?) -> Void)
```

Return a Computers Recovery Lock Password

Return a Computers Recovery Lock Password

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of computer record

// Return a Computers Recovery Lock Password
ComputerInventoryAPI.v1ComputersInventoryIdViewRecoveryLockPasswordGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of computer record | 

### Return type

[**ComputerInventoryRecoveryLockPasswordResponse**](ComputerInventoryRecoveryLockPasswordResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

