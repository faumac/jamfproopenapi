# EnrollmentSettingsV3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**installSingleProfile** | **Bool** |  | [optional] [default to false]
**signingMdmProfileEnabled** | **Bool** |  | [optional] [default to false]
**mdmSigningCertificate** | [**CertificateIdentityV2**](CertificateIdentityV2.md) |  | [optional] 
**restrictReenrollment** | **Bool** |  | [optional] [default to false]
**flushLocationInformation** | **Bool** |  | [optional] [default to false]
**flushLocationHistoryInformation** | **Bool** |  | [optional] [default to false]
**flushPolicyHistory** | **Bool** |  | [optional] [default to false]
**flushExtensionAttributes** | **Bool** |  | [optional] [default to false]
**flushMdmCommandsOnReenroll** | **String** |  | [optional] [default to .deleteEverythingExceptAcknowledged]
**macOsEnterpriseEnrollmentEnabled** | **Bool** |  | [optional] [default to false]
**managementUsername** | **String** |  | [default to ""]
**createManagementAccount** | **Bool** |  | [optional] [default to true]
**hideManagementAccount** | **Bool** |  | [optional] [default to false]
**allowSshOnlyManagementAccount** | **Bool** |  | [optional] [default to false]
**ensureSshRunning** | **Bool** |  | [optional] [default to true]
**launchSelfService** | **Bool** |  | [optional] [default to false]
**signQuickAdd** | **Bool** |  | [optional] [default to false]
**developerCertificateIdentity** | [**CertificateIdentityV2**](CertificateIdentityV2.md) |  | [optional] 
**developerCertificateIdentityDetails** | [**CertificateDetails**](CertificateDetails.md) |  | [optional] 
**mdmSigningCertificateDetails** | [**CertificateDetails**](CertificateDetails.md) |  | [optional] 
**iosEnterpriseEnrollmentEnabled** | **Bool** |  | [optional] [default to true]
**iosPersonalEnrollmentEnabled** | **Bool** |  | [optional] [default to false]
**personalDeviceEnrollmentType** | **String** |  | [optional] [default to .personaldeviceprofiles]
**accountDrivenUserEnrollmentEnabled** | **Bool** |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


