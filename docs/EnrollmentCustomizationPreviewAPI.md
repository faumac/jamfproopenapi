# EnrollmentCustomizationPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1EnrollmentCustomizationIdAllGet**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidallget) | **GET** /v1/enrollment-customization/{id}/all | Get all Panels for single Enrollment Customization 
[**v1EnrollmentCustomizationIdAllPanelIdDelete**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidallpaneliddelete) | **DELETE** /v1/enrollment-customization/{id}/all/{panel-id} | Delete a single Panel from an Enrollment Customization 
[**v1EnrollmentCustomizationIdAllPanelIdGet**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidallpanelidget) | **GET** /v1/enrollment-customization/{id}/all/{panel-id} | Get a single Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdLdapPanelIdDelete**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidldappaneliddelete) | **DELETE** /v1/enrollment-customization/{id}/ldap/{panel-id} | Delete an LDAP single panel from an Enrollment Customization 
[**v1EnrollmentCustomizationIdLdapPanelIdGet**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidldappanelidget) | **GET** /v1/enrollment-customization/{id}/ldap/{panel-id} | Get a single LDAP panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdLdapPanelIdPut**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidldappanelidput) | **PUT** /v1/enrollment-customization/{id}/ldap/{panel-id} | Update a single LDAP Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdLdapPost**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidldappost) | **POST** /v1/enrollment-customization/{id}/ldap | Create an LDAP Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdSsoPanelIdDelete**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidssopaneliddelete) | **DELETE** /v1/enrollment-customization/{id}/sso/{panel-id} | Delete a single SSO Panel from an Enrollment Customization 
[**v1EnrollmentCustomizationIdSsoPanelIdGet**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidssopanelidget) | **GET** /v1/enrollment-customization/{id}/sso/{panel-id} | Get a single SSO Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdSsoPanelIdPut**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidssopanelidput) | **PUT** /v1/enrollment-customization/{id}/sso/{panel-id} | Update a single SSO Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdSsoPost**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidssopost) | **POST** /v1/enrollment-customization/{id}/sso | Create an SSO Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdTextPanelIdDelete**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidtextpaneliddelete) | **DELETE** /v1/enrollment-customization/{id}/text/{panel-id} | Delete a Text single Panel from an Enrollment Customization 
[**v1EnrollmentCustomizationIdTextPanelIdGet**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidtextpanelidget) | **GET** /v1/enrollment-customization/{id}/text/{panel-id} | Get a single Text Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdTextPanelIdMarkdownGet**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidtextpanelidmarkdownget) | **GET** /v1/enrollment-customization/{id}/text/{panel-id}/markdown | Get the markdown output of a single Text Panel for a single Enrollment 
[**v1EnrollmentCustomizationIdTextPanelIdPut**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidtextpanelidput) | **PUT** /v1/enrollment-customization/{id}/text/{panel-id} | Update a single Text Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationIdTextPost**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationidtextpost) | **POST** /v1/enrollment-customization/{id}/text | Create a Text Panel for a single Enrollment Customization 
[**v1EnrollmentCustomizationParseMarkdownPost**](EnrollmentCustomizationPreviewAPI.md#v1enrollmentcustomizationparsemarkdownpost) | **POST** /v1/enrollment-customization/parse-markdown | Parse the given string as markdown text and return Html output 


# **v1EnrollmentCustomizationIdAllGet**
```swift
    open class func v1EnrollmentCustomizationIdAllGet(id: Int, completion: @escaping (_ data: EnrollmentCustomizationPanelList?, _ error: Error?) -> Void)
```

Get all Panels for single Enrollment Customization 

Get all panels for single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier

// Get all Panels for single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdAllGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 

### Return type

[**EnrollmentCustomizationPanelList**](EnrollmentCustomizationPanelList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdAllPanelIdDelete**
```swift
    open class func v1EnrollmentCustomizationIdAllPanelIdDelete(id: Int, panelId: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a single Panel from an Enrollment Customization 

Delete a single panel from an Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Delete a single Panel from an Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdAllPanelIdDelete(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdAllPanelIdGet**
```swift
    open class func v1EnrollmentCustomizationIdAllPanelIdGet(id: Int, panelId: Int, completion: @escaping (_ data: GetEnrollmentCustomizationPanel?, _ error: Error?) -> Void)
```

Get a single Panel for a single Enrollment Customization 

Get a single panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Get a single Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdAllPanelIdGet(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

[**GetEnrollmentCustomizationPanel**](GetEnrollmentCustomizationPanel.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdLdapPanelIdDelete**
```swift
    open class func v1EnrollmentCustomizationIdLdapPanelIdDelete(id: Int, panelId: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an LDAP single panel from an Enrollment Customization 

Delete an LDAP single Panel from an Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Delete an LDAP single panel from an Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdLdapPanelIdDelete(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdLdapPanelIdGet**
```swift
    open class func v1EnrollmentCustomizationIdLdapPanelIdGet(id: Int, panelId: Int, completion: @escaping (_ data: GetEnrollmentCustomizationPanelLdapAuth?, _ error: Error?) -> Void)
```

Get a single LDAP panel for a single Enrollment Customization 

Get a single LDAP panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Get a single LDAP panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdLdapPanelIdGet(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

[**GetEnrollmentCustomizationPanelLdapAuth**](GetEnrollmentCustomizationPanelLdapAuth.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdLdapPanelIdPut**
```swift
    open class func v1EnrollmentCustomizationIdLdapPanelIdPut(id: Int, panelId: Int, enrollmentCustomizationPanelLdapAuth: EnrollmentCustomizationPanelLdapAuth, completion: @escaping (_ data: GetEnrollmentCustomizationPanelLdapAuth?, _ error: Error?) -> Void)
```

Update a single LDAP Panel for a single Enrollment Customization 

Update a single LDAP panel for a single enrollment customization. If multiple LDAP access groups are defined with the same name and id, only one will be saved.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier
let enrollmentCustomizationPanelLdapAuth = EnrollmentCustomizationPanelLdapAuth(displayName: "displayName_example", rank: 123, usernameLabel: "usernameLabel_example", passwordLabel: "passwordLabel_example", title: "title_example", backButtonText: "backButtonText_example", continueButtonText: "continueButtonText_example", ldapGroupAccess: [EnrollmentCustomizationLdapGroupAccess(ldapServerId: 123, groupName: "groupName_example")]) // EnrollmentCustomizationPanelLdapAuth | Enrollment Customization Panel to update

// Update a single LDAP Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdLdapPanelIdPut(id: id, panelId: panelId, enrollmentCustomizationPanelLdapAuth: enrollmentCustomizationPanelLdapAuth) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 
 **enrollmentCustomizationPanelLdapAuth** | [**EnrollmentCustomizationPanelLdapAuth**](EnrollmentCustomizationPanelLdapAuth.md) | Enrollment Customization Panel to update | 

### Return type

[**GetEnrollmentCustomizationPanelLdapAuth**](GetEnrollmentCustomizationPanelLdapAuth.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdLdapPost**
```swift
    open class func v1EnrollmentCustomizationIdLdapPost(id: Int, enrollmentCustomizationPanelLdapAuth: EnrollmentCustomizationPanelLdapAuth, completion: @escaping (_ data: GetEnrollmentCustomizationPanelLdapAuth?, _ error: Error?) -> Void)
```

Create an LDAP Panel for a single Enrollment Customization 

Create an LDAP panel for a single enrollment customization. If multiple LDAP access groups are defined with the same name and id, only one will be saved.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let enrollmentCustomizationPanelLdapAuth = EnrollmentCustomizationPanelLdapAuth(displayName: "displayName_example", rank: 123, usernameLabel: "usernameLabel_example", passwordLabel: "passwordLabel_example", title: "title_example", backButtonText: "backButtonText_example", continueButtonText: "continueButtonText_example", ldapGroupAccess: [EnrollmentCustomizationLdapGroupAccess(ldapServerId: 123, groupName: "groupName_example")]) // EnrollmentCustomizationPanelLdapAuth | Enrollment Customization Panel to create

// Create an LDAP Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdLdapPost(id: id, enrollmentCustomizationPanelLdapAuth: enrollmentCustomizationPanelLdapAuth) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **enrollmentCustomizationPanelLdapAuth** | [**EnrollmentCustomizationPanelLdapAuth**](EnrollmentCustomizationPanelLdapAuth.md) | Enrollment Customization Panel to create | 

### Return type

[**GetEnrollmentCustomizationPanelLdapAuth**](GetEnrollmentCustomizationPanelLdapAuth.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdSsoPanelIdDelete**
```swift
    open class func v1EnrollmentCustomizationIdSsoPanelIdDelete(id: Int, panelId: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a single SSO Panel from an Enrollment Customization 

Delete a single SSO panel from an Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Delete a single SSO Panel from an Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdSsoPanelIdDelete(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdSsoPanelIdGet**
```swift
    open class func v1EnrollmentCustomizationIdSsoPanelIdGet(id: Int, panelId: Int, completion: @escaping (_ data: GetEnrollmentCustomizationPanelSsoAuth?, _ error: Error?) -> Void)
```

Get a single SSO Panel for a single Enrollment Customization 

Get a single SSO panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Get a single SSO Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdSsoPanelIdGet(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

[**GetEnrollmentCustomizationPanelSsoAuth**](GetEnrollmentCustomizationPanelSsoAuth.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdSsoPanelIdPut**
```swift
    open class func v1EnrollmentCustomizationIdSsoPanelIdPut(id: Int, panelId: Int, enrollmentCustomizationPanelSsoAuth: EnrollmentCustomizationPanelSsoAuth, completion: @escaping (_ data: GetEnrollmentCustomizationPanelSsoAuth?, _ error: Error?) -> Void)
```

Update a single SSO Panel for a single Enrollment Customization 

Update a single SSO panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier
let enrollmentCustomizationPanelSsoAuth = EnrollmentCustomizationPanelSsoAuth(displayName: "displayName_example", rank: 123, isUseJamfConnect: false, longNameAttribute: "longNameAttribute_example", shortNameAttribute: "shortNameAttribute_example", isGroupEnrollmentAccessEnabled: false, groupEnrollmentAccessName: "groupEnrollmentAccessName_example") // EnrollmentCustomizationPanelSsoAuth | Enrollment Customization Panel to update

// Update a single SSO Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdSsoPanelIdPut(id: id, panelId: panelId, enrollmentCustomizationPanelSsoAuth: enrollmentCustomizationPanelSsoAuth) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 
 **enrollmentCustomizationPanelSsoAuth** | [**EnrollmentCustomizationPanelSsoAuth**](EnrollmentCustomizationPanelSsoAuth.md) | Enrollment Customization Panel to update | 

### Return type

[**GetEnrollmentCustomizationPanelSsoAuth**](GetEnrollmentCustomizationPanelSsoAuth.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdSsoPost**
```swift
    open class func v1EnrollmentCustomizationIdSsoPost(id: Int, enrollmentCustomizationPanelSsoAuth: EnrollmentCustomizationPanelSsoAuth, completion: @escaping (_ data: GetEnrollmentCustomizationPanelSsoAuth?, _ error: Error?) -> Void)
```

Create an SSO Panel for a single Enrollment Customization 

Create an SSO panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let enrollmentCustomizationPanelSsoAuth = EnrollmentCustomizationPanelSsoAuth(displayName: "displayName_example", rank: 123, isUseJamfConnect: false, longNameAttribute: "longNameAttribute_example", shortNameAttribute: "shortNameAttribute_example", isGroupEnrollmentAccessEnabled: false, groupEnrollmentAccessName: "groupEnrollmentAccessName_example") // EnrollmentCustomizationPanelSsoAuth | Enrollment Customization Panel to create

// Create an SSO Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdSsoPost(id: id, enrollmentCustomizationPanelSsoAuth: enrollmentCustomizationPanelSsoAuth) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **enrollmentCustomizationPanelSsoAuth** | [**EnrollmentCustomizationPanelSsoAuth**](EnrollmentCustomizationPanelSsoAuth.md) | Enrollment Customization Panel to create | 

### Return type

[**GetEnrollmentCustomizationPanelSsoAuth**](GetEnrollmentCustomizationPanelSsoAuth.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdTextPanelIdDelete**
```swift
    open class func v1EnrollmentCustomizationIdTextPanelIdDelete(id: Int, panelId: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Text single Panel from an Enrollment Customization 

Delete a Text single panel from an Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Delete a Text single Panel from an Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdTextPanelIdDelete(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdTextPanelIdGet**
```swift
    open class func v1EnrollmentCustomizationIdTextPanelIdGet(id: Int, panelId: Int, completion: @escaping (_ data: GetEnrollmentCustomizationPanelText?, _ error: Error?) -> Void)
```

Get a single Text Panel for a single Enrollment Customization 

Get a single Text panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Get a single Text Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdTextPanelIdGet(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

[**GetEnrollmentCustomizationPanelText**](GetEnrollmentCustomizationPanelText.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdTextPanelIdMarkdownGet**
```swift
    open class func v1EnrollmentCustomizationIdTextPanelIdMarkdownGet(id: Int, panelId: Int, completion: @escaping (_ data: Markdown?, _ error: Error?) -> Void)
```

Get the markdown output of a single Text Panel for a single Enrollment 

Get the markdown output of a single Text panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier

// Get the markdown output of a single Text Panel for a single Enrollment 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdTextPanelIdMarkdownGet(id: id, panelId: panelId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 

### Return type

[**Markdown**](Markdown.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdTextPanelIdPut**
```swift
    open class func v1EnrollmentCustomizationIdTextPanelIdPut(id: Int, panelId: Int, enrollmentCustomizationPanelText: EnrollmentCustomizationPanelText, completion: @escaping (_ data: GetEnrollmentCustomizationPanelText?, _ error: Error?) -> Void)
```

Update a single Text Panel for a single Enrollment Customization 

Update a single Text panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let panelId = 987 // Int | Panel object identifier
let enrollmentCustomizationPanelText = EnrollmentCustomizationPanelText(displayName: "displayName_example", rank: 123, body: "body_example", subtext: "subtext_example", title: "title_example", backButtonText: "backButtonText_example", continueButtonText: "continueButtonText_example") // EnrollmentCustomizationPanelText | Enrollment Customization Panel to update

// Update a single Text Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdTextPanelIdPut(id: id, panelId: panelId, enrollmentCustomizationPanelText: enrollmentCustomizationPanelText) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **panelId** | **Int** | Panel object identifier | 
 **enrollmentCustomizationPanelText** | [**EnrollmentCustomizationPanelText**](EnrollmentCustomizationPanelText.md) | Enrollment Customization Panel to update | 

### Return type

[**GetEnrollmentCustomizationPanelText**](GetEnrollmentCustomizationPanelText.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdTextPost**
```swift
    open class func v1EnrollmentCustomizationIdTextPost(id: Int, enrollmentCustomizationPanelText: EnrollmentCustomizationPanelText, completion: @escaping (_ data: GetEnrollmentCustomizationPanelText?, _ error: Error?) -> Void)
```

Create a Text Panel for a single Enrollment Customization 

Create a Text panel for a single enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let enrollmentCustomizationPanelText = EnrollmentCustomizationPanelText(displayName: "displayName_example", rank: 123, body: "body_example", subtext: "subtext_example", title: "title_example", backButtonText: "backButtonText_example", continueButtonText: "continueButtonText_example") // EnrollmentCustomizationPanelText | Enrollment Customization Panel to create

// Create a Text Panel for a single Enrollment Customization 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationIdTextPost(id: id, enrollmentCustomizationPanelText: enrollmentCustomizationPanelText) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **enrollmentCustomizationPanelText** | [**EnrollmentCustomizationPanelText**](EnrollmentCustomizationPanelText.md) | Enrollment Customization Panel to create | 

### Return type

[**GetEnrollmentCustomizationPanelText**](GetEnrollmentCustomizationPanelText.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationParseMarkdownPost**
```swift
    open class func v1EnrollmentCustomizationParseMarkdownPost(markdown: Markdown, completion: @escaping (_ data: Markdown?, _ error: Error?) -> Void)
```

Parse the given string as markdown text and return Html output 

Parse the given string as markdown text and return Html output

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let markdown = Markdown(markdown: "markdown_example") // Markdown | Enrollment Customization Panel to create

// Parse the given string as markdown text and return Html output 
EnrollmentCustomizationPreviewAPI.v1EnrollmentCustomizationParseMarkdownPost(markdown: markdown) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **markdown** | [**Markdown**](Markdown.md) | Enrollment Customization Panel to create | 

### Return type

[**Markdown**](Markdown.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

