# ApiIntegrationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | 
**authorizationScopes** | **[String]** |  | 
**displayName** | **String** |  | 
**enabled** | **Bool** |  | 
**accessTokenLifetimeSeconds** | **Int** |  | [optional] 
**appType** | **String** | Type of API Client:     * &#x60;CLIENT_CREDENTIALS&#x60; - A client ID and secret have been generated for this integration.     * &#x60;NATIVE_APP_OAUTH&#x60; - A native app (i.e., Jamf Reset) has been linked to this integration for auth code grant type via Managed App Config.     * &#x60;NONE&#x60; - No client is currently associated with this integration.  | [readonly] 
**clientId** | **String** |  | [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


