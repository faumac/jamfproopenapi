# JamfProInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isVppTokenEnabled** | **Bool** |  | [optional] 
**isDepAccountEnabled** | **Bool** |  | [optional] 
**isByodEnabled** | **Bool** |  | [optional] 
**isUserMigrationEnabled** | **Bool** |  | [optional] 
**isCloudDeploymentsEnabled** | **Bool** |  | [optional] 
**isPatchEnabled** | **Bool** |  | [optional] 
**isSsoSamlEnabled** | **Bool** |  | [optional] 
**isSmtpEnabled** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


