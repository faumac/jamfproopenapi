# MobileDeviceUserAndLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**realName** | **String** |  | [optional] 
**emailAddress** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**departmentId** | **String** |  | [optional] 
**buildingId** | **String** |  | [optional] 
**room** | **String** |  | [optional] 
**building** | **String** |  | [optional] 
**department** | **String** |  | [optional] 
**extensionAttributes** | [MobileDeviceExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


