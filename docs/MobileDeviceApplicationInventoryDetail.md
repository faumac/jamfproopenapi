# MobileDeviceApplicationInventoryDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**shortVersion** | **String** |  | [optional] 
**managementStatus** | **String** |  | [optional] 
**validationStatus** | **Bool** |  | [optional] 
**bundleSize** | **String** |  | [optional] 
**dynamicSize** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


