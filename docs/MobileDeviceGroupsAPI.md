# MobileDeviceGroupsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MobileDeviceGroupsGet**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsget) | **GET** /v1/mobile-device-groups | Return the list of all Mobile Device Groups 
[**v1MobileDeviceGroupsStaticGroupMembershipIdGet**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsstaticgroupmembershipidget) | **GET** /v1/mobile-device-groups/static-group-membership/{id} | Get Static Group Membership by Id 
[**v1MobileDeviceGroupsStaticGroupsGet**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsstaticgroupsget) | **GET** /v1/mobile-device-groups/static-groups | Get Static Groups 
[**v1MobileDeviceGroupsStaticGroupsIdDelete**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsstaticgroupsiddelete) | **DELETE** /v1/mobile-device-groups/static-groups/{id} | Remove Static Group by Id 
[**v1MobileDeviceGroupsStaticGroupsIdGet**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsstaticgroupsidget) | **GET** /v1/mobile-device-groups/static-groups/{id} | Get Static Group by Id 
[**v1MobileDeviceGroupsStaticGroupsIdPatch**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsstaticgroupsidpatch) | **PATCH** /v1/mobile-device-groups/static-groups/{id} | Update membership of a static group. 
[**v1MobileDeviceGroupsStaticGroupsPost**](MobileDeviceGroupsAPI.md#v1mobiledevicegroupsstaticgroupspost) | **POST** /v1/mobile-device-groups/static-groups | Create membership of a static group. 


# **v1MobileDeviceGroupsGet**
```swift
    open class func v1MobileDeviceGroupsGet(completion: @escaping (_ data: [MobileDeviceGroup]?, _ error: Error?) -> Void)
```

Return the list of all Mobile Device Groups 

Returns the list of all mobile device groups. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return the list of all Mobile Device Groups 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[MobileDeviceGroup]**](MobileDeviceGroup.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDeviceGroupsStaticGroupMembershipIdGet**
```swift
    open class func v1MobileDeviceGroupsStaticGroupMembershipIdGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: InventoryListMobileDeviceSearchResults?, _ error: Error?) -> Void)
```

Get Static Group Membership by Id 

Get Static Group Membership by Id 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of static-group
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is mobileDeviceId:asc. Multiple sort criteria are supported and must be separated with a comma.   Fields allowed in the sort: `airPlayPassword`, `appAnalyticsEnabled`, `assetTag`, `availableSpaceMb`,  `batteryLevel`, `bluetoothLowEnergyCapable`, `bluetoothMacAddress`, `capacityMb`,  `lostModeEnabledDate`, `declarativeDeviceManagementEnabled`, `deviceId`, `deviceLocatorServiceEnabled`, `devicePhoneNumber`, `diagnosticAndUsageReportingEnabled`, `displayName`, `doNotDisturbEnabled`,  `enrollmentSessionTokenValid`, `exchangeDeviceId`, `cloudBackupEnabled`, `osBuild`, `osRapidSecurityResponse`, `osSupplementalBuildVersion`, `osVersion`, `ipAddress`, `itunesStoreAccountActive`, `mobileDeviceId`, `languages`, `lastBackupDate`, `lastEnrolledDate`, `lastCloudBackupDate`, `lastInventoryUpdateDate`, `locales`, `locationServicesForSelfServiceMobileEnabled`, `lostModeEnabled`, `managed`, `mdmProfileExpirationDate`, `model`, `modelIdentifier`, `modelNumber`, `modemFirmwareVersion`, `quotaSize`, `residentUsers`, `serialNumber`, `sharedIpad`, `supervised`, `tethered`, `timeZone`, `udid`, `usedSpacePercentage`, `wifiMacAddress`, `deviceOwnershipType`, `building`, `department`, `emailAddress`, `fullName`, `userPhoneNumber`, `position`, `room`, `username`, `appleCareId`, `leaseExpirationDate`,`lifeExpectancyYears`, `poDate`, `poNumber`, `purchasePrice`, `purchasedOrLeased`, `purchasingAccount`, `purchasingContact`, `vendor`, `warrantyExpirationDate`, `activationLockEnabled`, `blockEncryptionCapable`, `dataProtection`, `fileEncryptionCapable`, `hardwareEncryptionSupported`, `jailbreakStatus`, `passcodeCompliant`, `passcodeCompliantWithProfile`, `passcodeLockGracePeriodEnforcedSeconds`, `passcodePresent`, `personalDeviceProfileCurrent`, `carrierSettingsVersion`, `cellularTechnology`, `currentCarrierNetwork`, `currentMobileCountryCode`, `currentMobileNetworkCode`,  `dataRoamingEnabled`, `eid`, `network`, `homeMobileCountryCode`,  `homeMobileNetworkCode`, `iccid`, `imei`, `imei2`, `meid`, `personalHotspotEnabled`, `voiceRoamingEnabled`, `roaming`  Example: `sort=displayName:desc,username:asc`  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter mobile device collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: `airPlayPassword`, `appAnalyticsEnabled`, `assetTag`, `availableSpaceMb`,  `batteryLevel`, `bluetoothLowEnergyCapable`, `bluetoothMacAddress`, `capacityMb`,  `declarativeDeviceManagementEnabled`, `deviceId`, `deviceLocatorServiceEnabled`, `devicePhoneNumber`, `diagnosticAndUsageReportingEnabled`, `displayName`, `doNotDisturbEnabled`, `exchangeDeviceId`,  `cloudBackupEnabled`, `osBuild`, `osSupplementalBuildVersion`, `osVersion`, `osRapidSecurityResponse`, `ipAddress`,  `itunesStoreAccountActive`, `mobileDeviceId`, `languages`, `locales`, `locationServicesForSelfServiceMobileEnabled`, `lostModeEnabled`, `managed`, `model`,  `modelIdentifier`, `modelNumber`, `modemFirmwareVersion`, `quotaSize`,  `residentUsers`, `serialNumber`, `sharedIpad`, `supervised`, `tethered`, `timeZone`, `udid`, `usedSpacePercentage`,  `wifiMacAddress`, `building`, `department`, `emailAddress`, `fullName`, `userPhoneNumber`, `position`, `room`, `username`, `appleCareId`, `lifeExpectancyYears`, `poNumber`,  `purchasePrice`, `purchasedOrLeased`, `purchasingAccount`, `purchasingContact`, `vendor`, `activationLockEnabled`, `blockEncryptionCapable`, `dataProtection`,  `fileEncryptionCapable`, `passcodeCompliant`, `passcodeCompliantWithProfile`, `passcodeLockGracePeriodEnforcedSeconds`, `passcodePresent`, `personalDeviceProfileCurrent`, `carrierSettingsVersion`, `currentCarrierNetwork`, `currentMobileCountryCode`, `currentMobileNetworkCode`, `dataRoamingEnabled`, `eid`, `network`, `homeMobileCountryCode`, `homeMobileNetworkCode`, `iccid`, `imei`, `imei2`, `meid`, `personalHotspotEnabled`,  `roaming`  This param can be combined with paging and sorting. Example: `filter=displayName==\"iPad\"`  (optional) (default to "")

// Get Static Group Membership by Id 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsStaticGroupMembershipIdGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of static-group | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is mobileDeviceId:asc. Multiple sort criteria are supported and must be separated with a comma.   Fields allowed in the sort: &#x60;airPlayPassword&#x60;, &#x60;appAnalyticsEnabled&#x60;, &#x60;assetTag&#x60;, &#x60;availableSpaceMb&#x60;,  &#x60;batteryLevel&#x60;, &#x60;bluetoothLowEnergyCapable&#x60;, &#x60;bluetoothMacAddress&#x60;, &#x60;capacityMb&#x60;,  &#x60;lostModeEnabledDate&#x60;, &#x60;declarativeDeviceManagementEnabled&#x60;, &#x60;deviceId&#x60;, &#x60;deviceLocatorServiceEnabled&#x60;, &#x60;devicePhoneNumber&#x60;, &#x60;diagnosticAndUsageReportingEnabled&#x60;, &#x60;displayName&#x60;, &#x60;doNotDisturbEnabled&#x60;,  &#x60;enrollmentSessionTokenValid&#x60;, &#x60;exchangeDeviceId&#x60;, &#x60;cloudBackupEnabled&#x60;, &#x60;osBuild&#x60;, &#x60;osRapidSecurityResponse&#x60;, &#x60;osSupplementalBuildVersion&#x60;, &#x60;osVersion&#x60;, &#x60;ipAddress&#x60;, &#x60;itunesStoreAccountActive&#x60;, &#x60;mobileDeviceId&#x60;, &#x60;languages&#x60;, &#x60;lastBackupDate&#x60;, &#x60;lastEnrolledDate&#x60;, &#x60;lastCloudBackupDate&#x60;, &#x60;lastInventoryUpdateDate&#x60;, &#x60;locales&#x60;, &#x60;locationServicesForSelfServiceMobileEnabled&#x60;, &#x60;lostModeEnabled&#x60;, &#x60;managed&#x60;, &#x60;mdmProfileExpirationDate&#x60;, &#x60;model&#x60;, &#x60;modelIdentifier&#x60;, &#x60;modelNumber&#x60;, &#x60;modemFirmwareVersion&#x60;, &#x60;quotaSize&#x60;, &#x60;residentUsers&#x60;, &#x60;serialNumber&#x60;, &#x60;sharedIpad&#x60;, &#x60;supervised&#x60;, &#x60;tethered&#x60;, &#x60;timeZone&#x60;, &#x60;udid&#x60;, &#x60;usedSpacePercentage&#x60;, &#x60;wifiMacAddress&#x60;, &#x60;deviceOwnershipType&#x60;, &#x60;building&#x60;, &#x60;department&#x60;, &#x60;emailAddress&#x60;, &#x60;fullName&#x60;, &#x60;userPhoneNumber&#x60;, &#x60;position&#x60;, &#x60;room&#x60;, &#x60;username&#x60;, &#x60;appleCareId&#x60;, &#x60;leaseExpirationDate&#x60;,&#x60;lifeExpectancyYears&#x60;, &#x60;poDate&#x60;, &#x60;poNumber&#x60;, &#x60;purchasePrice&#x60;, &#x60;purchasedOrLeased&#x60;, &#x60;purchasingAccount&#x60;, &#x60;purchasingContact&#x60;, &#x60;vendor&#x60;, &#x60;warrantyExpirationDate&#x60;, &#x60;activationLockEnabled&#x60;, &#x60;blockEncryptionCapable&#x60;, &#x60;dataProtection&#x60;, &#x60;fileEncryptionCapable&#x60;, &#x60;hardwareEncryptionSupported&#x60;, &#x60;jailbreakStatus&#x60;, &#x60;passcodeCompliant&#x60;, &#x60;passcodeCompliantWithProfile&#x60;, &#x60;passcodeLockGracePeriodEnforcedSeconds&#x60;, &#x60;passcodePresent&#x60;, &#x60;personalDeviceProfileCurrent&#x60;, &#x60;carrierSettingsVersion&#x60;, &#x60;cellularTechnology&#x60;, &#x60;currentCarrierNetwork&#x60;, &#x60;currentMobileCountryCode&#x60;, &#x60;currentMobileNetworkCode&#x60;,  &#x60;dataRoamingEnabled&#x60;, &#x60;eid&#x60;, &#x60;network&#x60;, &#x60;homeMobileCountryCode&#x60;,  &#x60;homeMobileNetworkCode&#x60;, &#x60;iccid&#x60;, &#x60;imei&#x60;, &#x60;imei2&#x60;, &#x60;meid&#x60;, &#x60;personalHotspotEnabled&#x60;, &#x60;voiceRoamingEnabled&#x60;, &#x60;roaming&#x60;  Example: &#x60;sort&#x3D;displayName:desc,username:asc&#x60;  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter mobile device collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: &#x60;airPlayPassword&#x60;, &#x60;appAnalyticsEnabled&#x60;, &#x60;assetTag&#x60;, &#x60;availableSpaceMb&#x60;,  &#x60;batteryLevel&#x60;, &#x60;bluetoothLowEnergyCapable&#x60;, &#x60;bluetoothMacAddress&#x60;, &#x60;capacityMb&#x60;,  &#x60;declarativeDeviceManagementEnabled&#x60;, &#x60;deviceId&#x60;, &#x60;deviceLocatorServiceEnabled&#x60;, &#x60;devicePhoneNumber&#x60;, &#x60;diagnosticAndUsageReportingEnabled&#x60;, &#x60;displayName&#x60;, &#x60;doNotDisturbEnabled&#x60;, &#x60;exchangeDeviceId&#x60;,  &#x60;cloudBackupEnabled&#x60;, &#x60;osBuild&#x60;, &#x60;osSupplementalBuildVersion&#x60;, &#x60;osVersion&#x60;, &#x60;osRapidSecurityResponse&#x60;, &#x60;ipAddress&#x60;,  &#x60;itunesStoreAccountActive&#x60;, &#x60;mobileDeviceId&#x60;, &#x60;languages&#x60;, &#x60;locales&#x60;, &#x60;locationServicesForSelfServiceMobileEnabled&#x60;, &#x60;lostModeEnabled&#x60;, &#x60;managed&#x60;, &#x60;model&#x60;,  &#x60;modelIdentifier&#x60;, &#x60;modelNumber&#x60;, &#x60;modemFirmwareVersion&#x60;, &#x60;quotaSize&#x60;,  &#x60;residentUsers&#x60;, &#x60;serialNumber&#x60;, &#x60;sharedIpad&#x60;, &#x60;supervised&#x60;, &#x60;tethered&#x60;, &#x60;timeZone&#x60;, &#x60;udid&#x60;, &#x60;usedSpacePercentage&#x60;,  &#x60;wifiMacAddress&#x60;, &#x60;building&#x60;, &#x60;department&#x60;, &#x60;emailAddress&#x60;, &#x60;fullName&#x60;, &#x60;userPhoneNumber&#x60;, &#x60;position&#x60;, &#x60;room&#x60;, &#x60;username&#x60;, &#x60;appleCareId&#x60;, &#x60;lifeExpectancyYears&#x60;, &#x60;poNumber&#x60;,  &#x60;purchasePrice&#x60;, &#x60;purchasedOrLeased&#x60;, &#x60;purchasingAccount&#x60;, &#x60;purchasingContact&#x60;, &#x60;vendor&#x60;, &#x60;activationLockEnabled&#x60;, &#x60;blockEncryptionCapable&#x60;, &#x60;dataProtection&#x60;,  &#x60;fileEncryptionCapable&#x60;, &#x60;passcodeCompliant&#x60;, &#x60;passcodeCompliantWithProfile&#x60;, &#x60;passcodeLockGracePeriodEnforcedSeconds&#x60;, &#x60;passcodePresent&#x60;, &#x60;personalDeviceProfileCurrent&#x60;, &#x60;carrierSettingsVersion&#x60;, &#x60;currentCarrierNetwork&#x60;, &#x60;currentMobileCountryCode&#x60;, &#x60;currentMobileNetworkCode&#x60;, &#x60;dataRoamingEnabled&#x60;, &#x60;eid&#x60;, &#x60;network&#x60;, &#x60;homeMobileCountryCode&#x60;, &#x60;homeMobileNetworkCode&#x60;, &#x60;iccid&#x60;, &#x60;imei&#x60;, &#x60;imei2&#x60;, &#x60;meid&#x60;, &#x60;personalHotspotEnabled&#x60;,  &#x60;roaming&#x60;  This param can be combined with paging and sorting. Example: &#x60;filter&#x3D;displayName&#x3D;&#x3D;\&quot;iPad\&quot;&#x60;  | [optional] [default to &quot;&quot;]

### Return type

[**InventoryListMobileDeviceSearchResults**](InventoryListMobileDeviceSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDeviceGroupsStaticGroupsGet**
```swift
    open class func v1MobileDeviceGroupsStaticGroupsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: StaticGroupSearchResults?, _ error: Error?) -> Void)
```

Get Static Groups 

Get Static Groups 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Available criteria to sort on: groupId, groupName, siteId. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter department collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: groupId, groupName, siteId. The siteId field can only be filtered by admins with full access. Any sited admin will have siteId filtered automatically. This param can be combined with paging and sorting. Example: groupName==\"staticGroup1\" (optional) (default to "")

// Get Static Groups 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsStaticGroupsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Available criteria to sort on: groupId, groupName, siteId. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter department collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: groupId, groupName, siteId. The siteId field can only be filtered by admins with full access. Any sited admin will have siteId filtered automatically. This param can be combined with paging and sorting. Example: groupName&#x3D;&#x3D;\&quot;staticGroup1\&quot; | [optional] [default to &quot;&quot;]

### Return type

[**StaticGroupSearchResults**](StaticGroupSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDeviceGroupsStaticGroupsIdDelete**
```swift
    open class func v1MobileDeviceGroupsStaticGroupsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove Static Group by Id 

Remove Static Group by Id 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of static-group

// Remove Static Group by Id 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsStaticGroupsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of static-group | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDeviceGroupsStaticGroupsIdGet**
```swift
    open class func v1MobileDeviceGroupsStaticGroupsIdGet(id: String, completion: @escaping (_ data: StaticGroup?, _ error: Error?) -> Void)
```

Get Static Group by Id 

Get Static Group by Id 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of static-group

// Get Static Group by Id 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsStaticGroupsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of static-group | 

### Return type

[**StaticGroup**](StaticGroup.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDeviceGroupsStaticGroupsIdPatch**
```swift
    open class func v1MobileDeviceGroupsStaticGroupsIdPatch(id: String, staticGroupAssignment: StaticGroupAssignment, completion: @escaping (_ data: StaticGroupAssignment?, _ error: Error?) -> Void)
```

Update membership of a static group. 

Update membership of a static group. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of a static group
let staticGroupAssignment = StaticGroupAssignment(groupId: "groupId_example", groupName: "groupName_example", siteId: "siteId_example", assignments: [Assignment(mobileDeviceId: "mobileDeviceId_example", selected: true)]) // StaticGroupAssignment | 

// Update membership of a static group. 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsStaticGroupsIdPatch(id: id, staticGroupAssignment: staticGroupAssignment) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of a static group | 
 **staticGroupAssignment** | [**StaticGroupAssignment**](StaticGroupAssignment.md) |  | 

### Return type

[**StaticGroupAssignment**](StaticGroupAssignment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDeviceGroupsStaticGroupsPost**
```swift
    open class func v1MobileDeviceGroupsStaticGroupsPost(staticGroupAssignment: StaticGroupAssignment, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create membership of a static group. 

Create membership of a static group. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let staticGroupAssignment = StaticGroupAssignment(groupId: "groupId_example", groupName: "groupName_example", siteId: "siteId_example", assignments: [Assignment(mobileDeviceId: "mobileDeviceId_example", selected: true)]) // StaticGroupAssignment | 

// Create membership of a static group. 
MobileDeviceGroupsAPI.v1MobileDeviceGroupsStaticGroupsPost(staticGroupAssignment: staticGroupAssignment) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **staticGroupAssignment** | [**StaticGroupAssignment**](StaticGroupAssignment.md) |  | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

