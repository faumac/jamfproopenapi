# DeviceEnrollmentsDevicesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1DeviceEnrollmentsIdDevicesGet**](DeviceEnrollmentsDevicesAPI.md#v1deviceenrollmentsiddevicesget) | **GET** /v1/device-enrollments/{id}/devices | Retrieve a list of Devices assigned to the supplied id 


# **v1DeviceEnrollmentsIdDevicesGet**
```swift
    open class func v1DeviceEnrollmentsIdDevicesGet(id: String, completion: @escaping (_ data: DeviceEnrollmentDeviceSearchResults?, _ error: Error?) -> Void)
```

Retrieve a list of Devices assigned to the supplied id 

Retrieves a list of devices assigned to the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier

// Retrieve a list of Devices assigned to the supplied id 
DeviceEnrollmentsDevicesAPI.v1DeviceEnrollmentsIdDevicesGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 

### Return type

[**DeviceEnrollmentDeviceSearchResults**](DeviceEnrollmentDeviceSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

