# ExtensionAttributeV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**value** | **[String]** |  | [optional] 
**extensionAttributeCollectionAllowed** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


