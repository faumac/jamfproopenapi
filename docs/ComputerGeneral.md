# ComputerGeneral

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**lastIpAddress** | **String** |  | [optional] 
**lastReportedIp** | **String** |  | [optional] 
**jamfBinaryVersion** | **String** |  | [optional] 
**platform** | **String** |  | [optional] 
**barcode1** | **String** |  | [optional] 
**barcode2** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**remoteManagement** | [**ComputerRemoteManagement**](ComputerRemoteManagement.md) |  | [optional] 
**supervised** | **Bool** |  | [optional] 
**mdmCapable** | [**ComputerMdmCapability**](ComputerMdmCapability.md) |  | [optional] 
**reportDate** | **Date** |  | [optional] 
**lastContactTime** | **Date** |  | [optional] 
**lastCloudBackupDate** | **Date** |  | [optional] 
**lastEnrolledDate** | **Date** |  | [optional] 
**mdmProfileExpiration** | **Date** |  | [optional] 
**initialEntryDate** | **Date** |  | [optional] 
**distributionPoint** | **String** |  | [optional] 
**enrollmentMethod** | [**EnrollmentMethod**](EnrollmentMethod.md) |  | [optional] 
**site** | [**V1Site**](V1Site.md) |  | [optional] 
**itunesStoreAccountActive** | **Bool** |  | [optional] 
**enrolledViaAutomatedDeviceEnrollment** | **Bool** |  | [optional] 
**userApprovedMdm** | **Bool** |  | [optional] 
**declarativeDeviceManagementEnabled** | **Bool** |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 
**managementId** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


