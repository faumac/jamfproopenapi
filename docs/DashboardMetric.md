# DashboardMetric

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** | Usually a number associated with the tag; i.e. 23 Pending Computers | [optional] 
**enabled** | **Bool** | Logical to decide whether metric should be enabled or disabled; i.e. Policy can be at Retrying-Disabled status | [optional] [default to true]
**tag** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


