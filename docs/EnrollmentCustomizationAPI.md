# EnrollmentCustomizationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1EnrollmentCustomizationGet**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationget) | **GET** /v1/enrollment-customization | Retrieve sorted and paged Enrollment Customizations 
[**v1EnrollmentCustomizationIdDelete**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationiddelete) | **DELETE** /v1/enrollment-customization/{id} | Delete an Enrollment Customization with the supplied id 
[**v1EnrollmentCustomizationIdGet**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationidget) | **GET** /v1/enrollment-customization/{id} | Retrieve an Enrollment Customization with the supplied id 
[**v1EnrollmentCustomizationIdHistoryGet**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationidhistoryget) | **GET** /v1/enrollment-customization/{id}/history | Get sorted and paged Enrollment Customization history objects 
[**v1EnrollmentCustomizationIdHistoryPost**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationidhistorypost) | **POST** /v1/enrollment-customization/{id}/history | Add Enrollment Customization history object notes 
[**v1EnrollmentCustomizationIdPrestagesGet**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationidprestagesget) | **GET** /v1/enrollment-customization/{id}/prestages | Retrieve the list of Prestages using this Enrollment Customization 
[**v1EnrollmentCustomizationIdPut**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationidput) | **PUT** /v1/enrollment-customization/{id} | Update an Enrollment Customization 
[**v1EnrollmentCustomizationImagesPost**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationimagespost) | **POST** /v1/enrollment-customization/images | Upload an image
[**v1EnrollmentCustomizationPost**](EnrollmentCustomizationAPI.md#v1enrollmentcustomizationpost) | **POST** /v1/enrollment-customization | Create an Enrollment Customization 
[**v2EnrollmentCustomizationsGet**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsget) | **GET** /v2/enrollment-customizations | Retrieve sorted and paged Enrollment Customizations 
[**v2EnrollmentCustomizationsIdDelete**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsiddelete) | **DELETE** /v2/enrollment-customizations/{id} | Delete an Enrollment Customization with the supplied id 
[**v2EnrollmentCustomizationsIdGet**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsidget) | **GET** /v2/enrollment-customizations/{id} | Retrieve an Enrollment Customization with the supplied id 
[**v2EnrollmentCustomizationsIdHistoryGet**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsidhistoryget) | **GET** /v2/enrollment-customizations/{id}/history | Get sorted and paged Enrollment Customization history objects 
[**v2EnrollmentCustomizationsIdHistoryPost**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsidhistorypost) | **POST** /v2/enrollment-customizations/{id}/history | Add Enrollment Customization history object notes 
[**v2EnrollmentCustomizationsIdPrestagesGet**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsidprestagesget) | **GET** /v2/enrollment-customizations/{id}/prestages | Retrieve the list of Prestages using this Enrollment Customization 
[**v2EnrollmentCustomizationsIdPut**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsidput) | **PUT** /v2/enrollment-customizations/{id} | Update an Enrollment Customization 
[**v2EnrollmentCustomizationsImagesIdGet**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsimagesidget) | **GET** /v2/enrollment-customizations/images/{id} | Download an enrollment customization image 
[**v2EnrollmentCustomizationsImagesPost**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationsimagespost) | **POST** /v2/enrollment-customizations/images | Upload an image
[**v2EnrollmentCustomizationsPost**](EnrollmentCustomizationAPI.md#v2enrollmentcustomizationspost) | **POST** /v2/enrollment-customizations | Create an Enrollment Customization 


# **v1EnrollmentCustomizationGet**
```swift
    open class func v1EnrollmentCustomizationGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: EnrollmentCustomizationSearchResults?, _ error: Error?) -> Void)
```

Retrieve sorted and paged Enrollment Customizations 

Retrieves sorted and paged Enrollment Customizations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "id:asc")

// Retrieve sorted and paged Enrollment Customizations 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;id:asc&quot;]

### Return type

[**EnrollmentCustomizationSearchResults**](EnrollmentCustomizationSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdDelete**
```swift
    open class func v1EnrollmentCustomizationIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an Enrollment Customization with the supplied id 

Deletes an Enrollment Customization with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier

// Delete an Enrollment Customization with the supplied id 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdGet**
```swift
    open class func v1EnrollmentCustomizationIdGet(id: Int, completion: @escaping (_ data: GetEnrollmentCustomization?, _ error: Error?) -> Void)
```

Retrieve an Enrollment Customization with the supplied id 

Retrieves an Enrollment Customization with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier

// Retrieve an Enrollment Customization with the supplied id 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 

### Return type

[**GetEnrollmentCustomization**](GetEnrollmentCustomization.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdHistoryGet**
```swift
    open class func v1EnrollmentCustomizationIdHistoryGet(id: Int, page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get sorted and paged Enrollment Customization history objects 

Gets sorted and paged enrollment customization history objects

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is duplicated for each sort criterion, e.g., ...&sort=name%2Casc&sort=date%2Cdesc (optional)

// Get sorted and paged Enrollment Customization history objects 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationIdHistoryGet(id: id, page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name%2Casc&amp;sort&#x3D;date%2Cdesc | [optional] 

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdHistoryPost**
```swift
    open class func v1EnrollmentCustomizationIdHistoryPost(id: Int, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Enrollment Customization history object notes 

Adds enrollment customization history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Enrollment Customization history object notes 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdPrestagesGet**
```swift
    open class func v1EnrollmentCustomizationIdPrestagesGet(id: Int, completion: @escaping (_ data: PrestageDependencies?, _ error: Error?) -> Void)
```

Retrieve the list of Prestages using this Enrollment Customization 

Retrieves the list of Prestages using this Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier

// Retrieve the list of Prestages using this Enrollment Customization 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationIdPrestagesGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 

### Return type

[**PrestageDependencies**](PrestageDependencies.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationIdPut**
```swift
    open class func v1EnrollmentCustomizationIdPut(id: Int, enrollmentCustomization: EnrollmentCustomization, completion: @escaping (_ data: GetEnrollmentCustomization?, _ error: Error?) -> Void)
```

Update an Enrollment Customization 

Updates an Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Enrollment Customization identifier
let enrollmentCustomization = EnrollmentCustomization(siteId: 123, displayName: "displayName_example", description: "description_example", enrollmentCustomizationBrandingSettings: EnrollmentCustomizationBrandingSettings(textColor: "textColor_example", buttonColor: "buttonColor_example", buttonTextColor: "buttonTextColor_example", backgroundColor: "backgroundColor_example", iconUrl: "iconUrl_example")) // EnrollmentCustomization | Enrollment Customization to update

// Update an Enrollment Customization 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationIdPut(id: id, enrollmentCustomization: enrollmentCustomization) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Enrollment Customization identifier | 
 **enrollmentCustomization** | [**EnrollmentCustomization**](EnrollmentCustomization.md) | Enrollment Customization to update | 

### Return type

[**GetEnrollmentCustomization**](GetEnrollmentCustomization.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationImagesPost**
```swift
    open class func v1EnrollmentCustomizationImagesPost(file: URL, completion: @escaping (_ data: BrandingImageUrl?, _ error: Error?) -> Void)
```

Upload an image

Uploads an image

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let file = URL(string: "https://example.com")! // URL | The file to upload

// Upload an image
EnrollmentCustomizationAPI.v1EnrollmentCustomizationImagesPost(file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **URL** | The file to upload | 

### Return type

[**BrandingImageUrl**](BrandingImageUrl.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1EnrollmentCustomizationPost**
```swift
    open class func v1EnrollmentCustomizationPost(enrollmentCustomization: EnrollmentCustomization, completion: @escaping (_ data: GetEnrollmentCustomization?, _ error: Error?) -> Void)
```

Create an Enrollment Customization 

Create an enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let enrollmentCustomization = EnrollmentCustomization(siteId: 123, displayName: "displayName_example", description: "description_example", enrollmentCustomizationBrandingSettings: EnrollmentCustomizationBrandingSettings(textColor: "textColor_example", buttonColor: "buttonColor_example", buttonTextColor: "buttonTextColor_example", backgroundColor: "backgroundColor_example", iconUrl: "iconUrl_example")) // EnrollmentCustomization | Enrollment customization to create.

// Create an Enrollment Customization 
EnrollmentCustomizationAPI.v1EnrollmentCustomizationPost(enrollmentCustomization: enrollmentCustomization) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enrollmentCustomization** | [**EnrollmentCustomization**](EnrollmentCustomization.md) | Enrollment customization to create. | 

### Return type

[**GetEnrollmentCustomization**](GetEnrollmentCustomization.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsGet**
```swift
    open class func v2EnrollmentCustomizationsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: EnrollmentCustomizationSearchResultsV2?, _ error: Error?) -> Void)
```

Retrieve sorted and paged Enrollment Customizations 

Retrieves sorted and paged Enrollment Customizations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Retrieve sorted and paged Enrollment Customizations 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**EnrollmentCustomizationSearchResultsV2**](EnrollmentCustomizationSearchResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsIdDelete**
```swift
    open class func v2EnrollmentCustomizationsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an Enrollment Customization with the supplied id 

Deletes an Enrollment Customization with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Enrollment Customization identifier

// Delete an Enrollment Customization with the supplied id 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Enrollment Customization identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsIdGet**
```swift
    open class func v2EnrollmentCustomizationsIdGet(id: String, completion: @escaping (_ data: EnrollmentCustomizationV2?, _ error: Error?) -> Void)
```

Retrieve an Enrollment Customization with the supplied id 

Retrieves an Enrollment Customization with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Enrollment Customization identifier

// Retrieve an Enrollment Customization with the supplied id 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Enrollment Customization identifier | 

### Return type

[**EnrollmentCustomizationV2**](EnrollmentCustomizationV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsIdHistoryGet**
```swift
    open class func v2EnrollmentCustomizationsIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get sorted and paged Enrollment Customization history objects 

Gets sorted and paged enrollment customization history objects

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Enrollment Customization identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is duplicated for each sort criterion, e.g., ...&sort=name%2Casc&sort=date%2Cdesc (optional)

// Get sorted and paged Enrollment Customization history objects 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Enrollment Customization identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name%2Casc&amp;sort&#x3D;date%2Cdesc | [optional] 

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsIdHistoryPost**
```swift
    open class func v2EnrollmentCustomizationsIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Enrollment Customization history object notes 

Adds enrollment customization history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Enrollment Customization identifier
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Enrollment Customization history object notes 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Enrollment Customization identifier | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsIdPrestagesGet**
```swift
    open class func v2EnrollmentCustomizationsIdPrestagesGet(id: String, completion: @escaping (_ data: PrestageDependencies?, _ error: Error?) -> Void)
```

Retrieve the list of Prestages using this Enrollment Customization 

Retrieves the list of Prestages using this Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Enrollment Customization identifier

// Retrieve the list of Prestages using this Enrollment Customization 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsIdPrestagesGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Enrollment Customization identifier | 

### Return type

[**PrestageDependencies**](PrestageDependencies.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsIdPut**
```swift
    open class func v2EnrollmentCustomizationsIdPut(id: String, enrollmentCustomizationV2: EnrollmentCustomizationV2, completion: @escaping (_ data: EnrollmentCustomizationV2?, _ error: Error?) -> Void)
```

Update an Enrollment Customization 

Updates an Enrollment Customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Enrollment Customization identifier
let enrollmentCustomizationV2 = EnrollmentCustomizationV2(id: "id_example", siteId: "siteId_example", displayName: "displayName_example", description: "description_example", enrollmentCustomizationBrandingSettings: EnrollmentCustomizationBrandingSettings(textColor: "textColor_example", buttonColor: "buttonColor_example", buttonTextColor: "buttonTextColor_example", backgroundColor: "backgroundColor_example", iconUrl: "iconUrl_example")) // EnrollmentCustomizationV2 | Enrollment Customization to update

// Update an Enrollment Customization 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsIdPut(id: id, enrollmentCustomizationV2: enrollmentCustomizationV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Enrollment Customization identifier | 
 **enrollmentCustomizationV2** | [**EnrollmentCustomizationV2**](EnrollmentCustomizationV2.md) | Enrollment Customization to update | 

### Return type

[**EnrollmentCustomizationV2**](EnrollmentCustomizationV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsImagesIdGet**
```swift
    open class func v2EnrollmentCustomizationsImagesIdGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download an enrollment customization image 

Download an enrollment customization image

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of the enrollment customization image

// Download an enrollment customization image 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsImagesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of the enrollment customization image | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsImagesPost**
```swift
    open class func v2EnrollmentCustomizationsImagesPost(file: URL, completion: @escaping (_ data: BrandingImageUrl?, _ error: Error?) -> Void)
```

Upload an image

Uploads an image

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let file = URL(string: "https://example.com")! // URL | The file to upload

// Upload an image
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsImagesPost(file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **URL** | The file to upload | 

### Return type

[**BrandingImageUrl**](BrandingImageUrl.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentCustomizationsPost**
```swift
    open class func v2EnrollmentCustomizationsPost(enrollmentCustomizationV2: EnrollmentCustomizationV2, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create an Enrollment Customization 

Create an enrollment customization

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let enrollmentCustomizationV2 = EnrollmentCustomizationV2(id: "id_example", siteId: "siteId_example", displayName: "displayName_example", description: "description_example", enrollmentCustomizationBrandingSettings: EnrollmentCustomizationBrandingSettings(textColor: "textColor_example", buttonColor: "buttonColor_example", buttonTextColor: "buttonTextColor_example", backgroundColor: "backgroundColor_example", iconUrl: "iconUrl_example")) // EnrollmentCustomizationV2 | Enrollment customization to create.

// Create an Enrollment Customization 
EnrollmentCustomizationAPI.v2EnrollmentCustomizationsPost(enrollmentCustomizationV2: enrollmentCustomizationV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enrollmentCustomizationV2** | [**EnrollmentCustomizationV2**](EnrollmentCustomizationV2.md) | Enrollment customization to create. | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

