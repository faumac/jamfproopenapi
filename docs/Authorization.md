# Authorization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**AuthAccount**](AuthAccount.md) |  | [optional] 
**accountGroups** | [AccountGroup] |  | [optional] 
**sites** | [Site] |  | [optional] 
**authenticationType** | [**AuthenticationType**](AuthenticationType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


