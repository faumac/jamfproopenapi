# SupervisionIdentitiesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SupervisionIdentitiesGet**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiesget) | **GET** /v1/supervision-identities | Search for sorted and paged Supervision Identities 
[**v1SupervisionIdentitiesIdDelete**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiesiddelete) | **DELETE** /v1/supervision-identities/{id} | Delete a Supervision Identity with the supplied id 
[**v1SupervisionIdentitiesIdDownloadGet**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiesiddownloadget) | **GET** /v1/supervision-identities/{id}/download | Download the Supervision Identity .p12 file 
[**v1SupervisionIdentitiesIdGet**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiesidget) | **GET** /v1/supervision-identities/{id} | Retrieve a Supervision Identity with the supplied id 
[**v1SupervisionIdentitiesIdPut**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiesidput) | **PUT** /v1/supervision-identities/{id} | Update a Supervision Identity with the supplied information 
[**v1SupervisionIdentitiesPost**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiespost) | **POST** /v1/supervision-identities | Create a Supervision Identity for the supplied information 
[**v1SupervisionIdentitiesUploadPost**](SupervisionIdentitiesPreviewAPI.md#v1supervisionidentitiesuploadpost) | **POST** /v1/supervision-identities/upload | Upload the Supervision Identity .p12 file 


# **v1SupervisionIdentitiesGet**
```swift
    open class func v1SupervisionIdentitiesGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: SupervisionIdentitySearchResults?, _ error: Error?) -> Void)
```

Search for sorted and paged Supervision Identities 

Search for sorted and paged supervision identities

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "id:asc")

// Search for sorted and paged Supervision Identities 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;id:asc&quot;]

### Return type

[**SupervisionIdentitySearchResults**](SupervisionIdentitySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SupervisionIdentitiesIdDelete**
```swift
    open class func v1SupervisionIdentitiesIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Supervision Identity with the supplied id 

Deletes a Supervision Identity with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Supervision Identity identifier

// Delete a Supervision Identity with the supplied id 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Supervision Identity identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SupervisionIdentitiesIdDownloadGet**
```swift
    open class func v1SupervisionIdentitiesIdDownloadGet(id: Int, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download the Supervision Identity .p12 file 

Download the Supervision Identity .p12 file

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Supervision Identity identifier

// Download the Supervision Identity .p12 file 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesIdDownloadGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Supervision Identity identifier | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SupervisionIdentitiesIdGet**
```swift
    open class func v1SupervisionIdentitiesIdGet(id: Int, completion: @escaping (_ data: SupervisionIdentity?, _ error: Error?) -> Void)
```

Retrieve a Supervision Identity with the supplied id 

Retrieves a Supervision Identity with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Supervision Identity identifier

// Retrieve a Supervision Identity with the supplied id 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Supervision Identity identifier | 

### Return type

[**SupervisionIdentity**](SupervisionIdentity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SupervisionIdentitiesIdPut**
```swift
    open class func v1SupervisionIdentitiesIdPut(id: Int, supervisionIdentityUpdate: SupervisionIdentityUpdate, completion: @escaping (_ data: SupervisionIdentity?, _ error: Error?) -> Void)
```

Update a Supervision Identity with the supplied information 

Updates a Supervision Identity with the supplied information

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Supervision Identity identifier
let supervisionIdentityUpdate = SupervisionIdentityUpdate(displayName: "displayName_example") // SupervisionIdentityUpdate | 

// Update a Supervision Identity with the supplied information 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesIdPut(id: id, supervisionIdentityUpdate: supervisionIdentityUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Supervision Identity identifier | 
 **supervisionIdentityUpdate** | [**SupervisionIdentityUpdate**](SupervisionIdentityUpdate.md) |  | 

### Return type

[**SupervisionIdentity**](SupervisionIdentity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SupervisionIdentitiesPost**
```swift
    open class func v1SupervisionIdentitiesPost(supervisionIdentityCreate: SupervisionIdentityCreate, completion: @escaping (_ data: SupervisionIdentity?, _ error: Error?) -> Void)
```

Create a Supervision Identity for the supplied information 

Creates a Supervision Identity for the supplied information

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let supervisionIdentityCreate = SupervisionIdentityCreate(displayName: "displayName_example", password: "password_example") // SupervisionIdentityCreate | 

// Create a Supervision Identity for the supplied information 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesPost(supervisionIdentityCreate: supervisionIdentityCreate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supervisionIdentityCreate** | [**SupervisionIdentityCreate**](SupervisionIdentityCreate.md) |  | 

### Return type

[**SupervisionIdentity**](SupervisionIdentity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SupervisionIdentitiesUploadPost**
```swift
    open class func v1SupervisionIdentitiesUploadPost(supervisionIdentityCertificateUpload: SupervisionIdentityCertificateUpload, completion: @escaping (_ data: SupervisionIdentity?, _ error: Error?) -> Void)
```

Upload the Supervision Identity .p12 file 

Uploads the Supervision Identity .p12 file

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let supervisionIdentityCertificateUpload = SupervisionIdentityCertificateUpload(displayName: "displayName_example", password: "password_example", certificateData: 123) // SupervisionIdentityCertificateUpload | The base 64 encoded .p12 file alone with other needed information

// Upload the Supervision Identity .p12 file 
SupervisionIdentitiesPreviewAPI.v1SupervisionIdentitiesUploadPost(supervisionIdentityCertificateUpload: supervisionIdentityCertificateUpload) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supervisionIdentityCertificateUpload** | [**SupervisionIdentityCertificateUpload**](SupervisionIdentityCertificateUpload.md) | The base 64 encoded .p12 file alone with other needed information | 

### Return type

[**SupervisionIdentity**](SupervisionIdentity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

