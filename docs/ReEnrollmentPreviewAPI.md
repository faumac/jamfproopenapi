# ReEnrollmentPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ReenrollmentGet**](ReEnrollmentPreviewAPI.md#v1reenrollmentget) | **GET** /v1/reenrollment | Get Re-enrollment object 
[**v1ReenrollmentHistoryExportPost**](ReEnrollmentPreviewAPI.md#v1reenrollmenthistoryexportpost) | **POST** /v1/reenrollment/history/export | Export reenrollment history collection 
[**v1ReenrollmentHistoryGet**](ReEnrollmentPreviewAPI.md#v1reenrollmenthistoryget) | **GET** /v1/reenrollment/history | Get Re-enrollment history object 
[**v1ReenrollmentHistoryPost**](ReEnrollmentPreviewAPI.md#v1reenrollmenthistorypost) | **POST** /v1/reenrollment/history | Add specified Re-enrollment history object notes 
[**v1ReenrollmentPut**](ReEnrollmentPreviewAPI.md#v1reenrollmentput) | **PUT** /v1/reenrollment | Update the Re-enrollment object 


# **v1ReenrollmentGet**
```swift
    open class func v1ReenrollmentGet(completion: @escaping (_ data: Reenrollment?, _ error: Error?) -> Void)
```

Get Re-enrollment object 

Gets Re-enrollment object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Re-enrollment object 
ReEnrollmentPreviewAPI.v1ReenrollmentGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Reenrollment**](Reenrollment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ReenrollmentHistoryExportPost**
```swift
    open class func v1ReenrollmentHistoryExportPost(exportFields: [String]? = nil, exportLabels: [String]? = nil, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, exportParameters: ExportParameters? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Export reenrollment history collection 

Export reenrollment history collection 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let exportFields = ["inner_example"] // [String] | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields=id,username (optional)
let exportLabels = ["inner_example"] // [String] | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels=identifier,name with matching: export-fields=id,username (optional)
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=id:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name. This param can be combined with paging and sorting. Example: name==\"*script*\" (optional) (default to "")
let exportParameters = ExportParameters(page: 123, pageSize: 123, sort: ["sort_example"], filter: "filter_example", fields: [ExportField(fieldName: "fieldName_example", fieldLabelOverride: "fieldLabelOverride_example")]) // ExportParameters | Optional. Override query parameters since they can make URI exceed 2,000 character limit. (optional)

// Export reenrollment history collection 
ReEnrollmentPreviewAPI.v1ReenrollmentHistoryExportPost(exportFields: exportFields, exportLabels: exportLabels, page: page, pageSize: pageSize, sort: sort, filter: filter, exportParameters: exportParameters) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exportFields** | [**[String]**](String.md) | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields&#x3D;id,username | [optional] 
 **exportLabels** | [**[String]**](String.md) | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels&#x3D;identifier,name with matching: export-fields&#x3D;id,username | [optional] 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;id:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name. This param can be combined with paging and sorting. Example: name&#x3D;&#x3D;\&quot;*script*\&quot; | [optional] [default to &quot;&quot;]
 **exportParameters** | [**ExportParameters**](ExportParameters.md) | Optional. Override query parameters since they can make URI exceed 2,000 character limit. | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/csv,application/json, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ReenrollmentHistoryGet**
```swift
    open class func v1ReenrollmentHistoryGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Re-enrollment history object 

Gets Re-enrollment history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "date:desc")

// Get Re-enrollment history object 
ReEnrollmentPreviewAPI.v1ReenrollmentHistoryGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;date:desc&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ReenrollmentHistoryPost**
```swift
    open class func v1ReenrollmentHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add specified Re-enrollment history object notes 

Adds specified Re-enrollment history object notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add specified Re-enrollment history object notes 
ReEnrollmentPreviewAPI.v1ReenrollmentHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ReenrollmentPut**
```swift
    open class func v1ReenrollmentPut(reenrollment: Reenrollment, completion: @escaping (_ data: Reenrollment?, _ error: Error?) -> Void)
```

Update the Re-enrollment object 

Update the Re-enrollment object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let reenrollment = Reenrollment(isFlushPolicyHistoryEnabled: false, isFlushLocationInformationEnabled: false, isFlushLocationInformationHistoryEnabled: false, isFlushExtensionAttributesEnabled: false, flushMDMQueue: "flushMDMQueue_example") // Reenrollment | Re-enrollment object to update

// Update the Re-enrollment object 
ReEnrollmentPreviewAPI.v1ReenrollmentPut(reenrollment: reenrollment) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reenrollment** | [**Reenrollment**](Reenrollment.md) | Re-enrollment object to update | 

### Return type

[**Reenrollment**](Reenrollment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

