# InventoryPreloadCsvError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**field** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**line** | **Int** |  | [optional] 
**fieldSize** | **Int** |  | [optional] 
**deviceType** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


