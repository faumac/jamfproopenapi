# SessionCandidateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **String** | Device identifier | 
**deviceType** | **String** | Device type | 
**description** | **String** | Session description. To be used for additional context on the reason of the session | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


