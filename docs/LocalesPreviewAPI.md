# LocalesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1LocalesGet**](LocalesPreviewAPI.md#v1localesget) | **GET** /v1/locales | Return locales that can be used in other features 


# **v1LocalesGet**
```swift
    open class func v1LocalesGet(completion: @escaping (_ data: [Locale]?, _ error: Error?) -> Void)
```

Return locales that can be used in other features 

Returns locales that can be used in other features. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return locales that can be used in other features 
LocalesPreviewAPI.v1LocalesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Locale]**](Locale.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

