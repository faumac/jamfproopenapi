# JamfProNotificationsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**notificationsAlertsGet**](JamfProNotificationsPreviewAPI.md#notificationsalertsget) | **GET** /notifications/alerts | Get Notifications for user and site 
[**notificationsAlertsIdDelete**](JamfProNotificationsPreviewAPI.md#notificationsalertsiddelete) | **DELETE** /notifications/alerts/{id} | DEPRECATED - USE \&quot;alerts/{type}/{id}\&quot; INSTEAD. Deletes only Patch Management notifications. 
[**notificationsAlertsTypeIdDelete**](JamfProNotificationsPreviewAPI.md#notificationsalertstypeiddelete) | **DELETE** /notifications/alerts/{type}/{id} | Delete Notifications 


# **notificationsAlertsGet**
```swift
    open class func notificationsAlertsGet(completion: @escaping (_ data: [Notification]?, _ error: Error?) -> Void)
```

Get Notifications for user and site 

Gets notifications for user and site 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Notifications for user and site 
JamfProNotificationsPreviewAPI.notificationsAlertsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Notification]**](Notification.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notificationsAlertsIdDelete**
```swift
    open class func notificationsAlertsIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

DEPRECATED - USE \"alerts/{type}/{id}\" INSTEAD. Deletes only Patch Management notifications. 

DEPRECATED - USE \"alerts/{type}/{id}\" INSTEAD. Deletes only Patch Management notifications. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance ID of the notification

// DEPRECATED - USE \"alerts/{type}/{id}\" INSTEAD. Deletes only Patch Management notifications. 
JamfProNotificationsPreviewAPI.notificationsAlertsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance ID of the notification | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notificationsAlertsTypeIdDelete**
```swift
    open class func notificationsAlertsTypeIdDelete(id: Int, type: NotificationType, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Notifications 

Deletes notifications. Option for 'type' is 'PATCH_UPDATE'. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance ID of the notification
let type = NotificationType() // NotificationType | type of the notification

// Delete Notifications 
JamfProNotificationsPreviewAPI.notificationsAlertsTypeIdDelete(id: id, type: type) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance ID of the notification | 
 **type** | [**NotificationType**](.md) | type of the notification | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

