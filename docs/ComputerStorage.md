# ComputerStorage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bootDriveAvailableSpaceMegabytes** | **Int64** |  | [optional] [readonly] 
**disks** | [ComputerDisk] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


