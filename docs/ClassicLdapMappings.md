# ClassicLdapMappings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userObjectMapIdTo** | **String** |  | 
**userObjectMapUsernameTo** | **String** |  | 
**userObjectMapRealNameTo** | **String** |  | 
**userObjectMapEmailTo** | **String** |  | 
**userObjectMapDepartmentTo** | **String** |  | 
**userObjectMapBuildingTo** | **String** |  | [default to ""]
**userObjectMapRoomTo** | **String** |  | [default to ""]
**userObjectMapPhoneTo** | **String** |  | [default to ""]
**userObjectMapPositionTo** | **String** |  | 
**userObjectMapUuidTo** | **String** |  | 
**userGroupObjectMapIdTo** | **String** |  | 
**userGroupObjectMapGroupNameTo** | **String** |  | 
**userGroupObjectMapUuidTo** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


