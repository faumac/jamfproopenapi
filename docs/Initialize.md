# Initialize

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activationCode** | **String** |  | 
**institutionName** | **String** |  | 
**isEulaAccepted** | **Bool** |  | 
**username** | **String** |  | 
**password** | **String** |  | 
**email** | **String** |  | [optional] 
**jssUrl** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


