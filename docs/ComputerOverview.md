# ComputerOverview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**location** | [**ComputerLocation**](ComputerLocation.md) |  | [optional] 
**name** | **String** |  | [optional] 
**udid** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**lastContactDate** | **String** |  | [optional] 
**lastReportDate** | **String** |  | [optional] 
**lastEnrolledDate** | **String** |  | [optional] 
**operatingSystemVersion** | **String** |  | [optional] 
**operatingSystemBuild** | **String** |  | [optional] 
**operatingSystemSupplementalBuildVersion** | **String** | Collected for macOS 13.0 or later | [optional] 
**operatingSystemRapidSecurityResponse** | **String** | Collected for macOS 13.0 or later | [optional] 
**ipAddress** | **String** |  | [optional] 
**macAddress** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**mdmAccessRights** | **Int** |  | [optional] 
**isManaged** | **Bool** |  | [optional] 
**managementId** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


