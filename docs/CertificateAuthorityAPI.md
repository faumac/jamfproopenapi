# CertificateAuthorityAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1PkiCertificateAuthorityActiveDerGet**](CertificateAuthorityAPI.md#v1pkicertificateauthorityactivederget) | **GET** /v1/pki/certificate-authority/active/der | Returns X.509 of active Certificate Authority (CA) in DER format
[**v1PkiCertificateAuthorityActiveGet**](CertificateAuthorityAPI.md#v1pkicertificateauthorityactiveget) | **GET** /v1/pki/certificate-authority/active | Returns X.509 details of the active Certificate Authority (CA)
[**v1PkiCertificateAuthorityActivePemGet**](CertificateAuthorityAPI.md#v1pkicertificateauthorityactivepemget) | **GET** /v1/pki/certificate-authority/active/pem | Returns active Certificate Authority (CA) in PEM format
[**v1PkiCertificateAuthorityIdDerGet**](CertificateAuthorityAPI.md#v1pkicertificateauthorityidderget) | **GET** /v1/pki/certificate-authority/{id}/der | Returns X.509 current Certificate Authority (CA) with provided ID in DER format
[**v1PkiCertificateAuthorityIdGet**](CertificateAuthorityAPI.md#v1pkicertificateauthorityidget) | **GET** /v1/pki/certificate-authority/{id} | Returns X.509 details of Certificate Authority (CA) with provided ID
[**v1PkiCertificateAuthorityIdPemGet**](CertificateAuthorityAPI.md#v1pkicertificateauthorityidpemget) | **GET** /v1/pki/certificate-authority/{id}/pem | Returns current Certificate Authority (CA) with provided ID in PEM format


# **v1PkiCertificateAuthorityActiveDerGet**
```swift
    open class func v1PkiCertificateAuthorityActiveDerGet(completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Returns X.509 of active Certificate Authority (CA) in DER format

Returns X.509 of active Certificate Authority (CA) in DER format

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Returns X.509 of active Certificate Authority (CA) in DER format
CertificateAuthorityAPI.v1PkiCertificateAuthorityActiveDerGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pkix-cert

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiCertificateAuthorityActiveGet**
```swift
    open class func v1PkiCertificateAuthorityActiveGet(completion: @escaping (_ data: CertificateRecord?, _ error: Error?) -> Void)
```

Returns X.509 details of the active Certificate Authority (CA)

Returns X.509 details of the active Certificate Authority (CA)

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Returns X.509 details of the active Certificate Authority (CA)
CertificateAuthorityAPI.v1PkiCertificateAuthorityActiveGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CertificateRecord**](CertificateRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiCertificateAuthorityActivePemGet**
```swift
    open class func v1PkiCertificateAuthorityActivePemGet(completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Returns active Certificate Authority (CA) in PEM format

Returns active Certificate Authority (CA) in PEM format

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Returns active Certificate Authority (CA) in PEM format
CertificateAuthorityAPI.v1PkiCertificateAuthorityActivePemGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pem-certificate-chain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiCertificateAuthorityIdDerGet**
```swift
    open class func v1PkiCertificateAuthorityIdDerGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Returns X.509 current Certificate Authority (CA) with provided ID in DER format

Returns X.509 current Certificate Authority (CA) with provided ID in DER format

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | UUID of the Certificate Authority (CA)

// Returns X.509 current Certificate Authority (CA) with provided ID in DER format
CertificateAuthorityAPI.v1PkiCertificateAuthorityIdDerGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | UUID of the Certificate Authority (CA) | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pkix-cert, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiCertificateAuthorityIdGet**
```swift
    open class func v1PkiCertificateAuthorityIdGet(id: String, completion: @escaping (_ data: CertificateRecord?, _ error: Error?) -> Void)
```

Returns X.509 details of Certificate Authority (CA) with provided ID

Returns X.509 details of Certificate Authority (CA) with provided ID

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | UUID of the Certificate Authority (CA)

// Returns X.509 details of Certificate Authority (CA) with provided ID
CertificateAuthorityAPI.v1PkiCertificateAuthorityIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | UUID of the Certificate Authority (CA) | 

### Return type

[**CertificateRecord**](CertificateRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1PkiCertificateAuthorityIdPemGet**
```swift
    open class func v1PkiCertificateAuthorityIdPemGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Returns current Certificate Authority (CA) with provided ID in PEM format

Returns current Certificate Authority (CA) with provided ID in PEM format

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | UUID of the Certificate Authority (CA)

// Returns current Certificate Authority (CA) with provided ID in PEM format
CertificateAuthorityAPI.v1PkiCertificateAuthorityIdPemGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | UUID of the Certificate Authority (CA) | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pem-certificate-chain, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

