# LinkedConnectProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** |  | [optional] [readonly] 
**profileId** | **String** |  | [optional] [readonly] 
**profileName** | **String** |  | [optional] [readonly] 
**profileScopeDescription** | **String** |  | [optional] [readonly] 
**version** | **String** | Must be a valid Jamf Connect version 2.3.0 or higher. Versions are listed here &#x60;https://www.jamf.com/resources/product-documentation/jamf-connect-administrators-guide/&#x60; | [optional] 
**autoDeploymentType** | **String** | Determines how the server will behave regarding application updates and installs on the devices that have the configuration profile installed. * &#x60;PATCH_UPDATES&#x60; - Server handles initial installation of the application and any patch updates. * &#x60;MINOR_AND_PATCH_UPDATES&#x60; - Server handles initial installation of the application and any patch and minor updates. * &#x60;INITIAL_INSTALLATION_ONLY&#x60; - Server only handles initial installation of the application. Updates will have to be done manually. * &#x60;NONE&#x60; - Server does not handle any installations or updates for the application. Version is ignored for this type.  | [optional] [default to ._none]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


