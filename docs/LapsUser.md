# LapsUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientManagementId** | **String** |  | [optional] 
**guid** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**userSource** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


