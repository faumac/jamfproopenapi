# LdapConfigurationUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cloudIdPCommon** | [**CloudIdPCommon**](CloudIdPCommon.md) |  | 
**server** | [**CloudLdapServerUpdate**](CloudLdapServerUpdate.md) |  | 
**mappings** | [**CloudLdapMappingsRequest**](CloudLdapMappingsRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


