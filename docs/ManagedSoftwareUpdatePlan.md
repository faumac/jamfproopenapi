# ManagedSoftwareUpdatePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planUuid** | **String** |  | [readonly] 
**device** | [**PlanDevice**](PlanDevice.md) |  | 
**updateAction** | **String** |  | [readonly] 
**versionType** | **String** |  | [readonly] 
**specificVersion** | **String** | Optional. Indicates the specific version to update to. Only available when the version type is set to specific version, otherwise defaults to NO_SPECIFIC_VERSION. | [optional] [readonly] [default to "NO_SPECIFIC_VERSION"]
**maxDeferrals** | **Int** | Not applicable to all managed software update plans | [readonly] 
**forceInstallLocalDateTime** | **String** | Optional. Indicates the local date and time of the device to force update by. | [optional] 
**status** | [**PlanStatus**](PlanStatus.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


