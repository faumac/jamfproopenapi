# DeviceCommunicationSettingsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1DeviceCommunicationSettingsGet**](DeviceCommunicationSettingsAPI.md#v1devicecommunicationsettingsget) | **GET** /v1/device-communication-settings | Retrieves all settings for device communication 
[**v1DeviceCommunicationSettingsHistoryGet**](DeviceCommunicationSettingsAPI.md#v1devicecommunicationsettingshistoryget) | **GET** /v1/device-communication-settings/history | Get Device Communication settings history 
[**v1DeviceCommunicationSettingsHistoryPost**](DeviceCommunicationSettingsAPI.md#v1devicecommunicationsettingshistorypost) | **POST** /v1/device-communication-settings/history | Add Device Communication Settings history notes 
[**v1DeviceCommunicationSettingsPut**](DeviceCommunicationSettingsAPI.md#v1devicecommunicationsettingsput) | **PUT** /v1/device-communication-settings | Update device communication settings 


# **v1DeviceCommunicationSettingsGet**
```swift
    open class func v1DeviceCommunicationSettingsGet(completion: @escaping (_ data: DeviceCommunicationSettings?, _ error: Error?) -> Void)
```

Retrieves all settings for device communication 

Retrieves all device communication settings, including automatic renewal of the MDM profile. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieves all settings for device communication 
DeviceCommunicationSettingsAPI.v1DeviceCommunicationSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DeviceCommunicationSettings**](DeviceCommunicationSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceCommunicationSettingsHistoryGet**
```swift
    open class func v1DeviceCommunicationSettingsHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Device Communication settings history 

Gets Device Communication settings history 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Device Communication settings history 
DeviceCommunicationSettingsAPI.v1DeviceCommunicationSettingsHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceCommunicationSettingsHistoryPost**
```swift
    open class func v1DeviceCommunicationSettingsHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Device Communication Settings history notes 

Adds Device Communication Settings history notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Device Communication Settings history notes 
DeviceCommunicationSettingsAPI.v1DeviceCommunicationSettingsHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceCommunicationSettingsPut**
```swift
    open class func v1DeviceCommunicationSettingsPut(deviceCommunicationSettings: DeviceCommunicationSettings, completion: @escaping (_ data: DeviceCommunicationSettings?, _ error: Error?) -> Void)
```

Update device communication settings 

Update device communication settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let deviceCommunicationSettings = DeviceCommunicationSettings(autoRenewMobileDeviceMdmProfileWhenCaRenewed: true, autoRenewMobileDeviceMdmProfileWhenDeviceIdentityCertExpiring: true, autoRenewComputerMdmProfileWhenCaRenewed: true, autoRenewComputerMdmProfileWhenDeviceIdentityCertExpiring: true, mdmProfileMobileDeviceExpirationLimitInDays: 123, mdmProfileComputerExpirationLimitInDays: 123) // DeviceCommunicationSettings | 

// Update device communication settings 
DeviceCommunicationSettingsAPI.v1DeviceCommunicationSettingsPut(deviceCommunicationSettings: deviceCommunicationSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceCommunicationSettings** | [**DeviceCommunicationSettings**](DeviceCommunicationSettings.md) |  | 

### Return type

[**DeviceCommunicationSettings**](DeviceCommunicationSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

