# MobileDevicePrestageNames

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignNamesUsing** | **String** |  | [optional] 
**prestageDeviceNames** | [MobileDevicePrestageName] |  | [optional] 
**deviceNamePrefix** | **String** |  | [optional] 
**deviceNameSuffix** | **String** |  | [optional] 
**singleDeviceName** | **String** |  | [optional] 
**isManageNames** | **Bool** |  | [optional] 
**isDeviceNamingConfigured** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


