# ConnectionConfigurationCandidateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **String** | An identifier of a site which Team Viewer Remote Administration will be configured on | 
**displayName** | **String** | Name for Team Viewer Connection Configuration | 
**scriptToken** | **String** | Token which is used for connecting to Team Viewer | 
**enabled** | **Bool** | Defines the intent to enable or disable Team Viewer connection | 
**sessionTimeout** | **Int** | Number of minutes before the session expires | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


