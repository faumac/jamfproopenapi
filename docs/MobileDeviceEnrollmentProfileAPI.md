# MobileDeviceEnrollmentProfileAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MobileDeviceEnrollmentProfileIdDownloadProfileGet**](MobileDeviceEnrollmentProfileAPI.md#v1mobiledeviceenrollmentprofileiddownloadprofileget) | **GET** /v1/mobile-device-enrollment-profile/{id}/download-profile | Retrieve the MDM Enrollment Profile 


# **v1MobileDeviceEnrollmentProfileIdDownloadProfileGet**
```swift
    open class func v1MobileDeviceEnrollmentProfileIdDownloadProfileGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Retrieve the MDM Enrollment Profile 

Retrieve the MDM Enrollment Profile

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | MDM Enrollment Profile identifier

// Retrieve the MDM Enrollment Profile 
MobileDeviceEnrollmentProfileAPI.v1MobileDeviceEnrollmentProfileIdDownloadProfileGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | MDM Enrollment Profile identifier | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-apple-aspen-config

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

