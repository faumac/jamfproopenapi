# ComputerApplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**path** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**macAppStore** | **Bool** |  | [optional] 
**sizeMegabytes** | **Int** |  | [optional] 
**bundleId** | **String** |  | [optional] 
**updateAvailable** | **Bool** |  | [optional] 
**externalVersionId** | **String** | The app&#39;s external version ID. It can be used in the iTunes Search API to decide if the app needs to be updated | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


