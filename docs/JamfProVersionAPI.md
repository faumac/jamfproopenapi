# JamfProVersionAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfProVersionGet**](JamfProVersionAPI.md#v1jamfproversionget) | **GET** /v1/jamf-pro-version | Return information about the Jamf Pro including the current version 


# **v1JamfProVersionGet**
```swift
    open class func v1JamfProVersionGet(completion: @escaping (_ data: JamfProVersion?, _ error: Error?) -> Void)
```

Return information about the Jamf Pro including the current version 

Returns information about the Jamf Pro including the current version. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return information about the Jamf Pro including the current version 
JamfProVersionAPI.v1JamfProVersionGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**JamfProVersion**](JamfProVersion.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

