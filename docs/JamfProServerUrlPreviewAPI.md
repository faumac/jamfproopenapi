# JamfProServerUrlPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfProServerUrlGet**](JamfProServerUrlPreviewAPI.md#v1jamfproserverurlget) | **GET** /v1/jamf-pro-server-url | Get Jamf Pro Server URL settings 
[**v1JamfProServerUrlHistoryGet**](JamfProServerUrlPreviewAPI.md#v1jamfproserverurlhistoryget) | **GET** /v1/jamf-pro-server-url/history | Get Jamf Pro Server URL settings history 
[**v1JamfProServerUrlHistoryPost**](JamfProServerUrlPreviewAPI.md#v1jamfproserverurlhistorypost) | **POST** /v1/jamf-pro-server-url/history | Add Jamf Pro Server URL settings history notes 
[**v1JamfProServerUrlPut**](JamfProServerUrlPreviewAPI.md#v1jamfproserverurlput) | **PUT** /v1/jamf-pro-server-url | Update Jamf Pro Server URL settings 


# **v1JamfProServerUrlGet**
```swift
    open class func v1JamfProServerUrlGet(completion: @escaping (_ data: JamfProServerUrl?, _ error: Error?) -> Void)
```

Get Jamf Pro Server URL settings 

Get Jamf Pro Server URL settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Jamf Pro Server URL settings 
JamfProServerUrlPreviewAPI.v1JamfProServerUrlGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**JamfProServerUrl**](JamfProServerUrl.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProServerUrlHistoryGet**
```swift
    open class func v1JamfProServerUrlHistoryGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Jamf Pro Server URL settings history 

Gets Jamf Pro Server URL settings history

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "date:desc")

// Get Jamf Pro Server URL settings history 
JamfProServerUrlPreviewAPI.v1JamfProServerUrlHistoryGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;date:desc&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProServerUrlHistoryPost**
```swift
    open class func v1JamfProServerUrlHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Jamf Pro Server URL settings history notes 

Adds Jamf Pro Server URL settings history notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Jamf Pro Server URL settings history notes 
JamfProServerUrlPreviewAPI.v1JamfProServerUrlHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfProServerUrlPut**
```swift
    open class func v1JamfProServerUrlPut(jamfProServerUrl: JamfProServerUrl, completion: @escaping (_ data: JamfProServerUrl?, _ error: Error?) -> Void)
```

Update Jamf Pro Server URL settings 

Update Jamf Pro Server URL settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let jamfProServerUrl = JamfProServerUrl(url: "url_example", unsecuredEnrollmentUrl: "unsecuredEnrollmentUrl_example") // JamfProServerUrl | Jamf Pro Server URL settings object

// Update Jamf Pro Server URL settings 
JamfProServerUrlPreviewAPI.v1JamfProServerUrlPut(jamfProServerUrl: jamfProServerUrl) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jamfProServerUrl** | [**JamfProServerUrl**](JamfProServerUrl.md) | Jamf Pro Server URL settings object | 

### Return type

[**JamfProServerUrl**](JamfProServerUrl.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

