# AccountPreferences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** |  | [optional] 
**dateFormat** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**timezone** | **String** |  | [optional] 
**isDisableRelativeDates** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


