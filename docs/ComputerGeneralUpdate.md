# ComputerGeneralUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**lastIpAddress** | **String** |  | [optional] 
**barcode1** | **String** |  | [optional] 
**barcode2** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


