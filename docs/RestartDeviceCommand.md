# RestartDeviceCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rebuildKernelCache** | **Bool** |  | [optional] 
**kextPaths** | **[String]** | Only used if RebuildKernelCache is true | [optional] 
**notifyUser** | **Bool** |  | [optional] 
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


