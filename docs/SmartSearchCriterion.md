# SmartSearchCriterion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**priority** | **Int** |  | [optional] 
**andOr** | **String** |  | [optional] 
**searchType** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**openingParen** | **Bool** |  | [optional] 
**closingParen** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


