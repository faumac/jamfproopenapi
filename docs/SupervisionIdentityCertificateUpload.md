# SupervisionIdentityCertificateUpload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**password** | **String** |  | 
**certificateData** | **Data** | The base 64 encoded supervision identity certificate data | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


