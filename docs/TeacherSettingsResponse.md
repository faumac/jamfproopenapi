# TeacherSettingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isEnabled** | **Bool** |  | [optional] 
**timezoneId** | **String** |  | [optional] 
**autoClear** | **String** |  | [optional] 
**maxRestrictionLengthSeconds** | **Int** |  | [optional] 
**displayNameType** | **String** |  | [optional] 
**features** | [**TeacherFeatures**](TeacherFeatures.md) |  | [optional] 
**safelistedApps** | [SafelistedApp] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


