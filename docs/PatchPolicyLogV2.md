# PatchPolicyLogV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**patchPolicyId** | **String** |  | [optional] 
**deviceName** | **String** |  | [optional] 
**deviceId** | **String** |  | [optional] 
**statusCode** | **Int** |  | [optional] 
**statusDate** | **Date** |  | [optional] 
**statusEnum** | **String** |  | [optional] 
**attemptNumber** | **Int** |  | [optional] 
**ignoredForPatchPolicyId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


