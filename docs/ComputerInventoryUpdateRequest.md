# ComputerInventoryUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**udid** | **String** |  | [optional] 
**general** | [**ComputerGeneralUpdate**](ComputerGeneralUpdate.md) |  | [optional] 
**purchasing** | [**ComputerPurchase**](ComputerPurchase.md) |  | [optional] 
**userAndLocation** | [**ComputerUserAndLocation**](ComputerUserAndLocation.md) |  | [optional] 
**hardware** | [**ComputerHardwareUpdate**](ComputerHardwareUpdate.md) |  | [optional] 
**operatingSystem** | [**ComputerOperatingSystemUpdate**](ComputerOperatingSystemUpdate.md) |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


