# StaticGroupSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 
**groupName** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**count** | **Int** | membership count | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


