# EbookLimitations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**networkSegments** | **[String]** |  | [optional] 
**users** | [EbookLimitationsUsersInner] |  | [optional] 
**userGroups** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


