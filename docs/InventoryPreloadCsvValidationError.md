# InventoryPreloadCsvValidationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**httpStatus** | **Int** | HTTP status of the response | [optional] 
**errors** | [InventoryPreloadCsvValidationErrorCause] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


