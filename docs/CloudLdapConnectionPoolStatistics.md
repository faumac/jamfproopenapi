# CloudLdapConnectionPoolStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numConnectionsClosedDefunct** | **Int64** |  | [optional] 
**numConnectionsClosedExpired** | **Int64** |  | [optional] 
**numConnectionsClosedUnneeded** | **Int64** |  | [optional] 
**numFailedCheckouts** | **Int64** |  | [optional] 
**numFailedConnectionAttempts** | **Int64** |  | [optional] 
**numReleasedValid** | **Int64** |  | [optional] 
**numSuccessfulCheckouts** | **Int64** |  | [optional] 
**numSuccessfulCheckoutsNewConnection** | **Int64** |  | [optional] 
**numSuccessfulConnectionAttempts** | **Int64** |  | [optional] 
**maximumAvailableConnections** | **Int64** |  | [optional] 
**numSuccessfulCheckoutsWithoutWaiting** | **Int64** |  | [optional] 
**numSuccessfulCheckoutsAfterWaiting** | **Int64** |  | [optional] 
**numAvailableConnections** | **Int64** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


