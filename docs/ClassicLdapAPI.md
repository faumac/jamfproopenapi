# ClassicLdapAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ClassicLdapIdGet**](ClassicLdapAPI.md#v1classicldapidget) | **GET** /v1/classic-ldap/{id} | Get mappings for OnPrem Ldap configuration with given id.


# **v1ClassicLdapIdGet**
```swift
    open class func v1ClassicLdapIdGet(id: String, completion: @escaping (_ data: ClassicLdapMappings?, _ error: Error?) -> Void)
```

Get mappings for OnPrem Ldap configuration with given id.

Get mappings for OnPrem Ldap configuration with given id.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | OnPrem Ldap identifier

// Get mappings for OnPrem Ldap configuration with given id.
ClassicLdapAPI.v1ClassicLdapIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | OnPrem Ldap identifier | 

### Return type

[**ClassicLdapMappings**](ClassicLdapMappings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

