# EnrollmentAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1AdueSessionTokenSettingsGet**](EnrollmentAPI.md#v1aduesessiontokensettingsget) | **GET** /v1/adue-session-token-settings | Retrieve the Account Driven User Enrollment Session Token Settings 
[**v1AdueSessionTokenSettingsPut**](EnrollmentAPI.md#v1aduesessiontokensettingsput) | **PUT** /v1/adue-session-token-settings | Update Account Driven User Enrollment Session Token Settings. 
[**v2EnrollmentAccessGroupsGet**](EnrollmentAPI.md#v2enrollmentaccessgroupsget) | **GET** /v2/enrollment/access-groups | Retrieve the configured LDAP groups configured for User-Initiated Enrollment 
[**v2EnrollmentAccessGroupsPost**](EnrollmentAPI.md#v2enrollmentaccessgroupspost) | **POST** /v2/enrollment/access-groups | Add the configured LDAP group for User-Initiated Enrollment. 
[**v2EnrollmentAccessGroupsServerIdGroupIdDelete**](EnrollmentAPI.md#v2enrollmentaccessgroupsserveridgroupiddelete) | **DELETE** /v2/enrollment/access-groups/{serverId}/{groupId} | Delete an LDAP group&#39;s access to user initiated Enrollment 
[**v2EnrollmentAccessGroupsServerIdGroupIdGet**](EnrollmentAPI.md#v2enrollmentaccessgroupsserveridgroupidget) | **GET** /v2/enrollment/access-groups/{serverId}/{groupId} | Retrieve the configured LDAP groups configured for User-Initiated Enrollment 
[**v2EnrollmentAccessGroupsServerIdGroupIdPut**](EnrollmentAPI.md#v2enrollmentaccessgroupsserveridgroupidput) | **PUT** /v2/enrollment/access-groups/{serverId}/{groupId} | Modify the configured LDAP groups configured for User-Initiated Enrollment 
[**v2EnrollmentFilteredLanguageCodesGet**](EnrollmentAPI.md#v2enrollmentfilteredlanguagecodesget) | **GET** /v2/enrollment/filtered-language-codes | Retrieve the list of languages and corresponding ISO 639-1 Codes but only those not already added to Enrollment 
[**v2EnrollmentGet**](EnrollmentAPI.md#v2enrollmentget) | **GET** /v2/enrollment | Get Enrollment object and Re-enrollment settings 
[**v2EnrollmentHistoryExportPost**](EnrollmentAPI.md#v2enrollmenthistoryexportpost) | **POST** /v2/enrollment/history/export | Export enrollment history collection 
[**v2EnrollmentHistoryGet**](EnrollmentAPI.md#v2enrollmenthistoryget) | **GET** /v2/enrollment/history | Get sorted and paged Enrollment history object 
[**v2EnrollmentHistoryPost**](EnrollmentAPI.md#v2enrollmenthistorypost) | **POST** /v2/enrollment/history | Add Enrollment history object notes 
[**v2EnrollmentLanguageCodesGet**](EnrollmentAPI.md#v2enrollmentlanguagecodesget) | **GET** /v2/enrollment/language-codes | Retrieve the list of languages and corresponding ISO 639-1 Codes 
[**v2EnrollmentLanguagesDeleteMultiplePost**](EnrollmentAPI.md#v2enrollmentlanguagesdeletemultiplepost) | **POST** /v2/enrollment/languages/delete-multiple | Delete multiple configured languages from User-Initiated Enrollment settings 
[**v2EnrollmentLanguagesGet**](EnrollmentAPI.md#v2enrollmentlanguagesget) | **GET** /v2/enrollment/languages | Get an array of the language codes that have Enrollment messaging 
[**v2EnrollmentLanguagesLanguageIdDelete**](EnrollmentAPI.md#v2enrollmentlanguageslanguageiddelete) | **DELETE** /v2/enrollment/languages/{languageId} | Delete the Enrollment messaging for a language 
[**v2EnrollmentLanguagesLanguageIdGet**](EnrollmentAPI.md#v2enrollmentlanguageslanguageidget) | **GET** /v2/enrollment/languages/{languageId} | Retrieve the Enrollment messaging for a language 
[**v2EnrollmentLanguagesLanguageIdPut**](EnrollmentAPI.md#v2enrollmentlanguageslanguageidput) | **PUT** /v2/enrollment/languages/{languageId} | Edit Enrollment messaging for a language 
[**v2EnrollmentPut**](EnrollmentAPI.md#v2enrollmentput) | **PUT** /v2/enrollment | Update Enrollment object 
[**v3EnrollmentAccessGroupsGet**](EnrollmentAPI.md#v3enrollmentaccessgroupsget) | **GET** /v3/enrollment/access-groups | Retrieve the configured LDAP groups configured for User-Initiated Enrollment. 
[**v3EnrollmentAccessGroupsIdDelete**](EnrollmentAPI.md#v3enrollmentaccessgroupsiddelete) | **DELETE** /v3/enrollment/access-groups/{id} | Delete an LDAP group&#39;s access to user initiated Enrollment. 
[**v3EnrollmentAccessGroupsIdGet**](EnrollmentAPI.md#v3enrollmentaccessgroupsidget) | **GET** /v3/enrollment/access-groups/{id} | Retrieve the configured LDAP groups configured for User-Initiated Enrollment 
[**v3EnrollmentAccessGroupsIdPut**](EnrollmentAPI.md#v3enrollmentaccessgroupsidput) | **PUT** /v3/enrollment/access-groups/{id} | Modify the configured LDAP groups configured for User-Initiated Enrollment. Only exiting Access Groups can be updated. 
[**v3EnrollmentAccessGroupsPost**](EnrollmentAPI.md#v3enrollmentaccessgroupspost) | **POST** /v3/enrollment/access-groups | Add the configured LDAP group for User-Initiated Enrollment. 
[**v3EnrollmentFilteredLanguageCodesGet**](EnrollmentAPI.md#v3enrollmentfilteredlanguagecodesget) | **GET** /v3/enrollment/filtered-language-codes | Retrieve the list of languages and corresponding ISO 639-1 Codes but only those not already added to Enrollment 
[**v3EnrollmentGet**](EnrollmentAPI.md#v3enrollmentget) | **GET** /v3/enrollment | Get Enrollment object and Re-enrollment settings 
[**v3EnrollmentLanguageCodesGet**](EnrollmentAPI.md#v3enrollmentlanguagecodesget) | **GET** /v3/enrollment/language-codes | Retrieve the list of languages and corresponding ISO 639-1 Codes 
[**v3EnrollmentLanguagesDeleteMultiplePost**](EnrollmentAPI.md#v3enrollmentlanguagesdeletemultiplepost) | **POST** /v3/enrollment/languages/delete-multiple | Delete multiple configured languages from User-Initiated Enrollment settings 
[**v3EnrollmentLanguagesGet**](EnrollmentAPI.md#v3enrollmentlanguagesget) | **GET** /v3/enrollment/languages | Get an array of the language codes that have Enrollment messaging 
[**v3EnrollmentLanguagesLanguageIdDelete**](EnrollmentAPI.md#v3enrollmentlanguageslanguageiddelete) | **DELETE** /v3/enrollment/languages/{languageId} | Delete the Enrollment messaging for a language 
[**v3EnrollmentLanguagesLanguageIdGet**](EnrollmentAPI.md#v3enrollmentlanguageslanguageidget) | **GET** /v3/enrollment/languages/{languageId} | Retrieve the Enrollment messaging for a language 
[**v3EnrollmentLanguagesLanguageIdPut**](EnrollmentAPI.md#v3enrollmentlanguageslanguageidput) | **PUT** /v3/enrollment/languages/{languageId} | Edit Enrollment messaging for a language 
[**v3EnrollmentPut**](EnrollmentAPI.md#v3enrollmentput) | **PUT** /v3/enrollment | Update Enrollment object 


# **v1AdueSessionTokenSettingsGet**
```swift
    open class func v1AdueSessionTokenSettingsGet(completion: @escaping (_ data: AccountDrivenUserEnrollmentSessionTokenSettings?, _ error: Error?) -> Void)
```

Retrieve the Account Driven User Enrollment Session Token Settings 

Retrieve the Account Driven User Enrollment Session Token Settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the Account Driven User Enrollment Session Token Settings 
EnrollmentAPI.v1AdueSessionTokenSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AccountDrivenUserEnrollmentSessionTokenSettings**](AccountDrivenUserEnrollmentSessionTokenSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdueSessionTokenSettingsPut**
```swift
    open class func v1AdueSessionTokenSettingsPut(accountDrivenUserEnrollmentSessionTokenSettings: AccountDrivenUserEnrollmentSessionTokenSettings, completion: @escaping (_ data: AccountDrivenUserEnrollmentSessionTokenSettings?, _ error: Error?) -> Void)
```

Update Account Driven User Enrollment Session Token Settings. 

Update the Account Driven User Enrollment Session Token Settings object. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let accountDrivenUserEnrollmentSessionTokenSettings = AccountDrivenUserEnrollmentSessionTokenSettings(enabled: false, expirationIntervalDays: 123, expirationIntervalSeconds: 123) // AccountDrivenUserEnrollmentSessionTokenSettings | Update Account Driven User Enrollment Session Token Settings.

// Update Account Driven User Enrollment Session Token Settings. 
EnrollmentAPI.v1AdueSessionTokenSettingsPut(accountDrivenUserEnrollmentSessionTokenSettings: accountDrivenUserEnrollmentSessionTokenSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountDrivenUserEnrollmentSessionTokenSettings** | [**AccountDrivenUserEnrollmentSessionTokenSettings**](AccountDrivenUserEnrollmentSessionTokenSettings.md) | Update Account Driven User Enrollment Session Token Settings. | 

### Return type

[**AccountDrivenUserEnrollmentSessionTokenSettings**](AccountDrivenUserEnrollmentSessionTokenSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentAccessGroupsGet**
```swift
    open class func v2EnrollmentAccessGroupsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, allUsersOptionFirst: Bool? = nil, completion: @escaping (_ data: AccessGroupsV2SearchResults?, _ error: Error?) -> Void)
```

Retrieve the configured LDAP groups configured for User-Initiated Enrollment 

Retrieves the configured LDAP groups configured for User-Initiated Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `name:asc`. Multiple sort criteria are supported and must be separated with a comma. Example: `sort=date:desc,name:asc`.  (optional)
let allUsersOptionFirst = true // Bool | Return \"All LDAP Users\" option on the first position if it is present in the current page (optional) (default to false)

// Retrieve the configured LDAP groups configured for User-Initiated Enrollment 
EnrollmentAPI.v2EnrollmentAccessGroupsGet(page: page, pageSize: pageSize, sort: sort, allUsersOptionFirst: allUsersOptionFirst) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;name:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma. Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 
 **allUsersOptionFirst** | **Bool** | Return \&quot;All LDAP Users\&quot; option on the first position if it is present in the current page | [optional] [default to false]

### Return type

[**AccessGroupsV2SearchResults**](AccessGroupsV2SearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentAccessGroupsPost**
```swift
    open class func v2EnrollmentAccessGroupsPost(enrollmentAccessGroupV2: EnrollmentAccessGroupV2, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add the configured LDAP group for User-Initiated Enrollment. 

Add the configured LDAP group for User-Initiated Enrollment. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let enrollmentAccessGroupV2 = EnrollmentAccessGroupV2(id: "id_example", ldapServerId: "ldapServerId_example", name: "name_example", siteId: "siteId_example", enterpriseEnrollmentEnabled: false, personalEnrollmentEnabled: false, accountDrivenUserEnrollmentEnabled: false, requireEula: false) // EnrollmentAccessGroupV2 | Configured LDAP group to create.

// Add the configured LDAP group for User-Initiated Enrollment. 
EnrollmentAPI.v2EnrollmentAccessGroupsPost(enrollmentAccessGroupV2: enrollmentAccessGroupV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enrollmentAccessGroupV2** | [**EnrollmentAccessGroupV2**](EnrollmentAccessGroupV2.md) | Configured LDAP group to create. | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentAccessGroupsServerIdGroupIdDelete**
```swift
    open class func v2EnrollmentAccessGroupsServerIdGroupIdDelete(serverId: String, groupId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an LDAP group's access to user initiated Enrollment 

Deletes an LDAP group's access to user initiated enrollment. The group \"All LDAP Users\" cannot be deleted, but it can be modified to disallow User-Initiated Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let serverId = "serverId_example" // String | LDAP server id
let groupId = "groupId_example" // String | LDAP group id.

// Delete an LDAP group's access to user initiated Enrollment 
EnrollmentAPI.v2EnrollmentAccessGroupsServerIdGroupIdDelete(serverId: serverId, groupId: groupId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **String** | LDAP server id | 
 **groupId** | **String** | LDAP group id. | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentAccessGroupsServerIdGroupIdGet**
```swift
    open class func v2EnrollmentAccessGroupsServerIdGroupIdGet(serverId: String, groupId: String, completion: @escaping (_ data: EnrollmentAccessGroupV2?, _ error: Error?) -> Void)
```

Retrieve the configured LDAP groups configured for User-Initiated Enrollment 

Retrieves the configured LDAP groups configured for User-Initiated Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let serverId = "serverId_example" // String | LDAP server id.
let groupId = "groupId_example" // String | LDAP group id.

// Retrieve the configured LDAP groups configured for User-Initiated Enrollment 
EnrollmentAPI.v2EnrollmentAccessGroupsServerIdGroupIdGet(serverId: serverId, groupId: groupId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **String** | LDAP server id. | 
 **groupId** | **String** | LDAP group id. | 

### Return type

[**EnrollmentAccessGroupV2**](EnrollmentAccessGroupV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentAccessGroupsServerIdGroupIdPut**
```swift
    open class func v2EnrollmentAccessGroupsServerIdGroupIdPut(serverId: String, groupId: String, enrollmentAccessGroupV2: EnrollmentAccessGroupV2? = nil, completion: @escaping (_ data: EnrollmentAccessGroupV2?, _ error: Error?) -> Void)
```

Modify the configured LDAP groups configured for User-Initiated Enrollment 

Modifies the configured LDAP groups configured for User-Initiated Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let serverId = "serverId_example" // String | LDAP server id.
let groupId = "groupId_example" // String | LDAP group id.
let enrollmentAccessGroupV2 = EnrollmentAccessGroupV2(id: "id_example", ldapServerId: "ldapServerId_example", name: "name_example", siteId: "siteId_example", enterpriseEnrollmentEnabled: false, personalEnrollmentEnabled: false, accountDrivenUserEnrollmentEnabled: false, requireEula: false) // EnrollmentAccessGroupV2 |  (optional)

// Modify the configured LDAP groups configured for User-Initiated Enrollment 
EnrollmentAPI.v2EnrollmentAccessGroupsServerIdGroupIdPut(serverId: serverId, groupId: groupId, enrollmentAccessGroupV2: enrollmentAccessGroupV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serverId** | **String** | LDAP server id. | 
 **groupId** | **String** | LDAP group id. | 
 **enrollmentAccessGroupV2** | [**EnrollmentAccessGroupV2**](EnrollmentAccessGroupV2.md) |  | [optional] 

### Return type

[**EnrollmentAccessGroupV2**](EnrollmentAccessGroupV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentFilteredLanguageCodesGet**
```swift
    open class func v2EnrollmentFilteredLanguageCodesGet(completion: @escaping (_ data: [LanguageCode]?, _ error: Error?) -> Void)
```

Retrieve the list of languages and corresponding ISO 639-1 Codes but only those not already added to Enrollment 

Retrieves the list of languages and corresponding ISO 639-1 Codes, but only those not already added to Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the list of languages and corresponding ISO 639-1 Codes but only those not already added to Enrollment 
EnrollmentAPI.v2EnrollmentFilteredLanguageCodesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LanguageCode]**](LanguageCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentGet**
```swift
    open class func v2EnrollmentGet(completion: @escaping (_ data: EnrollmentSettingsV2?, _ error: Error?) -> Void)
```

Get Enrollment object and Re-enrollment settings 

Gets Enrollment object and re-enrollment settings. The settings can be altered without providing the existing management password by providing the following value for `managementPassword`: `\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff`. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Enrollment object and Re-enrollment settings 
EnrollmentAPI.v2EnrollmentGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EnrollmentSettingsV2**](EnrollmentSettingsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentHistoryExportPost**
```swift
    open class func v2EnrollmentHistoryExportPost(exportFields: [String]? = nil, exportLabels: [String]? = nil, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, exportParameters: ExportParameters? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Export enrollment history collection 

Export enrollment history collection 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let exportFields = ["inner_example"] // [String] | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields=id,username (optional)
let exportLabels = ["inner_example"] // [String] | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels=identifier,name with matching: export-fields=id,username (optional)
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=id:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name. This param can be combined with paging and sorting. Example: name==\"*script*\" (optional) (default to "")
let exportParameters = ExportParameters(page: 123, pageSize: 123, sort: ["sort_example"], filter: "filter_example", fields: [ExportField(fieldName: "fieldName_example", fieldLabelOverride: "fieldLabelOverride_example")]) // ExportParameters | Optional. Override query parameters since they can make URI exceed 2,000 character limit. (optional)

// Export enrollment history collection 
EnrollmentAPI.v2EnrollmentHistoryExportPost(exportFields: exportFields, exportLabels: exportLabels, page: page, pageSize: pageSize, sort: sort, filter: filter, exportParameters: exportParameters) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exportFields** | [**[String]**](String.md) | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields&#x3D;id,username | [optional] 
 **exportLabels** | [**[String]**](String.md) | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels&#x3D;identifier,name with matching: export-fields&#x3D;id,username | [optional] 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;id:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name. This param can be combined with paging and sorting. Example: name&#x3D;&#x3D;\&quot;*script*\&quot; | [optional] [default to &quot;&quot;]
 **exportParameters** | [**ExportParameters**](ExportParameters.md) | Optional. Override query parameters since they can make URI exceed 2,000 character limit. | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/csv,application/json, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentHistoryGet**
```swift
    open class func v2EnrollmentHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get sorted and paged Enrollment history object 

Gets sorted and paged Enrollment history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `date:desc`. Multiple sort criteria are supported and must be separated with a comma. Example: `sort=date:desc,name:asc`.  (optional)

// Get sorted and paged Enrollment history object 
EnrollmentAPI.v2EnrollmentHistoryGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;date:desc&#x60;. Multiple sort criteria are supported and must be separated with a comma. Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentHistoryPost**
```swift
    open class func v2EnrollmentHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Enrollment history object notes 

Adds Enrollment history object notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Enrollment history object notes 
EnrollmentAPI.v2EnrollmentHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentLanguageCodesGet**
```swift
    open class func v2EnrollmentLanguageCodesGet(completion: @escaping (_ data: [LanguageCode]?, _ error: Error?) -> Void)
```

Retrieve the list of languages and corresponding ISO 639-1 Codes 

Retrieves the list of languages and corresponding ISO 639-1 Codes.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the list of languages and corresponding ISO 639-1 Codes 
EnrollmentAPI.v2EnrollmentLanguageCodesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LanguageCode]**](LanguageCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentLanguagesDeleteMultiplePost**
```swift
    open class func v2EnrollmentLanguagesDeleteMultiplePost(ids: Ids, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete multiple configured languages from User-Initiated Enrollment settings 

Delete multiple configured languages from User-Initiated Enrollment settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ids = Ids(ids: ["ids_example"]) // Ids | ids of each language to delete

// Delete multiple configured languages from User-Initiated Enrollment settings 
EnrollmentAPI.v2EnrollmentLanguagesDeleteMultiplePost(ids: ids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**Ids**](Ids.md) | ids of each language to delete | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentLanguagesGet**
```swift
    open class func v2EnrollmentLanguagesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: ProcessTextsSearchResults?, _ error: Error?) -> Void)
```

Get an array of the language codes that have Enrollment messaging 

Returns an array of the language codes that have enrollment messaging currently configured.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is `languageCode:asc`. Multiple sort criteria are supported and must be separated with a comma. Example: `sort=date:desc,name:asc`.  (optional)

// Get an array of the language codes that have Enrollment messaging 
EnrollmentAPI.v2EnrollmentLanguagesGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is &#x60;languageCode:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma. Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 

### Return type

[**ProcessTextsSearchResults**](ProcessTextsSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentLanguagesLanguageIdDelete**
```swift
    open class func v2EnrollmentLanguagesLanguageIdDelete(languageId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the Enrollment messaging for a language 

Delete the enrollment messaging for a language.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let languageId = "languageId_example" // String | Two letter ISO 639-1 Language Code

// Delete the Enrollment messaging for a language 
EnrollmentAPI.v2EnrollmentLanguagesLanguageIdDelete(languageId: languageId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **languageId** | **String** | Two letter ISO 639-1 Language Code | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentLanguagesLanguageIdGet**
```swift
    open class func v2EnrollmentLanguagesLanguageIdGet(languageId: String, completion: @escaping (_ data: EnrollmentProcessTextObject?, _ error: Error?) -> Void)
```

Retrieve the Enrollment messaging for a language 

Retrieves the enrollment messaging for a language.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let languageId = "languageId_example" // String | Two letter ISO 639-1 Language Code

// Retrieve the Enrollment messaging for a language 
EnrollmentAPI.v2EnrollmentLanguagesLanguageIdGet(languageId: languageId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **languageId** | **String** | Two letter ISO 639-1 Language Code | 

### Return type

[**EnrollmentProcessTextObject**](EnrollmentProcessTextObject.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentLanguagesLanguageIdPut**
```swift
    open class func v2EnrollmentLanguagesLanguageIdPut(languageId: String, enrollmentProcessTextObject: EnrollmentProcessTextObject? = nil, completion: @escaping (_ data: EnrollmentProcessTextObject?, _ error: Error?) -> Void)
```

Edit Enrollment messaging for a language 

Edit enrollment messaging for a language.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let languageId = "languageId_example" // String | Two letter ISO 639-1 Language Code
let enrollmentProcessTextObject = EnrollmentProcessTextObject(languageCode: "languageCode_example", name: "name_example", title: "title_example", loginDescription: "loginDescription_example", username: "username_example", password: "password_example", loginButton: "loginButton_example", deviceClassDescription: "deviceClassDescription_example", deviceClassPersonal: "deviceClassPersonal_example", deviceClassPersonalDescription: "deviceClassPersonalDescription_example", deviceClassEnterprise: "deviceClassEnterprise_example", deviceClassEnterpriseDescription: "deviceClassEnterpriseDescription_example", deviceClassButton: "deviceClassButton_example", personalEula: "personalEula_example", enterpriseEula: "enterpriseEula_example", eulaButton: "eulaButton_example", siteDescription: "siteDescription_example", certificateText: "certificateText_example", certificateButton: "certificateButton_example", certificateProfileName: "certificateProfileName_example", certificateProfileDescription: "certificateProfileDescription_example", personalText: "personalText_example", personalButton: "personalButton_example", personalProfileName: "personalProfileName_example", personalProfileDescription: "personalProfileDescription_example", userEnrollmentText: "userEnrollmentText_example", userEnrollmentButton: "userEnrollmentButton_example", userEnrollmentProfileName: "userEnrollmentProfileName_example", userEnrollmentProfileDescription: "userEnrollmentProfileDescription_example", enterpriseText: "enterpriseText_example", enterpriseButton: "enterpriseButton_example", enterpriseProfileName: "enterpriseProfileName_example", enterpriseProfileDescription: "enterpriseProfileDescription_example", enterprisePending: "enterprisePending_example", quickAddText: "quickAddText_example", quickAddButton: "quickAddButton_example", quickAddName: "quickAddName_example", quickAddPending: "quickAddPending_example", completeMessage: "completeMessage_example", failedMessage: "failedMessage_example", tryAgainButton: "tryAgainButton_example", checkNowButton: "checkNowButton_example", checkEnrollmentMessage: "checkEnrollmentMessage_example", logoutButton: "logoutButton_example") // EnrollmentProcessTextObject |  (optional)

// Edit Enrollment messaging for a language 
EnrollmentAPI.v2EnrollmentLanguagesLanguageIdPut(languageId: languageId, enrollmentProcessTextObject: enrollmentProcessTextObject) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **languageId** | **String** | Two letter ISO 639-1 Language Code | 
 **enrollmentProcessTextObject** | [**EnrollmentProcessTextObject**](EnrollmentProcessTextObject.md) |  | [optional] 

### Return type

[**EnrollmentProcessTextObject**](EnrollmentProcessTextObject.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2EnrollmentPut**
```swift
    open class func v2EnrollmentPut(enrollmentSettingsV2: EnrollmentSettingsV2, completion: @escaping (_ data: EnrollmentSettingsV2?, _ error: Error?) -> Void)
```

Update Enrollment object 

Update enrollment object. Regarding the `developerCertificateIdentity`, if this object is omitted, the certificate will not be deleted from Jamf Pro. The `identityKeystore` is the entire cert file as a base64 encoded string. The `md5Sum` field is not required in the PUT request, but is calculated and returned in the response. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let enrollmentSettingsV2 = EnrollmentSettingsV2(installSingleProfile: false, signingMdmProfileEnabled: false, mdmSigningCertificate: CertificateIdentityV2(filename: "filename_example", keystorePassword: "keystorePassword_example", identityKeystore: 123, md5Sum: "md5Sum_example"), restrictReenrollment: false, flushLocationInformation: false, flushLocationHistoryInformation: false, flushPolicyHistory: false, flushExtensionAttributes: false, flushMdmCommandsOnReenroll: "flushMdmCommandsOnReenroll_example", macOsEnterpriseEnrollmentEnabled: false, managementUsername: "managementUsername_example", managementPassword: "managementPassword_example", managementPasswordSet: true, passwordType: "passwordType_example", randomPasswordLength: 123, createManagementAccount: false, hideManagementAccount: false, allowSshOnlyManagementAccount: false, ensureSshRunning: false, launchSelfService: false, signQuickAdd: false, developerCertificateIdentity: nil, developerCertificateIdentityDetails: CertificateDetails(subject: "subject_example", serialNumber: "serialNumber_example"), mdmSigningCertificateDetails: nil, iosEnterpriseEnrollmentEnabled: false, iosPersonalEnrollmentEnabled: false, personalDeviceEnrollmentType: "personalDeviceEnrollmentType_example", accountDrivenUserEnrollmentEnabled: false) // EnrollmentSettingsV2 | Update enrollment

// Update Enrollment object 
EnrollmentAPI.v2EnrollmentPut(enrollmentSettingsV2: enrollmentSettingsV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enrollmentSettingsV2** | [**EnrollmentSettingsV2**](EnrollmentSettingsV2.md) | Update enrollment | 

### Return type

[**EnrollmentSettingsV2**](EnrollmentSettingsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentAccessGroupsGet**
```swift
    open class func v3EnrollmentAccessGroupsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, allUsersOptionFirst: Bool? = nil, completion: @escaping (_ data: AccessGroupsPreviewSearchResults?, _ error: Error?) -> Void)
```

Retrieve the configured LDAP groups configured for User-Initiated Enrollment. 

Retrieves the configured LDAP groups configured for User-Initiated Enrollment. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `name:asc`. Multiple sort criteria are supported and must be separated with a comma. Example: `sort=date:desc,name:asc`.  (optional)
let allUsersOptionFirst = true // Bool | Return \"All LDAP Users\" option on the first position if it is present in the current page (optional) (default to false)

// Retrieve the configured LDAP groups configured for User-Initiated Enrollment. 
EnrollmentAPI.v3EnrollmentAccessGroupsGet(page: page, pageSize: pageSize, sort: sort, allUsersOptionFirst: allUsersOptionFirst) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;name:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma. Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 
 **allUsersOptionFirst** | **Bool** | Return \&quot;All LDAP Users\&quot; option on the first position if it is present in the current page | [optional] [default to false]

### Return type

[**AccessGroupsPreviewSearchResults**](AccessGroupsPreviewSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentAccessGroupsIdDelete**
```swift
    open class func v3EnrollmentAccessGroupsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an LDAP group's access to user initiated Enrollment. 

Deletes an LDAP group's access to user initiated enrollment. The group \"All LDAP Users\" cannot be deleted, but it can be modified to disallow User-Initiated Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Autogenerated Access Group ID.

// Delete an LDAP group's access to user initiated Enrollment. 
EnrollmentAPI.v3EnrollmentAccessGroupsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Autogenerated Access Group ID. | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentAccessGroupsIdGet**
```swift
    open class func v3EnrollmentAccessGroupsIdGet(id: String, completion: @escaping (_ data: EnrollmentAccessGroupPreview?, _ error: Error?) -> Void)
```

Retrieve the configured LDAP groups configured for User-Initiated Enrollment 

Retrieves the configured LDAP groups configured for User-Initiated Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Autogenerated Access Group ID.

// Retrieve the configured LDAP groups configured for User-Initiated Enrollment 
EnrollmentAPI.v3EnrollmentAccessGroupsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Autogenerated Access Group ID. | 

### Return type

[**EnrollmentAccessGroupPreview**](EnrollmentAccessGroupPreview.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentAccessGroupsIdPut**
```swift
    open class func v3EnrollmentAccessGroupsIdPut(id: String, enrollmentAccessGroupPreview: EnrollmentAccessGroupPreview, completion: @escaping (_ data: EnrollmentAccessGroupPreview?, _ error: Error?) -> Void)
```

Modify the configured LDAP groups configured for User-Initiated Enrollment. Only exiting Access Groups can be updated. 

Modify the configured LDAP groups configured for User-Initiated Enrollment. Only exiting Access Groups can be updated. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Autogenerated Access Group ID.
let enrollmentAccessGroupPreview = EnrollmentAccessGroupPreview(id: "id_example", groupId: "groupId_example", ldapServerId: "ldapServerId_example", name: "name_example", siteId: "siteId_example", enterpriseEnrollmentEnabled: false, personalEnrollmentEnabled: false, accountDrivenUserEnrollmentEnabled: false, requireEula: false) // EnrollmentAccessGroupPreview | 

// Modify the configured LDAP groups configured for User-Initiated Enrollment. Only exiting Access Groups can be updated. 
EnrollmentAPI.v3EnrollmentAccessGroupsIdPut(id: id, enrollmentAccessGroupPreview: enrollmentAccessGroupPreview) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Autogenerated Access Group ID. | 
 **enrollmentAccessGroupPreview** | [**EnrollmentAccessGroupPreview**](EnrollmentAccessGroupPreview.md) |  | 

### Return type

[**EnrollmentAccessGroupPreview**](EnrollmentAccessGroupPreview.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentAccessGroupsPost**
```swift
    open class func v3EnrollmentAccessGroupsPost(enrollmentAccessGroupPreview: EnrollmentAccessGroupPreview, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add the configured LDAP group for User-Initiated Enrollment. 

Add the configured LDAP group for User-Initiated Enrollment. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let enrollmentAccessGroupPreview = EnrollmentAccessGroupPreview(id: "id_example", groupId: "groupId_example", ldapServerId: "ldapServerId_example", name: "name_example", siteId: "siteId_example", enterpriseEnrollmentEnabled: false, personalEnrollmentEnabled: false, accountDrivenUserEnrollmentEnabled: false, requireEula: false) // EnrollmentAccessGroupPreview | Configured LDAP group to create.

// Add the configured LDAP group for User-Initiated Enrollment. 
EnrollmentAPI.v3EnrollmentAccessGroupsPost(enrollmentAccessGroupPreview: enrollmentAccessGroupPreview) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enrollmentAccessGroupPreview** | [**EnrollmentAccessGroupPreview**](EnrollmentAccessGroupPreview.md) | Configured LDAP group to create. | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentFilteredLanguageCodesGet**
```swift
    open class func v3EnrollmentFilteredLanguageCodesGet(completion: @escaping (_ data: [LanguageCode]?, _ error: Error?) -> Void)
```

Retrieve the list of languages and corresponding ISO 639-1 Codes but only those not already added to Enrollment 

Retrieves the list of languages and corresponding ISO 639-1 Codes, but only those not already added to Enrollment.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the list of languages and corresponding ISO 639-1 Codes but only those not already added to Enrollment 
EnrollmentAPI.v3EnrollmentFilteredLanguageCodesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LanguageCode]**](LanguageCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentGet**
```swift
    open class func v3EnrollmentGet(completion: @escaping (_ data: EnrollmentSettingsV3?, _ error: Error?) -> Void)
```

Get Enrollment object and Re-enrollment settings 

Gets Enrollment object and re-enrollment settings. The settings can be altered without providing the existing management password by providing the following value for `managementPassword`: `\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff\\uffff`. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Enrollment object and Re-enrollment settings 
EnrollmentAPI.v3EnrollmentGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EnrollmentSettingsV3**](EnrollmentSettingsV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentLanguageCodesGet**
```swift
    open class func v3EnrollmentLanguageCodesGet(completion: @escaping (_ data: [LanguageCode]?, _ error: Error?) -> Void)
```

Retrieve the list of languages and corresponding ISO 639-1 Codes 

Retrieves the list of languages and corresponding ISO 639-1 Codes.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the list of languages and corresponding ISO 639-1 Codes 
EnrollmentAPI.v3EnrollmentLanguageCodesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LanguageCode]**](LanguageCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentLanguagesDeleteMultiplePost**
```swift
    open class func v3EnrollmentLanguagesDeleteMultiplePost(ids: Ids, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete multiple configured languages from User-Initiated Enrollment settings 

Delete multiple configured languages from User-Initiated Enrollment settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ids = Ids(ids: ["ids_example"]) // Ids | ids of each language to delete

// Delete multiple configured languages from User-Initiated Enrollment settings 
EnrollmentAPI.v3EnrollmentLanguagesDeleteMultiplePost(ids: ids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**Ids**](Ids.md) | ids of each language to delete | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentLanguagesGet**
```swift
    open class func v3EnrollmentLanguagesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: ProcessTextsSearchResults?, _ error: Error?) -> Void)
```

Get an array of the language codes that have Enrollment messaging 

Returns an array of the language codes that have enrollment messaging currently configured.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is `languageCode:asc`. Multiple sort criteria are supported and must be separated with a comma. Example: `sort=date:desc,name:asc`.  (optional)

// Get an array of the language codes that have Enrollment messaging 
EnrollmentAPI.v3EnrollmentLanguagesGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is &#x60;languageCode:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma. Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 

### Return type

[**ProcessTextsSearchResults**](ProcessTextsSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentLanguagesLanguageIdDelete**
```swift
    open class func v3EnrollmentLanguagesLanguageIdDelete(languageId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the Enrollment messaging for a language 

Delete the enrollment messaging for a language.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let languageId = "languageId_example" // String | Two letter ISO 639-1 Language Code

// Delete the Enrollment messaging for a language 
EnrollmentAPI.v3EnrollmentLanguagesLanguageIdDelete(languageId: languageId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **languageId** | **String** | Two letter ISO 639-1 Language Code | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentLanguagesLanguageIdGet**
```swift
    open class func v3EnrollmentLanguagesLanguageIdGet(languageId: String, completion: @escaping (_ data: EnrollmentProcessTextObject?, _ error: Error?) -> Void)
```

Retrieve the Enrollment messaging for a language 

Retrieves the enrollment messaging for a language.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let languageId = "languageId_example" // String | Two letter ISO 639-1 Language Code

// Retrieve the Enrollment messaging for a language 
EnrollmentAPI.v3EnrollmentLanguagesLanguageIdGet(languageId: languageId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **languageId** | **String** | Two letter ISO 639-1 Language Code | 

### Return type

[**EnrollmentProcessTextObject**](EnrollmentProcessTextObject.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentLanguagesLanguageIdPut**
```swift
    open class func v3EnrollmentLanguagesLanguageIdPut(languageId: String, enrollmentProcessTextObject: EnrollmentProcessTextObject? = nil, completion: @escaping (_ data: EnrollmentProcessTextObject?, _ error: Error?) -> Void)
```

Edit Enrollment messaging for a language 

Edit enrollment messaging for a language.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let languageId = "languageId_example" // String | Two letter ISO 639-1 Language Code
let enrollmentProcessTextObject = EnrollmentProcessTextObject(languageCode: "languageCode_example", name: "name_example", title: "title_example", loginDescription: "loginDescription_example", username: "username_example", password: "password_example", loginButton: "loginButton_example", deviceClassDescription: "deviceClassDescription_example", deviceClassPersonal: "deviceClassPersonal_example", deviceClassPersonalDescription: "deviceClassPersonalDescription_example", deviceClassEnterprise: "deviceClassEnterprise_example", deviceClassEnterpriseDescription: "deviceClassEnterpriseDescription_example", deviceClassButton: "deviceClassButton_example", personalEula: "personalEula_example", enterpriseEula: "enterpriseEula_example", eulaButton: "eulaButton_example", siteDescription: "siteDescription_example", certificateText: "certificateText_example", certificateButton: "certificateButton_example", certificateProfileName: "certificateProfileName_example", certificateProfileDescription: "certificateProfileDescription_example", personalText: "personalText_example", personalButton: "personalButton_example", personalProfileName: "personalProfileName_example", personalProfileDescription: "personalProfileDescription_example", userEnrollmentText: "userEnrollmentText_example", userEnrollmentButton: "userEnrollmentButton_example", userEnrollmentProfileName: "userEnrollmentProfileName_example", userEnrollmentProfileDescription: "userEnrollmentProfileDescription_example", enterpriseText: "enterpriseText_example", enterpriseButton: "enterpriseButton_example", enterpriseProfileName: "enterpriseProfileName_example", enterpriseProfileDescription: "enterpriseProfileDescription_example", enterprisePending: "enterprisePending_example", quickAddText: "quickAddText_example", quickAddButton: "quickAddButton_example", quickAddName: "quickAddName_example", quickAddPending: "quickAddPending_example", completeMessage: "completeMessage_example", failedMessage: "failedMessage_example", tryAgainButton: "tryAgainButton_example", checkNowButton: "checkNowButton_example", checkEnrollmentMessage: "checkEnrollmentMessage_example", logoutButton: "logoutButton_example") // EnrollmentProcessTextObject |  (optional)

// Edit Enrollment messaging for a language 
EnrollmentAPI.v3EnrollmentLanguagesLanguageIdPut(languageId: languageId, enrollmentProcessTextObject: enrollmentProcessTextObject) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **languageId** | **String** | Two letter ISO 639-1 Language Code | 
 **enrollmentProcessTextObject** | [**EnrollmentProcessTextObject**](EnrollmentProcessTextObject.md) |  | [optional] 

### Return type

[**EnrollmentProcessTextObject**](EnrollmentProcessTextObject.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3EnrollmentPut**
```swift
    open class func v3EnrollmentPut(enrollmentSettingsV3: EnrollmentSettingsV3, completion: @escaping (_ data: EnrollmentSettingsV3?, _ error: Error?) -> Void)
```

Update Enrollment object 

Update enrollment object. Regarding the `developerCertificateIdentity`, if this object is omitted, the certificate will not be deleted from Jamf Pro. The `identityKeystore` is the entire cert file as a base64 encoded string. The `md5Sum` field is not required in the PUT request, but is calculated and returned in the response. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let enrollmentSettingsV3 = EnrollmentSettingsV3(installSingleProfile: false, signingMdmProfileEnabled: false, mdmSigningCertificate: CertificateIdentityV2(filename: "filename_example", keystorePassword: "keystorePassword_example", identityKeystore: 123, md5Sum: "md5Sum_example"), restrictReenrollment: false, flushLocationInformation: false, flushLocationHistoryInformation: false, flushPolicyHistory: false, flushExtensionAttributes: false, flushMdmCommandsOnReenroll: "flushMdmCommandsOnReenroll_example", macOsEnterpriseEnrollmentEnabled: false, managementUsername: "managementUsername_example", createManagementAccount: false, hideManagementAccount: false, allowSshOnlyManagementAccount: false, ensureSshRunning: false, launchSelfService: false, signQuickAdd: false, developerCertificateIdentity: nil, developerCertificateIdentityDetails: CertificateDetails(subject: "subject_example", serialNumber: "serialNumber_example"), mdmSigningCertificateDetails: nil, iosEnterpriseEnrollmentEnabled: false, iosPersonalEnrollmentEnabled: false, personalDeviceEnrollmentType: "personalDeviceEnrollmentType_example", accountDrivenUserEnrollmentEnabled: false) // EnrollmentSettingsV3 | Update enrollment

// Update Enrollment object 
EnrollmentAPI.v3EnrollmentPut(enrollmentSettingsV3: enrollmentSettingsV3) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enrollmentSettingsV3** | [**EnrollmentSettingsV3**](EnrollmentSettingsV3.md) | Update enrollment | 

### Return type

[**EnrollmentSettingsV3**](EnrollmentSettingsV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

