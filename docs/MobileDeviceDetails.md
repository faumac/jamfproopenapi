# MobileDeviceDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**lastInventoryUpdateTimestamp** | **Date** |  | [optional] 
**osVersion** | **String** |  | [optional] 
**osBuild** | **String** |  | [optional] 
**osSupplementalBuildVersion** | **String** | Collected for iOS 16 and iPadOS 16.1 or later | [optional] 
**osRapidSecurityResponse** | **String** | Collected for iOS 16 and iPadOS 16.1 or later | [optional] 
**softwareUpdateDeviceId** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**udid** | **String** |  | [optional] 
**ipAddress** | **String** |  | [optional] 
**wifiMacAddress** | **String** |  | [optional] 
**bluetoothMacAddress** | **String** |  | [optional] 
**isManaged** | **Bool** |  | [optional] 
**initialEntryTimestamp** | **Date** |  | [optional] 
**lastEnrollmentTimestamp** | **Date** |  | [optional] 
**deviceOwnershipLevel** | **String** |  | [optional] 
**site** | [**IdAndName**](IdAndName.md) |  | [optional] 
**extensionAttributes** | [ExtensionAttribute] |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**type** | **String** | Based on the value of this either ios, appleTv, android objects will be populated. | [optional] 
**ios** | [**IosDetails**](IosDetails.md) |  | [optional] 
**appleTv** | [**AppleTvDetails**](AppleTvDetails.md) |  | [optional] 
**android** | [**AndroidDetails**](AndroidDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


