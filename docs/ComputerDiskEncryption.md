# ComputerDiskEncryption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bootPartitionEncryptionDetails** | [**ComputerPartitionEncryption**](ComputerPartitionEncryption.md) |  | [optional] 
**individualRecoveryKeyValidityStatus** | **String** |  | [optional] 
**institutionalRecoveryKeyPresent** | **Bool** |  | [optional] 
**diskEncryptionConfigurationName** | **String** |  | [optional] 
**fileVault2EnabledUserNames** | **[String]** |  | [optional] 
**fileVault2EligibilityMessage** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


