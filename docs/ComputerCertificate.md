# ComputerCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commonName** | **String** |  | [optional] 
**identity** | **Bool** |  | [optional] 
**expirationDate** | **Date** |  | [optional] 
**username** | **String** |  | [optional] 
**lifecycleStatus** | **String** |  | [optional] 
**certificateStatus** | **String** |  | [optional] 
**subjectName** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**sha1Fingerprint** | **String** |  | [optional] 
**issuedDate** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


