# CloudAzureAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1CloudAzureDefaultsMappingsGet**](CloudAzureAPI.md#v1cloudazuredefaultsmappingsget) | **GET** /v1/cloud-azure/defaults/mappings | Get default mappings
[**v1CloudAzureDefaultsServerConfigurationGet**](CloudAzureAPI.md#v1cloudazuredefaultsserverconfigurationget) | **GET** /v1/cloud-azure/defaults/server-configuration | Get default server configuration
[**v1CloudAzureIdDelete**](CloudAzureAPI.md#v1cloudazureiddelete) | **DELETE** /v1/cloud-azure/{id} | Delete Cloud Identity Provider configuration.
[**v1CloudAzureIdGet**](CloudAzureAPI.md#v1cloudazureidget) | **GET** /v1/cloud-azure/{id} | Get Azure Cloud Identity Provider configuration with given ID.
[**v1CloudAzureIdPut**](CloudAzureAPI.md#v1cloudazureidput) | **PUT** /v1/cloud-azure/{id} | Update Azure Cloud Identity Provider configuration
[**v1CloudAzurePost**](CloudAzureAPI.md#v1cloudazurepost) | **POST** /v1/cloud-azure | Create Azure Cloud Identity Provider configuration


# **v1CloudAzureDefaultsMappingsGet**
```swift
    open class func v1CloudAzureDefaultsMappingsGet(completion: @escaping (_ data: AzureMappings?, _ error: Error?) -> Void)
```

Get default mappings

This is the default set of attributes that allows you to return the data you need from Azure AD. Some fields may be empty and may be edited when creating a new configuration.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get default mappings
CloudAzureAPI.v1CloudAzureDefaultsMappingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AzureMappings**](AzureMappings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudAzureDefaultsServerConfigurationGet**
```swift
    open class func v1CloudAzureDefaultsServerConfigurationGet(completion: @escaping (_ data: AzureServerConfiguration?, _ error: Error?) -> Void)
```

Get default server configuration

This is the default set of attributes that allows you to return the data you need from Azure AD. Some fields may be empty and may be edited when creating a new configuration.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get default server configuration
CloudAzureAPI.v1CloudAzureDefaultsServerConfigurationGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AzureServerConfiguration**](AzureServerConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudAzureIdDelete**
```swift
    open class func v1CloudAzureIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Cloud Identity Provider configuration.

Delete Cloud Identity Provider configuration.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Delete Cloud Identity Provider configuration.
CloudAzureAPI.v1CloudAzureIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudAzureIdGet**
```swift
    open class func v1CloudAzureIdGet(id: String, completion: @escaping (_ data: AzureConfiguration?, _ error: Error?) -> Void)
```

Get Azure Cloud Identity Provider configuration with given ID.

Get Azure Cloud Identity Provider configuration with given ID.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Get Azure Cloud Identity Provider configuration with given ID.
CloudAzureAPI.v1CloudAzureIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**AzureConfiguration**](AzureConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudAzureIdPut**
```swift
    open class func v1CloudAzureIdPut(id: String, azureConfigurationUpdate: AzureConfigurationUpdate, completion: @escaping (_ data: AzureConfiguration?, _ error: Error?) -> Void)
```

Update Azure Cloud Identity Provider configuration

Update Azure Cloud Identity Provider configuration. Cannot be used for partial updates, all content body must be sent.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let azureConfigurationUpdate = AzureConfigurationUpdate(cloudIdPCommon: CloudIdPCommon(id: "id_example", displayName: "displayName_example", providerName: "providerName_example"), server: AzureServerConfigurationUpdate(id: "id_example", enabled: true, mappings: AzureMappings(userId: "userId_example", userName: "userName_example", realName: "realName_example", email: "email_example", department: "department_example", building: "building_example", room: "room_example", phone: "phone_example", position: "position_example", groupId: "groupId_example", groupName: "groupName_example"), searchTimeout: 123, transitiveMembershipEnabled: false, transitiveMembershipUserField: "transitiveMembershipUserField_example", transitiveDirectoryMembershipEnabled: false, membershipCalculationOptimizationEnabled: true)) // AzureConfigurationUpdate | Azure Cloud Identity Provider configuration to update

// Update Azure Cloud Identity Provider configuration
CloudAzureAPI.v1CloudAzureIdPut(id: id, azureConfigurationUpdate: azureConfigurationUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **azureConfigurationUpdate** | [**AzureConfigurationUpdate**](AzureConfigurationUpdate.md) | Azure Cloud Identity Provider configuration to update | 

### Return type

[**AzureConfiguration**](AzureConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudAzurePost**
```swift
    open class func v1CloudAzurePost(azureConfigurationRequest: AzureConfigurationRequest, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Azure Cloud Identity Provider configuration

Create new Azure Cloud Identity Provider configuration with unique display name.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let azureConfigurationRequest = AzureConfigurationRequest(cloudIdPCommon: CloudIdPCommonRequest(displayName: "displayName_example", providerName: "providerName_example"), server: AzureServerConfigurationRequest(id: "id_example", tenantId: "tenantId_example", enabled: true, mappings: AzureMappings(userId: "userId_example", userName: "userName_example", realName: "realName_example", email: "email_example", department: "department_example", building: "building_example", room: "room_example", phone: "phone_example", position: "position_example", groupId: "groupId_example", groupName: "groupName_example"), searchTimeout: 123, transitiveMembershipEnabled: false, transitiveMembershipUserField: "transitiveMembershipUserField_example", transitiveDirectoryMembershipEnabled: false, membershipCalculationOptimizationEnabled: true, code: "code_example")) // AzureConfigurationRequest | Azure Cloud Identity Provider configuration to create

// Create Azure Cloud Identity Provider configuration
CloudAzureAPI.v1CloudAzurePost(azureConfigurationRequest: azureConfigurationRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **azureConfigurationRequest** | [**AzureConfigurationRequest**](AzureConfigurationRequest.md) | Azure Cloud Identity Provider configuration to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

