# CurrentAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**username** | **String** |  | [optional] 
**realName** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**preferences** | [**AccountPreferences**](AccountPreferences.md) |  | [optional] 
**isMultiSiteAdmin** | **Bool** |  | [optional] 
**accessLevel** | **String** |  | [optional] 
**privilegeSet** | **String** |  | [optional] 
**privileges** | **[String]** |  | [optional] 
**groupIds** | **[Int]** |  | [optional] 
**currentSiteId** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


