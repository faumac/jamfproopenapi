# PatchManagementAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2PatchManagementAcceptDisclaimerPost**](PatchManagementAPI.md#v2patchmanagementacceptdisclaimerpost) | **POST** /v2/patch-management-accept-disclaimer | Accept Patch Management disclaimer 


# **v2PatchManagementAcceptDisclaimerPost**
```swift
    open class func v2PatchManagementAcceptDisclaimerPost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Accept Patch Management disclaimer 

Accept Patch Management disclaimer

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Accept Patch Management disclaimer 
PatchManagementAPI.v2PatchManagementAcceptDisclaimerPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

