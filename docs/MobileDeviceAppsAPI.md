# MobileDeviceAppsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MobileDeviceAppsReinstallAppConfigPost**](MobileDeviceAppsAPI.md#v1mobiledeviceappsreinstallappconfigpost) | **POST** /v1/mobile-device-apps/reinstall-app-config | Reinstall App Config for Managed iOS Apps 


# **v1MobileDeviceAppsReinstallAppConfigPost**
```swift
    open class func v1MobileDeviceAppsReinstallAppConfigPost(appConfigReinstallCode: AppConfigReinstallCode, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Reinstall App Config for Managed iOS Apps 

Redeploys the managed app configuration for a specific app on a specific device using the $APP_CONFIG_REINSTALL_CODE generated during deployment.  This endpoint does not require authorization, only the re-install code. The code does not contain any user authentication information.  For example usage, see the following Teacher app documentation: [Teacher App Manged App Configuration](https://learn.jamf.com/bundle/jamf-teacher-configuration-guide/page/Jamf_Teacher_Integration_with_Jamf_Pro.html) 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let appConfigReinstallCode = AppConfigReinstallCode(reinstallCode: "reinstallCode_example") // AppConfigReinstallCode | The $APP_CONFIG_REINSTALL_CODE variable for the specific device and app supplied by the managed iOS app's current App Config. 

// Reinstall App Config for Managed iOS Apps 
MobileDeviceAppsAPI.v1MobileDeviceAppsReinstallAppConfigPost(appConfigReinstallCode: appConfigReinstallCode) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appConfigReinstallCode** | [**AppConfigReinstallCode**](AppConfigReinstallCode.md) | The $APP_CONFIG_REINSTALL_CODE variable for the specific device and app supplied by the managed iOS app&#39;s current App Config.  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

