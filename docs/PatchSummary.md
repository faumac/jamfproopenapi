# PatchSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**softwareTitleId** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**latestVersion** | **String** |  | [optional] 
**releaseDate** | **Date** |  | [optional] 
**upToDate** | **Int** |  | [optional] 
**outOfDate** | **Int** |  | [optional] 
**onDashboard** | **Bool** |  | [optional] 
**softwareTitleConfigurationId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


