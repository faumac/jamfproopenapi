# SsoKeystore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keys** | [CertificateKey] |  | [optional] 
**key** | **String** |  | [default to " "]
**password** | **String** |  | 
**type** | **String** |  | 
**keystoreSetupType** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


