# SharedDeviceConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quotaSize** | **Int** |  | [optional] 
**residentUsers** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


