# MacosManagedSoftwareUpdatesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MacosManagedSoftwareUpdatesAvailableUpdatesGet**](MacosManagedSoftwareUpdatesAPI.md#v1macosmanagedsoftwareupdatesavailableupdatesget) | **GET** /v1/macos-managed-software-updates/available-updates | Retrieve available MacOs Managed Software Updates
[**v1MacosManagedSoftwareUpdatesSendUpdatesPost**](MacosManagedSoftwareUpdatesAPI.md#v1macosmanagedsoftwareupdatessendupdatespost) | **POST** /v1/macos-managed-software-updates/send-updates | Send MacOs Managed Software Updates


# **v1MacosManagedSoftwareUpdatesAvailableUpdatesGet**
```swift
    open class func v1MacosManagedSoftwareUpdatesAvailableUpdatesGet(completion: @escaping (_ data: AvailableUpdates?, _ error: Error?) -> Void)
```

Retrieve available MacOs Managed Software Updates

Retrieves available MacOs Managed Software Updates

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve available MacOs Managed Software Updates
MacosManagedSoftwareUpdatesAPI.v1MacosManagedSoftwareUpdatesAvailableUpdatesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AvailableUpdates**](AvailableUpdates.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MacosManagedSoftwareUpdatesSendUpdatesPost**
```swift
    open class func v1MacosManagedSoftwareUpdatesSendUpdatesPost(macOsManagedSoftwareUpdate: MacOsManagedSoftwareUpdate, completion: @escaping (_ data: MacOsManagedSoftwareUpdateResponse?, _ error: Error?) -> Void)
```

Send MacOs Managed Software Updates

Sends MacOs Managed Software Updates

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let macOsManagedSoftwareUpdate = MacOsManagedSoftwareUpdate(deviceIds: ["deviceIds_example"], groupId: "groupId_example", maxDeferrals: 123, version: "version_example", skipVersionVerification: false, applyMajorUpdate: false, updateAction: "updateAction_example", forceRestart: false, priority: "priority_example") // MacOsManagedSoftwareUpdate | MacOs Managed Software Update to send

// Send MacOs Managed Software Updates
MacosManagedSoftwareUpdatesAPI.v1MacosManagedSoftwareUpdatesSendUpdatesPost(macOsManagedSoftwareUpdate: macOsManagedSoftwareUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **macOsManagedSoftwareUpdate** | [**MacOsManagedSoftwareUpdate**](MacOsManagedSoftwareUpdate.md) | MacOs Managed Software Update to send | 

### Return type

[**MacOsManagedSoftwareUpdateResponse**](MacOsManagedSoftwareUpdateResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

