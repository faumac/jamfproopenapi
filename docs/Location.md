# Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**realName** | **String** |  | [optional] 
**emailAddress** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**department** | [**IdAndName**](IdAndName.md) |  | [optional] 
**building** | [**IdAndName**](IdAndName.md) |  | [optional] 
**room** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


