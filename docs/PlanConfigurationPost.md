# PlanConfigurationPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updateAction** | **String** |  | 
**versionType** | **String** |  | 
**specificVersion** | **String** | Optional. Indicates the specific version to update to. Only available when the version type is set to specific version, otherwise defaults to NO_SPECIFIC_VERSION. | [optional] [default to "NO_SPECIFIC_VERSION"]
**maxDeferrals** | **Int** | Required when the provided updateAction is DOWNLOAD_INSTALL_ALLOW_DEFERRAL, not applicable to all managed software update plans | [optional] 
**forceInstallLocalDateTime** | **String** | Optional. Indicates the local date and time of the device to force update by. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


