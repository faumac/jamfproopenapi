# MobileDeviceIosGeneral

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**udid** | **String** |  | [optional] 
**displayName** | **String** |  | [optional] 
**assetTag** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**lastInventoryUpdateDate** | **Date** |  | [optional] 
**osVersion** | **String** |  | [optional] 
**osRapidSecurityResponse** | **String** |  | [optional] 
**osBuild** | **String** |  | [optional] 
**osSupplementalBuildVersion** | **String** |  | [optional] 
**softwareUpdateDeviceId** | **String** |  | [optional] 
**ipAddress** | **String** |  | [optional] 
**managed** | **Bool** |  | [optional] 
**supervised** | **Bool** |  | [optional] 
**deviceOwnershipType** | **String** |  | [optional] 
**enrollmentMethodPrestage** | [**EnrollmentMethodPrestage**](EnrollmentMethodPrestage.md) |  | [optional] 
**enrollmentSessionTokenValid** | **Bool** |  | [optional] 
**lastEnrolledDate** | **Date** |  | [optional] 
**mdmProfileExpirationDate** | **Date** |  | [optional] 
**timeZone** | **String** | IANA time zone database name | [optional] 
**declarativeDeviceManagementEnabled** | **Bool** |  | [optional] 
**extensionAttributes** | [MobileDeviceExtensionAttribute] |  | [optional] 
**sharedIpad** | **Bool** |  | [optional] 
**diagnosticAndUsageReportingEnabled** | **Bool** |  | [optional] 
**appAnalyticsEnabled** | **Bool** |  | [optional] 
**residentUsers** | **Int** |  | [optional] 
**quotaSize** | **Int** |  | [optional] 
**temporarySessionOnly** | **Bool** |  | [optional] 
**temporarySessionTimeout** | **Int** |  | [optional] 
**userSessionTimeout** | **Int** |  | [optional] 
**syncedToComputer** | **Int** |  | [optional] 
**maximumSharediPadUsersStored** | **Int** |  | [optional] 
**lastBackupDate** | **Date** |  | [optional] 
**deviceLocatorServiceEnabled** | **Bool** |  | [optional] 
**doNotDisturbEnabled** | **Bool** |  | [optional] 
**cloudBackupEnabled** | **Bool** |  | [optional] 
**lastCloudBackupDate** | **Date** |  | [optional] 
**locationServicesForSelfServiceMobileEnabled** | **Bool** |  | [optional] 
**itunesStoreAccountActive** | **Bool** |  | [optional] 
**exchangeDeviceId** | **String** |  | [optional] 
**tethered** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


