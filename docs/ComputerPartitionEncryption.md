# ComputerPartitionEncryption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partitionName** | **String** |  | [optional] 
**partitionFileVault2State** | [**ComputerPartitionFileVault2State**](ComputerPartitionFileVault2State.md) |  | [optional] 
**partitionFileVault2Percent** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


