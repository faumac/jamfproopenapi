# EnrollmentCustomizationBrandingSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**textColor** | **String** |  | 
**buttonColor** | **String** |  | 
**buttonTextColor** | **String** |  | 
**backgroundColor** | **String** |  | 
**iconUrl** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


