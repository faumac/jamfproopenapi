# IosBrandingConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**brandingName** | **String** |  | 
**iconId** | **Int** |  | [optional] 
**headerBackgroundColorCode** | **String** |  | 
**menuIconColorCode** | **String** |  | 
**brandingNameColorCode** | **String** |  | 
**statusBarTextColor** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


