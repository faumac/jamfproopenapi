# PrestagePurchasingInformationV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**leased** | **Bool** |  | 
**purchased** | **Bool** |  | 
**appleCareId** | **String** |  | 
**poNumber** | **String** |  | 
**vendor** | **String** |  | 
**purchasePrice** | **String** |  | 
**lifeExpectancy** | **Int** |  | 
**purchasingAccount** | **String** |  | 
**purchasingContact** | **String** |  | 
**leaseDate** | **String** |  | 
**poDate** | **String** |  | 
**warrantyDate** | **String** |  | 
**versionLock** | **Int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


