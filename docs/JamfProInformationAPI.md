# JamfProInformationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfProInformationGet**](JamfProInformationAPI.md#v1jamfproinformationget) | **GET** /v1/jamf-pro-information | Get basic information about the Jamf Pro Server 
[**v2JamfProInformationGet**](JamfProInformationAPI.md#v2jamfproinformationget) | **GET** /v2/jamf-pro-information | Get basic information about the Jamf Pro Server 


# **v1JamfProInformationGet**
```swift
    open class func v1JamfProInformationGet(completion: @escaping (_ data: JamfProInformation?, _ error: Error?) -> Void)
```

Get basic information about the Jamf Pro Server 

Deprecated version of the endpoint. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get basic information about the Jamf Pro Server 
JamfProInformationAPI.v1JamfProInformationGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**JamfProInformation**](JamfProInformation.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2JamfProInformationGet**
```swift
    open class func v2JamfProInformationGet(completion: @escaping (_ data: JamfProInformationV2?, _ error: Error?) -> Void)
```

Get basic information about the Jamf Pro Server 

Get basic information about the Jamf Pro Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get basic information about the Jamf Pro Server 
JamfProInformationAPI.v2JamfProInformationGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**JamfProInformationV2**](JamfProInformationV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

