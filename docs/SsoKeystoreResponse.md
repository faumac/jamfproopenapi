# SsoKeystoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | [optional] [default to " "]
**keys** | [CertificateKey] |  | [optional] 
**type** | **String** |  | [optional] 
**keystoreSetupType** | **String** |  | [optional] 
**keystoreFileName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


