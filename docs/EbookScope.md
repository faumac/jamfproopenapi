# EbookScope

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allComputers** | **Bool** |  | [optional] 
**allMobileDevices** | **Bool** |  | [optional] 
**allUsers** | **Bool** |  | [optional] 
**computerIds** | **[String]** |  | [optional] 
**computerGroupIds** | **[String]** |  | [optional] 
**mobileDeviceIds** | **[String]** |  | [optional] 
**mobileDeviceGroupIds** | **[String]** |  | [optional] 
**buildingIds** | **[String]** |  | [optional] 
**departmentIds** | **[String]** |  | [optional] 
**userIds** | **[String]** |  | [optional] 
**userGroupIds** | **[String]** |  | [optional] 
**classroomIds** | **[String]** |  | [optional] 
**limitations** | [**EbookLimitations**](EbookLimitations.md) |  | [optional] 
**exclusions** | [**EbookExclusions**](EbookExclusions.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


