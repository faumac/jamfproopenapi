# UpdateMobileDeviceV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Mobile Device Name. When updated, Jamf Pro sends an MDM settings command to the device (device must be supervised). | [optional] 
**enforceName** | **Bool** | Enforce the mobile device name. Device must be supervised. If set to true, Jamf Pro will revert the Mobile Device Name to the ‘name’ value each time the device checks in. | [optional] 
**assetTag** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**timeZone** | **String** | IANA time zone database name | [optional] 
**location** | [**LocationV2**](LocationV2.md) |  | [optional] 
**updatedExtensionAttributes** | [ExtensionAttributeV2] |  | [optional] 
**ios** | [**UpdateIosV2**](UpdateIosV2.md) |  | [optional] 
**tvos** | [**UpdateTvOs**](UpdateTvOs.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


