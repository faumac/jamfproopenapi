# SsoSettingsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SsoDependenciesGet**](SsoSettingsAPI.md#v1ssodependenciesget) | **GET** /v1/sso/dependencies | Retrieve the list of Enrollment Customizations using SSO 
[**v1SsoDisablePost**](SsoSettingsAPI.md#v1ssodisablepost) | **POST** /v1/sso/disable | Disable SSO 
[**v1SsoGet**](SsoSettingsAPI.md#v1ssoget) | **GET** /v1/sso | Retrieve the current Single Sign On configuration settings 
[**v1SsoHistoryGet**](SsoSettingsAPI.md#v1ssohistoryget) | **GET** /v1/sso/history | Get SSO history object 
[**v1SsoHistoryPost**](SsoSettingsAPI.md#v1ssohistorypost) | **POST** /v1/sso/history | Add SSO history object notes 
[**v1SsoMetadataDownloadGet**](SsoSettingsAPI.md#v1ssometadatadownloadget) | **GET** /v1/sso/metadata/download | Download the Jamf Pro SAML metadata file 
[**v1SsoPut**](SsoSettingsAPI.md#v1ssoput) | **PUT** /v1/sso | Updates the current Single Sign On configuration settings 
[**v1SsoValidatePost**](SsoSettingsAPI.md#v1ssovalidatepost) | **POST** /v1/sso/validate | Endpoint for validation of a saml metadata url 


# **v1SsoDependenciesGet**
```swift
    open class func v1SsoDependenciesGet(completion: @escaping (_ data: EnrollmentCustomizationDependencies?, _ error: Error?) -> Void)
```

Retrieve the list of Enrollment Customizations using SSO 

Retrieves the list of Enrollment Customizations using SSO

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the list of Enrollment Customizations using SSO 
SsoSettingsAPI.v1SsoDependenciesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EnrollmentCustomizationDependencies**](EnrollmentCustomizationDependencies.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoDisablePost**
```swift
    open class func v1SsoDisablePost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Disable SSO 

Disable SSO

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Disable SSO 
SsoSettingsAPI.v1SsoDisablePost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoGet**
```swift
    open class func v1SsoGet(completion: @escaping (_ data: SsoSettings?, _ error: Error?) -> Void)
```

Retrieve the current Single Sign On configuration settings 

Retrieves the current Single Sign On configuration settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the current Single Sign On configuration settings 
SsoSettingsAPI.v1SsoGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoSettings**](SsoSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoHistoryGet**
```swift
    open class func v1SsoHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get SSO history object 

Gets SSO history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get SSO history object 
SsoSettingsAPI.v1SsoHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoHistoryPost**
```swift
    open class func v1SsoHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add SSO history object notes 

Adds SSO history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add SSO history object notes 
SsoSettingsAPI.v1SsoHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoMetadataDownloadGet**
```swift
    open class func v1SsoMetadataDownloadGet(completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download the Jamf Pro SAML metadata file 

Download the Jamf Pro SAML metadata file

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Download the Jamf Pro SAML metadata file 
SsoSettingsAPI.v1SsoMetadataDownloadGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoPut**
```swift
    open class func v1SsoPut(ssoSettings: SsoSettings, completion: @escaping (_ data: SsoSettings?, _ error: Error?) -> Void)
```

Updates the current Single Sign On configuration settings 

Updates the current Single Sign On configuration settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ssoSettings = SsoSettings(ssoForEnrollmentEnabled: false, ssoBypassAllowed: false, ssoEnabled: false, ssoForMacOsSelfServiceEnabled: false, tokenExpirationDisabled: false, userAttributeEnabled: false, userAttributeName: "userAttributeName_example", userMapping: "userMapping_example", enrollmentSsoForAdueEnabled: false, enrollmentSsoConfig: EnrollmentSsoConfig(hosts: ["hosts_example"], managementHint: "managementHint_example"), groupEnrollmentAccessEnabled: false, groupAttributeName: "groupAttributeName_example", groupRdnKey: "groupRdnKey_example", groupEnrollmentAccessName: "groupEnrollmentAccessName_example", idpProviderType: "idpProviderType_example", idpUrl: "idpUrl_example", entityId: "entityId_example", metadataFileName: "metadataFileName_example", otherProviderTypeName: "otherProviderTypeName_example", federationMetadataFile: 123, metadataSource: "metadataSource_example", sessionTimeout: 123) // SsoSettings | 

// Updates the current Single Sign On configuration settings 
SsoSettingsAPI.v1SsoPut(ssoSettings: ssoSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssoSettings** | [**SsoSettings**](SsoSettings.md) |  | 

### Return type

[**SsoSettings**](SsoSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoValidatePost**
```swift
    open class func v1SsoValidatePost(ssoMetadataUrl: SsoMetadataUrl, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Endpoint for validation of a saml metadata url 

Validation of a content available under provided metadata URL.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ssoMetadataUrl = SsoMetadataUrl(idpUrl: "idpUrl_example") // SsoMetadataUrl | url to validate

// Endpoint for validation of a saml metadata url 
SsoSettingsAPI.v1SsoValidatePost(ssoMetadataUrl: ssoMetadataUrl) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssoMetadataUrl** | [**SsoMetadataUrl**](SsoMetadataUrl.md) | url to validate | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

