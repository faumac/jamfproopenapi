# ComputerHardwareUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**networkAdapterType** | **String** |  | [optional] 
**macAddress** | **String** |  | [optional] 
**altNetworkAdapterType** | **String** |  | [optional] 
**altMacAddress** | **String** |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


