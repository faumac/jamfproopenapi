# CloudLdapMappingsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userMappings** | [**UserMappings**](UserMappings.md) |  | 
**groupMappings** | [**GroupMappings**](GroupMappings.md) |  | 
**membershipMappings** | [**MembershipMappings**](MembershipMappings.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


