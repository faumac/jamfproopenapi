# PatchSoftwareTitleConfigurationPatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | [optional] 
**categoryId** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 
**uiNotifications** | **Bool** |  | [optional] 
**emailNotifications** | **Bool** |  | [optional] 
**softwareTitleId** | **String** |  | [optional] 
**packages** | [PatchSoftwareTitlePackages] |  | [optional] 
**extensionAttributes** | [PatchSoftwareTitleConfigurationExtensionAttributes] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


