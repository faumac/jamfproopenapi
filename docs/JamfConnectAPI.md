# JamfConnectAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfConnectConfigProfilesGet**](JamfConnectAPI.md#v1jamfconnectconfigprofilesget) | **GET** /v1/jamf-connect/config-profiles | Search for config profiles linked to Jamf Connect 
[**v1JamfConnectConfigProfilesIdPut**](JamfConnectAPI.md#v1jamfconnectconfigprofilesidput) | **PUT** /v1/jamf-connect/config-profiles/{id} | Update the way the Jamf Connect app gets updated on computers within scope of the associated configuration profile. 
[**v1JamfConnectDeploymentsIdTasksGet**](JamfConnectAPI.md#v1jamfconnectdeploymentsidtasksget) | **GET** /v1/jamf-connect/deployments/{id}/tasks | Search for deployment tasks for a config profile linked to Jamf Connect 
[**v1JamfConnectDeploymentsIdTasksRetryPost**](JamfConnectAPI.md#v1jamfconnectdeploymentsidtasksretrypost) | **POST** /v1/jamf-connect/deployments/{id}/tasks/retry | Request a retry of Connect install tasks 
[**v1JamfConnectGet**](JamfConnectAPI.md#v1jamfconnectget) | **GET** /v1/jamf-connect | Get the Jamf Connect settings that you have access to see 
[**v1JamfConnectHistoryGet**](JamfConnectAPI.md#v1jamfconnecthistoryget) | **GET** /v1/jamf-connect/history | Get Jamf Connect history 
[**v1JamfConnectHistoryPost**](JamfConnectAPI.md#v1jamfconnecthistorypost) | **POST** /v1/jamf-connect/history | Add Jamf Connect history notes 


# **v1JamfConnectConfigProfilesGet**
```swift
    open class func v1JamfConnectConfigProfilesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: LinkedConnectProfileSearchResults?, _ error: Error?) -> Void)
```

Search for config profiles linked to Jamf Connect 

Search for config profiles linked to Jamf Connect

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Search for config profiles linked to Jamf Connect 
JamfConnectAPI.v1JamfConnectConfigProfilesGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**LinkedConnectProfileSearchResults**](LinkedConnectProfileSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfConnectConfigProfilesIdPut**
```swift
    open class func v1JamfConnectConfigProfilesIdPut(id: String, linkedConnectProfile: LinkedConnectProfile? = nil, completion: @escaping (_ data: LinkedConnectProfile?, _ error: Error?) -> Void)
```

Update the way the Jamf Connect app gets updated on computers within scope of the associated configuration profile. 

Update the way the Jamf Connect app gets updated on computers within scope of the associated configuration profile.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | the UUID of the profile to update
let linkedConnectProfile = LinkedConnectProfile(uuid: "uuid_example", profileId: "profileId_example", profileName: "profileName_example", profileScopeDescription: "profileScopeDescription_example", version: "version_example", autoDeploymentType: "autoDeploymentType_example") // LinkedConnectProfile | Updatable Jamf Connect Settings (optional)

// Update the way the Jamf Connect app gets updated on computers within scope of the associated configuration profile. 
JamfConnectAPI.v1JamfConnectConfigProfilesIdPut(id: id, linkedConnectProfile: linkedConnectProfile) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | the UUID of the profile to update | 
 **linkedConnectProfile** | [**LinkedConnectProfile**](LinkedConnectProfile.md) | Updatable Jamf Connect Settings | [optional] 

### Return type

[**LinkedConnectProfile**](LinkedConnectProfile.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfConnectDeploymentsIdTasksGet**
```swift
    open class func v1JamfConnectDeploymentsIdTasksGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: DeploymentTaskSearchResults?, _ error: Error?) -> Void)
```

Search for deployment tasks for a config profile linked to Jamf Connect 

Search for config profiles linked to Jamf Connect

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | the UUID of the Jamf Connect deployment
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Search for deployment tasks for a config profile linked to Jamf Connect 
JamfConnectAPI.v1JamfConnectDeploymentsIdTasksGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | the UUID of the Jamf Connect deployment | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**DeploymentTaskSearchResults**](DeploymentTaskSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfConnectDeploymentsIdTasksRetryPost**
```swift
    open class func v1JamfConnectDeploymentsIdTasksRetryPost(id: String, ids: Ids, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Request a retry of Connect install tasks 

Request a retry of Connect install tasks 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | the UUID of the deployment associated with the retry
let ids = Ids(ids: ["ids_example"]) // Ids | task IDs to retry

// Request a retry of Connect install tasks 
JamfConnectAPI.v1JamfConnectDeploymentsIdTasksRetryPost(id: id, ids: ids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | the UUID of the deployment associated with the retry | 
 **ids** | [**Ids**](Ids.md) | task IDs to retry | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfConnectGet**
```swift
    open class func v1JamfConnectGet(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Get the Jamf Connect settings that you have access to see 

Get the Jamf Connect settings that you have access to see.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the Jamf Connect settings that you have access to see 
JamfConnectAPI.v1JamfConnectGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfConnectHistoryGet**
```swift
    open class func v1JamfConnectHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Jamf Connect history 

Get Jamf Connect history 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Jamf Connect history 
JamfConnectAPI.v1JamfConnectHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1JamfConnectHistoryPost**
```swift
    open class func v1JamfConnectHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Jamf Connect history notes 

Add Jamf Connect history notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Jamf Connect history notes 
JamfConnectAPI.v1JamfConnectHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

