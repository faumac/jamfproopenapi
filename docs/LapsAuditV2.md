# LapsAuditV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**viewedBy** | **String** |  | [optional] 
**dateSeen** | **Date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


