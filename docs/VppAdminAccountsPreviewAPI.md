# VppAdminAccountsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vppAdminAccountsGet**](VppAdminAccountsPreviewAPI.md#vppadminaccountsget) | **GET** /vpp/admin-accounts | Found all VPP Admin Accounts 


# **vppAdminAccountsGet**
```swift
    open class func vppAdminAccountsGet(completion: @escaping (_ data: [VppAdminAccount]?, _ error: Error?) -> Void)
```

Found all VPP Admin Accounts 

Found all vpp admin accounts. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Found all VPP Admin Accounts 
VppAdminAccountsPreviewAPI.vppAdminAccountsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[VppAdminAccount]**](VppAdminAccount.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

