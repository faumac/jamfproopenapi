# AssignRemoveProfileResponseSyncState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**profileUUID** | **String** |  | [optional] 
**syncStatus** | **String** |  | [optional] 
**failureCount** | **Int** |  | [optional] 
**timestamp** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


