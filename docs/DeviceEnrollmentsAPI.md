# DeviceEnrollmentsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1DeviceEnrollmentsGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsget) | **GET** /v1/device-enrollments | Read all sorted and paged Device Enrollment instances 
[**v1DeviceEnrollmentsIdDelete**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsiddelete) | **DELETE** /v1/device-enrollments/{id} | Delete a Device Enrollment Instance with the supplied id 
[**v1DeviceEnrollmentsIdDisownPost**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsiddisownpost) | **POST** /v1/device-enrollments/{id}/disown | Disown devices from the given Device Enrollment Instance 
[**v1DeviceEnrollmentsIdGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsidget) | **GET** /v1/device-enrollments/{id} | Retrieve a Device Enrollment Instance with the supplied id 
[**v1DeviceEnrollmentsIdHistoryGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsidhistoryget) | **GET** /v1/device-enrollments/{id}/history | Get sorted and paged Device Enrollment history objects 
[**v1DeviceEnrollmentsIdHistoryPost**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsidhistorypost) | **POST** /v1/device-enrollments/{id}/history | Add Device Enrollment history object notes 
[**v1DeviceEnrollmentsIdPut**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsidput) | **PUT** /v1/device-enrollments/{id} | Update a Device Enrollment Instance with the supplied id 
[**v1DeviceEnrollmentsIdSyncsGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsidsyncsget) | **GET** /v1/device-enrollments/{id}/syncs | Get all instance sync states for a single Device Enrollment Instance 
[**v1DeviceEnrollmentsIdSyncsLatestGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsidsyncslatestget) | **GET** /v1/device-enrollments/{id}/syncs/latest | Get the latest sync state for a single Device Enrollment Instance 
[**v1DeviceEnrollmentsIdUploadTokenPut**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsiduploadtokenput) | **PUT** /v1/device-enrollments/{id}/upload-token | Update a Device Enrollment Instance with the supplied Token 
[**v1DeviceEnrollmentsPublicKeyGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentspublickeyget) | **GET** /v1/device-enrollments/public-key | Retrieve the Jamf Pro Device Enrollment public key 
[**v1DeviceEnrollmentsSyncsGet**](DeviceEnrollmentsAPI.md#v1deviceenrollmentssyncsget) | **GET** /v1/device-enrollments/syncs | Get all instance sync states for all Device Enrollment Instances 
[**v1DeviceEnrollmentsUploadTokenPost**](DeviceEnrollmentsAPI.md#v1deviceenrollmentsuploadtokenpost) | **POST** /v1/device-enrollments/upload-token | Create a Device Enrollment Instance with the supplied Token 


# **v1DeviceEnrollmentsGet**
```swift
    open class func v1DeviceEnrollmentsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: DeviceEnrollmentInstanceSearchResults?, _ error: Error?) -> Void)
```

Read all sorted and paged Device Enrollment instances 

Search for sorted and paged device enrollment instances

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Read all sorted and paged Device Enrollment instances 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**DeviceEnrollmentInstanceSearchResults**](DeviceEnrollmentInstanceSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdDelete**
```swift
    open class func v1DeviceEnrollmentsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Device Enrollment Instance with the supplied id 

Deletes a Device Enrollment Instance with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier

// Delete a Device Enrollment Instance with the supplied id 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdDisownPost**
```swift
    open class func v1DeviceEnrollmentsIdDisownPost(id: String, deviceEnrollmentDisownBody: DeviceEnrollmentDisownBody? = nil, completion: @escaping (_ data: DeviceEnrollmentDisownResponse?, _ error: Error?) -> Void)
```

Disown devices from the given Device Enrollment Instance 

Disowns devices from the given device enrollment instance

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier
let deviceEnrollmentDisownBody = DeviceEnrollmentDisownBody(devices: ["devices_example"]) // DeviceEnrollmentDisownBody | List of device serial numbers to disown (optional)

// Disown devices from the given Device Enrollment Instance 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdDisownPost(id: id, deviceEnrollmentDisownBody: deviceEnrollmentDisownBody) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 
 **deviceEnrollmentDisownBody** | [**DeviceEnrollmentDisownBody**](DeviceEnrollmentDisownBody.md) | List of device serial numbers to disown | [optional] 

### Return type

[**DeviceEnrollmentDisownResponse**](DeviceEnrollmentDisownResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdGet**
```swift
    open class func v1DeviceEnrollmentsIdGet(id: String, completion: @escaping (_ data: DeviceEnrollmentInstance?, _ error: Error?) -> Void)
```

Retrieve a Device Enrollment Instance with the supplied id 

Retrieves a Device Enrollment Instance with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier

// Retrieve a Device Enrollment Instance with the supplied id 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 

### Return type

[**DeviceEnrollmentInstance**](DeviceEnrollmentInstance.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdHistoryGet**
```swift
    open class func v1DeviceEnrollmentsIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get sorted and paged Device Enrollment history objects 

Gets sorted and paged device enrollment history objects

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is duplicated for each sort criterion, e.g., ...&sort=name%2Casc&sort=date%2Cdesc (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default search is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: search=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get sorted and paged Device Enrollment history objects 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property,asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name%2Casc&amp;sort&#x3D;date%2Cdesc | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default search is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: search&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdHistoryPost**
```swift
    open class func v1DeviceEnrollmentsIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Device Enrollment history object notes 

Adds device enrollment history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Device Enrollment history object notes 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdPut**
```swift
    open class func v1DeviceEnrollmentsIdPut(id: String, deviceEnrollmentInstance: DeviceEnrollmentInstance, completion: @escaping (_ data: DeviceEnrollmentInstance?, _ error: Error?) -> Void)
```

Update a Device Enrollment Instance with the supplied id 

Updates a Device Enrollment Instance with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier
let deviceEnrollmentInstance = DeviceEnrollmentInstance(id: "id_example", name: "name_example", supervisionIdentityId: "supervisionIdentityId_example", siteId: "siteId_example", serverName: "serverName_example", serverUuid: "serverUuid_example", adminId: "adminId_example", orgName: "orgName_example", orgEmail: "orgEmail_example", orgPhone: "orgPhone_example", orgAddress: "orgAddress_example", tokenExpirationDate: "tokenExpirationDate_example") // DeviceEnrollmentInstance | 

// Update a Device Enrollment Instance with the supplied id 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdPut(id: id, deviceEnrollmentInstance: deviceEnrollmentInstance) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 
 **deviceEnrollmentInstance** | [**DeviceEnrollmentInstance**](DeviceEnrollmentInstance.md) |  | 

### Return type

[**DeviceEnrollmentInstance**](DeviceEnrollmentInstance.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdSyncsGet**
```swift
    open class func v1DeviceEnrollmentsIdSyncsGet(id: String, completion: @escaping (_ data: [DeviceEnrollmentInstanceSyncStatus]?, _ error: Error?) -> Void)
```

Get all instance sync states for a single Device Enrollment Instance 

Get all instance sync states for a single instance

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier

// Get all instance sync states for a single Device Enrollment Instance 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdSyncsGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 

### Return type

[**[DeviceEnrollmentInstanceSyncStatus]**](DeviceEnrollmentInstanceSyncStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdSyncsLatestGet**
```swift
    open class func v1DeviceEnrollmentsIdSyncsLatestGet(id: String, completion: @escaping (_ data: DeviceEnrollmentInstanceSyncStatus?, _ error: Error?) -> Void)
```

Get the latest sync state for a single Device Enrollment Instance 

Get the latest sync state for a single instance

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier

// Get the latest sync state for a single Device Enrollment Instance 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdSyncsLatestGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 

### Return type

[**DeviceEnrollmentInstanceSyncStatus**](DeviceEnrollmentInstanceSyncStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsIdUploadTokenPut**
```swift
    open class func v1DeviceEnrollmentsIdUploadTokenPut(id: String, deviceEnrollmentToken: DeviceEnrollmentToken, completion: @escaping (_ data: DeviceEnrollmentInstance?, _ error: Error?) -> Void)
```

Update a Device Enrollment Instance with the supplied Token 

Updates a device enrollment instance with the supplied token.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Device Enrollment Instance identifier
let deviceEnrollmentToken = DeviceEnrollmentToken(tokenFileName: "tokenFileName_example", encodedToken: 123) // DeviceEnrollmentToken | The downloaded token base 64 encoded from the MDM server to be used to create a new Device Enrollment Instance.

// Update a Device Enrollment Instance with the supplied Token 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsIdUploadTokenPut(id: id, deviceEnrollmentToken: deviceEnrollmentToken) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Device Enrollment Instance identifier | 
 **deviceEnrollmentToken** | [**DeviceEnrollmentToken**](DeviceEnrollmentToken.md) | The downloaded token base 64 encoded from the MDM server to be used to create a new Device Enrollment Instance. | 

### Return type

[**DeviceEnrollmentInstance**](DeviceEnrollmentInstance.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsPublicKeyGet**
```swift
    open class func v1DeviceEnrollmentsPublicKeyGet(completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Retrieve the Jamf Pro Device Enrollment public key 

Retrieve the Jamf Pro device enrollment public key

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the Jamf Pro Device Enrollment public key 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsPublicKeyGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-pem-file

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsSyncsGet**
```swift
    open class func v1DeviceEnrollmentsSyncsGet(completion: @escaping (_ data: [DeviceEnrollmentInstanceSyncStatus]?, _ error: Error?) -> Void)
```

Get all instance sync states for all Device Enrollment Instances 

Get all instance sync states for all instances

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all instance sync states for all Device Enrollment Instances 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsSyncsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[DeviceEnrollmentInstanceSyncStatus]**](DeviceEnrollmentInstanceSyncStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeviceEnrollmentsUploadTokenPost**
```swift
    open class func v1DeviceEnrollmentsUploadTokenPost(deviceEnrollmentToken: DeviceEnrollmentToken, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Device Enrollment Instance with the supplied Token 

Creates a device enrollment instance with the supplied token.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let deviceEnrollmentToken = DeviceEnrollmentToken(tokenFileName: "tokenFileName_example", encodedToken: 123) // DeviceEnrollmentToken | The downloaded token base 64 encoded from the MDM server to be used to create a new Device Enrollment Instance.

// Create a Device Enrollment Instance with the supplied Token 
DeviceEnrollmentsAPI.v1DeviceEnrollmentsUploadTokenPost(deviceEnrollmentToken: deviceEnrollmentToken) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceEnrollmentToken** | [**DeviceEnrollmentToken**](DeviceEnrollmentToken.md) | The downloaded token base 64 encoded from the MDM server to be used to create a new Device Enrollment Instance. | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

