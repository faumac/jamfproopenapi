# ComputerPartition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] [readonly] 
**sizeMegabytes** | **Int64** | Partition Size in MB. | [optional] [readonly] 
**availableMegabytes** | **Int64** | Available space in MB. | [optional] [readonly] 
**partitionType** | **String** |  | [optional] [readonly] 
**percentUsed** | **Int** | Percentage of space used. | [optional] [readonly] 
**fileVault2State** | [**ComputerPartitionFileVault2State**](ComputerPartitionFileVault2State.md) |  | [optional] 
**fileVault2ProgressPercent** | **Int** | Percentage progress of current FileVault 2 operation. | [optional] 
**lvmManaged** | **Bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


