# PatchPolicyDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**enabled** | **Bool** |  | [optional] 
**targetPatchVersion** | **String** |  | [optional] 
**deploymentMethod** | **String** |  | [optional] 
**softwareTitleId** | **String** |  | [optional] 
**softwareTitleConfigurationId** | **String** |  | [optional] 
**killAppsDelayMinutes** | **Int** |  | [optional] 
**killAppsMessage** | **String** |  | [optional] 
**downgrade** | **Bool** |  | [optional] 
**patchUnknownVersion** | **Bool** |  | [optional] 
**notificationHeader** | **String** |  | [optional] 
**selfServiceEnforceDeadline** | **Bool** |  | [optional] 
**selfServiceDeadline** | **Int** |  | [optional] 
**installButtonText** | **String** |  | [optional] 
**selfServiceDescription** | **String** |  | [optional] 
**iconId** | **String** |  | [optional] 
**reminderFrequency** | **Int** |  | [optional] 
**reminderEnabled** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


