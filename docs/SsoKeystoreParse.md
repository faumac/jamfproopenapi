# SsoKeystoreParse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keystorePassword** | **String** |  | 
**keystoreFile** | **Data** |  | 
**keystoreFileName** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


