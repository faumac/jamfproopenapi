# SitesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SitesGet**](SitesAPI.md#v1sitesget) | **GET** /v1/sites | Find all sites 


# **v1SitesGet**
```swift
    open class func v1SitesGet(completion: @escaping (_ data: [V1Site]?, _ error: Error?) -> Void)
```

Find all sites 

Find all sites. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Find all sites 
SitesAPI.v1SitesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[V1Site]**](V1Site.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

