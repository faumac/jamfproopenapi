# GroupMappings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**objectClassLimitation** | **String** |  | 
**objectClasses** | **String** |  | 
**searchBase** | **String** |  | 
**searchScope** | **String** |  | 
**groupID** | **String** |  | 
**groupName** | **String** |  | 
**groupUuid** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


