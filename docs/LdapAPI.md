# LdapAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ldapGroupsGet**](LdapAPI.md#ldapgroupsget) | **GET** /ldap/groups | Retrieve the configured access groups that contain the text in the search param 
[**ldapServersGet**](LdapAPI.md#ldapserversget) | **GET** /ldap/servers | Retrieve all Servers including LDAP and Cloud Identity Providers. 
[**v1LdapGroupsGet**](LdapAPI.md#v1ldapgroupsget) | **GET** /v1/ldap/groups | Retrieve the configured access groups that contain the text in the search param 
[**v1LdapLdapServersGet**](LdapAPI.md#v1ldapldapserversget) | **GET** /v1/ldap/ldap-servers | Retrieve all LDAP Servers. 
[**v1LdapServersGet**](LdapAPI.md#v1ldapserversget) | **GET** /v1/ldap/servers | Retrieve all Servers including LDAP and Cloud Identity Providers. 


# **ldapGroupsGet**
```swift
    open class func ldapGroupsGet(q: String? = nil, completion: @escaping (_ data: LdapGroupSearchResults?, _ error: Error?) -> Void)
```

Retrieve the configured access groups that contain the text in the search param 

Retrieves the configured access groups that contain the text in the searchParam.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let q = "q_example" // String | Will perform a \"contains\" search on the names of access groups (optional) (default to "null")

// Retrieve the configured access groups that contain the text in the search param 
LdapAPI.ldapGroupsGet(q: q) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String** | Will perform a \&quot;contains\&quot; search on the names of access groups | [optional] [default to &quot;null&quot;]

### Return type

[**LdapGroupSearchResults**](LdapGroupSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ldapServersGet**
```swift
    open class func ldapServersGet(completion: @escaping (_ data: [LdapServer]?, _ error: Error?) -> Void)
```

Retrieve all Servers including LDAP and Cloud Identity Providers. 

Retrieve all Servers including LDAP and Cloud Identity Providers.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve all Servers including LDAP and Cloud Identity Providers. 
LdapAPI.ldapServersGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LdapServer]**](LdapServer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LdapGroupsGet**
```swift
    open class func v1LdapGroupsGet(q: String? = nil, completion: @escaping (_ data: LdapGroupSearchResults?, _ error: Error?) -> Void)
```

Retrieve the configured access groups that contain the text in the search param 

Retrieves the configured access groups that contain the text in the searchParam.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let q = "q_example" // String | Will perform a \"contains\" search on the names of access groups (optional) (default to "null")

// Retrieve the configured access groups that contain the text in the search param 
LdapAPI.v1LdapGroupsGet(q: q) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String** | Will perform a \&quot;contains\&quot; search on the names of access groups | [optional] [default to &quot;null&quot;]

### Return type

[**LdapGroupSearchResults**](LdapGroupSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LdapLdapServersGet**
```swift
    open class func v1LdapLdapServersGet(completion: @escaping (_ data: [LdapServer]?, _ error: Error?) -> Void)
```

Retrieve all LDAP Servers. 

Retrieves all not migrated, LDAP Servers.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve all LDAP Servers. 
LdapAPI.v1LdapLdapServersGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LdapServer]**](LdapServer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1LdapServersGet**
```swift
    open class func v1LdapServersGet(completion: @escaping (_ data: [LdapServer]?, _ error: Error?) -> Void)
```

Retrieve all Servers including LDAP and Cloud Identity Providers. 

Retrieve all active Servers including LDAP and Cloud Identity Providers.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve all Servers including LDAP and Cloud Identity Providers. 
LdapAPI.v1LdapServersGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LdapServer]**](LdapServer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

