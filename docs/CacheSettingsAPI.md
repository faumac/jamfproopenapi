# CacheSettingsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1CacheSettingsGet**](CacheSettingsAPI.md#v1cachesettingsget) | **GET** /v1/cache-settings | Get Cache Settings 
[**v1CacheSettingsPut**](CacheSettingsAPI.md#v1cachesettingsput) | **PUT** /v1/cache-settings | Update Cache Settings 


# **v1CacheSettingsGet**
```swift
    open class func v1CacheSettingsGet(completion: @escaping (_ data: CacheSettings?, _ error: Error?) -> Void)
```

Get Cache Settings 

gets cache settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Cache Settings 
CacheSettingsAPI.v1CacheSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CacheSettings**](CacheSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CacheSettingsPut**
```swift
    open class func v1CacheSettingsPut(cacheSettings: CacheSettings, completion: @escaping (_ data: CacheSettings?, _ error: Error?) -> Void)
```

Update Cache Settings 

updates cache settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let cacheSettings = CacheSettings(id: "id_example", name: "name_example", cacheType: "cacheType_example", timeToLiveSeconds: 123, timeToIdleSeconds: 123, directoryTimeToLiveSeconds: 123, ehcacheMaxBytesLocalHeap: "ehcacheMaxBytesLocalHeap_example", cacheUniqueId: "cacheUniqueId_example", elasticache: false, memcachedEndpoints: [MemcachedEndpoints(id: "id_example", name: "name_example", hostName: "hostName_example", port: 123, enabled: true, jssCacheConfigurationId: 123)]) // CacheSettings | 

// Update Cache Settings 
CacheSettingsAPI.v1CacheSettingsPut(cacheSettings: cacheSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cacheSettings** | [**CacheSettings**](CacheSettings.md) |  | 

### Return type

[**CacheSettings**](CacheSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

