# ComputerInventoryCollectionSettingsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ComputerInventoryCollectionSettingsCustomPathIdDelete**](ComputerInventoryCollectionSettingsAPI.md#v1computerinventorycollectionsettingscustompathiddelete) | **DELETE** /v1/computer-inventory-collection-settings/custom-path/{id} | Delete Custom Path from Computer Inventory Collection Settings
[**v1ComputerInventoryCollectionSettingsCustomPathPost**](ComputerInventoryCollectionSettingsAPI.md#v1computerinventorycollectionsettingscustompathpost) | **POST** /v1/computer-inventory-collection-settings/custom-path | Create Computer Inventory Collection Settings Custom Path
[**v1ComputerInventoryCollectionSettingsGet**](ComputerInventoryCollectionSettingsAPI.md#v1computerinventorycollectionsettingsget) | **GET** /v1/computer-inventory-collection-settings | Returns computer inventory settings
[**v1ComputerInventoryCollectionSettingsPatch**](ComputerInventoryCollectionSettingsAPI.md#v1computerinventorycollectionsettingspatch) | **PATCH** /v1/computer-inventory-collection-settings | Update computer inventory settings


# **v1ComputerInventoryCollectionSettingsCustomPathIdDelete**
```swift
    open class func v1ComputerInventoryCollectionSettingsCustomPathIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Custom Path from Computer Inventory Collection Settings

Delete Custom Path from Computer Inventory Collection Settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of Custom Path

// Delete Custom Path from Computer Inventory Collection Settings
ComputerInventoryCollectionSettingsAPI.v1ComputerInventoryCollectionSettingsCustomPathIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of Custom Path | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerInventoryCollectionSettingsCustomPathPost**
```swift
    open class func v1ComputerInventoryCollectionSettingsCustomPathPost(createPath: CreatePath, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Computer Inventory Collection Settings Custom Path

Creates a custom search path to use when collecting applications, fonts, and plug-ins.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let createPath = CreatePath(scope: "scope_example", path: "path_example") // CreatePath | Computer inventory settings to update

// Create Computer Inventory Collection Settings Custom Path
ComputerInventoryCollectionSettingsAPI.v1ComputerInventoryCollectionSettingsCustomPathPost(createPath: createPath) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createPath** | [**CreatePath**](CreatePath.md) | Computer inventory settings to update | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerInventoryCollectionSettingsGet**
```swift
    open class func v1ComputerInventoryCollectionSettingsGet(completion: @escaping (_ data: ComputerInventoryCollectionSettings?, _ error: Error?) -> Void)
```

Returns computer inventory settings

Returns computer inventory settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Returns computer inventory settings
ComputerInventoryCollectionSettingsAPI.v1ComputerInventoryCollectionSettingsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ComputerInventoryCollectionSettings**](ComputerInventoryCollectionSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerInventoryCollectionSettingsPatch**
```swift
    open class func v1ComputerInventoryCollectionSettingsPatch(computerInventoryCollectionSettings: ComputerInventoryCollectionSettings, completion: @escaping (_ data: ComputerInventoryCollectionSettings?, _ error: Error?) -> Void)
```

Update computer inventory settings

Update computer inventory settings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let computerInventoryCollectionSettings = ComputerInventoryCollectionSettings(computerInventoryCollectionPreferences: ComputerInventoryCollectionPreferences(monitorApplicationUsage: true, includeFonts: true, includePlugins: true, includePackages: true, includeSoftwareUpdates: true, includeAccounts: true, calculateSizes: false, includeHiddenAccounts: true, includePrinters: true, includeServices: true, collectSyncedMobileDeviceInfo: false, updateLdapInfoOnComputerInventorySubmissions: false, monitorBeacons: true, allowChangingUserAndLocation: true, useUnixUserPaths: true), applicationPaths: [AppPath(id: "id_example", path: "path_example")], fontPaths: [FontPath(id: "id_example", path: "path_example")], pluginPaths: [PluginPath(id: "id_example", path: "path_example")]) // ComputerInventoryCollectionSettings | Computer inventory settings to update

// Update computer inventory settings
ComputerInventoryCollectionSettingsAPI.v1ComputerInventoryCollectionSettingsPatch(computerInventoryCollectionSettings: computerInventoryCollectionSettings) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **computerInventoryCollectionSettings** | [**ComputerInventoryCollectionSettings**](ComputerInventoryCollectionSettings.md) | Computer inventory settings to update | 

### Return type

[**ComputerInventoryCollectionSettings**](ComputerInventoryCollectionSettings.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

