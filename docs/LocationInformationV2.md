# LocationInformationV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**realname** | **String** |  | 
**phone** | **String** |  | 
**email** | **String** |  | 
**room** | **String** |  | 
**position** | **String** |  | 
**departmentId** | **String** |  | 
**buildingId** | **String** |  | 
**id** | **String** |  | 
**versionLock** | **Int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


