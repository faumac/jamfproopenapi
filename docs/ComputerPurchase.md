# ComputerPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**leased** | **Bool** |  | [optional] 
**purchased** | **Bool** |  | [optional] 
**poNumber** | **String** |  | [optional] 
**poDate** | **Date** |  | [optional] 
**vendor** | **String** |  | [optional] 
**warrantyDate** | **Date** |  | [optional] 
**appleCareId** | **String** |  | [optional] 
**leaseDate** | **Date** |  | [optional] 
**purchasePrice** | **String** |  | [optional] 
**lifeExpectancy** | **Int** |  | [optional] 
**purchasingAccount** | **String** |  | [optional] 
**purchasingContact** | **String** |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


