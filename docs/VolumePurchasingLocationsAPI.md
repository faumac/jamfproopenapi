# VolumePurchasingLocationsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1VolumePurchasingLocationsGet**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsget) | **GET** /v1/volume-purchasing-locations | Retrieve Volume Purchasing Locations
[**v1VolumePurchasingLocationsIdContentGet**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidcontentget) | **GET** /v1/volume-purchasing-locations/{id}/content | Retrieve the Volume Purchasing Content for the Volume Purchasing Location with the supplied id
[**v1VolumePurchasingLocationsIdDelete**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsiddelete) | **DELETE** /v1/volume-purchasing-locations/{id} | Delete a Volume Purchasing Location with the supplied id
[**v1VolumePurchasingLocationsIdGet**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidget) | **GET** /v1/volume-purchasing-locations/{id} | Retrieve a Volume Purchasing Location with the supplied id
[**v1VolumePurchasingLocationsIdHistoryGet**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidhistoryget) | **GET** /v1/volume-purchasing-locations/{id}/history | Get specified Volume Purchasing Location history object 
[**v1VolumePurchasingLocationsIdHistoryPost**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidhistorypost) | **POST** /v1/volume-purchasing-locations/{id}/history | Add specified Volume Purchasing Location history object notes 
[**v1VolumePurchasingLocationsIdPatch**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidpatch) | **PATCH** /v1/volume-purchasing-locations/{id} | Update a Volume Purchasing Location
[**v1VolumePurchasingLocationsIdReclaimPost**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidreclaimpost) | **POST** /v1/volume-purchasing-locations/{id}/reclaim | Reclaim a Volume Purchasing Location with the supplied id
[**v1VolumePurchasingLocationsIdRevokeLicensesPost**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationsidrevokelicensespost) | **POST** /v1/volume-purchasing-locations/{id}/revoke-licenses | Revoke licenses for a Volume Purchasing Location with the supplied id
[**v1VolumePurchasingLocationsPost**](VolumePurchasingLocationsAPI.md#v1volumepurchasinglocationspost) | **POST** /v1/volume-purchasing-locations | Create a Volume Purchasing Location


# **v1VolumePurchasingLocationsGet**
```swift
    open class func v1VolumePurchasingLocationsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: VolumePurchasingLocations?, _ error: Error?) -> Void)
```

Retrieve Volume Purchasing Locations

Retrieves Volume Purchasing Locations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Volume Purchasing Location collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name, appleId, organizationName, tokenExpiration, countryCode, locationName, automaticallyPopulatePurchasedContent, and sendNotificationWhenNoLongerAssigned. This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve Volume Purchasing Locations
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Volume Purchasing Location collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name, appleId, organizationName, tokenExpiration, countryCode, locationName, automaticallyPopulatePurchasedContent, and sendNotificationWhenNoLongerAssigned. This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**VolumePurchasingLocations**](VolumePurchasingLocations.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdContentGet**
```swift
    open class func v1VolumePurchasingLocationsIdContentGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: VolumePurchasingContentList?, _ error: Error?) -> Void)
```

Retrieve the Volume Purchasing Content for the Volume Purchasing Location with the supplied id

Retrieves the Volume Purchasing Content for the Volume Purchasing Location with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Location identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Volume Purchasing Content collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: name, licenseCountTotal, licenseCountInUse, licenseCountReported, contentType, and pricingParam. This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve the Volume Purchasing Content for the Volume Purchasing Location with the supplied id
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdContentGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Location identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Volume Purchasing Content collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: name, licenseCountTotal, licenseCountInUse, licenseCountReported, contentType, and pricingParam. This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**VolumePurchasingContentList**](VolumePurchasingContentList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdDelete**
```swift
    open class func v1VolumePurchasingLocationsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Volume Purchasing Location with the supplied id

Deletes a Volume Purchasing Location with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Location identifier

// Delete a Volume Purchasing Location with the supplied id
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Location identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdGet**
```swift
    open class func v1VolumePurchasingLocationsIdGet(id: String, completion: @escaping (_ data: VolumePurchasingLocation?, _ error: Error?) -> Void)
```

Retrieve a Volume Purchasing Location with the supplied id

Retrieves a Volume Purchasing Location with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Location identifier

// Retrieve a Volume Purchasing Location with the supplied id
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Location identifier | 

### Return type

[**VolumePurchasingLocation**](VolumePurchasingLocation.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdHistoryGet**
```swift
    open class func v1VolumePurchasingLocationsIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get specified Volume Purchasing Location history object 

Gets specified Volume Purchasing Location history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of Volume Purchasing Location history record
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma.  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get specified Volume Purchasing Location history object 
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of Volume Purchasing Location history record | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma.  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdHistoryPost**
```swift
    open class func v1VolumePurchasingLocationsIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add specified Volume Purchasing Location history object notes 

Adds specified Volume Purchasing Location history object notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of Volume Purchasing Location history record
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history note to create

// Add specified Volume Purchasing Location history object notes 
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of Volume Purchasing Location history record | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history note to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdPatch**
```swift
    open class func v1VolumePurchasingLocationsIdPatch(id: String, volumePurchasingLocationPatch: VolumePurchasingLocationPatch, completion: @escaping (_ data: VolumePurchasingLocation?, _ error: Error?) -> Void)
```

Update a Volume Purchasing Location

Updates a Volume Purchasing Location

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Location identifier
let volumePurchasingLocationPatch = VolumePurchasingLocationPatch(name: "name_example", automaticallyPopulatePurchasedContent: false, sendNotificationWhenNoLongerAssigned: false, autoRegisterManagedUsers: false, siteId: "siteId_example", serviceToken: "serviceToken_example") // VolumePurchasingLocationPatch | Volume Purchasing Location to update

// Update a Volume Purchasing Location
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdPatch(id: id, volumePurchasingLocationPatch: volumePurchasingLocationPatch) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Location identifier | 
 **volumePurchasingLocationPatch** | [**VolumePurchasingLocationPatch**](VolumePurchasingLocationPatch.md) | Volume Purchasing Location to update | 

### Return type

[**VolumePurchasingLocation**](VolumePurchasingLocation.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/merge-patch+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdReclaimPost**
```swift
    open class func v1VolumePurchasingLocationsIdReclaimPost(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Reclaim a Volume Purchasing Location with the supplied id

Reclaims a Volume Purchasing Location with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Location identifier

// Reclaim a Volume Purchasing Location with the supplied id
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdReclaimPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Location identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsIdRevokeLicensesPost**
```swift
    open class func v1VolumePurchasingLocationsIdRevokeLicensesPost(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Revoke licenses for a Volume Purchasing Location with the supplied id

Revokes licenses for a Volume Purchasing Location with the supplied id. The licenses must be revokable - any asset whose licenses are irrevocable will not be revoked.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Volume Purchasing Location identifier

// Revoke licenses for a Volume Purchasing Location with the supplied id
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsIdRevokeLicensesPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Volume Purchasing Location identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1VolumePurchasingLocationsPost**
```swift
    open class func v1VolumePurchasingLocationsPost(volumePurchasingLocationPost: VolumePurchasingLocationPost, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Volume Purchasing Location

Creates a Volume Purchasing Location using an sToken

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let volumePurchasingLocationPost = VolumePurchasingLocationPost(name: "name_example", automaticallyPopulatePurchasedContent: false, sendNotificationWhenNoLongerAssigned: false, autoRegisterManagedUsers: false, siteId: "siteId_example", serviceToken: "serviceToken_example") // VolumePurchasingLocationPost | Volume Purchasing Location to create

// Create a Volume Purchasing Location
VolumePurchasingLocationsAPI.v1VolumePurchasingLocationsPost(volumePurchasingLocationPost: volumePurchasingLocationPost) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volumePurchasingLocationPost** | [**VolumePurchasingLocationPost**](VolumePurchasingLocationPost.md) | Volume Purchasing Location to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

