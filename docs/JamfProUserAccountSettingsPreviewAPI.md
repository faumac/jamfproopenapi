# JamfProUserAccountSettingsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userObjPreferenceKeyDelete**](JamfProUserAccountSettingsPreviewAPI.md#userobjpreferencekeydelete) | **DELETE** /user/obj/preference/{key} | Remove specified setting for authenticated user 
[**userObjPreferenceKeyGet**](JamfProUserAccountSettingsPreviewAPI.md#userobjpreferencekeyget) | **GET** /user/obj/preference/{key} | Get the user setting for the authenticated user and key 
[**userObjPreferenceKeyPut**](JamfProUserAccountSettingsPreviewAPI.md#userobjpreferencekeyput) | **PUT** /user/obj/preference/{key} | Persist the user setting 


# **userObjPreferenceKeyDelete**
```swift
    open class func userObjPreferenceKeyDelete(key: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified setting for authenticated user 

Remove specified setting for authenticated user 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let key = "key_example" // String | key of user setting to be persisted

// Remove specified setting for authenticated user 
JamfProUserAccountSettingsPreviewAPI.userObjPreferenceKeyDelete(key: key) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String** | key of user setting to be persisted | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userObjPreferenceKeyGet**
```swift
    open class func userObjPreferenceKeyGet(key: String, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Get the user setting for the authenticated user and key 

Gets the user setting for the authenticated user and key. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let key = "key_example" // String | user setting to be retrieved

// Get the user setting for the authenticated user and key 
JamfProUserAccountSettingsPreviewAPI.userObjPreferenceKeyGet(key: key) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String** | user setting to be retrieved | 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userObjPreferenceKeyPut**
```swift
    open class func userObjPreferenceKeyPut(key: String, body: String? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Persist the user setting 

Persists the user setting 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let key = "key_example" // String | key of user setting to be persisted
let body = "body_example" // String | user setting value to be persisted (optional)

// Persist the user setting 
JamfProUserAccountSettingsPreviewAPI.userObjPreferenceKeyPut(key: key, body: body) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String** | key of user setting to be persisted | 
 **body** | **String** | user setting value to be persisted | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

