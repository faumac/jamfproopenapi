# MdmCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** |  | [optional] 
**dateSent** | **Date** |  | [optional] 
**client** | [**MdmCommandClient**](MdmCommandClient.md) |  | [optional] 
**commandState** | [**MdmCommandState**](MdmCommandState.md) |  | [optional] 
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


