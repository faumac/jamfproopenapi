# MobileDeviceTvOsInventory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mobileDeviceId** | **String** |  | [optional] [readonly] 
**deviceType** | **String** | Based on the value of this type either ios or appleTv objects will be populated. | 
**hardware** | [**MobileDeviceHardware**](MobileDeviceHardware.md) |  | [optional] 
**userAndLocation** | [**MobileDeviceUserAndLocation**](MobileDeviceUserAndLocation.md) |  | [optional] 
**purchasing** | [**MobileDevicePurchasing**](MobileDevicePurchasing.md) |  | [optional] 
**applications** | [MobileDeviceApplicationInventoryDetail] |  | [optional] 
**certificates** | [MobileDeviceCertificate] |  | [optional] 
**profiles** | [MobileDeviceProfile] |  | [optional] 
**userProfiles** | [MobileDeviceUserProfile] |  | [optional] 
**extensionAttributes** | [MobileDeviceExtensionAttribute] |  | [optional] 
**general** | [**MobileDeviceTvOsGeneral**](MobileDeviceTvOsGeneral.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


