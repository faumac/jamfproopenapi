# EnableLostModeCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lostModeMessage** | **String** |  | [optional] 
**lostModePhone** | **String** |  | [optional] 
**lostModeFootnote** | **String** |  | [optional] 
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


