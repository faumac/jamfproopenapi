# SelfServiceLoginSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userLoginLevel** | **String** | login setting to tell clients how to let users log in  | 
**allowRememberMe** | **Bool** | true if remember me functionality is allowed, false if not  | [optional] [default to false]
**authType** | **String** | login type to be used when asking users to log in  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


