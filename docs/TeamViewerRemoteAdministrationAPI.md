# TeamViewerRemoteAdministrationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsGet**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerconfigurationidsessionsget) | **GET** /preview/remote-administration-configurations/team-viewer/{configurationId}/sessions | Get a paginated list of sessions 
[**previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsPost**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerconfigurationidsessionspost) | **POST** /preview/remote-administration-configurations/team-viewer/{configurationId}/sessions | Create a new session
[**previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdClosePost**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerconfigurationidsessionssessionidclosepost) | **POST** /preview/remote-administration-configurations/team-viewer/{configurationId}/sessions/{sessionId}/close | Close a session
[**previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdGet**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerconfigurationidsessionssessionidget) | **GET** /preview/remote-administration-configurations/team-viewer/{configurationId}/sessions/{sessionId} | Get a session by its ID 
[**previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdResendNotificationPost**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerconfigurationidsessionssessionidresendnotificationpost) | **POST** /preview/remote-administration-configurations/team-viewer/{configurationId}/sessions/{sessionId}/resend-notification | Resend nofications for a session
[**previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdStatusGet**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerconfigurationidsessionssessionidstatusget) | **GET** /preview/remote-administration-configurations/team-viewer/{configurationId}/sessions/{sessionId}/status | Get a session status by its ID 
[**previewRemoteAdministrationConfigurationsTeamViewerIdDelete**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamvieweriddelete) | **DELETE** /preview/remote-administration-configurations/team-viewer/{id} | Delete Team Viewer Remote Administration connection configuration
[**previewRemoteAdministrationConfigurationsTeamViewerIdGet**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamvieweridget) | **GET** /preview/remote-administration-configurations/team-viewer/{id} | Get Team Viewer Remote Administration connection configuration
[**previewRemoteAdministrationConfigurationsTeamViewerIdPatch**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamvieweridpatch) | **PATCH** /preview/remote-administration-configurations/team-viewer/{id} | Update Team Viewer Remote Administration connection configuration
[**previewRemoteAdministrationConfigurationsTeamViewerIdStatusGet**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamvieweridstatusget) | **GET** /preview/remote-administration-configurations/team-viewer/{id}/status | Get Team Viewer Remote Administration connection status
[**previewRemoteAdministrationConfigurationsTeamViewerPost**](TeamViewerRemoteAdministrationAPI.md#previewremoteadministrationconfigurationsteamviewerpost) | **POST** /preview/remote-administration-configurations/team-viewer | Create Team Viewer Remote Administration connection configuration


# **previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsGet**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsGet(configurationId: String, page: Int? = nil, pageSize: Int? = nil, filter: String? = nil, completion: @escaping (_ data: SessionDetailsSearchResults?, _ error: Error?) -> Void)
```

Get a paginated list of sessions 

Returns a paginated list of sessions for a given configuration ID

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let configurationId = "configurationId_example" // String | ID of the Team Viewer connection configuration
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter sessions collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: `deviceId`, `deviceType`, `state`  This param can be combined with paging.  (optional) (default to "")

// Get a paginated list of sessions 
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsGet(configurationId: configurationId, page: page, pageSize: pageSize, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationId** | **String** | ID of the Team Viewer connection configuration | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **filter** | **String** | Query in the RSQL format, allowing to filter sessions collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: &#x60;deviceId&#x60;, &#x60;deviceType&#x60;, &#x60;state&#x60;  This param can be combined with paging.  | [optional] [default to &quot;&quot;]

### Return type

[**SessionDetailsSearchResults**](SessionDetailsSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsPost**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsPost(configurationId: String, sessionCandidateRequest: SessionCandidateRequest, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a new session

Creates a new Team Viewer session to be used to establish a remote connection

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let configurationId = "configurationId_example" // String | ID of the Team Viewer connection configuration
let sessionCandidateRequest = SessionCandidateRequest(deviceId: "deviceId_example", deviceType: "deviceType_example", description: "description_example") // SessionCandidateRequest | Team Viewer session attributes

// Create a new session
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsPost(configurationId: configurationId, sessionCandidateRequest: sessionCandidateRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationId** | **String** | ID of the Team Viewer connection configuration | 
 **sessionCandidateRequest** | [**SessionCandidateRequest**](SessionCandidateRequest.md) | Team Viewer session attributes | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdClosePost**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdClosePost(configurationId: String, sessionId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Close a session

Changes the session state from open to close. Closing a session means it is not possible to establish new remote connection between devices

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let configurationId = "configurationId_example" // String | ID of the Team Viewer connection configuration
let sessionId = "sessionId_example" // String | ID of the Team Viewer session

// Close a session
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdClosePost(configurationId: configurationId, sessionId: sessionId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationId** | **String** | ID of the Team Viewer connection configuration | 
 **sessionId** | **String** | ID of the Team Viewer session | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdGet**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdGet(configurationId: String, sessionId: String, completion: @escaping (_ data: SessionDetails?, _ error: Error?) -> Void)
```

Get a session by its ID 

Returns a session details if found.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let configurationId = "configurationId_example" // String | ID of the Team Viewer connection configuration
let sessionId = "sessionId_example" // String | ID of the Team Viewer session

// Get a session by its ID 
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdGet(configurationId: configurationId, sessionId: sessionId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationId** | **String** | ID of the Team Viewer connection configuration | 
 **sessionId** | **String** | ID of the Team Viewer session | 

### Return type

[**SessionDetails**](SessionDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdResendNotificationPost**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdResendNotificationPost(configurationId: String, sessionId: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Resend nofications for a session

Resends configured notifications (e.g. Self Service push notifications).

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let configurationId = "configurationId_example" // String | ID of the Team Viewer connection configuration
let sessionId = "sessionId_example" // String | ID of the Team Viewer session

// Resend nofications for a session
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdResendNotificationPost(configurationId: configurationId, sessionId: sessionId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationId** | **String** | ID of the Team Viewer connection configuration | 
 **sessionId** | **String** | ID of the Team Viewer session | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdStatusGet**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdStatusGet(configurationId: String, sessionId: String, completion: @escaping (_ data: SessionStatus?, _ error: Error?) -> Void)
```

Get a session status by its ID 

Returns a session status if found.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let configurationId = "configurationId_example" // String | ID of the Team Viewer connection configuration
let sessionId = "sessionId_example" // String | ID of the Team Viewer session

// Get a session status by its ID 
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerConfigurationIdSessionsSessionIdStatusGet(configurationId: configurationId, sessionId: sessionId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **configurationId** | **String** | ID of the Team Viewer connection configuration | 
 **sessionId** | **String** | ID of the Team Viewer session | 

### Return type

[**SessionStatus**](SessionStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerIdDelete**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Team Viewer Remote Administration connection configuration

Deletes Team Viewer Remote Administration connection configuration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Team Viewer connection configuration

// Delete Team Viewer Remote Administration connection configuration
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Team Viewer connection configuration | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerIdGet**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerIdGet(id: String, completion: @escaping (_ data: ConnectionConfigurationResponse?, _ error: Error?) -> Void)
```

Get Team Viewer Remote Administration connection configuration

Returns Team Viewer Remote Administration connection configuration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Team Viewer connection configuration

// Get Team Viewer Remote Administration connection configuration
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Team Viewer connection configuration | 

### Return type

[**ConnectionConfigurationResponse**](ConnectionConfigurationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerIdPatch**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerIdPatch(id: String, connectionConfigurationUpdateRequest: ConnectionConfigurationUpdateRequest, completion: @escaping (_ data: ConnectionConfigurationResponse?, _ error: Error?) -> Void)
```

Update Team Viewer Remote Administration connection configuration

Updates Team Viewer Remote Administration connection configuration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Team Viewer connection configuration
let connectionConfigurationUpdateRequest = ConnectionConfigurationUpdateRequest(displayName: "displayName_example", enabled: true, sessionTimeout: 123, token: "token_example") // ConnectionConfigurationUpdateRequest | Team Viewer connection configuration update request

// Update Team Viewer Remote Administration connection configuration
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerIdPatch(id: id, connectionConfigurationUpdateRequest: connectionConfigurationUpdateRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Team Viewer connection configuration | 
 **connectionConfigurationUpdateRequest** | [**ConnectionConfigurationUpdateRequest**](ConnectionConfigurationUpdateRequest.md) | Team Viewer connection configuration update request | 

### Return type

[**ConnectionConfigurationResponse**](ConnectionConfigurationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerIdStatusGet**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerIdStatusGet(id: String, completion: @escaping (_ data: ConnectionConfigurationStatusResponse?, _ error: Error?) -> Void)
```

Get Team Viewer Remote Administration connection status

Returns Team Viewer Remote Administration connection status

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | ID of the Team Viewer connection configuration

// Get Team Viewer Remote Administration connection status
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerIdStatusGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | ID of the Team Viewer connection configuration | 

### Return type

[**ConnectionConfigurationStatusResponse**](ConnectionConfigurationStatusResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **previewRemoteAdministrationConfigurationsTeamViewerPost**
```swift
    open class func previewRemoteAdministrationConfigurationsTeamViewerPost(connectionConfigurationCandidateRequest: ConnectionConfigurationCandidateRequest, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Team Viewer Remote Administration connection configuration

Creates Team Viewer Remote Administration connection configuration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let connectionConfigurationCandidateRequest = ConnectionConfigurationCandidateRequest(siteId: "siteId_example", displayName: "displayName_example", scriptToken: "scriptToken_example", enabled: true, sessionTimeout: 123) // ConnectionConfigurationCandidateRequest | Team Viewer connection configuration create definition

// Create Team Viewer Remote Administration connection configuration
TeamViewerRemoteAdministrationAPI.previewRemoteAdministrationConfigurationsTeamViewerPost(connectionConfigurationCandidateRequest: connectionConfigurationCandidateRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connectionConfigurationCandidateRequest** | [**ConnectionConfigurationCandidateRequest**](ConnectionConfigurationCandidateRequest.md) | Team Viewer connection configuration create definition | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

