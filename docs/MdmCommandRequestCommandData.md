# MdmCommandRequestCommandData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 
**lostModeMessage** | **String** |  | [optional] 
**lostModePhone** | **String** |  | [optional] 
**lostModeFootnote** | **String** |  | [optional] 
**preserveDataPlan** | **Bool** | If true, preserve the data plan on an iPhone or iPad with eSIM functionality, if one exists. This value is available in iOS 11 and later. | [optional] [default to false]
**disallowProximitySetup** | **Bool** | If true, disable Proximity Setup on the next reboot and skip the pane in Setup Assistant. This value is available in iOS 11 and later. Prior to iOS 14, don’t use this option with any other option. | [optional] [default to false]
**pin** | **String** | The six-character PIN for Find My. This value is available in macOS 10.8 and later. | [optional] 
**obliterationBehavior** | **String** | This key defines the fallback behavior for erasing a device. | [optional] 
**returnToService** | [**EraseDeviceCommandReturnToService**](EraseDeviceCommandReturnToService.md) |  | [optional] 
**userName** | **String** |  | [optional] 
**forceDeletion** | **Bool** |  | [optional] 
**deleteAllUsers** | **Bool** |  | [optional] 
**bootstrapTokenAllowed** | **Bool** |  | [optional] 
**bluetooth** | **Bool** |  | [optional] 
**appAnalytics** | [**AppAnalyticsSetting**](AppAnalyticsSetting.md) |  | [optional] 
**diagnosticSubmission** | [**DiagnosticSubmissionSetting**](DiagnosticSubmissionSetting.md) |  | [optional] 
**dataRoaming** | [**DataRoamingSetting**](DataRoamingSetting.md) |  | [optional] 
**voiceRoaming** | [**VoiceRoamingSetting**](VoiceRoamingSetting.md) |  | [optional] 
**personalHotspot** | [**PersonalHotspotSetting**](PersonalHotspotSetting.md) |  | [optional] 
**maximumResidentUsers** | **Int** |  | [optional] 
**deviceName** | **String** |  | [optional] 
**applicationAttributes** | [**ApplicationAttributes**](ApplicationAttributes.md) |  | [optional] 
**sharedDeviceConfiguration** | [**SharedDeviceConfiguration**](SharedDeviceConfiguration.md) |  | [optional] 
**applicationConfiguration** | [**ApplicationConfiguration**](ApplicationConfiguration.md) |  | [optional] 
**timeZone** | **String** |  | [optional] 
**softwareUpdateSettings** | [**SoftwareUpdateSettings**](SoftwareUpdateSettings.md) |  | [optional] 
**passcodeLockGracePeriod** | **Int** | The number of seconds before a locked screen requires the user to enter the device passcode to unlock it. (Shared iPad Only) | [optional] 
**rebuildKernelCache** | **Bool** |  | [optional] 
**kextPaths** | **[String]** | Only used if RebuildKernelCache is true | [optional] 
**notifyUser** | **Bool** |  | [optional] 
**newPassword** | **String** | The new password for Recovery Lock. Set as an empty string to clear the Recovery Lock password. | [optional] 
**data** | **String** | Base64 encoded data to be sent with the command | [optional] 
**guid** | **String** | The unique identifier of the local administrator account. Must match the GUID of an administrator account that MDM created during Device Enrollment Program (DEP) enrollment. | [optional] 
**password** | **String** | The new password for the local administrator account. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


