# PatchSoftwareTitleConfigurationsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2PatchSoftwareTitleConfigurationsGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsget) | **GET** /v2/patch-software-title-configurations | Retrieve Patch Software Title Configurations
[**v2PatchSoftwareTitleConfigurationsIdDashboardDelete**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsiddashboarddelete) | **DELETE** /v2/patch-software-title-configurations/{id}/dashboard | Remove a software title configuration from the dashboard 
[**v2PatchSoftwareTitleConfigurationsIdDashboardGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsiddashboardget) | **GET** /v2/patch-software-title-configurations/{id}/dashboard | Return whether or not the requested software title configuration is on the dashboard 
[**v2PatchSoftwareTitleConfigurationsIdDashboardPost**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsiddashboardpost) | **POST** /v2/patch-software-title-configurations/{id}/dashboard | Add a software title configuration to the dashboard 
[**v2PatchSoftwareTitleConfigurationsIdDefinitionsGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsiddefinitionsget) | **GET** /v2/patch-software-title-configurations/{id}/definitions | Retrieve Patch Software Title Definitions with the supplied id
[**v2PatchSoftwareTitleConfigurationsIdDelete**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsiddelete) | **DELETE** /v2/patch-software-title-configurations/{id} | Delete Patch Software Title Configurations with the supplied id
[**v2PatchSoftwareTitleConfigurationsIdDependenciesGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsiddependenciesget) | **GET** /v2/patch-software-title-configurations/{id}/dependencies | Retrieve list of Patch Software Title Configuration Dependencies
[**v2PatchSoftwareTitleConfigurationsIdExportReportGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidexportreportget) | **GET** /v2/patch-software-title-configurations/{id}/export-report | Export Patch Reporting Data
[**v2PatchSoftwareTitleConfigurationsIdExtensionAttributesGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidextensionattributesget) | **GET** /v2/patch-software-title-configurations/{id}/extension-attributes | Retrieve Software Title Extension Attributes with the supplied id
[**v2PatchSoftwareTitleConfigurationsIdGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidget) | **GET** /v2/patch-software-title-configurations/{id} | Retrieve Patch Software Title Configurations with the supplied id
[**v2PatchSoftwareTitleConfigurationsIdHistoryGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidhistoryget) | **GET** /v2/patch-software-title-configurations/{id}/history | Get specified Patch Software Title Configuration history object 
[**v2PatchSoftwareTitleConfigurationsIdHistoryPost**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidhistorypost) | **POST** /v2/patch-software-title-configurations/{id}/history | Add Patch Software Title Configuration history object notes 
[**v2PatchSoftwareTitleConfigurationsIdPatch**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidpatch) | **PATCH** /v2/patch-software-title-configurations/{id} | Update Patch Software Title Configurations
[**v2PatchSoftwareTitleConfigurationsIdPatchReportGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidpatchreportget) | **GET** /v2/patch-software-title-configurations/{id}/patch-report | Retrieve Patch Software Title Configuration Patch Report
[**v2PatchSoftwareTitleConfigurationsIdPatchSummaryGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidpatchsummaryget) | **GET** /v2/patch-software-title-configurations/{id}/patch-summary | Return Active Patch Summary
[**v2PatchSoftwareTitleConfigurationsIdPatchSummaryVersionsGet**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationsidpatchsummaryversionsget) | **GET** /v2/patch-software-title-configurations/{id}/patch-summary/versions | Returns patch versions
[**v2PatchSoftwareTitleConfigurationsPost**](PatchSoftwareTitleConfigurationsAPI.md#v2patchsoftwaretitleconfigurationspost) | **POST** /v2/patch-software-title-configurations | Create Patch Software Title Configurations


# **v2PatchSoftwareTitleConfigurationsGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsGet(completion: @escaping (_ data: [PatchSoftwareTitleConfiguration]?, _ error: Error?) -> Void)
```

Retrieve Patch Software Title Configurations

Retrieves patch software title configurations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve Patch Software Title Configurations
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[PatchSoftwareTitleConfiguration]**](PatchSoftwareTitleConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdDashboardDelete**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdDashboardDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove a software title configuration from the dashboard 

Removes a software title configuration from the dashboard.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | software title configuration id

// Remove a software title configuration from the dashboard 
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdDashboardDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | software title configuration id | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdDashboardGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdDashboardGet(id: String, completion: @escaping (_ data: SoftwareTitleConfigurationOnDashboard?, _ error: Error?) -> Void)
```

Return whether or not the requested software title configuration is on the dashboard 

Returns whether or not the requested software title configuration is on the dashboard

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | software title configuration id

// Return whether or not the requested software title configuration is on the dashboard 
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdDashboardGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | software title configuration id | 

### Return type

[**SoftwareTitleConfigurationOnDashboard**](SoftwareTitleConfigurationOnDashboard.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdDashboardPost**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdDashboardPost(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Add a software title configuration to the dashboard 

Adds asoftware title configuration to the dashboard.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | software title configuration id

// Add a software title configuration to the dashboard 
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdDashboardPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | software title configuration id | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdDefinitionsGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdDefinitionsGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: PatchSoftwareTitleDefinitions?, _ error: Error?) -> Void)
```

Retrieve Patch Software Title Definitions with the supplied id

Retrieves patch software title definitions with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is absoluteOrderId:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Patch Software Title Definition collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, version, minimumOperatingSystem, releaseDate, reboot, standalone and absoluteOrderId. This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve Patch Software Title Definitions with the supplied id
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdDefinitionsGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is absoluteOrderId:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Patch Software Title Definition collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, version, minimumOperatingSystem, releaseDate, reboot, standalone and absoluteOrderId. This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**PatchSoftwareTitleDefinitions**](PatchSoftwareTitleDefinitions.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdDelete**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Patch Software Title Configurations with the supplied id

Deletes Patch Software Title Configurations with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configurations identifier

// Delete Patch Software Title Configurations with the supplied id
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configurations identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdDependenciesGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdDependenciesGet(id: String, completion: @escaping (_ data: PatchSoftwareTitleConfigurationDependencies?, _ error: Error?) -> Void)
```

Retrieve list of Patch Software Title Configuration Dependencies

Retrieve list of Patch Software Title Configuration Dependencies

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configuration Id

// Retrieve list of Patch Software Title Configuration Dependencies
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdDependenciesGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configuration Id | 

### Return type

[**PatchSoftwareTitleConfigurationDependencies**](PatchSoftwareTitleConfigurationDependencies.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdExportReportGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdExportReportGet(id: String, columnsToExport: [String], page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, accept: String? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Export Patch Reporting Data

Export Patch Reporting Data

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configurations identifier
let columnsToExport = ["inner_example"] // [String] | List of column names to export
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int | Leave blank for full export (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is computerName:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=id:desc,name:asc Supported fields: computerName, deviceId, username, operatingSystemVersion, lastContactTime, buildingName, departmentName, siteName, version (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Patch Report collection on version equality only. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: version. Comparators allowed in the query: ==, != This param can be combined with paging and sorting. (optional) (default to "")
let accept = "accept_example" // String | File (optional)

// Export Patch Reporting Data
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdExportReportGet(id: id, columnsToExport: columnsToExport, page: page, pageSize: pageSize, sort: sort, filter: filter, accept: accept) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configurations identifier | 
 **columnsToExport** | [**[String]**](String.md) | List of column names to export | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** | Leave blank for full export | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is computerName:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;id:desc,name:asc Supported fields: computerName, deviceId, username, operatingSystemVersion, lastContactTime, buildingName, departmentName, siteName, version | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Patch Report collection on version equality only. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: version. Comparators allowed in the query: &#x3D;&#x3D;, !&#x3D; This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]
 **accept** | **String** | File | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv, text/tab, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdExtensionAttributesGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdExtensionAttributesGet(id: String, completion: @escaping (_ data: [PatchSoftwareTitleExtensionAttributes]?, _ error: Error?) -> Void)
```

Retrieve Software Title Extension Attributes with the supplied id

Retrieves software title extension attributes with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title identifier

// Retrieve Software Title Extension Attributes with the supplied id
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdExtensionAttributesGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title identifier | 

### Return type

[**[PatchSoftwareTitleExtensionAttributes]**](PatchSoftwareTitleExtensionAttributes.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdGet(id: String, completion: @escaping (_ data: PatchSoftwareTitleConfiguration?, _ error: Error?) -> Void)
```

Retrieve Patch Software Title Configurations with the supplied id

Retrieves Patch Software Title Configurations with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configurations identifier

// Retrieve Patch Software Title Configurations with the supplied id
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configurations identifier | 

### Return type

[**PatchSoftwareTitleConfiguration**](PatchSoftwareTitleConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdHistoryGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get specified Patch Software Title Configuration history object 

Gets specified Patch Software Title Configuration history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configuration Id
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma.  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get specified Patch Software Title Configuration history object 
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configuration Id | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma.  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdHistoryPost**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Patch Software Title Configuration history object notes 

Adds Patch Software Title Configuration history object notes

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configuration Id
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Patch Software Title Configuration history object notes 
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configuration Id | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdPatch**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdPatch(id: String, patchSoftwareTitleConfigurationPatch: PatchSoftwareTitleConfigurationPatch, completion: @escaping (_ data: PatchSoftwareTitleConfiguration?, _ error: Error?) -> Void)
```

Update Patch Software Title Configurations

Updates Patch Software Title Configurations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configurations identifier
let patchSoftwareTitleConfigurationPatch = PatchSoftwareTitleConfigurationPatch(displayName: "displayName_example", categoryId: "categoryId_example", siteId: "siteId_example", uiNotifications: false, emailNotifications: false, softwareTitleId: "softwareTitleId_example", packages: [PatchSoftwareTitlePackages(packageId: "packageId_example", version: "version_example", displayName: "displayName_example")], extensionAttributes: [PatchSoftwareTitleConfigurationExtensionAttributes(accepted: false, eaId: "eaId_example")]) // PatchSoftwareTitleConfigurationPatch | Patch Software Title Configurations to update

// Update Patch Software Title Configurations
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdPatch(id: id, patchSoftwareTitleConfigurationPatch: patchSoftwareTitleConfigurationPatch) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configurations identifier | 
 **patchSoftwareTitleConfigurationPatch** | [**PatchSoftwareTitleConfigurationPatch**](PatchSoftwareTitleConfigurationPatch.md) | Patch Software Title Configurations to update | 

### Return type

[**PatchSoftwareTitleConfiguration**](PatchSoftwareTitleConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/merge-patch+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdPatchReportGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdPatchReportGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: PatchSoftwareTitleReportSearchResult?, _ error: Error?) -> Void)
```

Retrieve Patch Software Title Configuration Patch Report

Retrieve Patch Software Title Configuration Patch Report

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch Software Title Configurations identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is computerName:asc. Multiple sort criteria are supported and must be separated with a comma. Supported fields: computerName, deviceId, username, operatingSystemVersion, lastContactTime, buildingName, departmentName, siteName, version (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Patch Report collection on version equality only. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: version. Comparators allowed in the query: ==, != This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve Patch Software Title Configuration Patch Report
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdPatchReportGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch Software Title Configurations identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is computerName:asc. Multiple sort criteria are supported and must be separated with a comma. Supported fields: computerName, deviceId, username, operatingSystemVersion, lastContactTime, buildingName, departmentName, siteName, version | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Patch Report collection on version equality only. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: version. Comparators allowed in the query: &#x3D;&#x3D;, !&#x3D; This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**PatchSoftwareTitleReportSearchResult**](PatchSoftwareTitleReportSearchResult.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdPatchSummaryGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdPatchSummaryGet(id: String, completion: @escaping (_ data: PatchSummary?, _ error: Error?) -> Void)
```

Return Active Patch Summary

Returns active patch summary.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch id

// Return Active Patch Summary
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdPatchSummaryGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch id | 

### Return type

[**PatchSummary**](PatchSummary.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsIdPatchSummaryVersionsGet**
```swift
    open class func v2PatchSoftwareTitleConfigurationsIdPatchSummaryVersionsGet(id: String, completion: @escaping (_ data: [PatchSummaryVersion]?, _ error: Error?) -> Void)
```

Returns patch versions

Returns patch versions

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Patch id

// Returns patch versions
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsIdPatchSummaryVersionsGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Patch id | 

### Return type

[**[PatchSummaryVersion]**](PatchSummaryVersion.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchSoftwareTitleConfigurationsPost**
```swift
    open class func v2PatchSoftwareTitleConfigurationsPost(patchSoftwareTitleConfigurationBase: PatchSoftwareTitleConfigurationBase, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Patch Software Title Configurations

Creates Patch Software Title Configurations using sToken

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let patchSoftwareTitleConfigurationBase = PatchSoftwareTitleConfigurationBase(displayName: "displayName_example", categoryId: "categoryId_example", siteId: "siteId_example", uiNotifications: false, emailNotifications: false, softwareTitleId: "softwareTitleId_example", jamfOfficial: false, extensionAttributes: [PatchSoftwareTitleConfigurationExtensionAttributes(accepted: false, eaId: "eaId_example")], softwareTitleName: "softwareTitleName_example", softwareTitleNameId: "softwareTitleNameId_example", softwareTitlePublisher: "softwareTitlePublisher_example", patchSourceName: "patchSourceName_example", patchSourceEnabled: true) // PatchSoftwareTitleConfigurationBase | Software title configurations to create

// Create Patch Software Title Configurations
PatchSoftwareTitleConfigurationsAPI.v2PatchSoftwareTitleConfigurationsPost(patchSoftwareTitleConfigurationBase: patchSoftwareTitleConfigurationBase) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patchSoftwareTitleConfigurationBase** | [**PatchSoftwareTitleConfigurationBase**](PatchSoftwareTitleConfigurationBase.md) | Software title configurations to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

