# ComputerPackageReceipts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**installedByJamfPro** | **[String]** |  | [optional] 
**installedByInstallerSwu** | **[String]** |  | [optional] 
**cached** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


