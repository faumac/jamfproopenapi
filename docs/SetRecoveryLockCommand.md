# SetRecoveryLockCommand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newPassword** | **String** | The new password for Recovery Lock. Set as an empty string to clear the Recovery Lock password. | [optional] 
**commandType** | [**MdmCommandType**](MdmCommandType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


