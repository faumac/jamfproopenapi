# CsaToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshExpiration** | **Int** |  | [optional] 
**scopes** | **[String]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


