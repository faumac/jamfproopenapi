# ManagedSoftwareUpdatesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ManagedSoftwareUpdatesAvailableUpdatesGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesavailableupdatesget) | **GET** /v1/managed-software-updates/available-updates | Retrieve available macOS and iOS Managed Software Updates
[**v1ManagedSoftwareUpdatesPlansFeatureToggleGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplansfeaturetoggleget) | **GET** /v1/managed-software-updates/plans/feature-toggle | Retrieve Status of the Feature Toggle
[**v1ManagedSoftwareUpdatesPlansFeatureTogglePut**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplansfeaturetoggleput) | **PUT** /v1/managed-software-updates/plans/feature-toggle | Updates Feature Toggle Value
[**v1ManagedSoftwareUpdatesPlansGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplansget) | **GET** /v1/managed-software-updates/plans | Retrieve Managed Software Update Plans
[**v1ManagedSoftwareUpdatesPlansGroupIdGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplansgroupidget) | **GET** /v1/managed-software-updates/plans/group/{id} | Retrieve Managed Software Update Plans for a Group
[**v1ManagedSoftwareUpdatesPlansGroupPost**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplansgrouppost) | **POST** /v1/managed-software-updates/plans/group | Create Managed Software Update Plans for a Group
[**v1ManagedSoftwareUpdatesPlansIdGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplansidget) | **GET** /v1/managed-software-updates/plans/{id} | Retrieve a Managed Software Update Plan
[**v1ManagedSoftwareUpdatesPlansPost**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesplanspost) | **POST** /v1/managed-software-updates/plans | Create a Managed Software Update Plan
[**v1ManagedSoftwareUpdatesUpdateStatusesComputerGroupsIdGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesupdatestatusescomputergroupsidget) | **GET** /v1/managed-software-updates/update-statuses/computer-groups/{id} | Retrieve Managed Software Update Statuses for Computer Groups
[**v1ManagedSoftwareUpdatesUpdateStatusesComputersIdGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesupdatestatusescomputersidget) | **GET** /v1/managed-software-updates/update-statuses/computers/{id} | Retrieve Managed Software Update Statuses for Computers
[**v1ManagedSoftwareUpdatesUpdateStatusesGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesupdatestatusesget) | **GET** /v1/managed-software-updates/update-statuses | Retrieve Managed Software Update Statuses
[**v1ManagedSoftwareUpdatesUpdateStatusesMobileDeviceGroupsIdGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesupdatestatusesmobiledevicegroupsidget) | **GET** /v1/managed-software-updates/update-statuses/mobile-device-groups/{id} | Retrieve Managed Software Update Statuses for Mobile Device Groups
[**v1ManagedSoftwareUpdatesUpdateStatusesMobileDevicesIdGet**](ManagedSoftwareUpdatesAPI.md#v1managedsoftwareupdatesupdatestatusesmobiledevicesidget) | **GET** /v1/managed-software-updates/update-statuses/mobile-devices/{id} | Retrieve Managed Software Update Statuses for Mobile Devices


# **v1ManagedSoftwareUpdatesAvailableUpdatesGet**
```swift
    open class func v1ManagedSoftwareUpdatesAvailableUpdatesGet(completion: @escaping (_ data: AvailableOsUpdates?, _ error: Error?) -> Void)
```

Retrieve available macOS and iOS Managed Software Updates

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieves available macOS and iOS Managed Software Updates 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve available macOS and iOS Managed Software Updates
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesAvailableUpdatesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AvailableOsUpdates**](AvailableOsUpdates.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansFeatureToggleGet**
```swift
    open class func v1ManagedSoftwareUpdatesPlansFeatureToggleGet(completion: @escaping (_ data: ManagedSoftwareUpdatePlanToggle?, _ error: Error?) -> Void)
```

Retrieve Status of the Feature Toggle

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieves Status of the Feature Toggle 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve Status of the Feature Toggle
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansFeatureToggleGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ManagedSoftwareUpdatePlanToggle**](ManagedSoftwareUpdatePlanToggle.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansFeatureTogglePut**
```swift
    open class func v1ManagedSoftwareUpdatesPlansFeatureTogglePut(managedSoftwareUpdatePlanToggle: ManagedSoftwareUpdatePlanToggle, completion: @escaping (_ data: ManagedSoftwareUpdatePlanToggle?, _ error: Error?) -> Void)
```

Updates Feature Toggle Value

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Updates the value of the Feature Toggle 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let managedSoftwareUpdatePlanToggle = ManagedSoftwareUpdatePlanToggle(toggle: false, forceInstallLocalDateEnabled: false, dssEnabled: false) // ManagedSoftwareUpdatePlanToggle | 

// Updates Feature Toggle Value
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansFeatureTogglePut(managedSoftwareUpdatePlanToggle: managedSoftwareUpdatePlanToggle) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managedSoftwareUpdatePlanToggle** | [**ManagedSoftwareUpdatePlanToggle**](ManagedSoftwareUpdatePlanToggle.md) |  | 

### Return type

[**ManagedSoftwareUpdatePlanToggle**](ManagedSoftwareUpdatePlanToggle.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansGet**
```swift
    open class func v1ManagedSoftwareUpdatesPlansGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: ManagedSoftwareUpdatePlans?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Plans

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieve Managed Software Update Plans 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is planUuid:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Managed Software Updates collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: planUuid, device.deviceId, device.objectType, updateAction, versionType, specificVersion, maxDeferrals, forceInstallLocalDateTime, state. (optional) (default to "")

// Retrieve Managed Software Update Plans
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is planUuid:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Managed Software Updates collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: planUuid, device.deviceId, device.objectType, updateAction, versionType, specificVersion, maxDeferrals, forceInstallLocalDateTime, state. | [optional] [default to &quot;&quot;]

### Return type

[**ManagedSoftwareUpdatePlans**](ManagedSoftwareUpdatePlans.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansGroupIdGet**
```swift
    open class func v1ManagedSoftwareUpdatesPlansGroupIdGet(id: String, groupType: GroupType_v1ManagedSoftwareUpdatesPlansGroupIdGet, completion: @escaping (_ data: ManagedSoftwareUpdatePlans?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Plans for a Group

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieves Managed Software Update Plans for a Group 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Managed Software Update Group Id
let groupType = "groupType_example" // String | Managed Software Update Group Type, Available options are \"COMPUTER_GROUP\" or \"MOBILE_DEVICE_GROUP\"

// Retrieve Managed Software Update Plans for a Group
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansGroupIdGet(id: id, groupType: groupType) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Managed Software Update Group Id | 
 **groupType** | **String** | Managed Software Update Group Type, Available options are \&quot;COMPUTER_GROUP\&quot; or \&quot;MOBILE_DEVICE_GROUP\&quot; | 

### Return type

[**ManagedSoftwareUpdatePlans**](ManagedSoftwareUpdatePlans.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansGroupPost**
```swift
    open class func v1ManagedSoftwareUpdatesPlansGroupPost(managedSoftwareUpdatePlanGroupPost: ManagedSoftwareUpdatePlanGroupPost, completion: @escaping (_ data: ManagedSoftwareUpdatePlanPostResponse?, _ error: Error?) -> Void)
```

Create Managed Software Update Plans for a Group

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Creates Managed Software Update Plans for a Group 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let managedSoftwareUpdatePlanGroupPost = ManagedSoftwareUpdatePlanGroupPost(group: PlanGroupPost(groupId: "groupId_example", objectType: "objectType_example"), config: PlanConfigurationPost(updateAction: "updateAction_example", versionType: "versionType_example", specificVersion: "specificVersion_example", maxDeferrals: 123, forceInstallLocalDateTime: "forceInstallLocalDateTime_example")) // ManagedSoftwareUpdatePlanGroupPost | Managed Software Update Plan to create for Group

// Create Managed Software Update Plans for a Group
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansGroupPost(managedSoftwareUpdatePlanGroupPost: managedSoftwareUpdatePlanGroupPost) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managedSoftwareUpdatePlanGroupPost** | [**ManagedSoftwareUpdatePlanGroupPost**](ManagedSoftwareUpdatePlanGroupPost.md) | Managed Software Update Plan to create for Group | 

### Return type

[**ManagedSoftwareUpdatePlanPostResponse**](ManagedSoftwareUpdatePlanPostResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansIdGet**
```swift
    open class func v1ManagedSoftwareUpdatesPlansIdGet(id: String, completion: @escaping (_ data: ManagedSoftwareUpdatePlan?, _ error: Error?) -> Void)
```

Retrieve a Managed Software Update Plan

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieves a Managed Software Update Plan 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Managed Software Update Plan Uuid

// Retrieve a Managed Software Update Plan
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Managed Software Update Plan Uuid | 

### Return type

[**ManagedSoftwareUpdatePlan**](ManagedSoftwareUpdatePlan.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesPlansPost**
```swift
    open class func v1ManagedSoftwareUpdatesPlansPost(managedSoftwareUpdatePlanPost: ManagedSoftwareUpdatePlanPost, completion: @escaping (_ data: ManagedSoftwareUpdatePlanPostResponse?, _ error: Error?) -> Void)
```

Create a Managed Software Update Plan

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Creates a Managed Software Update Plan. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let managedSoftwareUpdatePlanPost = ManagedSoftwareUpdatePlanPost(devices: [PlanDevicePost(deviceId: "deviceId_example", objectType: "objectType_example")], config: PlanConfigurationPost(updateAction: "updateAction_example", versionType: "versionType_example", specificVersion: "specificVersion_example", maxDeferrals: 123, forceInstallLocalDateTime: "forceInstallLocalDateTime_example")) // ManagedSoftwareUpdatePlanPost | Managed Software Update Plan to create

// Create a Managed Software Update Plan
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesPlansPost(managedSoftwareUpdatePlanPost: managedSoftwareUpdatePlanPost) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **managedSoftwareUpdatePlanPost** | [**ManagedSoftwareUpdatePlanPost**](ManagedSoftwareUpdatePlanPost.md) | Managed Software Update Plan to create | 

### Return type

[**ManagedSoftwareUpdatePlanPostResponse**](ManagedSoftwareUpdatePlanPostResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesUpdateStatusesComputerGroupsIdGet**
```swift
    open class func v1ManagedSoftwareUpdatesUpdateStatusesComputerGroupsIdGet(id: String, completion: @escaping (_ data: ManagedSoftwareUpdateStatuses?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Statuses for Computer Groups

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieve Managed Software Update Statuses for Computer Groups 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Group identifier

// Retrieve Managed Software Update Statuses for Computer Groups
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesUpdateStatusesComputerGroupsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Group identifier | 

### Return type

[**ManagedSoftwareUpdateStatuses**](ManagedSoftwareUpdateStatuses.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesUpdateStatusesComputersIdGet**
```swift
    open class func v1ManagedSoftwareUpdatesUpdateStatusesComputersIdGet(id: String, completion: @escaping (_ data: ManagedSoftwareUpdateStatuses?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Statuses for Computers

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieve Managed Software Update Statuses for Computers 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer identifier

// Retrieve Managed Software Update Statuses for Computers
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesUpdateStatusesComputersIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer identifier | 

### Return type

[**ManagedSoftwareUpdateStatuses**](ManagedSoftwareUpdateStatuses.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesUpdateStatusesGet**
```swift
    open class func v1ManagedSoftwareUpdatesUpdateStatusesGet(filter: String? = nil, completion: @escaping (_ data: ManagedSoftwareUpdateStatuses?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Statuses

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieve Managed Software Update Statuses 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Managed Software Updates collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: osUpdatesStatusId, device.deviceId, device.objectType, downloaded, downloadPercentComplete, productKey, status, deferralsRemaining, maxDeferrals, nextScheduledInstall, created and updated. (optional) (default to "")

// Retrieve Managed Software Update Statuses
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesUpdateStatusesGet(filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** | Query in the RSQL format, allowing to filter Managed Software Updates collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: osUpdatesStatusId, device.deviceId, device.objectType, downloaded, downloadPercentComplete, productKey, status, deferralsRemaining, maxDeferrals, nextScheduledInstall, created and updated. | [optional] [default to &quot;&quot;]

### Return type

[**ManagedSoftwareUpdateStatuses**](ManagedSoftwareUpdateStatuses.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesUpdateStatusesMobileDeviceGroupsIdGet**
```swift
    open class func v1ManagedSoftwareUpdatesUpdateStatusesMobileDeviceGroupsIdGet(id: String, completion: @escaping (_ data: ManagedSoftwareUpdateStatuses?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Statuses for Mobile Device Groups

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieve Managed Software Update Statuses for Mobile Device Groups 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device Group identifier

// Retrieve Managed Software Update Statuses for Mobile Device Groups
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesUpdateStatusesMobileDeviceGroupsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device Group identifier | 

### Return type

[**ManagedSoftwareUpdateStatuses**](ManagedSoftwareUpdateStatuses.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ManagedSoftwareUpdatesUpdateStatusesMobileDevicesIdGet**
```swift
    open class func v1ManagedSoftwareUpdatesUpdateStatusesMobileDevicesIdGet(id: String, completion: @escaping (_ data: ManagedSoftwareUpdateStatuses?, _ error: Error?) -> Void)
```

Retrieve Managed Software Update Statuses for Mobile Devices

## BETA    These endpoints are available for use in your workflows. It is extremely important to exercise caution when implementing these endpoints, because elements such as the URL or the response could change while this feature is in Beta. If this is the case, we will communicate that upcoming change but there will be no deprecation timeline. Any breaking changes to the API that occur *after* the Beta phase __will be__ held to the standard year-long deprecation timeline.    Retrieve Managed Software Update Statuses for Mobile Devices 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Mobile Device identifier

// Retrieve Managed Software Update Statuses for Mobile Devices
ManagedSoftwareUpdatesAPI.v1ManagedSoftwareUpdatesUpdateStatusesMobileDevicesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Mobile Device identifier | 

### Return type

[**ManagedSoftwareUpdateStatuses**](ManagedSoftwareUpdateStatuses.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

