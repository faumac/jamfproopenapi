# TvOsDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**modelNumber** | **String** |  | [optional] 
**supervised** | **Bool** |  | [optional] 
**airplayPassword** | **String** |  | [optional] 
**deviceId** | **String** |  | [optional] 
**locales** | **String** |  | [optional] 
**purchasing** | [**PurchasingV2**](PurchasingV2.md) |  | [optional] 
**configurationProfiles** | [ConfigurationProfile] |  | [optional] 
**certificates** | [MobileDeviceCertificateV2] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


