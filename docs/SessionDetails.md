# SessionDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Session identifier | [optional] 
**code** | **String** | Sessions code | [optional] 
**description** | **String** | Session description. To be used for additional context on the reason of the session | [optional] 
**supporterLink** | **String** | Supporter session URL | [optional] 
**endUserLink** | **String** | End user session URL | [optional] 
**deviceId** | **String** | Device identifier | [optional] 
**deviceName** | **String** | Device name if found - null otherwise | [optional] 
**deviceType** | **String** | Device type | [optional] 
**state** | **String** | Session state | [optional] 
**creatorId** | **String** | ID of session creator if session created by Jamf Pro local user, null otherwise | [optional] 
**creatorName** | **String** | Username of the session creator | [optional] 
**createdAt** | **Date** | Session creation time | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


