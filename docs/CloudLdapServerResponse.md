# CloudLdapServerResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**enabled** | **Bool** |  | [optional] 
**serverUrl** | **String** |  | [optional] 
**domainName** | **String** |  | [optional] 
**port** | **Int** |  | [optional] 
**keystore** | [**CloudLdapKeystore**](CloudLdapKeystore.md) |  | [optional] 
**connectionTimeout** | **Int** |  | [optional] 
**searchTimeout** | **Int** |  | [optional] 
**useWildcards** | **Bool** |  | [optional] 
**connectionType** | **String** |  | [optional] 
**membershipCalculationOptimizationEnabled** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


