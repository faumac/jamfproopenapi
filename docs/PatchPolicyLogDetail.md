# PatchPolicyLogDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**attemptNumber** | **Int** |  | [optional] 
**deviceId** | **String** |  | [optional] 
**actions** | [PatchPolicyLogDetailAction] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


