# StartupStatusAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**startupStatusGet**](StartupStatusAPI.md#startupstatusget) | **GET** /startup-status | Retrieve information about application startup 


# **startupStatusGet**
```swift
    open class func startupStatusGet(completion: @escaping (_ data: StartupStatus?, _ error: Error?) -> Void)
```

Retrieve information about application startup 

Retrieves information about application startup. Current startup operation taking place (if any) and overall startup completion percentage.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve information about application startup 
StartupStatusAPI.startupStatusGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StartupStatus**](StartupStatus.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

