# AppStoreCountryCodesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1AppStoreCountryCodesGet**](AppStoreCountryCodesPreviewAPI.md#v1appstorecountrycodesget) | **GET** /v1/app-store-country-codes | Return a list of Countries and the associated Codes 


# **v1AppStoreCountryCodesGet**
```swift
    open class func v1AppStoreCountryCodesGet(completion: @escaping (_ data: CountryCodes?, _ error: Error?) -> Void)
```

Return a list of Countries and the associated Codes 

Returns a list of countries and the associated codes that can be use for the App Store locale 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return a list of Countries and the associated Codes 
AppStoreCountryCodesPreviewAPI.v1AppStoreCountryCodesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CountryCodes**](CountryCodes.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

