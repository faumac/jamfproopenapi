# ConnectionConfigurationStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connectionVerificationResult** | **String** | connection configuration status for Team Viewer | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


