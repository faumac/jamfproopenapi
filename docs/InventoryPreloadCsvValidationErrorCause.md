# InventoryPreloadCsvValidationErrorCause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | Error-specific code that can be used to identify localization string, etc. | [optional] 
**field** | **String** | Name of the field that caused the error. | 
**description** | **String** | A general description of error for troubleshooting/debugging. Generally this text should not be displayed to a user; instead refer to errorCode and it&#39;s localized text | [optional] 
**id** | **String** | id of object with error. Optional. | [optional] 
**value** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**line** | **Int** |  | [optional] 
**fieldSize** | **Int** |  | [optional] 
**deviceType** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


