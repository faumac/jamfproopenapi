# ObjectHistoryV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**date** | **String** |  | [optional] 
**note** | **String** |  | [optional] 
**details** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


