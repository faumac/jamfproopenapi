# ApplicationConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configuration** | **String** |  | [optional] 
**identifier** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


