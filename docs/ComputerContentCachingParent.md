# ComputerContentCachingParent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentCachingParentId** | **String** |  | [optional] [readonly] 
**address** | **String** |  | [optional] [readonly] 
**alerts** | [**ComputerContentCachingParentAlert**](ComputerContentCachingParentAlert.md) |  | [optional] 
**details** | [**ComputerContentCachingParentDetails**](ComputerContentCachingParentDetails.md) |  | [optional] 
**guid** | **String** |  | [optional] [readonly] 
**healthy** | **Bool** |  | [optional] [readonly] 
**port** | **Int64** |  | [optional] [readonly] 
**version** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


