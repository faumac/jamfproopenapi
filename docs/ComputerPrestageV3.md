# ComputerPrestageV3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**mandatory** | **Bool** |  | 
**mdmRemovable** | **Bool** |  | 
**supportPhoneNumber** | **String** |  | 
**supportEmailAddress** | **String** |  | 
**department** | **String** |  | 
**defaultPrestage** | **Bool** |  | 
**enrollmentSiteId** | **String** |  | 
**keepExistingSiteMembership** | **Bool** |  | 
**keepExistingLocationInformation** | **Bool** |  | 
**requireAuthentication** | **Bool** |  | 
**authenticationPrompt** | **String** |  | 
**preventActivationLock** | **Bool** |  | 
**enableDeviceBasedActivationLock** | **Bool** |  | 
**deviceEnrollmentProgramInstanceId** | **String** |  | 
**skipSetupItems** | **[String: Bool]** |  | [optional] 
**locationInformation** | [**LocationInformationV2**](LocationInformationV2.md) |  | 
**purchasingInformation** | [**PrestagePurchasingInformationV2**](PrestagePurchasingInformationV2.md) |  | 
**anchorCertificates** | **[String]** | The Base64 encoded PEM Certificate | [optional] 
**enrollmentCustomizationId** | **String** |  | [optional] 
**language** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**autoAdvanceSetup** | **Bool** |  | 
**installProfilesDuringSetup** | **Bool** |  | 
**prestageInstalledProfileIds** | **[String]** |  | 
**customPackageIds** | **[String]** |  | 
**customPackageDistributionPointId** | **String** |  | 
**enableRecoveryLock** | **Bool** |  | [optional] 
**recoveryLockPasswordType** | **String** |  | [optional] 
**rotateRecoveryLockPassword** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


