# AppleTvDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**modelNumber** | **String** |  | [optional] 
**isSupervised** | **Bool** |  | [optional] 
**airplayPassword** | **String** |  | [optional] 
**deviceId** | **String** |  | [optional] 
**locales** | **String** |  | [optional] 
**purchasing** | [**Purchasing**](Purchasing.md) |  | [optional] 
**configurationProfiles** | [ConfigurationProfile] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


