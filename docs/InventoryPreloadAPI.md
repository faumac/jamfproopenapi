# InventoryPreloadAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryPreloadCsvTemplateGet**](InventoryPreloadAPI.md#inventorypreloadcsvtemplateget) | **GET** /inventory-preload/csv-template | Get the Inventory Preload CSV template 
[**inventoryPreloadDelete**](InventoryPreloadAPI.md#inventorypreloaddelete) | **DELETE** /inventory-preload | Delete all Inventory Preload records 
[**inventoryPreloadGet**](InventoryPreloadAPI.md#inventorypreloadget) | **GET** /inventory-preload | Return all Inventory Preload records 
[**inventoryPreloadHistoryGet**](InventoryPreloadAPI.md#inventorypreloadhistoryget) | **GET** /inventory-preload/history | Get Inventory Preload history entries 
[**inventoryPreloadHistoryNotesPost**](InventoryPreloadAPI.md#inventorypreloadhistorynotespost) | **POST** /inventory-preload/history/notes | Add Inventory Preload history object notes 
[**inventoryPreloadIdDelete**](InventoryPreloadAPI.md#inventorypreloadiddelete) | **DELETE** /inventory-preload/{id} | Delete an Inventory Preload record 
[**inventoryPreloadIdGet**](InventoryPreloadAPI.md#inventorypreloadidget) | **GET** /inventory-preload/{id} | Get an Inventory Preload record 
[**inventoryPreloadIdPut**](InventoryPreloadAPI.md#inventorypreloadidput) | **PUT** /inventory-preload/{id} | Update an Inventory Preload record 
[**inventoryPreloadPost**](InventoryPreloadAPI.md#inventorypreloadpost) | **POST** /inventory-preload | Create a new Inventory Preload record using JSON or CSV 
[**inventoryPreloadValidateCsvPost**](InventoryPreloadAPI.md#inventorypreloadvalidatecsvpost) | **POST** /inventory-preload/validate-csv | Validate a given CSV file 
[**v1InventoryPreloadCsvTemplateGet**](InventoryPreloadAPI.md#v1inventorypreloadcsvtemplateget) | **GET** /v1/inventory-preload/csv-template | Retrieve the Inventory Preload CSV template 
[**v1InventoryPreloadDelete**](InventoryPreloadAPI.md#v1inventorypreloaddelete) | **DELETE** /v1/inventory-preload | Delete all Inventory Preload records 
[**v1InventoryPreloadGet**](InventoryPreloadAPI.md#v1inventorypreloadget) | **GET** /v1/inventory-preload | Return all Inventory Preload records 
[**v1InventoryPreloadHistoryGet**](InventoryPreloadAPI.md#v1inventorypreloadhistoryget) | **GET** /v1/inventory-preload/history | Get Inventory Preload history entries 
[**v1InventoryPreloadHistoryPost**](InventoryPreloadAPI.md#v1inventorypreloadhistorypost) | **POST** /v1/inventory-preload/history | Add Inventory Preload history object notes 
[**v1InventoryPreloadIdDelete**](InventoryPreloadAPI.md#v1inventorypreloadiddelete) | **DELETE** /v1/inventory-preload/{id} | Delete an Inventory Preload record 
[**v1InventoryPreloadIdGet**](InventoryPreloadAPI.md#v1inventorypreloadidget) | **GET** /v1/inventory-preload/{id} | Get an Inventory Preload record 
[**v1InventoryPreloadIdPut**](InventoryPreloadAPI.md#v1inventorypreloadidput) | **PUT** /v1/inventory-preload/{id} | Update an Inventory Preload record 
[**v1InventoryPreloadPost**](InventoryPreloadAPI.md#v1inventorypreloadpost) | **POST** /v1/inventory-preload | Create a new Inventory Preload record using JSON or CSV 
[**v1InventoryPreloadValidateCsvPost**](InventoryPreloadAPI.md#v1inventorypreloadvalidatecsvpost) | **POST** /v1/inventory-preload/validate-csv | Validate a given CSV file 
[**v2InventoryPreloadCsvGet**](InventoryPreloadAPI.md#v2inventorypreloadcsvget) | **GET** /v2/inventory-preload/csv | Download all Inventory Preload records
[**v2InventoryPreloadCsvPost**](InventoryPreloadAPI.md#v2inventorypreloadcsvpost) | **POST** /v2/inventory-preload/csv | Create one or more new Inventory Preload records using CSV 
[**v2InventoryPreloadCsvTemplateGet**](InventoryPreloadAPI.md#v2inventorypreloadcsvtemplateget) | **GET** /v2/inventory-preload/csv-template | Download the Inventory Preload CSV template
[**v2InventoryPreloadCsvValidatePost**](InventoryPreloadAPI.md#v2inventorypreloadcsvvalidatepost) | **POST** /v2/inventory-preload/csv-validate | Validate a given CSV file 
[**v2InventoryPreloadEaColumnsGet**](InventoryPreloadAPI.md#v2inventorypreloadeacolumnsget) | **GET** /v2/inventory-preload/ea-columns | Retrieve a list of extension attribute columns 
[**v2InventoryPreloadExportPost**](InventoryPreloadAPI.md#v2inventorypreloadexportpost) | **POST** /v2/inventory-preload/export | Export a collection of inventory preload records 
[**v2InventoryPreloadHistoryGet**](InventoryPreloadAPI.md#v2inventorypreloadhistoryget) | **GET** /v2/inventory-preload/history | Get Inventory Preload history entries 
[**v2InventoryPreloadHistoryPost**](InventoryPreloadAPI.md#v2inventorypreloadhistorypost) | **POST** /v2/inventory-preload/history | Add Inventory Preload history object notes
[**v2InventoryPreloadRecordsDeleteAllPost**](InventoryPreloadAPI.md#v2inventorypreloadrecordsdeleteallpost) | **POST** /v2/inventory-preload/records/delete-all | Delete all Inventory Preload records 
[**v2InventoryPreloadRecordsGet**](InventoryPreloadAPI.md#v2inventorypreloadrecordsget) | **GET** /v2/inventory-preload/records | Return all Inventory Preload records
[**v2InventoryPreloadRecordsIdDelete**](InventoryPreloadAPI.md#v2inventorypreloadrecordsiddelete) | **DELETE** /v2/inventory-preload/records/{id} | Delete an Inventory Preload record 
[**v2InventoryPreloadRecordsIdGet**](InventoryPreloadAPI.md#v2inventorypreloadrecordsidget) | **GET** /v2/inventory-preload/records/{id} | Get an Inventory Preload record
[**v2InventoryPreloadRecordsIdPut**](InventoryPreloadAPI.md#v2inventorypreloadrecordsidput) | **PUT** /v2/inventory-preload/records/{id} | Update an Inventory Preload record
[**v2InventoryPreloadRecordsPost**](InventoryPreloadAPI.md#v2inventorypreloadrecordspost) | **POST** /v2/inventory-preload/records | Create a new Inventory Preload record using JSON


# **inventoryPreloadCsvTemplateGet**
```swift
    open class func inventoryPreloadCsvTemplateGet(completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Get the Inventory Preload CSV template 

Retrieves the Inventory Preload CSV template.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the Inventory Preload CSV template 
InventoryPreloadAPI.inventoryPreloadCsvTemplateGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadDelete**
```swift
    open class func inventoryPreloadDelete(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete all Inventory Preload records 

Deletes all Inventory Preload records.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete all Inventory Preload records 
InventoryPreloadAPI.inventoryPreloadDelete() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadGet**
```swift
    open class func inventoryPreloadGet(page: Int? = nil, pagesize: Int? = nil, sort: Sort_inventoryPreloadGet? = nil, sortBy: String? = nil, completion: @escaping (_ data: [InventoryPreloadRecordSearchResults]?, _ error: Error?) -> Void)
```

Return all Inventory Preload records 

Returns all Inventory Preload records.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pagesize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String |  (optional) (default to .asc)
let sortBy = "sortBy_example" // String |  (optional) (default to "id")

// Return all Inventory Preload records 
InventoryPreloadAPI.inventoryPreloadGet(page: page, pagesize: pagesize, sort: sort, sortBy: sortBy) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** |  | [optional] [default to .asc]
 **sortBy** | **String** |  | [optional] [default to &quot;id&quot;]

### Return type

[**[InventoryPreloadRecordSearchResults]**](InventoryPreloadRecordSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadHistoryGet**
```swift
    open class func inventoryPreloadHistoryGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Inventory Preload history entries 

Gets Inventory Preload history entries.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "date:desc")

// Get Inventory Preload history entries 
InventoryPreloadAPI.inventoryPreloadHistoryGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;date:desc&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadHistoryNotesPost**
```swift
    open class func inventoryPreloadHistoryNotesPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Inventory Preload history object notes 

Adds Inventory Preload history object notes.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Inventory Preload history object notes 
InventoryPreloadAPI.inventoryPreloadHistoryNotesPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadIdDelete**
```swift
    open class func inventoryPreloadIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an Inventory Preload record 

Deletes an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Inventory Preload identifier

// Delete an Inventory Preload record 
InventoryPreloadAPI.inventoryPreloadIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Inventory Preload identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadIdGet**
```swift
    open class func inventoryPreloadIdGet(id: Int, completion: @escaping (_ data: InventoryPreloadRecord?, _ error: Error?) -> Void)
```

Get an Inventory Preload record 

Retrieves an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Inventory Preload identifier

// Get an Inventory Preload record 
InventoryPreloadAPI.inventoryPreloadIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Inventory Preload identifier | 

### Return type

[**InventoryPreloadRecord**](InventoryPreloadRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadIdPut**
```swift
    open class func inventoryPreloadIdPut(id: Int, inventoryPreloadRecord: InventoryPreloadRecord, completion: @escaping (_ data: InventoryPreloadRecord?, _ error: Error?) -> Void)
```

Update an Inventory Preload record 

Updates an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Inventory Preload identifier
let inventoryPreloadRecord = InventoryPreloadRecord(id: 123, serialNumber: "serialNumber_example", deviceType: "deviceType_example", username: "username_example", fullName: "fullName_example", emailAddress: "emailAddress_example", phoneNumber: "phoneNumber_example", position: "position_example", department: "department_example", building: "building_example", room: "room_example", poNumber: "poNumber_example", poDate: "poDate_example", warrantyExpiration: "warrantyExpiration_example", appleCareId: "appleCareId_example", lifeExpectancy: "lifeExpectancy_example", purchasePrice: "purchasePrice_example", purchasingContact: "purchasingContact_example", purchasingAccount: "purchasingAccount_example", leaseExpiration: "leaseExpiration_example", barCode1: "barCode1_example", barCode2: "barCode2_example", assetTag: "assetTag_example", vendor: "vendor_example", extensionAttributes: [InventoryPreloadExtensionAttribute(name: "name_example", value: "value_example")]) // InventoryPreloadRecord | Inventory Preload record to update

// Update an Inventory Preload record 
InventoryPreloadAPI.inventoryPreloadIdPut(id: id, inventoryPreloadRecord: inventoryPreloadRecord) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Inventory Preload identifier | 
 **inventoryPreloadRecord** | [**InventoryPreloadRecord**](InventoryPreloadRecord.md) | Inventory Preload record to update | 

### Return type

[**InventoryPreloadRecord**](InventoryPreloadRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadPost**
```swift
    open class func inventoryPreloadPost(inventoryPreloadRecord: InventoryPreloadRecord, completion: @escaping (_ data: InventoryPreloadRecord?, _ error: Error?) -> Void)
```

Create a new Inventory Preload record using JSON or CSV 

Create a new Inventory Preload record using JSON or CSV. A CSV template can be downloaded from /api/inventory-preload/csv-template. Serial number and device type are required. All other fields are optional. When a matching serial number exists in the Inventory Preload data, the record will be overwritten with the CSV data. If the CSV file contains a new username and an email address is provided, the new user is created in Jamf Pro. If the CSV file contains an existing username, the following user-related fields are updated in Jamf Pro. Full Name, Email Address, Phone Number, Position. This endpoint does not do full validation of each record in the CSV data. To do full validation, use the /inventory-preload/validate-csv endpoint first. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let inventoryPreloadRecord = InventoryPreloadRecord(id: 123, serialNumber: "serialNumber_example", deviceType: "deviceType_example", username: "username_example", fullName: "fullName_example", emailAddress: "emailAddress_example", phoneNumber: "phoneNumber_example", position: "position_example", department: "department_example", building: "building_example", room: "room_example", poNumber: "poNumber_example", poDate: "poDate_example", warrantyExpiration: "warrantyExpiration_example", appleCareId: "appleCareId_example", lifeExpectancy: "lifeExpectancy_example", purchasePrice: "purchasePrice_example", purchasingContact: "purchasingContact_example", purchasingAccount: "purchasingAccount_example", leaseExpiration: "leaseExpiration_example", barCode1: "barCode1_example", barCode2: "barCode2_example", assetTag: "assetTag_example", vendor: "vendor_example", extensionAttributes: [InventoryPreloadExtensionAttribute(name: "name_example", value: "value_example")]) // InventoryPreloadRecord | Inventory Preload record or records to be created

// Create a new Inventory Preload record using JSON or CSV 
InventoryPreloadAPI.inventoryPreloadPost(inventoryPreloadRecord: inventoryPreloadRecord) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventoryPreloadRecord** | [**InventoryPreloadRecord**](InventoryPreloadRecord.md) | Inventory Preload record or records to be created | 

### Return type

[**InventoryPreloadRecord**](InventoryPreloadRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/csv
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **inventoryPreloadValidateCsvPost**
```swift
    open class func inventoryPreloadValidateCsvPost(body: AnyCodable, completion: @escaping (_ data: InventoryPreloadCsvValidationSuccess?, _ error: Error?) -> Void)
```

Validate a given CSV file 

Validate a given CSV file. Serial number and device type are required. All other fields are optional. A CSV template can be downloaded from /api/inventory-preload/csv-template. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let body = "TODO" // AnyCodable | Inventory Preload records to be validated. A CSV template can be downloaded from /api/inventory-preload/csv-template

// Validate a given CSV file 
InventoryPreloadAPI.inventoryPreloadValidateCsvPost(body: body) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **AnyCodable** | Inventory Preload records to be validated. A CSV template can be downloaded from /api/inventory-preload/csv-template | 

### Return type

[**InventoryPreloadCsvValidationSuccess**](InventoryPreloadCsvValidationSuccess.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: text/csv
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadCsvTemplateGet**
```swift
    open class func v1InventoryPreloadCsvTemplateGet(completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Retrieve the Inventory Preload CSV template 

Retrieves the Inventory Preload CSV template.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the Inventory Preload CSV template 
InventoryPreloadAPI.v1InventoryPreloadCsvTemplateGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadDelete**
```swift
    open class func v1InventoryPreloadDelete(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete all Inventory Preload records 

Deletes all Inventory Preload records.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete all Inventory Preload records 
InventoryPreloadAPI.v1InventoryPreloadDelete() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadGet**
```swift
    open class func v1InventoryPreloadGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: InventoryPreloadRecordSearchResults?, _ error: Error?) -> Void)
```

Return all Inventory Preload records 

Returns all Inventory Preload records.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "id:asc")

// Return all Inventory Preload records 
InventoryPreloadAPI.v1InventoryPreloadGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;id:asc&quot;]

### Return type

[**InventoryPreloadRecordSearchResults**](InventoryPreloadRecordSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadHistoryGet**
```swift
    open class func v1InventoryPreloadHistoryGet(page: Int? = nil, size: Int? = nil, pagesize: Int? = nil, pageSize: Int? = nil, sort: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Inventory Preload history entries 

Gets Inventory Preload history entries.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let size = 987 // Int |  (optional) (default to 100)
let pagesize = 987 // Int |  (optional) (default to 100)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "date:desc")

// Get Inventory Preload history entries 
InventoryPreloadAPI.v1InventoryPreloadHistoryGet(page: page, size: size, pagesize: pagesize, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **size** | **Int** |  | [optional] [default to 100]
 **pagesize** | **Int** |  | [optional] [default to 100]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;date:desc&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadHistoryPost**
```swift
    open class func v1InventoryPreloadHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Inventory Preload history object notes 

Adds Inventory Preload history object notes.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Inventory Preload history object notes 
InventoryPreloadAPI.v1InventoryPreloadHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadIdDelete**
```swift
    open class func v1InventoryPreloadIdDelete(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an Inventory Preload record 

Deletes an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Inventory Preload identifier

// Delete an Inventory Preload record 
InventoryPreloadAPI.v1InventoryPreloadIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Inventory Preload identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadIdGet**
```swift
    open class func v1InventoryPreloadIdGet(id: Int, completion: @escaping (_ data: InventoryPreloadRecord?, _ error: Error?) -> Void)
```

Get an Inventory Preload record 

Retrieves an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Inventory Preload identifier

// Get an Inventory Preload record 
InventoryPreloadAPI.v1InventoryPreloadIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Inventory Preload identifier | 

### Return type

[**InventoryPreloadRecord**](InventoryPreloadRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadIdPut**
```swift
    open class func v1InventoryPreloadIdPut(id: Int, inventoryPreloadRecord: InventoryPreloadRecord, completion: @escaping (_ data: InventoryPreloadRecord?, _ error: Error?) -> Void)
```

Update an Inventory Preload record 

Updates an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Inventory Preload identifier
let inventoryPreloadRecord = InventoryPreloadRecord(id: 123, serialNumber: "serialNumber_example", deviceType: "deviceType_example", username: "username_example", fullName: "fullName_example", emailAddress: "emailAddress_example", phoneNumber: "phoneNumber_example", position: "position_example", department: "department_example", building: "building_example", room: "room_example", poNumber: "poNumber_example", poDate: "poDate_example", warrantyExpiration: "warrantyExpiration_example", appleCareId: "appleCareId_example", lifeExpectancy: "lifeExpectancy_example", purchasePrice: "purchasePrice_example", purchasingContact: "purchasingContact_example", purchasingAccount: "purchasingAccount_example", leaseExpiration: "leaseExpiration_example", barCode1: "barCode1_example", barCode2: "barCode2_example", assetTag: "assetTag_example", vendor: "vendor_example", extensionAttributes: [InventoryPreloadExtensionAttribute(name: "name_example", value: "value_example")]) // InventoryPreloadRecord | Inventory Preload record to update

// Update an Inventory Preload record 
InventoryPreloadAPI.v1InventoryPreloadIdPut(id: id, inventoryPreloadRecord: inventoryPreloadRecord) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Inventory Preload identifier | 
 **inventoryPreloadRecord** | [**InventoryPreloadRecord**](InventoryPreloadRecord.md) | Inventory Preload record to update | 

### Return type

[**InventoryPreloadRecord**](InventoryPreloadRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadPost**
```swift
    open class func v1InventoryPreloadPost(inventoryPreloadRecord: InventoryPreloadRecord, completion: @escaping (_ data: InventoryPreloadRecord?, _ error: Error?) -> Void)
```

Create a new Inventory Preload record using JSON or CSV 

Create a new Inventory Preload record using JSON or CSV. A CSV template can be downloaded from /api/inventory-preload/csv-template. Serial number and device type are required. All other fields are optional. When a matching serial number exists in the Inventory Preload data, the record will be overwritten with the CSV data. If the CSV file contains a new username and an email address is provided, the new user is created in Jamf Pro. If the CSV file contains an existing username, the following user-related fields are updated in Jamf Pro. Full Name, Email Address, Phone Number, Position. This endpoint does not do full validation of each record in the CSV data. To do full validation, use the /inventory-preload/validate-csv endpoint first. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let inventoryPreloadRecord = InventoryPreloadRecord(id: 123, serialNumber: "serialNumber_example", deviceType: "deviceType_example", username: "username_example", fullName: "fullName_example", emailAddress: "emailAddress_example", phoneNumber: "phoneNumber_example", position: "position_example", department: "department_example", building: "building_example", room: "room_example", poNumber: "poNumber_example", poDate: "poDate_example", warrantyExpiration: "warrantyExpiration_example", appleCareId: "appleCareId_example", lifeExpectancy: "lifeExpectancy_example", purchasePrice: "purchasePrice_example", purchasingContact: "purchasingContact_example", purchasingAccount: "purchasingAccount_example", leaseExpiration: "leaseExpiration_example", barCode1: "barCode1_example", barCode2: "barCode2_example", assetTag: "assetTag_example", vendor: "vendor_example", extensionAttributes: [InventoryPreloadExtensionAttribute(name: "name_example", value: "value_example")]) // InventoryPreloadRecord | Inventory Preload record or records to be created

// Create a new Inventory Preload record using JSON or CSV 
InventoryPreloadAPI.v1InventoryPreloadPost(inventoryPreloadRecord: inventoryPreloadRecord) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventoryPreloadRecord** | [**InventoryPreloadRecord**](InventoryPreloadRecord.md) | Inventory Preload record or records to be created | 

### Return type

[**InventoryPreloadRecord**](InventoryPreloadRecord.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/csv
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1InventoryPreloadValidateCsvPost**
```swift
    open class func v1InventoryPreloadValidateCsvPost(body: AnyCodable, completion: @escaping (_ data: InventoryPreloadCsvValidationSuccess?, _ error: Error?) -> Void)
```

Validate a given CSV file 

Validate a given CSV file. Serial number and device type are required. All other fields are optional. A CSV template can be downloaded from /api/inventory-preload/csv-template. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let body = "TODO" // AnyCodable | Inventory Preload records to be validated. A CSV template can be downloaded from /api/inventory-preload/csv-template

// Validate a given CSV file 
InventoryPreloadAPI.v1InventoryPreloadValidateCsvPost(body: body) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **AnyCodable** | Inventory Preload records to be validated. A CSV template can be downloaded from /api/inventory-preload/csv-template | 

### Return type

[**InventoryPreloadCsvValidationSuccess**](InventoryPreloadCsvValidationSuccess.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: text/csv
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadCsvGet**
```swift
    open class func v2InventoryPreloadCsvGet(completion: @escaping (_ data: String?, _ error: Error?) -> Void)
```

Download all Inventory Preload records

Returns all Inventory Preload records as a CSV file.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Download all Inventory Preload records
InventoryPreloadAPI.v2InventoryPreloadCsvGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadCsvPost**
```swift
    open class func v2InventoryPreloadCsvPost(file: String, completion: @escaping (_ data: [HrefResponse]?, _ error: Error?) -> Void)
```

Create one or more new Inventory Preload records using CSV 

Create one or more new Inventory Preload records using CSV. A CSV template can be downloaded from /v2/inventory-preload/csv-template. Serial number and device type are required. All other fields are optional. When a matching serial number exists in the Inventory Preload data, the record will be overwritten with the CSV data. If the CSV file contains a new username and an email address is provided, the new user is created in Jamf Pro. If the CSV file contains an existing username, the following user-related fields are updated in Jamf Pro. Full Name, Email Address, Phone Number, Position. This endpoint does not do full validation of each record in the CSV data. To do full validation, use the `/v2/inventory-preload/csv-validate` endpoint first. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let file = "file_example" // String | The CSV file to upload

// Create one or more new Inventory Preload records using CSV 
InventoryPreloadAPI.v2InventoryPreloadCsvPost(file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **String** | The CSV file to upload | 

### Return type

[**[HrefResponse]**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadCsvTemplateGet**
```swift
    open class func v2InventoryPreloadCsvTemplateGet(completion: @escaping (_ data: String?, _ error: Error?) -> Void)
```

Download the Inventory Preload CSV template

Retrieves the Inventory Preload CSV file template.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Download the Inventory Preload CSV template
InventoryPreloadAPI.v2InventoryPreloadCsvTemplateGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadCsvValidatePost**
```swift
    open class func v2InventoryPreloadCsvValidatePost(file: String, completion: @escaping (_ data: InventoryPreloadCsvValidationSuccess?, _ error: Error?) -> Void)
```

Validate a given CSV file 

Validate a given CSV file. Serial number and device type are required. All other fields are optional. A CSV template can be downloaded from `/v2/inventory-preload/csv-template`. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let file = "file_example" // String | The CSV file to upload

// Validate a given CSV file 
InventoryPreloadAPI.v2InventoryPreloadCsvValidatePost(file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **String** | The CSV file to upload | 

### Return type

[**InventoryPreloadCsvValidationSuccess**](InventoryPreloadCsvValidationSuccess.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadEaColumnsGet**
```swift
    open class func v2InventoryPreloadEaColumnsGet(completion: @escaping (_ data: InventoryPreloadExtensionAttributeColumnResult?, _ error: Error?) -> Void)
```

Retrieve a list of extension attribute columns 

Retrieve a list of extension attribute columns currently associated with inventory preload records 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve a list of extension attribute columns 
InventoryPreloadAPI.v2InventoryPreloadEaColumnsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InventoryPreloadExtensionAttributeColumnResult**](InventoryPreloadExtensionAttributeColumnResult.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadExportPost**
```swift
    open class func v2InventoryPreloadExportPost(exportFields: [String]? = nil, exportLabels: [String]? = nil, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, exportParameters: ExportParameters? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Export a collection of inventory preload records 

Export a collection of inventory preload records 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let exportFields = ["inner_example"] // [String] | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields=id,username (optional)
let exportLabels = ["inner_example"] // [String] | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels=identifier,name with matching: export-fields=id,username (optional)
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `id:asc`. Multiple sort criteria are supported and must be separated with a comma. All inventory preload fields are supported, however fields added by extension attributes are not supported. If sorting by deviceType, use `0` for Computer and `1` for Mobile Device.  Example: `sort=date:desc,name:asc`.  (optional)
let filter = "filter_example" // String | Allowing to filter inventory preload records. Default search is empty query - returning all results for the requested page. All inventory preload fields are supported, however fields added by extension attributes are not supported. If filtering by deviceType, use `0` for Computer and `1` for Mobile Device.  Query in the RSQL format, allowing `==`, `!=`, `>`, `<`, and `=in=`.  Example: `filter=categoryName==\"Category\"`  (optional) (default to "")
let exportParameters = ExportParameters(page: 123, pageSize: 123, sort: ["sort_example"], filter: "filter_example", fields: [ExportField(fieldName: "fieldName_example", fieldLabelOverride: "fieldLabelOverride_example")]) // ExportParameters | Optional. Override query parameters since they can make URI exceed 2,000 character limit. (optional)

// Export a collection of inventory preload records 
InventoryPreloadAPI.v2InventoryPreloadExportPost(exportFields: exportFields, exportLabels: exportLabels, page: page, pageSize: pageSize, sort: sort, filter: filter, exportParameters: exportParameters) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exportFields** | [**[String]**](String.md) | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields&#x3D;id,username | [optional] 
 **exportLabels** | [**[String]**](String.md) | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels&#x3D;identifier,name with matching: export-fields&#x3D;id,username | [optional] 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;id:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma. All inventory preload fields are supported, however fields added by extension attributes are not supported. If sorting by deviceType, use &#x60;0&#x60; for Computer and &#x60;1&#x60; for Mobile Device.  Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 
 **filter** | **String** | Allowing to filter inventory preload records. Default search is empty query - returning all results for the requested page. All inventory preload fields are supported, however fields added by extension attributes are not supported. If filtering by deviceType, use &#x60;0&#x60; for Computer and &#x60;1&#x60; for Mobile Device.  Query in the RSQL format, allowing &#x60;&#x3D;&#x3D;&#x60;, &#x60;!&#x3D;&#x60;, &#x60;&gt;&#x60;, &#x60;&lt;&#x60;, and &#x60;&#x3D;in&#x3D;&#x60;.  Example: &#x60;filter&#x3D;categoryName&#x3D;&#x3D;\&quot;Category\&quot;&#x60;  | [optional] [default to &quot;&quot;]
 **exportParameters** | [**ExportParameters**](ExportParameters.md) | Optional. Override query parameters since they can make URI exceed 2,000 character limit. | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/csv, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadHistoryGet**
```swift
    open class func v2InventoryPreloadHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Inventory Preload history entries 

Gets Inventory Preload history entries.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `date:desc`. Multiple sort criteria are supported and must be separated with a comma.  Example: `sort=date:desc,name:asc`.  (optional)
let filter = "filter_example" // String | Allows filtering inventory preload history records. Default search is empty query - returning all results for the requested page. All inventory preload history fields are supported.  Query in the RSQL format, allowing `==`, `!=`, `>`, `<`, and `=in=`.  Example: `filter=username==\"admin\"`  (optional) (default to "")

// Get Inventory Preload history entries 
InventoryPreloadAPI.v2InventoryPreloadHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;date:desc&#x60;. Multiple sort criteria are supported and must be separated with a comma.  Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 
 **filter** | **String** | Allows filtering inventory preload history records. Default search is empty query - returning all results for the requested page. All inventory preload history fields are supported.  Query in the RSQL format, allowing &#x60;&#x3D;&#x3D;&#x60;, &#x60;!&#x3D;&#x60;, &#x60;&gt;&#x60;, &#x60;&lt;&#x60;, and &#x60;&#x3D;in&#x3D;&#x60;.  Example: &#x60;filter&#x3D;username&#x3D;&#x3D;\&quot;admin\&quot;&#x60;  | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadHistoryPost**
```swift
    open class func v2InventoryPreloadHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Inventory Preload history object notes

Adds Inventory Preload history object notes.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | History notes to create

// Add Inventory Preload history object notes
InventoryPreloadAPI.v2InventoryPreloadHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | History notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadRecordsDeleteAllPost**
```swift
    open class func v2InventoryPreloadRecordsDeleteAllPost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete all Inventory Preload records 

Deletes all Inventory Preload records.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete all Inventory Preload records 
InventoryPreloadAPI.v2InventoryPreloadRecordsDeleteAllPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadRecordsGet**
```swift
    open class func v2InventoryPreloadRecordsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: InventoryPreloadRecordSearchResultsV2?, _ error: Error?) -> Void)
```

Return all Inventory Preload records

Returns all Inventory Preload records.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: `property:asc/desc`. Default sort is `id:asc`. Multiple sort criteria are supported and must be separated with a comma. All inventory preload fields are supported, however fields added by extension attributes are not supported. If sorting by deviceType, use `0` for Computer and `1` for Mobile Device.  Example: `sort=date:desc,name:asc`.  (optional)
let filter = "filter_example" // String | Allowing to filter inventory preload records. Default search is empty query - returning all results for the requested page. All inventory preload fields are supported, however fields added by extension attributes are not supported. If filtering by deviceType, use `0` for Computer and `1` for Mobile Device.  Query in the RSQL format, allowing `==`, `!=`, `>`, `<`, and `=in=`.  Example: `filter=categoryName==\"Category\"`  (optional) (default to "")

// Return all Inventory Preload records
InventoryPreloadAPI.v2InventoryPreloadRecordsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: &#x60;property:asc/desc&#x60;. Default sort is &#x60;id:asc&#x60;. Multiple sort criteria are supported and must be separated with a comma. All inventory preload fields are supported, however fields added by extension attributes are not supported. If sorting by deviceType, use &#x60;0&#x60; for Computer and &#x60;1&#x60; for Mobile Device.  Example: &#x60;sort&#x3D;date:desc,name:asc&#x60;.  | [optional] 
 **filter** | **String** | Allowing to filter inventory preload records. Default search is empty query - returning all results for the requested page. All inventory preload fields are supported, however fields added by extension attributes are not supported. If filtering by deviceType, use &#x60;0&#x60; for Computer and &#x60;1&#x60; for Mobile Device.  Query in the RSQL format, allowing &#x60;&#x3D;&#x3D;&#x60;, &#x60;!&#x3D;&#x60;, &#x60;&gt;&#x60;, &#x60;&lt;&#x60;, and &#x60;&#x3D;in&#x3D;&#x60;.  Example: &#x60;filter&#x3D;categoryName&#x3D;&#x3D;\&quot;Category\&quot;&#x60;  | [optional] [default to &quot;&quot;]

### Return type

[**InventoryPreloadRecordSearchResultsV2**](InventoryPreloadRecordSearchResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadRecordsIdDelete**
```swift
    open class func v2InventoryPreloadRecordsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete an Inventory Preload record 

Deletes an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Inventory Preload identifier

// Delete an Inventory Preload record 
InventoryPreloadAPI.v2InventoryPreloadRecordsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Inventory Preload identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadRecordsIdGet**
```swift
    open class func v2InventoryPreloadRecordsIdGet(id: String, completion: @escaping (_ data: InventoryPreloadRecordV2?, _ error: Error?) -> Void)
```

Get an Inventory Preload record

Retrieves an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Inventory Preload identifier

// Get an Inventory Preload record
InventoryPreloadAPI.v2InventoryPreloadRecordsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Inventory Preload identifier | 

### Return type

[**InventoryPreloadRecordV2**](InventoryPreloadRecordV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadRecordsIdPut**
```swift
    open class func v2InventoryPreloadRecordsIdPut(id: String, inventoryPreloadRecordV2: InventoryPreloadRecordV2, completion: @escaping (_ data: InventoryPreloadRecordV2?, _ error: Error?) -> Void)
```

Update an Inventory Preload record

Updates an Inventory Preload record.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Inventory Preload identifier
let inventoryPreloadRecordV2 = InventoryPreloadRecordV2(id: "id_example", serialNumber: "serialNumber_example", deviceType: "deviceType_example", username: "username_example", fullName: "fullName_example", emailAddress: "emailAddress_example", phoneNumber: "phoneNumber_example", position: "position_example", department: "department_example", building: "building_example", room: "room_example", poNumber: "poNumber_example", poDate: "poDate_example", warrantyExpiration: "warrantyExpiration_example", appleCareId: "appleCareId_example", lifeExpectancy: "lifeExpectancy_example", purchasePrice: "purchasePrice_example", purchasingContact: "purchasingContact_example", purchasingAccount: "purchasingAccount_example", leaseExpiration: "leaseExpiration_example", barCode1: "barCode1_example", barCode2: "barCode2_example", assetTag: "assetTag_example", vendor: "vendor_example", extensionAttributes: [InventoryPreloadExtensionAttribute(name: "name_example", value: "value_example")]) // InventoryPreloadRecordV2 | Inventory Preload record to update

// Update an Inventory Preload record
InventoryPreloadAPI.v2InventoryPreloadRecordsIdPut(id: id, inventoryPreloadRecordV2: inventoryPreloadRecordV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Inventory Preload identifier | 
 **inventoryPreloadRecordV2** | [**InventoryPreloadRecordV2**](InventoryPreloadRecordV2.md) | Inventory Preload record to update | 

### Return type

[**InventoryPreloadRecordV2**](InventoryPreloadRecordV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2InventoryPreloadRecordsPost**
```swift
    open class func v2InventoryPreloadRecordsPost(inventoryPreloadRecordV2: InventoryPreloadRecordV2, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a new Inventory Preload record using JSON

Create a new Inventory Preload record using JSON.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let inventoryPreloadRecordV2 = InventoryPreloadRecordV2(id: "id_example", serialNumber: "serialNumber_example", deviceType: "deviceType_example", username: "username_example", fullName: "fullName_example", emailAddress: "emailAddress_example", phoneNumber: "phoneNumber_example", position: "position_example", department: "department_example", building: "building_example", room: "room_example", poNumber: "poNumber_example", poDate: "poDate_example", warrantyExpiration: "warrantyExpiration_example", appleCareId: "appleCareId_example", lifeExpectancy: "lifeExpectancy_example", purchasePrice: "purchasePrice_example", purchasingContact: "purchasingContact_example", purchasingAccount: "purchasingAccount_example", leaseExpiration: "leaseExpiration_example", barCode1: "barCode1_example", barCode2: "barCode2_example", assetTag: "assetTag_example", vendor: "vendor_example", extensionAttributes: [InventoryPreloadExtensionAttribute(name: "name_example", value: "value_example")]) // InventoryPreloadRecordV2 | Inventory Preload record to be created.

// Create a new Inventory Preload record using JSON
InventoryPreloadAPI.v2InventoryPreloadRecordsPost(inventoryPreloadRecordV2: inventoryPreloadRecordV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventoryPreloadRecordV2** | [**InventoryPreloadRecordV2**](InventoryPreloadRecordV2.md) | Inventory Preload record to be created. | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

