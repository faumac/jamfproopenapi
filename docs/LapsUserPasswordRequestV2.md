# LapsUserPasswordRequestV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lapsUserPasswordList** | [LapsUserPasswordV2] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


