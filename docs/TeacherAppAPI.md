# TeacherAppAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1TeacherAppGet**](TeacherAppAPI.md#v1teacherappget) | **GET** /v1/teacher-app | Get the Jamf Teacher settings that you have access to see 
[**v1TeacherAppHistoryGet**](TeacherAppAPI.md#v1teacherapphistoryget) | **GET** /v1/teacher-app/history | Get Jamf Teacher app settings history 
[**v1TeacherAppHistoryPost**](TeacherAppAPI.md#v1teacherapphistorypost) | **POST** /v1/teacher-app/history | Add Jamf Teacher app settings history notes 
[**v1TeacherAppPut**](TeacherAppAPI.md#v1teacherappput) | **PUT** /v1/teacher-app | Update a Jamf Teacher settings object 


# **v1TeacherAppGet**
```swift
    open class func v1TeacherAppGet(completion: @escaping (_ data: TeacherSettingsResponse?, _ error: Error?) -> Void)
```

Get the Jamf Teacher settings that you have access to see 

Get the Jamf Teacher settings that you have access to see.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the Jamf Teacher settings that you have access to see 
TeacherAppAPI.v1TeacherAppGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**TeacherSettingsResponse**](TeacherSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1TeacherAppHistoryGet**
```swift
    open class func v1TeacherAppHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Jamf Teacher app settings history 

Gets Jamf Teacher app settings history 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the 'sort' query param is not duplicated for each sort criterion, e.g., ...&sort=name:asc,date:desc. Fields that can be sorted: status, updated (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Jamf Teacher app settings history 
TeacherAppAPI.v1TeacherAppHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort order is descending. Multiple sort criteria are supported and must be entered on separate lines in Swagger UI. In the URI the &#39;sort&#39; query param is not duplicated for each sort criterion, e.g., ...&amp;sort&#x3D;name:asc,date:desc. Fields that can be sorted: status, updated | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1TeacherAppHistoryPost**
```swift
    open class func v1TeacherAppHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add Jamf Teacher app settings history notes 

Adds Jamf Teacher app settings history notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Jamf Teacher app settings history notes 
TeacherAppAPI.v1TeacherAppHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1TeacherAppPut**
```swift
    open class func v1TeacherAppPut(teacherSettingsRequest: TeacherSettingsRequest, completion: @escaping (_ data: TeacherSettingsResponse?, _ error: Error?) -> Void)
```

Update a Jamf Teacher settings object 

Update a Jamf Teacher settings object.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let teacherSettingsRequest = TeacherSettingsRequest(isEnabled: true, timezoneId: "timezoneId_example", autoClear: "autoClear_example", maxRestrictionLengthSeconds: 123, safelistedApps: [SafelistedApp(name: "name_example", bundleId: "bundleId_example")]) // TeacherSettingsRequest | Teacher settings to create.

// Update a Jamf Teacher settings object 
TeacherAppAPI.v1TeacherAppPut(teacherSettingsRequest: teacherSettingsRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **teacherSettingsRequest** | [**TeacherSettingsRequest**](TeacherSettingsRequest.md) | Teacher settings to create. | 

### Return type

[**TeacherSettingsResponse**](TeacherSettingsResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

