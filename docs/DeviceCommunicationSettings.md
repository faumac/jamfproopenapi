# DeviceCommunicationSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**autoRenewMobileDeviceMdmProfileWhenCaRenewed** | **Bool** |  | [optional] 
**autoRenewMobileDeviceMdmProfileWhenDeviceIdentityCertExpiring** | **Bool** |  | [optional] 
**autoRenewComputerMdmProfileWhenCaRenewed** | **Bool** |  | [optional] 
**autoRenewComputerMdmProfileWhenDeviceIdentityCertExpiring** | **Bool** |  | [optional] 
**mdmProfileMobileDeviceExpirationLimitInDays** | **Int** |  | [optional] [default to ._180]
**mdmProfileComputerExpirationLimitInDays** | **Int** |  | [optional] [default to ._180]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


