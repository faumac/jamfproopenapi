# PatchSoftwareTitleDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** |  | [optional] 
**minimumOperatingSystem** | **String** |  | [optional] [default to "-1"]
**releaseDate** | **String** |  | [optional] [default to "-1"]
**rebootRequired** | **Bool** |  | [optional] [default to false]
**killApps** | [PatchSoftwareTitleConfigurationDefinitionKillApp] |  | [optional] 
**standalone** | **Bool** |  | [optional] [default to false]
**absoluteOrderId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


