# TimeZone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zoneId** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**displayName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


