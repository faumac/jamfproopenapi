# VolumePurchasingLocationPatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**automaticallyPopulatePurchasedContent** | **Bool** |  | [optional] 
**sendNotificationWhenNoLongerAssigned** | **Bool** |  | [optional] 
**autoRegisterManagedUsers** | **Bool** |  | [optional] [default to false]
**siteId** | **String** |  | [optional] 
**serviceToken** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


