# SelfServiceBrandingMacosAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SelfServiceBrandingMacosGet**](SelfServiceBrandingMacosAPI.md#v1selfservicebrandingmacosget) | **GET** /v1/self-service/branding/macos | Search for sorted and paged macOS branding configurations 
[**v1SelfServiceBrandingMacosIdDelete**](SelfServiceBrandingMacosAPI.md#v1selfservicebrandingmacosiddelete) | **DELETE** /v1/self-service/branding/macos/{id} | Delete the Self Service macOS branding configuration indicated by the provided id 
[**v1SelfServiceBrandingMacosIdGet**](SelfServiceBrandingMacosAPI.md#v1selfservicebrandingmacosidget) | **GET** /v1/self-service/branding/macos/{id} | Read a single Self Service macOS branding configuration indicated by the provided id 
[**v1SelfServiceBrandingMacosIdPut**](SelfServiceBrandingMacosAPI.md#v1selfservicebrandingmacosidput) | **PUT** /v1/self-service/branding/macos/{id} | Update a Self Service macOS branding configuration with the supplied details 
[**v1SelfServiceBrandingMacosPost**](SelfServiceBrandingMacosAPI.md#v1selfservicebrandingmacospost) | **POST** /v1/self-service/branding/macos | Create a Self Service macOS branding configuration with the supplied 


# **v1SelfServiceBrandingMacosGet**
```swift
    open class func v1SelfServiceBrandingMacosGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: MacOsBrandingSearchResults?, _ error: Error?) -> Void)
```

Search for sorted and paged macOS branding configurations 

Search for sorted and paged macOS branding configurations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=id:desc,brandingName:asc  (optional)

// Search for sorted and paged macOS branding configurations 
SelfServiceBrandingMacosAPI.v1SelfServiceBrandingMacosGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;id:desc,brandingName:asc  | [optional] 

### Return type

[**MacOsBrandingSearchResults**](MacOsBrandingSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingMacosIdDelete**
```swift
    open class func v1SelfServiceBrandingMacosIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the Self Service macOS branding configuration indicated by the provided id 

Delete the Self Service macOS branding configuration indicated by the provided id.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of macOS branding configuration

// Delete the Self Service macOS branding configuration indicated by the provided id 
SelfServiceBrandingMacosAPI.v1SelfServiceBrandingMacosIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of macOS branding configuration | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingMacosIdGet**
```swift
    open class func v1SelfServiceBrandingMacosIdGet(id: String, completion: @escaping (_ data: MacOsBrandingConfiguration?, _ error: Error?) -> Void)
```

Read a single Self Service macOS branding configuration indicated by the provided id 

Read a single Self Service macOS branding configuration indicated by the provided id.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of macOS branding configuration

// Read a single Self Service macOS branding configuration indicated by the provided id 
SelfServiceBrandingMacosAPI.v1SelfServiceBrandingMacosIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of macOS branding configuration | 

### Return type

[**MacOsBrandingConfiguration**](MacOsBrandingConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingMacosIdPut**
```swift
    open class func v1SelfServiceBrandingMacosIdPut(id: String, macOsBrandingConfiguration: MacOsBrandingConfiguration? = nil, completion: @escaping (_ data: MacOsBrandingConfiguration?, _ error: Error?) -> Void)
```

Update a Self Service macOS branding configuration with the supplied details 

Update a Self Service macOS branding configuration with the supplied details

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of macOS branding configuration
let macOsBrandingConfiguration = MacOsBrandingConfiguration(id: "id_example", applicationName: "applicationName_example", brandingName: "brandingName_example", brandingNameSecondary: "brandingNameSecondary_example", iconId: 123, brandingHeaderImageId: 123) // MacOsBrandingConfiguration | The macOS branding configuration values to update (optional)

// Update a Self Service macOS branding configuration with the supplied details 
SelfServiceBrandingMacosAPI.v1SelfServiceBrandingMacosIdPut(id: id, macOsBrandingConfiguration: macOsBrandingConfiguration) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of macOS branding configuration | 
 **macOsBrandingConfiguration** | [**MacOsBrandingConfiguration**](MacOsBrandingConfiguration.md) | The macOS branding configuration values to update | [optional] 

### Return type

[**MacOsBrandingConfiguration**](MacOsBrandingConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingMacosPost**
```swift
    open class func v1SelfServiceBrandingMacosPost(macOsBrandingConfiguration: MacOsBrandingConfiguration? = nil, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Self Service macOS branding configuration with the supplied 

Create a Self Service macOS branding configuration with the supplied details

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let macOsBrandingConfiguration = MacOsBrandingConfiguration(id: "id_example", applicationName: "applicationName_example", brandingName: "brandingName_example", brandingNameSecondary: "brandingNameSecondary_example", iconId: 123, brandingHeaderImageId: 123) // MacOsBrandingConfiguration | The macOS branding configuration to create (optional)

// Create a Self Service macOS branding configuration with the supplied 
SelfServiceBrandingMacosAPI.v1SelfServiceBrandingMacosPost(macOsBrandingConfiguration: macOsBrandingConfiguration) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **macOsBrandingConfiguration** | [**MacOsBrandingConfiguration**](MacOsBrandingConfiguration.md) | The macOS branding configuration to create | [optional] 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

