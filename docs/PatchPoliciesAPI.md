# PatchPoliciesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2PatchPoliciesGet**](PatchPoliciesAPI.md#v2patchpoliciesget) | **GET** /v2/patch-policies | Retrieve Patch Policies
[**v2PatchPoliciesIdDashboardDelete**](PatchPoliciesAPI.md#v2patchpoliciesiddashboarddelete) | **DELETE** /v2/patch-policies/{id}/dashboard | Remove a patch policy from the dashboard 
[**v2PatchPoliciesIdDashboardGet**](PatchPoliciesAPI.md#v2patchpoliciesiddashboardget) | **GET** /v2/patch-policies/{id}/dashboard | Return whether or not the requested patch policy is on the dashboard 
[**v2PatchPoliciesIdDashboardPost**](PatchPoliciesAPI.md#v2patchpoliciesiddashboardpost) | **POST** /v2/patch-policies/{id}/dashboard | Add a patch policy to the dashboard 
[**v2PatchPoliciesPolicyDetailsGet**](PatchPoliciesAPI.md#v2patchpoliciespolicydetailsget) | **GET** /v2/patch-policies/policy-details | Retrieve Patch Policies


# **v2PatchPoliciesGet**
```swift
    open class func v2PatchPoliciesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: PatchPolicies?, _ error: Error?) -> Void)
```

Retrieve Patch Policies

Retrieves a list of patch policies.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Patch Policy collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, policyName, policyEnabled, policyTargetVersion, policyDeploymentMethod, softwareTitle, softwareTitleConfigurationId, pending, completed, deferred, and failed. This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve Patch Policies
PatchPoliciesAPI.v2PatchPoliciesGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Patch Policy collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, policyName, policyEnabled, policyTargetVersion, policyDeploymentMethod, softwareTitle, softwareTitleConfigurationId, pending, completed, deferred, and failed. This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**PatchPolicies**](PatchPolicies.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdDashboardDelete**
```swift
    open class func v2PatchPoliciesIdDashboardDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove a patch policy from the dashboard 

Removes a patch policy from the dashboard.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id

// Remove a patch policy from the dashboard 
PatchPoliciesAPI.v2PatchPoliciesIdDashboardDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdDashboardGet**
```swift
    open class func v2PatchPoliciesIdDashboardGet(id: String, completion: @escaping (_ data: PatchPolicyV2OnDashboard?, _ error: Error?) -> Void)
```

Return whether or not the requested patch policy is on the dashboard 

Returns whether or not the requested patch policy is on the dashboard

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id

// Return whether or not the requested patch policy is on the dashboard 
PatchPoliciesAPI.v2PatchPoliciesIdDashboardGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 

### Return type

[**PatchPolicyV2OnDashboard**](PatchPolicyV2OnDashboard.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesIdDashboardPost**
```swift
    open class func v2PatchPoliciesIdDashboardPost(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Add a patch policy to the dashboard 

Adds a patch policy to the dashboard.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | patch policy id

// Add a patch policy to the dashboard 
PatchPoliciesAPI.v2PatchPoliciesIdDashboardPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | patch policy id | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2PatchPoliciesPolicyDetailsGet**
```swift
    open class func v2PatchPoliciesPolicyDetailsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: PatchPolicyDetails?, _ error: Error?) -> Void)
```

Retrieve Patch Policies

Retrieves a list of patch policies.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter Patch Policy collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name, enabled, targetPatchVersion, deploymentMethod, softwareTitleId, softwareTitleConfigurationId, killAppsDelayMinutes, killAppsMessage, isDowngrade, isPatchUnknownVersion, notificationHeader, selfServiceEnforceDeadline, selfServiceDeadline, installButtonText, selfServiceDescription, iconId, reminderFrequency, reminderEnabled. This param can be combined with paging and sorting. (optional) (default to "")

// Retrieve Patch Policies
PatchPoliciesAPI.v2PatchPoliciesPolicyDetailsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter Patch Policy collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name, enabled, targetPatchVersion, deploymentMethod, softwareTitleId, softwareTitleConfigurationId, killAppsDelayMinutes, killAppsMessage, isDowngrade, isPatchUnknownVersion, notificationHeader, selfServiceEnforceDeadline, selfServiceDeadline, installButtonText, selfServiceDescription, iconId, reminderFrequency, reminderEnabled. This param can be combined with paging and sorting. | [optional] [default to &quot;&quot;]

### Return type

[**PatchPolicyDetails**](PatchPolicyDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

