# MobileDevicesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MobileDevicesGet**](MobileDevicesAPI.md#v1mobiledevicesget) | **GET** /v1/mobile-devices | Get Mobile Device objects 
[**v1MobileDevicesIdDetailGet**](MobileDevicesAPI.md#v1mobiledevicesiddetailget) | **GET** /v1/mobile-devices/{id}/detail | Get Mobile Device 
[**v1MobileDevicesIdGet**](MobileDevicesAPI.md#v1mobiledevicesidget) | **GET** /v1/mobile-devices/{id} | Get Mobile Device 
[**v1MobileDevicesIdPatch**](MobileDevicesAPI.md#v1mobiledevicesidpatch) | **PATCH** /v1/mobile-devices/{id} | Update fields on a mobile device that are allowed to be modified by users 
[**v1SearchMobileDevicesPost**](MobileDevicesAPI.md#v1searchmobiledevicespost) | **POST** /v1/search-mobile-devices | Search Mobile Devices 
[**v2MobileDevicesDetailGet**](MobileDevicesAPI.md#v2mobiledevicesdetailget) | **GET** /v2/mobile-devices/detail | Return paginated Mobile Device Inventory records
[**v2MobileDevicesGet**](MobileDevicesAPI.md#v2mobiledevicesget) | **GET** /v2/mobile-devices | Get Mobile Device objects 
[**v2MobileDevicesIdDetailGet**](MobileDevicesAPI.md#v2mobiledevicesiddetailget) | **GET** /v2/mobile-devices/{id}/detail | Get Mobile Device 
[**v2MobileDevicesIdGet**](MobileDevicesAPI.md#v2mobiledevicesidget) | **GET** /v2/mobile-devices/{id} | Get Mobile Device 
[**v2MobileDevicesIdPatch**](MobileDevicesAPI.md#v2mobiledevicesidpatch) | **PATCH** /v2/mobile-devices/{id} | Update fields on a mobile device that are allowed to be modified by users 


# **v1MobileDevicesGet**
```swift
    open class func v1MobileDevicesGet(completion: @escaping (_ data: [MobileDevice]?, _ error: Error?) -> Void)
```

Get Mobile Device objects 

Gets Mobile Device objects. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Mobile Device objects 
MobileDevicesAPI.v1MobileDevicesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[MobileDevice]**](MobileDevice.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicesIdDetailGet**
```swift
    open class func v1MobileDevicesIdDetailGet(id: Int, completion: @escaping (_ data: MobileDeviceDetails?, _ error: Error?) -> Void)
```

Get Mobile Device 

Get MobileDevice

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance id of mobile device record

// Get Mobile Device 
MobileDevicesAPI.v1MobileDevicesIdDetailGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance id of mobile device record | 

### Return type

[**MobileDeviceDetails**](MobileDeviceDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicesIdGet**
```swift
    open class func v1MobileDevicesIdGet(id: Int, completion: @escaping (_ data: MobileDevice?, _ error: Error?) -> Void)
```

Get Mobile Device 

Get MobileDevice

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance id of mobile device record

// Get Mobile Device 
MobileDevicesAPI.v1MobileDevicesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance id of mobile device record | 

### Return type

[**MobileDevice**](MobileDevice.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MobileDevicesIdPatch**
```swift
    open class func v1MobileDevicesIdPatch(id: Int, updateMobileDevice: UpdateMobileDevice, completion: @escaping (_ data: MobileDeviceDetails?, _ error: Error?) -> Void)
```

Update fields on a mobile device that are allowed to be modified by users 

Updates fields on a mobile device that are allowed to be modified by users.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance id of mobile device record
let updateMobileDevice = UpdateMobileDevice(name: "name_example", assetTag: "assetTag_example", siteId: 123, location: Location(username: "username_example", realName: "realName_example", emailAddress: "emailAddress_example", position: "position_example", phoneNumber: "phoneNumber_example", department: IdAndName(id: 123, name: "name_example"), building: nil, room: "room_example"), updatedExtensionAttributes: [ExtensionAttribute(id: 123, name: "name_example", type: "type_example", value: ["value_example"], isExtensionAttributeCollectionAllowed: false)], ios: UpdateIos(purchasing: Purchasing(isPurchased: true, isLeased: false, poNumber: "poNumber_example", vendor: "vendor_example", appleCareId: "appleCareId_example", purchasePrice: "purchasePrice_example", purchasingAccount: "purchasingAccount_example", poDate: Date(), warrantyExpiresDate: Date(), leaseExpiresDate: Date(), lifeExpectancy: 123, purchasingContact: "purchasingContact_example")), appleTv: UpdateAppleTv(airplayPassword: "airplayPassword_example", purchasing: nil)) // UpdateMobileDevice | 

// Update fields on a mobile device that are allowed to be modified by users 
MobileDevicesAPI.v1MobileDevicesIdPatch(id: id, updateMobileDevice: updateMobileDevice) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance id of mobile device record | 
 **updateMobileDevice** | [**UpdateMobileDevice**](UpdateMobileDevice.md) |  | 

### Return type

[**MobileDeviceDetails**](MobileDeviceDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SearchMobileDevicesPost**
```swift
    open class func v1SearchMobileDevicesPost(mobileDeviceSearchParams: MobileDeviceSearchParams? = nil, completion: @escaping (_ data: MobileDeviceSearchResults?, _ error: Error?) -> Void)
```

Search Mobile Devices 

Search Mobile Devices

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let mobileDeviceSearchParams = MobileDeviceSearchParams(pageNumber: 123, pageSize: 123, isLoadToEnd: false, orderBy: [OrderBy(field: "field_example", direction: "direction_example")], udid: "udid_example", macAddress: "macAddress_example", name: "name_example", serialNumber: "serialNumber_example", osType: "osType_example", isManaged: false, excludedIds: [123]) // MobileDeviceSearchParams |  (optional)

// Search Mobile Devices 
MobileDevicesAPI.v1SearchMobileDevicesPost(mobileDeviceSearchParams: mobileDeviceSearchParams) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mobileDeviceSearchParams** | [**MobileDeviceSearchParams**](MobileDeviceSearchParams.md) |  | [optional] 

### Return type

[**MobileDeviceSearchResults**](MobileDeviceSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicesDetailGet**
```swift
    open class func v2MobileDevicesDetailGet(section: [MobileDeviceSection]? = nil, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: MobileDeviceInventorySearchResults?, _ error: Error?) -> Void)
```

Return paginated Mobile Device Inventory records

Return paginated Mobile Device Inventory records

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let section = [MobileDeviceSection()] // [MobileDeviceSection] | section of mobile device details, if not specified, General section data is returned. Multiple section parameters are supported, e.g. section=GENERAL&section=HARDWARE (optional)
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is displayName:asc. Multiple sort criteria are supported and must be separated with a comma.   Fields allowed in the sort: `airPlayPassword`, `appAnalyticsEnabled`, `assetTag`, `availableSpaceMb`,  `batteryLevel`, `bluetoothLowEnergyCapable`, `bluetoothMacAddress`, `capacityMb`,  `lostModeEnabledDate`, `declarativeDeviceManagementEnabled`, `deviceId`, `deviceLocatorServiceEnabled`, `devicePhoneNumber`, `diagnosticAndUsageReportingEnabled`, `displayName`, `doNotDisturbEnabled`,  `enrollmentSessionTokenValid`, `exchangeDeviceId`, `cloudBackupEnabled`, `osBuild`, `osSupplementalBuildVersion`, `osVersion`, `osRapidSecurityResponse`, `ipAddress`, `itunesStoreAccountActive`, `mobileDeviceId`, `languages`, `lastBackupDate`, `lastEnrolledDate`, `lastCloudBackupDate`, `lastInventoryUpdateDate`, `locales`, `locationServicesForSelfServiceMobileEnabled`, `lostModeEnabled`, `managed`, `mdmProfileExpirationDate`, `model`, `modelIdentifier`, `modelNumber`, `modemFirmwareVersion`, `quotaSize`, `residentUsers`, `serialNumber`, `sharedIpad`, `supervised`, `tethered`, `timeZone`, `udid`, `usedSpacePercentage`, `wifiMacAddress`, `deviceOwnershipType`, `building`, `department`, `emailAddress`, `fullName`, `userPhoneNumber`, `position`, `room`, `username`, `appleCareId`, `leaseExpirationDate`,`lifeExpectancyYears`, `poDate`, `poNumber`, `purchasePrice`, `purchasedOrLeased`, `purchasingAccount`, `purchasingContact`, `vendor`, `warrantyExpirationDate`, `activationLockEnabled`, `blockEncryptionCapable`, `dataProtection`, `fileEncryptionCapable`, `hardwareEncryptionSupported`, `jailbreakStatus`, `passcodeCompliant`, `passcodeCompliantWithProfile`, `passcodeLockGracePeriodEnforcedSeconds`, `passcodePresent`, `personalDeviceProfileCurrent`, `carrierSettingsVersion`, `cellularTechnology`, `currentCarrierNetwork`, `currentMobileCountryCode`, `currentMobileNetworkCode`,  `dataRoamingEnabled`, `eid`, `network`, `homeMobileCountryCode`,  `homeMobileNetworkCode`, `iccid`, `imei`, `imei2`, `meid`, `personalHotspotEnabled`, `voiceRoamingEnabled`, `roaming`  Example: `sort=displayName:desc,username:asc`  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter mobile device collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: `airPlayPassword`, `appAnalyticsEnabled`, `assetTag`, `availableSpaceMb`,  `batteryLevel`, `bluetoothLowEnergyCapable`, `bluetoothMacAddress`, `capacityMb`,  `declarativeDeviceManagementEnabled`, `deviceId`, `deviceLocatorServiceEnabled`, `devicePhoneNumber`, `diagnosticAndUsageReportingEnabled`, `displayName`, `doNotDisturbEnabled`, `exchangeDeviceId`,  `cloudBackupEnabled`, `osBuild`, `osSupplementalBuildVersion`, `osVersion`, `osRapidSecurityResponse`, `ipAddress`, `itunesStoreAccountActive`, `mobileDeviceId`, `languages`, `locales`, `locationServicesForSelfServiceMobileEnabled`, `lostModeEnabled`, `managed`, `model`,  `modelIdentifier`, `modelNumber`, `modemFirmwareVersion`, `quotaSize`,  `residentUsers`, `serialNumber`, `sharedIpad`, `supervised`, `tethered`, `timeZone`, `udid`, `usedSpacePercentage`,  `wifiMacAddress`, `building`, `department`, `emailAddress`, `fullName`, `userPhoneNumber`, `position`, `room`, `username`, `appleCareId`, `lifeExpectancyYears`, `poNumber`,  `purchasePrice`, `purchasedOrLeased`, `purchasingAccount`, `purchasingContact`, `vendor`, `activationLockEnabled`, `blockEncryptionCapable`, `dataProtection`,  `fileEncryptionCapable`, `passcodeCompliant`, `passcodeCompliantWithProfile`, `passcodeLockGracePeriodEnforcedSeconds`, `passcodePresent`, `personalDeviceProfileCurrent`, `carrierSettingsVersion`, `currentCarrierNetwork`, `currentMobileCountryCode`, `currentMobileNetworkCode`, `dataRoamingEnabled`, `eid`, `network`, `homeMobileCountryCode`, `homeMobileNetworkCode`, `iccid`, `imei`, `imei2`, `meid`, `personalHotspotEnabled`,  `roaming`  This param can be combined with paging and sorting. Example: `filter=displayName==\"iPad\"`  (optional) (default to "")

// Return paginated Mobile Device Inventory records
MobileDevicesAPI.v2MobileDevicesDetailGet(section: section, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **section** | [**[MobileDeviceSection]**](MobileDeviceSection.md) | section of mobile device details, if not specified, General section data is returned. Multiple section parameters are supported, e.g. section&#x3D;GENERAL&amp;section&#x3D;HARDWARE | [optional] 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is displayName:asc. Multiple sort criteria are supported and must be separated with a comma.   Fields allowed in the sort: &#x60;airPlayPassword&#x60;, &#x60;appAnalyticsEnabled&#x60;, &#x60;assetTag&#x60;, &#x60;availableSpaceMb&#x60;,  &#x60;batteryLevel&#x60;, &#x60;bluetoothLowEnergyCapable&#x60;, &#x60;bluetoothMacAddress&#x60;, &#x60;capacityMb&#x60;,  &#x60;lostModeEnabledDate&#x60;, &#x60;declarativeDeviceManagementEnabled&#x60;, &#x60;deviceId&#x60;, &#x60;deviceLocatorServiceEnabled&#x60;, &#x60;devicePhoneNumber&#x60;, &#x60;diagnosticAndUsageReportingEnabled&#x60;, &#x60;displayName&#x60;, &#x60;doNotDisturbEnabled&#x60;,  &#x60;enrollmentSessionTokenValid&#x60;, &#x60;exchangeDeviceId&#x60;, &#x60;cloudBackupEnabled&#x60;, &#x60;osBuild&#x60;, &#x60;osSupplementalBuildVersion&#x60;, &#x60;osVersion&#x60;, &#x60;osRapidSecurityResponse&#x60;, &#x60;ipAddress&#x60;, &#x60;itunesStoreAccountActive&#x60;, &#x60;mobileDeviceId&#x60;, &#x60;languages&#x60;, &#x60;lastBackupDate&#x60;, &#x60;lastEnrolledDate&#x60;, &#x60;lastCloudBackupDate&#x60;, &#x60;lastInventoryUpdateDate&#x60;, &#x60;locales&#x60;, &#x60;locationServicesForSelfServiceMobileEnabled&#x60;, &#x60;lostModeEnabled&#x60;, &#x60;managed&#x60;, &#x60;mdmProfileExpirationDate&#x60;, &#x60;model&#x60;, &#x60;modelIdentifier&#x60;, &#x60;modelNumber&#x60;, &#x60;modemFirmwareVersion&#x60;, &#x60;quotaSize&#x60;, &#x60;residentUsers&#x60;, &#x60;serialNumber&#x60;, &#x60;sharedIpad&#x60;, &#x60;supervised&#x60;, &#x60;tethered&#x60;, &#x60;timeZone&#x60;, &#x60;udid&#x60;, &#x60;usedSpacePercentage&#x60;, &#x60;wifiMacAddress&#x60;, &#x60;deviceOwnershipType&#x60;, &#x60;building&#x60;, &#x60;department&#x60;, &#x60;emailAddress&#x60;, &#x60;fullName&#x60;, &#x60;userPhoneNumber&#x60;, &#x60;position&#x60;, &#x60;room&#x60;, &#x60;username&#x60;, &#x60;appleCareId&#x60;, &#x60;leaseExpirationDate&#x60;,&#x60;lifeExpectancyYears&#x60;, &#x60;poDate&#x60;, &#x60;poNumber&#x60;, &#x60;purchasePrice&#x60;, &#x60;purchasedOrLeased&#x60;, &#x60;purchasingAccount&#x60;, &#x60;purchasingContact&#x60;, &#x60;vendor&#x60;, &#x60;warrantyExpirationDate&#x60;, &#x60;activationLockEnabled&#x60;, &#x60;blockEncryptionCapable&#x60;, &#x60;dataProtection&#x60;, &#x60;fileEncryptionCapable&#x60;, &#x60;hardwareEncryptionSupported&#x60;, &#x60;jailbreakStatus&#x60;, &#x60;passcodeCompliant&#x60;, &#x60;passcodeCompliantWithProfile&#x60;, &#x60;passcodeLockGracePeriodEnforcedSeconds&#x60;, &#x60;passcodePresent&#x60;, &#x60;personalDeviceProfileCurrent&#x60;, &#x60;carrierSettingsVersion&#x60;, &#x60;cellularTechnology&#x60;, &#x60;currentCarrierNetwork&#x60;, &#x60;currentMobileCountryCode&#x60;, &#x60;currentMobileNetworkCode&#x60;,  &#x60;dataRoamingEnabled&#x60;, &#x60;eid&#x60;, &#x60;network&#x60;, &#x60;homeMobileCountryCode&#x60;,  &#x60;homeMobileNetworkCode&#x60;, &#x60;iccid&#x60;, &#x60;imei&#x60;, &#x60;imei2&#x60;, &#x60;meid&#x60;, &#x60;personalHotspotEnabled&#x60;, &#x60;voiceRoamingEnabled&#x60;, &#x60;roaming&#x60;  Example: &#x60;sort&#x3D;displayName:desc,username:asc&#x60;  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter mobile device collection. Default filter is empty query - returning all results for the requested page.  Fields allowed in the query: &#x60;airPlayPassword&#x60;, &#x60;appAnalyticsEnabled&#x60;, &#x60;assetTag&#x60;, &#x60;availableSpaceMb&#x60;,  &#x60;batteryLevel&#x60;, &#x60;bluetoothLowEnergyCapable&#x60;, &#x60;bluetoothMacAddress&#x60;, &#x60;capacityMb&#x60;,  &#x60;declarativeDeviceManagementEnabled&#x60;, &#x60;deviceId&#x60;, &#x60;deviceLocatorServiceEnabled&#x60;, &#x60;devicePhoneNumber&#x60;, &#x60;diagnosticAndUsageReportingEnabled&#x60;, &#x60;displayName&#x60;, &#x60;doNotDisturbEnabled&#x60;, &#x60;exchangeDeviceId&#x60;,  &#x60;cloudBackupEnabled&#x60;, &#x60;osBuild&#x60;, &#x60;osSupplementalBuildVersion&#x60;, &#x60;osVersion&#x60;, &#x60;osRapidSecurityResponse&#x60;, &#x60;ipAddress&#x60;, &#x60;itunesStoreAccountActive&#x60;, &#x60;mobileDeviceId&#x60;, &#x60;languages&#x60;, &#x60;locales&#x60;, &#x60;locationServicesForSelfServiceMobileEnabled&#x60;, &#x60;lostModeEnabled&#x60;, &#x60;managed&#x60;, &#x60;model&#x60;,  &#x60;modelIdentifier&#x60;, &#x60;modelNumber&#x60;, &#x60;modemFirmwareVersion&#x60;, &#x60;quotaSize&#x60;,  &#x60;residentUsers&#x60;, &#x60;serialNumber&#x60;, &#x60;sharedIpad&#x60;, &#x60;supervised&#x60;, &#x60;tethered&#x60;, &#x60;timeZone&#x60;, &#x60;udid&#x60;, &#x60;usedSpacePercentage&#x60;,  &#x60;wifiMacAddress&#x60;, &#x60;building&#x60;, &#x60;department&#x60;, &#x60;emailAddress&#x60;, &#x60;fullName&#x60;, &#x60;userPhoneNumber&#x60;, &#x60;position&#x60;, &#x60;room&#x60;, &#x60;username&#x60;, &#x60;appleCareId&#x60;, &#x60;lifeExpectancyYears&#x60;, &#x60;poNumber&#x60;,  &#x60;purchasePrice&#x60;, &#x60;purchasedOrLeased&#x60;, &#x60;purchasingAccount&#x60;, &#x60;purchasingContact&#x60;, &#x60;vendor&#x60;, &#x60;activationLockEnabled&#x60;, &#x60;blockEncryptionCapable&#x60;, &#x60;dataProtection&#x60;,  &#x60;fileEncryptionCapable&#x60;, &#x60;passcodeCompliant&#x60;, &#x60;passcodeCompliantWithProfile&#x60;, &#x60;passcodeLockGracePeriodEnforcedSeconds&#x60;, &#x60;passcodePresent&#x60;, &#x60;personalDeviceProfileCurrent&#x60;, &#x60;carrierSettingsVersion&#x60;, &#x60;currentCarrierNetwork&#x60;, &#x60;currentMobileCountryCode&#x60;, &#x60;currentMobileNetworkCode&#x60;, &#x60;dataRoamingEnabled&#x60;, &#x60;eid&#x60;, &#x60;network&#x60;, &#x60;homeMobileCountryCode&#x60;, &#x60;homeMobileNetworkCode&#x60;, &#x60;iccid&#x60;, &#x60;imei&#x60;, &#x60;imei2&#x60;, &#x60;meid&#x60;, &#x60;personalHotspotEnabled&#x60;,  &#x60;roaming&#x60;  This param can be combined with paging and sorting. Example: &#x60;filter&#x3D;displayName&#x3D;&#x3D;\&quot;iPad\&quot;&#x60;  | [optional] [default to &quot;&quot;]

### Return type

[**MobileDeviceInventorySearchResults**](MobileDeviceInventorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicesGet**
```swift
    open class func v2MobileDevicesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: MobileDeviceSearchResultsV2?, _ error: Error?) -> Void)
```

Get Mobile Device objects 

Gets Mobile Device objects. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Get Mobile Device objects 
MobileDevicesAPI.v2MobileDevicesGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**MobileDeviceSearchResultsV2**](MobileDeviceSearchResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicesIdDetailGet**
```swift
    open class func v2MobileDevicesIdDetailGet(id: String, completion: @escaping (_ data: MobileDeviceDetailsGetV2?, _ error: Error?) -> Void)
```

Get Mobile Device 

Get MobileDevice

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of mobile device record

// Get Mobile Device 
MobileDevicesAPI.v2MobileDevicesIdDetailGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of mobile device record | 

### Return type

[**MobileDeviceDetailsGetV2**](MobileDeviceDetailsGetV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicesIdGet**
```swift
    open class func v2MobileDevicesIdGet(id: String, completion: @escaping (_ data: MobileDeviceV2?, _ error: Error?) -> Void)
```

Get Mobile Device 

Get MobileDevice

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of mobile device record

// Get Mobile Device 
MobileDevicesAPI.v2MobileDevicesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of mobile device record | 

### Return type

[**MobileDeviceV2**](MobileDeviceV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MobileDevicesIdPatch**
```swift
    open class func v2MobileDevicesIdPatch(id: String, updateMobileDeviceV2: UpdateMobileDeviceV2, completion: @escaping (_ data: MobileDeviceDetailsV2?, _ error: Error?) -> Void)
```

Update fields on a mobile device that are allowed to be modified by users 

Updates fields on a mobile device that are allowed to be modified by users.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of mobile device record
let updateMobileDeviceV2 = UpdateMobileDeviceV2(name: "name_example", enforceName: true, assetTag: "assetTag_example", siteId: "siteId_example", timeZone: "timeZone_example", location: LocationV2(username: "username_example", realName: "realName_example", emailAddress: "emailAddress_example", position: "position_example", phoneNumber: "phoneNumber_example", departmentId: "departmentId_example", buildingId: "buildingId_example", room: "room_example"), updatedExtensionAttributes: [ExtensionAttributeV2(id: "id_example", name: "name_example", type: "type_example", value: ["value_example"], extensionAttributeCollectionAllowed: false)], ios: UpdateIosV2(purchasing: PurchasingV2(purchased: true, leased: false, poNumber: "poNumber_example", vendor: "vendor_example", appleCareId: "appleCareId_example", purchasePrice: "purchasePrice_example", purchasingAccount: "purchasingAccount_example", poDate: Date(), warrantyExpiresDate: Date(), leaseExpiresDate: Date(), lifeExpectancy: 123, purchasingContact: "purchasingContact_example")), tvos: UpdateTvOs(airplayPassword: "airplayPassword_example", purchasing: nil)) // UpdateMobileDeviceV2 | 

// Update fields on a mobile device that are allowed to be modified by users 
MobileDevicesAPI.v2MobileDevicesIdPatch(id: id, updateMobileDeviceV2: updateMobileDeviceV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of mobile device record | 
 **updateMobileDeviceV2** | [**UpdateMobileDeviceV2**](UpdateMobileDeviceV2.md) |  | 

### Return type

[**MobileDeviceDetailsV2**](MobileDeviceDetailsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

