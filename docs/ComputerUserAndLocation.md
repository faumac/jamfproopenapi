# ComputerUserAndLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**realname** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**departmentId** | **String** |  | [optional] 
**buildingId** | **String** |  | [optional] 
**room** | **String** |  | [optional] 
**extensionAttributes** | [ComputerExtensionAttribute] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


