# ComputerDisk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**device** | **String** |  | [optional] [readonly] 
**model** | **String** |  | [optional] [readonly] 
**revision** | **String** |  | [optional] [readonly] 
**serialNumber** | **String** |  | [optional] [readonly] 
**sizeMegabytes** | **Int64** | Disk Size in MB. | [optional] [readonly] 
**smartStatus** | **String** | S.M.A.R.T Status | [optional] [readonly] 
**type** | **String** | Connection type attribute. | [optional] [readonly] 
**partitions** | [ComputerPartition] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


