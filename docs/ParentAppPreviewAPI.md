# ParentAppPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ParentAppGet**](ParentAppPreviewAPI.md#v1parentappget) | **GET** /v1/parent-app | Get the current Jamf Parent app settings 
[**v1ParentAppHistoryGet**](ParentAppPreviewAPI.md#v1parentapphistoryget) | **GET** /v1/parent-app/history | Get Jamf Parent app settings history 
[**v1ParentAppHistoryPost**](ParentAppPreviewAPI.md#v1parentapphistorypost) | **POST** /v1/parent-app/history | Add Jamf Parent app settings history notes 
[**v1ParentAppPut**](ParentAppPreviewAPI.md#v1parentappput) | **PUT** /v1/parent-app | Update Jamf Parent app settings 


# **v1ParentAppGet**
```swift
    open class func v1ParentAppGet(completion: @escaping (_ data: ParentApp?, _ error: Error?) -> Void)
```

Get the current Jamf Parent app settings 

Get the current Jamf Parent app settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get the current Jamf Parent app settings 
ParentAppPreviewAPI.v1ParentAppGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ParentApp**](ParentApp.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ParentAppHistoryGet**
```swift
    open class func v1ParentAppHistoryGet(page: Int? = nil, pageSize: Int? = nil, filter: String? = nil, sort: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Jamf Parent app settings history 

Gets Jamf Parent app settings history 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")
let sort = "sort_example" // String | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional) (default to "date:desc")

// Get Jamf Parent app settings history 
ParentAppPreviewAPI.v1ParentAppHistoryGet(page: page, pageSize: pageSize, filter: filter, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **filter** | **String** | Query in the RSQL format, allowing to filter results. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: status, updated, version This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]
 **sort** | **String** | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] [default to &quot;date:desc&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ParentAppHistoryPost**
```swift
    open class func v1ParentAppHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Jamf Parent app settings history notes 

Adds Jamf Parent app settings history notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Jamf Parent app settings history notes 
ParentAppPreviewAPI.v1ParentAppHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ParentAppPut**
```swift
    open class func v1ParentAppPut(parentApp: ParentApp, completion: @escaping (_ data: ParentApp?, _ error: Error?) -> Void)
```

Update Jamf Parent app settings 

Update Jamf Parent app settings 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let parentApp = ParentApp(timezoneId: "timezoneId_example", restrictedTimes: ParentApp_restrictedTimes(key: DayOfWeek()), deviceGroupId: 123, isEnabled: true, allowTemplates: true, disassociateOnWipeAndReEnroll: true, allowClearPasscode: true, safelistedApps: [SafelistedApp(name: "name_example", bundleId: "bundleId_example")]) // ParentApp | Jamf Parent app settings to save.

// Update Jamf Parent app settings 
ParentAppPreviewAPI.v1ParentAppPut(parentApp: parentApp) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parentApp** | [**ParentApp**](ParentApp.md) | Jamf Parent app settings to save. | 

### Return type

[**ParentApp**](ParentApp.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

