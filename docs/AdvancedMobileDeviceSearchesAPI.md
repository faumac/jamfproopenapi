# AdvancedMobileDeviceSearchesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1AdvancedMobileDeviceSearchesChoicesGet**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearcheschoicesget) | **GET** /v1/advanced-mobile-device-searches/choices | Get Mobile Device Advanced Search criteria choices 
[**v1AdvancedMobileDeviceSearchesDeleteMultiplePost**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearchesdeletemultiplepost) | **POST** /v1/advanced-mobile-device-searches/delete-multiple | Remove specified Advanced Search objects 
[**v1AdvancedMobileDeviceSearchesGet**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearchesget) | **GET** /v1/advanced-mobile-device-searches | Get Advanced Search objects 
[**v1AdvancedMobileDeviceSearchesIdDelete**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearchesiddelete) | **DELETE** /v1/advanced-mobile-device-searches/{id} | Remove specified Advanced Search object 
[**v1AdvancedMobileDeviceSearchesIdGet**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearchesidget) | **GET** /v1/advanced-mobile-device-searches/{id} | Get specified Advanced Search object 
[**v1AdvancedMobileDeviceSearchesIdPut**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearchesidput) | **PUT** /v1/advanced-mobile-device-searches/{id} | Get specified Advanced Search object 
[**v1AdvancedMobileDeviceSearchesPost**](AdvancedMobileDeviceSearchesAPI.md#v1advancedmobiledevicesearchespost) | **POST** /v1/advanced-mobile-device-searches | Create Advanced Search object 


# **v1AdvancedMobileDeviceSearchesChoicesGet**
```swift
    open class func v1AdvancedMobileDeviceSearchesChoicesGet(criteria: String, site: String? = nil, contains: String? = nil, completion: @escaping (_ data: AdvancedSearchCriteriaChoices?, _ error: Error?) -> Void)
```

Get Mobile Device Advanced Search criteria choices 

Gets Mobile Device Advanced Search criteria choices. A list of potentially valid choices can be found by navigating to the Criteria page of the Advanced Mobile Device Search creation process. A few are \"App Name\", \"Building\", and \"Display Name\". 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let criteria = "criteria_example" // String | 
let site = "site_example" // String |  (optional) (default to "-1")
let contains = "contains_example" // String |  (optional) (default to "null")

// Get Mobile Device Advanced Search criteria choices 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesChoicesGet(criteria: criteria, site: site, contains: contains) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **criteria** | **String** |  | 
 **site** | **String** |  | [optional] [default to &quot;-1&quot;]
 **contains** | **String** |  | [optional] [default to &quot;null&quot;]

### Return type

[**AdvancedSearchCriteriaChoices**](AdvancedSearchCriteriaChoices.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedMobileDeviceSearchesDeleteMultiplePost**
```swift
    open class func v1AdvancedMobileDeviceSearchesDeleteMultiplePost(ids: Ids, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified Advanced Search objects 

Removes specified Advanced Search Objects 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ids = Ids(ids: ["ids_example"]) // Ids | ids of the building to be deleted

// Remove specified Advanced Search objects 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesDeleteMultiplePost(ids: ids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**Ids**](Ids.md) | ids of the building to be deleted | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedMobileDeviceSearchesGet**
```swift
    open class func v1AdvancedMobileDeviceSearchesGet(completion: @escaping (_ data: AdvancedSearchSearchResults?, _ error: Error?) -> Void)
```

Get Advanced Search objects 

Gets Advanced Search Objects 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Advanced Search objects 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AdvancedSearchSearchResults**](AdvancedSearchSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedMobileDeviceSearchesIdDelete**
```swift
    open class func v1AdvancedMobileDeviceSearchesIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified Advanced Search object 

Removes specified Advanced Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of advanced search record

// Remove specified Advanced Search object 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of advanced search record | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedMobileDeviceSearchesIdGet**
```swift
    open class func v1AdvancedMobileDeviceSearchesIdGet(id: String, completion: @escaping (_ data: AdvancedSearch?, _ error: Error?) -> Void)
```

Get specified Advanced Search object 

Gets Specified Advanced Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of target Advanced Search

// Get specified Advanced Search object 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of target Advanced Search | 

### Return type

[**AdvancedSearch**](AdvancedSearch.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedMobileDeviceSearchesIdPut**
```swift
    open class func v1AdvancedMobileDeviceSearchesIdPut(id: String, advancedSearch: AdvancedSearch, completion: @escaping (_ data: AdvancedSearch?, _ error: Error?) -> Void)
```

Get specified Advanced Search object 

Gets Specified Advanced Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of target Advanced Search
let advancedSearch = AdvancedSearch(id: "id_example", name: "name_example", criteria: [SmartSearchCriterion(name: "name_example", priority: 123, andOr: "andOr_example", searchType: "searchType_example", value: "value_example", openingParen: false, closingParen: false)], displayFields: ["displayFields_example"], siteId: "siteId_example") // AdvancedSearch | 

// Get specified Advanced Search object 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesIdPut(id: id, advancedSearch: advancedSearch) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of target Advanced Search | 
 **advancedSearch** | [**AdvancedSearch**](AdvancedSearch.md) |  | 

### Return type

[**AdvancedSearch**](AdvancedSearch.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedMobileDeviceSearchesPost**
```swift
    open class func v1AdvancedMobileDeviceSearchesPost(advancedSearch: AdvancedSearch, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Advanced Search object 

Creates Advanced Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let advancedSearch = AdvancedSearch(id: "id_example", name: "name_example", criteria: [SmartSearchCriterion(name: "name_example", priority: 123, andOr: "andOr_example", searchType: "searchType_example", value: "value_example", openingParen: false, closingParen: false)], displayFields: ["displayFields_example"], siteId: "siteId_example") // AdvancedSearch | 

// Create Advanced Search object 
AdvancedMobileDeviceSearchesAPI.v1AdvancedMobileDeviceSearchesPost(advancedSearch: advancedSearch) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **advancedSearch** | [**AdvancedSearch**](AdvancedSearch.md) |  | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

