# CloudInformationAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1CloudInformationGet**](CloudInformationAPI.md#v1cloudinformationget) | **GET** /v1/cloud-information | Retrieve information related to cloud setup. 


# **v1CloudInformationGet**
```swift
    open class func v1CloudInformationGet(completion: @escaping (_ data: CloudResponse?, _ error: Error?) -> Void)
```

Retrieve information related to cloud setup. 

Retrieve information related to cloud setup. Retrieves information related to cloud setup. Provides details about cloud instance configuration. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve information related to cloud setup. 
CloudInformationAPI.v1CloudInformationGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CloudResponse**](CloudResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

