# TomcatSettingsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**settingsIssueTomcatSslCertificatePost**](TomcatSettingsPreviewAPI.md#settingsissuetomcatsslcertificatepost) | **POST** /settings/issueTomcatSslCertificate | Generate a SSL Certificate using Jamf Certificate Authority 


# **settingsIssueTomcatSslCertificatePost**
```swift
    open class func settingsIssueTomcatSslCertificatePost(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Generate a SSL Certificate using Jamf Certificate Authority 

generate a SSL Certificate using Jamf Certificate Authority

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Generate a SSL Certificate using Jamf Certificate Authority 
TomcatSettingsPreviewAPI.settingsIssueTomcatSslCertificatePost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

