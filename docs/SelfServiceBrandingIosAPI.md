# SelfServiceBrandingIosAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SelfServiceBrandingIosGet**](SelfServiceBrandingIosAPI.md#v1selfservicebrandingiosget) | **GET** /v1/self-service/branding/ios | Search for sorted and paged iOS branding configurations 
[**v1SelfServiceBrandingIosIdDelete**](SelfServiceBrandingIosAPI.md#v1selfservicebrandingiosiddelete) | **DELETE** /v1/self-service/branding/ios/{id} | Delete the Self Service iOS branding configuration indicated by the provided id 
[**v1SelfServiceBrandingIosIdGet**](SelfServiceBrandingIosAPI.md#v1selfservicebrandingiosidget) | **GET** /v1/self-service/branding/ios/{id} | Read a single Self Service iOS branding configuration indicated by the provided id 
[**v1SelfServiceBrandingIosIdPut**](SelfServiceBrandingIosAPI.md#v1selfservicebrandingiosidput) | **PUT** /v1/self-service/branding/ios/{id} | Update a Self Service iOS branding configuration with the supplied details 
[**v1SelfServiceBrandingIosPost**](SelfServiceBrandingIosAPI.md#v1selfservicebrandingiospost) | **POST** /v1/self-service/branding/ios | Create a Self Service iOS branding configuration with the supplied 


# **v1SelfServiceBrandingIosGet**
```swift
    open class func v1SelfServiceBrandingIosGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: IosBrandingSearchResults?, _ error: Error?) -> Void)
```

Search for sorted and paged iOS branding configurations 

Search for sorted and paged iOS branding configurations

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=id:desc,brandingName:asc  (optional)

// Search for sorted and paged iOS branding configurations 
SelfServiceBrandingIosAPI.v1SelfServiceBrandingIosGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;id:desc,brandingName:asc  | [optional] 

### Return type

[**IosBrandingSearchResults**](IosBrandingSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingIosIdDelete**
```swift
    open class func v1SelfServiceBrandingIosIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the Self Service iOS branding configuration indicated by the provided id 

Delete the Self Service iOS branding configuration indicated by the provided id.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of iOS branding configuration

// Delete the Self Service iOS branding configuration indicated by the provided id 
SelfServiceBrandingIosAPI.v1SelfServiceBrandingIosIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of iOS branding configuration | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingIosIdGet**
```swift
    open class func v1SelfServiceBrandingIosIdGet(id: String, completion: @escaping (_ data: IosBrandingConfiguration?, _ error: Error?) -> Void)
```

Read a single Self Service iOS branding configuration indicated by the provided id 

Read a single Self Service iOS branding configuration indicated by the provided id.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of iOS branding configuration

// Read a single Self Service iOS branding configuration indicated by the provided id 
SelfServiceBrandingIosAPI.v1SelfServiceBrandingIosIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of iOS branding configuration | 

### Return type

[**IosBrandingConfiguration**](IosBrandingConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingIosIdPut**
```swift
    open class func v1SelfServiceBrandingIosIdPut(id: String, iosBrandingConfiguration: IosBrandingConfiguration? = nil, completion: @escaping (_ data: IosBrandingConfiguration?, _ error: Error?) -> Void)
```

Update a Self Service iOS branding configuration with the supplied details 

Update a Self Service iOS branding configuration with the supplied details

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of iOS branding configuration
let iosBrandingConfiguration = IosBrandingConfiguration(id: "id_example", brandingName: "brandingName_example", iconId: 123, headerBackgroundColorCode: "headerBackgroundColorCode_example", menuIconColorCode: "menuIconColorCode_example", brandingNameColorCode: "brandingNameColorCode_example", statusBarTextColor: "statusBarTextColor_example") // IosBrandingConfiguration | The iOS branding configuration values to update (optional)

// Update a Self Service iOS branding configuration with the supplied details 
SelfServiceBrandingIosAPI.v1SelfServiceBrandingIosIdPut(id: id, iosBrandingConfiguration: iosBrandingConfiguration) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of iOS branding configuration | 
 **iosBrandingConfiguration** | [**IosBrandingConfiguration**](IosBrandingConfiguration.md) | The iOS branding configuration values to update | [optional] 

### Return type

[**IosBrandingConfiguration**](IosBrandingConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SelfServiceBrandingIosPost**
```swift
    open class func v1SelfServiceBrandingIosPost(iosBrandingConfiguration: IosBrandingConfiguration? = nil, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Self Service iOS branding configuration with the supplied 

Create a Self Service iOS branding configuration with the supplied details

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let iosBrandingConfiguration = IosBrandingConfiguration(id: "id_example", brandingName: "brandingName_example", iconId: 123, headerBackgroundColorCode: "headerBackgroundColorCode_example", menuIconColorCode: "menuIconColorCode_example", brandingNameColorCode: "brandingNameColorCode_example", statusBarTextColor: "statusBarTextColor_example") // IosBrandingConfiguration | The iOS branding configuration to create (optional)

// Create a Self Service iOS branding configuration with the supplied 
SelfServiceBrandingIosAPI.v1SelfServiceBrandingIosPost(iosBrandingConfiguration: iosBrandingConfiguration) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iosBrandingConfiguration** | [**IosBrandingConfiguration**](IosBrandingConfiguration.md) | The iOS branding configuration to create | [optional] 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

