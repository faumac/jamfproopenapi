# MemcachedEndpoints

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** |  | [optional] 
**hostName** | **String** |  | [optional] 
**port** | **Int** |  | [optional] 
**enabled** | **Bool** |  | [optional] 
**jssCacheConfigurationId** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


