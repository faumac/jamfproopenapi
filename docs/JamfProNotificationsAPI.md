# JamfProNotificationsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1NotificationsGet**](JamfProNotificationsAPI.md#v1notificationsget) | **GET** /v1/notifications | Get Notifications for user and site 
[**v1NotificationsTypeIdDelete**](JamfProNotificationsAPI.md#v1notificationstypeiddelete) | **DELETE** /v1/notifications/{type}/{id} | Delete Notifications 


# **v1NotificationsGet**
```swift
    open class func v1NotificationsGet(completion: @escaping (_ data: [NotificationV1]?, _ error: Error?) -> Void)
```

Get Notifications for user and site 

Gets notifications for user and site 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Notifications for user and site 
JamfProNotificationsAPI.v1NotificationsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[NotificationV1]**](NotificationV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1NotificationsTypeIdDelete**
```swift
    open class func v1NotificationsTypeIdDelete(id: String, type: NotificationType, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Notifications 

Deletes notifications with given type and id. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance ID of the notification
let type = NotificationType() // NotificationType | type of the notification

// Delete Notifications 
JamfProNotificationsAPI.v1NotificationsTypeIdDelete(id: id, type: type) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance ID of the notification | 
 **type** | [**NotificationType**](.md) | type of the notification | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

