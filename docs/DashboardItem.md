# DashboardItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**subtitle** | **String** |  | [optional] 
**info** | **String** | Additional information such as identifiers for a specific policy within a software patch | [optional] 
**enabled** | **Bool** | Logical to decide whether widget should be enabled or disabled; i.e. Policy | [optional] [default to true]
**metrics** | [DashboardMetric] |  | [optional] 
**details** | [DashboardDetail] |  | [optional] 
**error** | [**DashboardApiError**](DashboardApiError.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


