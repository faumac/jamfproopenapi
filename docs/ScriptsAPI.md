# ScriptsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ScriptsGet**](ScriptsAPI.md#v1scriptsget) | **GET** /v1/scripts | Search for sorted and paged Scripts 
[**v1ScriptsIdDelete**](ScriptsAPI.md#v1scriptsiddelete) | **DELETE** /v1/scripts/{id} | Delete a Script at the specified id 
[**v1ScriptsIdDownloadGet**](ScriptsAPI.md#v1scriptsiddownloadget) | **GET** /v1/scripts/{id}/download | Download a text file of the Script contents 
[**v1ScriptsIdGet**](ScriptsAPI.md#v1scriptsidget) | **GET** /v1/scripts/{id} | Retrieve a full script object 
[**v1ScriptsIdHistoryGet**](ScriptsAPI.md#v1scriptsidhistoryget) | **GET** /v1/scripts/{id}/history | Get specified Script history object 
[**v1ScriptsIdHistoryPost**](ScriptsAPI.md#v1scriptsidhistorypost) | **POST** /v1/scripts/{id}/history | Add specified Script history object notes 
[**v1ScriptsIdPut**](ScriptsAPI.md#v1scriptsidput) | **PUT** /v1/scripts/{id} | Replace the script at the id with the supplied information 
[**v1ScriptsPost**](ScriptsAPI.md#v1scriptspost) | **POST** /v1/scripts | Create a Script 


# **v1ScriptsGet**
```swift
    open class func v1ScriptsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: ScriptsSearchResults?, _ error: Error?) -> Void)
```

Search for sorted and paged Scripts 

Search for sorted and paged scripts

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Fields allowed in the query: `id`, `name`, `info`, `notes`, `priority`, `categoryId`, `categoryName`, `parameter4` up to `parameter11`, `osRequirements`, `scriptContents`. Example: sort=date:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter scripts collection. Default search is empty query - returning all results for the requested page. Fields allowed in the query: `id`, `name`, `info`, `notes`, `priority`, `categoryId`, `categoryName`, `parameter4` up to `parameter11`, `osRequirements`, `scriptContents`. This param can be combined with paging and sorting. Example: filter=categoryName==\"Category\" and name==\"*script name*\" (optional) (default to "")

// Search for sorted and paged Scripts 
ScriptsAPI.v1ScriptsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Fields allowed in the query: &#x60;id&#x60;, &#x60;name&#x60;, &#x60;info&#x60;, &#x60;notes&#x60;, &#x60;priority&#x60;, &#x60;categoryId&#x60;, &#x60;categoryName&#x60;, &#x60;parameter4&#x60; up to &#x60;parameter11&#x60;, &#x60;osRequirements&#x60;, &#x60;scriptContents&#x60;. Example: sort&#x3D;date:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter scripts collection. Default search is empty query - returning all results for the requested page. Fields allowed in the query: &#x60;id&#x60;, &#x60;name&#x60;, &#x60;info&#x60;, &#x60;notes&#x60;, &#x60;priority&#x60;, &#x60;categoryId&#x60;, &#x60;categoryName&#x60;, &#x60;parameter4&#x60; up to &#x60;parameter11&#x60;, &#x60;osRequirements&#x60;, &#x60;scriptContents&#x60;. This param can be combined with paging and sorting. Example: filter&#x3D;categoryName&#x3D;&#x3D;\&quot;Category\&quot; and name&#x3D;&#x3D;\&quot;*script name*\&quot; | [optional] [default to &quot;&quot;]

### Return type

[**ScriptsSearchResults**](ScriptsSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsIdDelete**
```swift
    open class func v1ScriptsIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Script at the specified id 

Deletes a script at the specified id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Script object identifier

// Delete a Script at the specified id 
ScriptsAPI.v1ScriptsIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Script object identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsIdDownloadGet**
```swift
    open class func v1ScriptsIdDownloadGet(id: String, completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download a text file of the Script contents 

Download a text file of the script contents

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of the script to be downloaded

// Download a text file of the Script contents 
ScriptsAPI.v1ScriptsIdDownloadGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of the script to be downloaded | 

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsIdGet**
```swift
    open class func v1ScriptsIdGet(id: String, completion: @escaping (_ data: Script?, _ error: Error?) -> Void)
```

Retrieve a full script object 

Retrieves a full script object

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Script object identifier

// Retrieve a full script object 
ScriptsAPI.v1ScriptsIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Script object identifier | 

### Return type

[**Script**](Script.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsIdHistoryGet**
```swift
    open class func v1ScriptsIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get specified Script history object 

Gets specified Script history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of script history record
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get specified Script history object 
ScriptsAPI.v1ScriptsIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of script history record | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsIdHistoryPost**
```swift
    open class func v1ScriptsIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add specified Script history object notes 

Adds specified Script history object notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of script history record
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add specified Script history object notes 
ScriptsAPI.v1ScriptsIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of script history record | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsIdPut**
```swift
    open class func v1ScriptsIdPut(id: String, script: Script, completion: @escaping (_ data: Script?, _ error: Error?) -> Void)
```

Replace the script at the id with the supplied information 

Replaces the script at the id with the supplied information

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Script object identifier
let script = Script(id: "id_example", name: "name_example", info: "info_example", notes: "notes_example", priority: "priority_example", categoryId: "categoryId_example", categoryName: "categoryName_example", parameter4: "parameter4_example", parameter5: "parameter5_example", parameter6: "parameter6_example", parameter7: "parameter7_example", parameter8: "parameter8_example", parameter9: "parameter9_example", parameter10: "parameter10_example", parameter11: "parameter11_example", osRequirements: "osRequirements_example", scriptContents: "scriptContents_example") // Script | new script to upload to existing id. ids defined in this body will be ignored

// Replace the script at the id with the supplied information 
ScriptsAPI.v1ScriptsIdPut(id: id, script: script) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Script object identifier | 
 **script** | [**Script**](Script.md) | new script to upload to existing id. ids defined in this body will be ignored | 

### Return type

[**Script**](Script.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ScriptsPost**
```swift
    open class func v1ScriptsPost(script: Script, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Script 

Creates a script

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let script = Script(id: "id_example", name: "name_example", info: "info_example", notes: "notes_example", priority: "priority_example", categoryId: "categoryId_example", categoryName: "categoryName_example", parameter4: "parameter4_example", parameter5: "parameter5_example", parameter6: "parameter6_example", parameter7: "parameter7_example", parameter8: "parameter8_example", parameter9: "parameter9_example", parameter10: "parameter10_example", parameter11: "parameter11_example", osRequirements: "osRequirements_example", scriptContents: "scriptContents_example") // Script | new script to create. ids defined in this body will be ignored

// Create a Script 
ScriptsAPI.v1ScriptsPost(script: script) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **script** | [**Script**](Script.md) | new script to create. ids defined in this body will be ignored | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

