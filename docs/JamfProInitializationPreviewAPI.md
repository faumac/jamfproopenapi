# JamfProInitializationPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**systemInitializeDatabaseConnectionPost**](JamfProInitializationPreviewAPI.md#systeminitializedatabaseconnectionpost) | **POST** /system/initialize-database-connection | Provide Database Password during startup 
[**systemInitializePost**](JamfProInitializationPreviewAPI.md#systeminitializepost) | **POST** /system/initialize | Set up fresh installed Jamf Pro Server 


# **systemInitializeDatabaseConnectionPost**
```swift
    open class func systemInitializeDatabaseConnectionPost(databasePassword: DatabasePassword, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Provide Database Password during startup 

Provide database password during startup. Endpoint is accessible when database password was not configured and Jamf Pro server has not been initialized yet.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let databasePassword = DatabasePassword(password: "password_example") // DatabasePassword | 

// Provide Database Password during startup 
JamfProInitializationPreviewAPI.systemInitializeDatabaseConnectionPost(databasePassword: databasePassword) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **databasePassword** | [**DatabasePassword**](DatabasePassword.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **systemInitializePost**
```swift
    open class func systemInitializePost(initialize: Initialize, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Set up fresh installed Jamf Pro Server 

Set up fresh installed Jamf Pro Server 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let initialize = Initialize(activationCode: "activationCode_example", institutionName: "institutionName_example", isEulaAccepted: false, username: "username_example", password: "password_example", email: "email_example", jssUrl: "jssUrl_example") // Initialize | 

// Set up fresh installed Jamf Pro Server 
JamfProInitializationPreviewAPI.systemInitializePost(initialize: initialize) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **initialize** | [**Initialize**](Initialize.md) |  | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

