# MobileDeviceCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commonName** | **String** |  | [optional] 
**identity** | **Bool** |  | [optional] 
**expirationDate** | **Date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


