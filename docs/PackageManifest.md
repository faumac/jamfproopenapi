# PackageManifest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | 
**hash** | **String** |  | 
**hashType** | **String** |  | 
**displayImageUrl** | **String** |  | [optional] 
**fullSizeImageUrl** | **String** |  | [optional] 
**bundleId** | **String** |  | 
**bundleVersion** | **String** |  | 
**subtitle** | **String** |  | [optional] 
**title** | **String** |  | 
**sizeInBytes** | **Int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


