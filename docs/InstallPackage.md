# InstallPackage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manifest** | [**PackageManifest**](PackageManifest.md) |  | 
**installAsManaged** | **Bool** |  | [optional] 
**devices** | **[Int]** |  | [optional] 
**groupId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


