# MobileDeviceSecurity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataProtected** | **Bool** |  | [optional] 
**blockLevelEncryptionCapable** | **Bool** |  | [optional] 
**fileLevelEncryptionCapable** | **Bool** |  | [optional] 
**passcodePresent** | **Bool** |  | [optional] 
**passcodeCompliant** | **Bool** |  | [optional] 
**passcodeCompliantWithProfile** | **Bool** |  | [optional] 
**hardwareEncryption** | **Int** |  | [optional] 
**activationLockEnabled** | **Bool** |  | [optional] 
**jailBreakDetected** | **Bool** |  | [optional] 
**passcodeLockGracePeriodEnforcedSeconds** | **Int** |  | [optional] 
**personalDeviceProfileCurrent** | **Bool** |  | [optional] 
**lostModeEnabled** | **Bool** |  | [optional] 
**lostModePersistent** | **Bool** |  | [optional] 
**lostModeMessage** | **String** |  | [optional] 
**lostModePhoneNumber** | **String** |  | [optional] 
**lostModeFootnote** | **String** |  | [optional] 
**lostModeLocation** | [**MobileDeviceLostModeLocation**](MobileDeviceLostModeLocation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


