# DashboardApiError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**httpStatusCode** | **Int** |  | [optional] [default to 500]
**id** | **String** |  | [optional] [default to ""]
**description** | **String** |  | [optional] [default to ""]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


