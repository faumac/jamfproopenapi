# IosDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**modelNumber** | **String** |  | [optional] 
**isSupervised** | **Bool** |  | [optional] 
**batteryLevel** | **Int** |  | [optional] 
**lastBackupTimestamp** | **Date** |  | [optional] 
**capacityMb** | **Int** |  | [optional] 
**availableMb** | **Int** |  | [optional] 
**percentageUsed** | **Int** |  | [optional] 
**isShared** | **Bool** |  | [optional] 
**isDeviceLocatorServiceEnabled** | **Bool** |  | [optional] 
**isDoNotDisturbEnabled** | **Bool** |  | [optional] 
**isCloudBackupEnabled** | **Bool** |  | [optional] 
**lastCloudBackupTimestamp** | **Date** |  | [optional] 
**isLocationServicesEnabled** | **Bool** |  | [optional] 
**isITunesStoreAccountActive** | **Bool** |  | [optional] 
**isBleCapable** | **Bool** |  | [optional] 
**computer** | [**IdAndName**](IdAndName.md) |  | [optional] 
**purchasing** | [**Purchasing**](Purchasing.md) |  | [optional] 
**security** | [**Security**](Security.md) |  | [optional] 
**network** | [**Network**](Network.md) |  | [optional] 
**applications** | [MobileDeviceApplication] |  | [optional] 
**certificates** | [MobileDeviceCertificateV1] |  | [optional] 
**ebooks** | [MobileDeviceEbook] |  | [optional] 
**configurationProfiles** | [ConfigurationProfile] |  | [optional] 
**provisioningProfiles** | [MobileDeviceProvisioningProfiles] |  | [optional] 
**attachments** | [MobileDeviceAttachment] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


