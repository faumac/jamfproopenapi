# MobileDeviceNetwork

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cellularTechnology** | **String** |  | [optional] 
**voiceRoamingEnabled** | **Bool** |  | [optional] 
**imei** | **String** |  | [optional] 
**iccid** | **String** |  | [optional] 
**meid** | **String** |  | [optional] 
**eid** | **String** | EID or \&quot;embedded identity document\&quot; is a number associated with the eSIM on a device | [optional] [readonly] 
**carrierSettingsVersion** | **String** |  | [optional] 
**currentCarrierNetwork** | **String** |  | [optional] 
**currentMobileCountryCode** | **String** |  | [optional] 
**currentMobileNetworkCode** | **String** |  | [optional] 
**homeCarrierNetwork** | **String** |  | [optional] 
**homeMobileCountryCode** | **String** |  | [optional] 
**homeMobileNetworkCode** | **String** |  | [optional] 
**dataRoamingEnabled** | **Bool** |  | [optional] 
**roaming** | **Bool** |  | [optional] 
**personalHotspotEnabled** | **Bool** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


