# ComputerContentCachingParentDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentCachingParentDetailsId** | **String** |  | [optional] [readonly] 
**acPower** | **Bool** |  | [optional] [readonly] 
**cacheSizeBytes** | **Int64** |  | [optional] [readonly] 
**capabilities** | [**ComputerContentCachingParentCapabilities**](ComputerContentCachingParentCapabilities.md) |  | [optional] 
**portable** | **Bool** |  | [optional] [readonly] 
**localNetwork** | [ComputerContentCachingParentLocalNetwork] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


