# MobileDeviceExtensionAttributesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**devicesExtensionAttributesGet**](MobileDeviceExtensionAttributesPreviewAPI.md#devicesextensionattributesget) | **GET** /devices/extensionAttributes | Get Mobile Device Extension Attribute values placed in select paramter 


# **devicesExtensionAttributesGet**
```swift
    open class func devicesExtensionAttributesGet(select: String? = nil, completion: @escaping (_ data: MobileDeviceExtensionAttributeResults?, _ error: Error?) -> Void)
```

Get Mobile Device Extension Attribute values placed in select paramter 

Gets Mobile Device Extension Attribute values placed in select parameter.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let select = "select_example" // String | Acceptable values currently include: * name  (optional) (default to "name")

// Get Mobile Device Extension Attribute values placed in select paramter 
MobileDeviceExtensionAttributesPreviewAPI.devicesExtensionAttributesGet(select: select) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **select** | **String** | Acceptable values currently include: * name  | [optional] [default to &quot;name&quot;]

### Return type

[**MobileDeviceExtensionAttributeResults**](MobileDeviceExtensionAttributeResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

