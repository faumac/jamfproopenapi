# ClientCheckInAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v2CheckInGet**](ClientCheckInAPI.md#v2checkinget) | **GET** /v2/check-in | Get Client Check-In settings 
[**v2CheckInHistoryGet**](ClientCheckInAPI.md#v2checkinhistoryget) | **GET** /v2/check-in/history | Get Client Check-In history object 
[**v2CheckInHistoryPost**](ClientCheckInAPI.md#v2checkinhistorypost) | **POST** /v2/check-in/history | Add a Note to Client Check-In History 
[**v2CheckInPut**](ClientCheckInAPI.md#v2checkinput) | **PUT** /v2/check-in | Update Client Check-In object 
[**v3CheckInGet**](ClientCheckInAPI.md#v3checkinget) | **GET** /v3/check-in | Get Client Check-In settings 
[**v3CheckInHistoryGet**](ClientCheckInAPI.md#v3checkinhistoryget) | **GET** /v3/check-in/history | Get Client Check-In history object 
[**v3CheckInHistoryPost**](ClientCheckInAPI.md#v3checkinhistorypost) | **POST** /v3/check-in/history | Add a Note to Client Check-In History 
[**v3CheckInPut**](ClientCheckInAPI.md#v3checkinput) | **PUT** /v3/check-in | Update Client Check-In object 


# **v2CheckInGet**
```swift
    open class func v2CheckInGet(completion: @escaping (_ data: ClientCheckInV2?, _ error: Error?) -> Void)
```

Get Client Check-In settings 

Gets `Client Check-In` object. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Client Check-In settings 
ClientCheckInAPI.v2CheckInGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ClientCheckInV2**](ClientCheckInV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CheckInHistoryGet**
```swift
    open class func v2CheckInHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResultsV1?, _ error: Error?) -> Void)
```

Get Client Check-In history object 

Gets Client Check-In history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,username:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Client Check-In history object 
ClientCheckInAPI.v2CheckInHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,username:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResultsV1**](HistorySearchResultsV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CheckInHistoryPost**
```swift
    open class func v2CheckInHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add a Note to Client Check-In History 

Adds Client Check-In history object notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add a Note to Client Check-In History 
ClientCheckInAPI.v2CheckInHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2CheckInPut**
```swift
    open class func v2CheckInPut(clientCheckInV2: ClientCheckInV2, completion: @escaping (_ data: ClientCheckInV2?, _ error: Error?) -> Void)
```

Update Client Check-In object 

Update Client Check-In object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientCheckInV2 = ClientCheckInV2(checkInFrequency: 123, createHooks: false, hookLog: false, hookPolicies: false, hookHideRestore: false, hookMcx: false, backgroundHooks: false, hookDisplayStatus: false, createStartupScript: false, startupLog: false, startupPolicies: false, startupSsh: false, startupMcx: false, enableLocalConfigurationProfiles: false) // ClientCheckInV2 | Client Check-In object to update

// Update Client Check-In object 
ClientCheckInAPI.v2CheckInPut(clientCheckInV2: clientCheckInV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientCheckInV2** | [**ClientCheckInV2**](ClientCheckInV2.md) | Client Check-In object to update | 

### Return type

[**ClientCheckInV2**](ClientCheckInV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3CheckInGet**
```swift
    open class func v3CheckInGet(completion: @escaping (_ data: ClientCheckInV3?, _ error: Error?) -> Void)
```

Get Client Check-In settings 

Gets `Client Check-In` object. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get Client Check-In settings 
ClientCheckInAPI.v3CheckInGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ClientCheckInV3**](ClientCheckInV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3CheckInHistoryGet**
```swift
    open class func v3CheckInHistoryGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResultsV1?, _ error: Error?) -> Void)
```

Get Client Check-In history object 

Gets Client Check-In history object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,username:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Client Check-In history object 
ClientCheckInAPI.v3CheckInHistoryGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is name:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,username:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResultsV1**](HistorySearchResultsV1.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3CheckInHistoryPost**
```swift
    open class func v3CheckInHistoryPost(objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Add a Note to Client Check-In History 

Adds Client Check-In history object notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add a Note to Client Check-In History 
ClientCheckInAPI.v3CheckInHistoryPost(objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3CheckInPut**
```swift
    open class func v3CheckInPut(clientCheckInV3: ClientCheckInV3, completion: @escaping (_ data: ClientCheckInV3?, _ error: Error?) -> Void)
```

Update Client Check-In object 

Update Client Check-In object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let clientCheckInV3 = ClientCheckInV3(checkInFrequency: 123, createHooks: false, hookLog: false, hookPolicies: false, createStartupScript: false, startupLog: false, startupPolicies: false, startupSsh: false, enableLocalConfigurationProfiles: false) // ClientCheckInV3 | Client Check-In object to update

// Update Client Check-In object 
ClientCheckInAPI.v3CheckInPut(clientCheckInV3: clientCheckInV3) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientCheckInV3** | [**ClientCheckInV3**](ClientCheckInV3.md) | Client Check-In object to update | 

### Return type

[**ClientCheckInV3**](ClientCheckInV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

