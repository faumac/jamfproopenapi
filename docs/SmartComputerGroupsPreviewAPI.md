# SmartComputerGroupsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ComputersIdRecalculateSmartGroupsPost**](SmartComputerGroupsPreviewAPI.md#v1computersidrecalculatesmartgroupspost) | **POST** /v1/computers/{id}/recalculate-smart-groups | Recalculate a smart group for the given id 
[**v1SmartComputerGroupsIdRecalculatePost**](SmartComputerGroupsPreviewAPI.md#v1smartcomputergroupsidrecalculatepost) | **POST** /v1/smart-computer-groups/{id}/recalculate | Recalculate the smart group for the given id 


# **v1ComputersIdRecalculateSmartGroupsPost**
```swift
    open class func v1ComputersIdRecalculateSmartGroupsPost(id: Int, completion: @escaping (_ data: RecalculationResults?, _ error: Error?) -> Void)
```

Recalculate a smart group for the given id 

Recalculates a smart group for the given id and then returns the count of smart groups the computer falls into 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | id of computer

// Recalculate a smart group for the given id 
SmartComputerGroupsPreviewAPI.v1ComputersIdRecalculateSmartGroupsPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | id of computer | 

### Return type

[**RecalculationResults**](RecalculationResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SmartComputerGroupsIdRecalculatePost**
```swift
    open class func v1SmartComputerGroupsIdRecalculatePost(id: Int, completion: @escaping (_ data: RecalculationResults?, _ error: Error?) -> Void)
```

Recalculate the smart group for the given id 

Recalculates the smart group for the given id and then returns the ids for the computers in the smart group 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance id of smart group

// Recalculate the smart group for the given id 
SmartComputerGroupsPreviewAPI.v1SmartComputerGroupsIdRecalculatePost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance id of smart group | 

### Return type

[**RecalculationResults**](RecalculationResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

