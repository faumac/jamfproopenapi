# ComputerContentCaching

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**computerContentCachingInformationId** | **String** |  | [optional] [readonly] 
**parents** | [ComputerContentCachingParent] |  | [optional] [readonly] 
**alerts** | [ComputerContentCachingAlert] |  | [optional] [readonly] 
**activated** | **Bool** |  | [optional] [readonly] 
**active** | **Bool** |  | [optional] [readonly] 
**actualCacheBytesUsed** | **Int64** |  | [optional] [readonly] 
**cacheDetails** | [ComputerContentCachingCacheDetail] |  | [optional] [readonly] 
**cacheBytesFree** | **Int64** |  | [optional] [readonly] 
**cacheBytesLimit** | **Int64** |  | [optional] [readonly] 
**cacheStatus** | **String** |  | [optional] [readonly] 
**cacheBytesUsed** | **Int64** |  | [optional] [readonly] 
**dataMigrationCompleted** | **Bool** |  | [optional] [readonly] 
**dataMigrationProgressPercentage** | **Int** |  | [optional] [readonly] 
**dataMigrationError** | [**ComputerContentCachingDataMigrationError**](ComputerContentCachingDataMigrationError.md) |  | [optional] 
**maxCachePressureLast1HourPercentage** | **Int** |  | [optional] [readonly] 
**personalCacheBytesFree** | **Int64** |  | [optional] [readonly] 
**personalCacheBytesLimit** | **Int64** |  | [optional] [readonly] 
**personalCacheBytesUsed** | **Int64** |  | [optional] [readonly] 
**port** | **Int64** |  | [optional] [readonly] 
**publicAddress** | **String** |  | [optional] [readonly] 
**registrationError** | **String** |  | [optional] [readonly] 
**registrationResponseCode** | **Int64** |  | [optional] [readonly] 
**registrationStarted** | **Date** |  | [optional] [readonly] 
**registrationStatus** | **String** |  | [optional] [readonly] 
**restrictedMedia** | **Bool** |  | [optional] [readonly] 
**serverGuid** | **String** |  | [optional] [readonly] 
**startupStatus** | **String** |  | [optional] [readonly] 
**tetheratorStatus** | **String** |  | [optional] [readonly] 
**totalBytesAreSince** | **Date** |  | [optional] [readonly] 
**totalBytesDropped** | **Int64** |  | [optional] [readonly] 
**totalBytesImported** | **Int64** |  | [optional] [readonly] 
**totalBytesReturnedToChildren** | **Int64** |  | [optional] [readonly] 
**totalBytesReturnedToClients** | **Int64** |  | [optional] [readonly] 
**totalBytesReturnedToPeers** | **Int64** |  | [optional] [readonly] 
**totalBytesStoredFromOrigin** | **Int64** |  | [optional] [readonly] 
**totalBytesStoredFromParents** | **Int64** |  | [optional] [readonly] 
**totalBytesStoredFromPeers** | **Int64** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


