# ComputerPrestagesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ComputerPrestagesIdScopeDelete**](ComputerPrestagesAPI.md#v1computerprestagesidscopedelete) | **DELETE** /v1/computer-prestages/{id}/scope | Remove device Scope for a specific Computer Prestage 
[**v1ComputerPrestagesIdScopeGet**](ComputerPrestagesAPI.md#v1computerprestagesidscopeget) | **GET** /v1/computer-prestages/{id}/scope | Get device Scope for a specific Computer Prestage 
[**v1ComputerPrestagesIdScopePost**](ComputerPrestagesAPI.md#v1computerprestagesidscopepost) | **POST** /v1/computer-prestages/{id}/scope | Add device Scope for a specific Computer Prestage 
[**v1ComputerPrestagesIdScopePut**](ComputerPrestagesAPI.md#v1computerprestagesidscopeput) | **PUT** /v1/computer-prestages/{id}/scope | Replace device Scope for a specific Computer Prestage 
[**v1ComputerPrestagesScopeGet**](ComputerPrestagesAPI.md#v1computerprestagesscopeget) | **GET** /v1/computer-prestages/scope | Get all device Scope for all Computer Prestages 
[**v2ComputerPrestagesGet**](ComputerPrestagesAPI.md#v2computerprestagesget) | **GET** /v2/computer-prestages | Get sorted and paged Computer Prestages 
[**v2ComputerPrestagesIdDelete**](ComputerPrestagesAPI.md#v2computerprestagesiddelete) | **DELETE** /v2/computer-prestages/{id} | Delete a Computer Prestage with the supplied id 
[**v2ComputerPrestagesIdGet**](ComputerPrestagesAPI.md#v2computerprestagesidget) | **GET** /v2/computer-prestages/{id} | Retrieve a Computer Prestage with the supplied id 
[**v2ComputerPrestagesIdPut**](ComputerPrestagesAPI.md#v2computerprestagesidput) | **PUT** /v2/computer-prestages/{id} | Update a Computer Prestage 
[**v2ComputerPrestagesIdScopeDeleteMultiplePost**](ComputerPrestagesAPI.md#v2computerprestagesidscopedeletemultiplepost) | **POST** /v2/computer-prestages/{id}/scope/delete-multiple | Remove device Scope for a specific Computer Prestage 
[**v2ComputerPrestagesIdScopeGet**](ComputerPrestagesAPI.md#v2computerprestagesidscopeget) | **GET** /v2/computer-prestages/{id}/scope | Get device Scope for a specific Computer Prestage 
[**v2ComputerPrestagesIdScopePost**](ComputerPrestagesAPI.md#v2computerprestagesidscopepost) | **POST** /v2/computer-prestages/{id}/scope | Add device Scope for a specific Computer Prestage 
[**v2ComputerPrestagesIdScopePut**](ComputerPrestagesAPI.md#v2computerprestagesidscopeput) | **PUT** /v2/computer-prestages/{id}/scope | Replace device Scope for a specific Computer Prestage 
[**v2ComputerPrestagesPost**](ComputerPrestagesAPI.md#v2computerprestagespost) | **POST** /v2/computer-prestages | Create a Computer Prestage 
[**v2ComputerPrestagesScopeGet**](ComputerPrestagesAPI.md#v2computerprestagesscopeget) | **GET** /v2/computer-prestages/scope | Get all device Scope for all Computer Prestages 
[**v3ComputerPrestagesGet**](ComputerPrestagesAPI.md#v3computerprestagesget) | **GET** /v3/computer-prestages | Get sorted and paged Computer Prestages 
[**v3ComputerPrestagesIdDelete**](ComputerPrestagesAPI.md#v3computerprestagesiddelete) | **DELETE** /v3/computer-prestages/{id} | Delete a Computer Prestage with the supplied id 
[**v3ComputerPrestagesIdGet**](ComputerPrestagesAPI.md#v3computerprestagesidget) | **GET** /v3/computer-prestages/{id} | Retrieve a Computer Prestage with the supplied id 
[**v3ComputerPrestagesIdPut**](ComputerPrestagesAPI.md#v3computerprestagesidput) | **PUT** /v3/computer-prestages/{id} | Update a Computer Prestage 
[**v3ComputerPrestagesPost**](ComputerPrestagesAPI.md#v3computerprestagespost) | **POST** /v3/computer-prestages | Create a Computer Prestage 


# **v1ComputerPrestagesIdScopeDelete**
```swift
    open class func v1ComputerPrestagesIdScopeDelete(id: Int, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Remove device Scope for a specific Computer Prestage 

Remove device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Computer Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to remove from scope

// Remove device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v1ComputerPrestagesIdScopeDelete(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Computer Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to remove from scope | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerPrestagesIdScopeGet**
```swift
    open class func v1ComputerPrestagesIdScopeGet(id: Int, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Get device Scope for a specific Computer Prestage 

Get device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Computer Prestage identifier

// Get device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v1ComputerPrestagesIdScopeGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Computer Prestage identifier | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerPrestagesIdScopePost**
```swift
    open class func v1ComputerPrestagesIdScopePost(id: Int, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Add device Scope for a specific Computer Prestage 

Add device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Computer Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Add device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v1ComputerPrestagesIdScopePost(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Computer Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerPrestagesIdScopePut**
```swift
    open class func v1ComputerPrestagesIdScopePut(id: Int, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponse?, _ error: Error?) -> Void)
```

Replace device Scope for a specific Computer Prestage 

Replace device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | Computer Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Replace device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v1ComputerPrestagesIdScopePut(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | Computer Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponse**](PrestageScopeResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ComputerPrestagesScopeGet**
```swift
    open class func v1ComputerPrestagesScopeGet(completion: @escaping (_ data: PrestageScope?, _ error: Error?) -> Void)
```

Get all device Scope for all Computer Prestages 

Get all device scope for all computer prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all device Scope for all Computer Prestages 
ComputerPrestagesAPI.v1ComputerPrestagesScopeGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PrestageScope**](PrestageScope.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesGet**
```swift
    open class func v2ComputerPrestagesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: ComputerPrestageSearchResultsV2?, _ error: Error?) -> Void)
```

Get sorted and paged Computer Prestages 

Gets sorted and paged computer prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Get sorted and paged Computer Prestages 
ComputerPrestagesAPI.v2ComputerPrestagesGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**ComputerPrestageSearchResultsV2**](ComputerPrestageSearchResultsV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdDelete**
```swift
    open class func v2ComputerPrestagesIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Computer Prestage with the supplied id 

Deletes a Computer Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier

// Delete a Computer Prestage with the supplied id 
ComputerPrestagesAPI.v2ComputerPrestagesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdGet**
```swift
    open class func v2ComputerPrestagesIdGet(id: String, completion: @escaping (_ data: GetComputerPrestageV2?, _ error: Error?) -> Void)
```

Retrieve a Computer Prestage with the supplied id 

Retrieves a Computer Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier

// Retrieve a Computer Prestage with the supplied id 
ComputerPrestagesAPI.v2ComputerPrestagesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 

### Return type

[**GetComputerPrestageV2**](GetComputerPrestageV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdPut**
```swift
    open class func v2ComputerPrestagesIdPut(id: String, putComputerPrestageV2: PutComputerPrestageV2, completion: @escaping (_ data: GetComputerPrestageV2?, _ error: Error?) -> Void)
```

Update a Computer Prestage 

Updates a Computer Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier
let putComputerPrestageV2 = PutComputerPrestageV2(displayName: "displayName_example", mandatory: false, mdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", defaultPrestage: false, enrollmentSiteId: "enrollmentSiteId_example", keepExistingSiteMembership: true, keepExistingLocationInformation: true, requireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", preventActivationLock: true, enableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: "deviceEnrollmentProgramInstanceId_example", skipSetupItems: "TODO", locationInformation: LocationInformationV2(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: "departmentId_example", buildingId: "buildingId_example", id: "id_example", versionLock: 123), purchasingInformation: PrestagePurchasingInformationV2(id: "id_example", leased: true, purchased: true, appleCareId: "appleCareId_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: "enrollmentCustomizationId_example", language: "language_example", region: "region_example", autoAdvanceSetup: true, installProfilesDuringSetup: true, prestageInstalledProfileIds: ["prestageInstalledProfileIds_example"], customPackageIds: ["customPackageIds_example"], customPackageDistributionPointId: "customPackageDistributionPointId_example", enableRecoveryLock: true, recoveryLockPasswordType: "recoveryLockPasswordType_example", rotateRecoveryLockPassword: true, recoveryLockPassword: "recoveryLockPassword_example", versionLock: 123) // PutComputerPrestageV2 | Computer Prestage to update

// Update a Computer Prestage 
ComputerPrestagesAPI.v2ComputerPrestagesIdPut(id: id, putComputerPrestageV2: putComputerPrestageV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 
 **putComputerPrestageV2** | [**PutComputerPrestageV2**](PutComputerPrestageV2.md) | Computer Prestage to update | 

### Return type

[**GetComputerPrestageV2**](GetComputerPrestageV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdScopeDeleteMultiplePost**
```swift
    open class func v2ComputerPrestagesIdScopeDeleteMultiplePost(id: String, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Remove device Scope for a specific Computer Prestage 

Remove device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to remove from scope

// Remove device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v2ComputerPrestagesIdScopeDeleteMultiplePost(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to remove from scope | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdScopeGet**
```swift
    open class func v2ComputerPrestagesIdScopeGet(id: String, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Get device Scope for a specific Computer Prestage 

Get device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier

// Get device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v2ComputerPrestagesIdScopeGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdScopePost**
```swift
    open class func v2ComputerPrestagesIdScopePost(id: String, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Add device Scope for a specific Computer Prestage 

Add device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Add device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v2ComputerPrestagesIdScopePost(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesIdScopePut**
```swift
    open class func v2ComputerPrestagesIdScopePut(id: String, prestageScopeUpdate: PrestageScopeUpdate, completion: @escaping (_ data: PrestageScopeResponseV2?, _ error: Error?) -> Void)
```

Replace device Scope for a specific Computer Prestage 

Replace device scope for a specific computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier
let prestageScopeUpdate = PrestageScopeUpdate(serialNumbers: ["serialNumbers_example"], versionLock: 123) // PrestageScopeUpdate | Serial Numbers to scope

// Replace device Scope for a specific Computer Prestage 
ComputerPrestagesAPI.v2ComputerPrestagesIdScopePut(id: id, prestageScopeUpdate: prestageScopeUpdate) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 
 **prestageScopeUpdate** | [**PrestageScopeUpdate**](PrestageScopeUpdate.md) | Serial Numbers to scope | 

### Return type

[**PrestageScopeResponseV2**](PrestageScopeResponseV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesPost**
```swift
    open class func v2ComputerPrestagesPost(postComputerPrestageV2: PostComputerPrestageV2, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Computer Prestage 

Create a computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let postComputerPrestageV2 = PostComputerPrestageV2(displayName: "displayName_example", mandatory: false, mdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", defaultPrestage: false, enrollmentSiteId: "enrollmentSiteId_example", keepExistingSiteMembership: true, keepExistingLocationInformation: true, requireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", preventActivationLock: true, enableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: "deviceEnrollmentProgramInstanceId_example", skipSetupItems: "TODO", locationInformation: LocationInformationV2(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: "departmentId_example", buildingId: "buildingId_example", id: "id_example", versionLock: 123), purchasingInformation: PrestagePurchasingInformationV2(id: "id_example", leased: true, purchased: true, appleCareId: "appleCareId_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: "enrollmentCustomizationId_example", language: "language_example", region: "region_example", autoAdvanceSetup: true, installProfilesDuringSetup: true, prestageInstalledProfileIds: ["prestageInstalledProfileIds_example"], customPackageIds: ["customPackageIds_example"], customPackageDistributionPointId: "customPackageDistributionPointId_example", enableRecoveryLock: true, recoveryLockPasswordType: "recoveryLockPasswordType_example", rotateRecoveryLockPassword: true, recoveryLockPassword: "recoveryLockPassword_example") // PostComputerPrestageV2 | Computer Prestage to create. ids defined in this body will be ignored

// Create a Computer Prestage 
ComputerPrestagesAPI.v2ComputerPrestagesPost(postComputerPrestageV2: postComputerPrestageV2) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postComputerPrestageV2** | [**PostComputerPrestageV2**](PostComputerPrestageV2.md) | Computer Prestage to create. ids defined in this body will be ignored | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2ComputerPrestagesScopeGet**
```swift
    open class func v2ComputerPrestagesScopeGet(completion: @escaping (_ data: PrestageScopeV2?, _ error: Error?) -> Void)
```

Get all device Scope for all Computer Prestages 

Get all device scope for all computer prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get all device Scope for all Computer Prestages 
ComputerPrestagesAPI.v2ComputerPrestagesScopeGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PrestageScopeV2**](PrestageScopeV2.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3ComputerPrestagesGet**
```swift
    open class func v3ComputerPrestagesGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: ComputerPrestageSearchResultsV3?, _ error: Error?) -> Void)
```

Get sorted and paged Computer Prestages 

Gets sorted and paged computer prestages

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Get sorted and paged Computer Prestages 
ComputerPrestagesAPI.v3ComputerPrestagesGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**ComputerPrestageSearchResultsV3**](ComputerPrestageSearchResultsV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3ComputerPrestagesIdDelete**
```swift
    open class func v3ComputerPrestagesIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete a Computer Prestage with the supplied id 

Deletes a Computer Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier

// Delete a Computer Prestage with the supplied id 
ComputerPrestagesAPI.v3ComputerPrestagesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3ComputerPrestagesIdGet**
```swift
    open class func v3ComputerPrestagesIdGet(id: String, completion: @escaping (_ data: GetComputerPrestageV3?, _ error: Error?) -> Void)
```

Retrieve a Computer Prestage with the supplied id 

Retrieves a Computer Prestage with the supplied id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier

// Retrieve a Computer Prestage with the supplied id 
ComputerPrestagesAPI.v3ComputerPrestagesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 

### Return type

[**GetComputerPrestageV3**](GetComputerPrestageV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3ComputerPrestagesIdPut**
```swift
    open class func v3ComputerPrestagesIdPut(id: String, putComputerPrestageV3: PutComputerPrestageV3, completion: @escaping (_ data: GetComputerPrestageV3?, _ error: Error?) -> Void)
```

Update a Computer Prestage 

Updates a Computer Prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Computer Prestage identifier
let putComputerPrestageV3 = PutComputerPrestageV3(displayName: "displayName_example", mandatory: false, mdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", defaultPrestage: false, enrollmentSiteId: "enrollmentSiteId_example", keepExistingSiteMembership: true, keepExistingLocationInformation: true, requireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", preventActivationLock: true, enableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: "deviceEnrollmentProgramInstanceId_example", skipSetupItems: "TODO", locationInformation: LocationInformationV2(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: "departmentId_example", buildingId: "buildingId_example", id: "id_example", versionLock: 123), purchasingInformation: PrestagePurchasingInformationV2(id: "id_example", leased: true, purchased: true, appleCareId: "appleCareId_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: "enrollmentCustomizationId_example", language: "language_example", region: "region_example", autoAdvanceSetup: true, installProfilesDuringSetup: true, prestageInstalledProfileIds: ["prestageInstalledProfileIds_example"], customPackageIds: ["customPackageIds_example"], customPackageDistributionPointId: "customPackageDistributionPointId_example", enableRecoveryLock: true, recoveryLockPasswordType: "recoveryLockPasswordType_example", rotateRecoveryLockPassword: true, accountSettings: AccountSettingsRequest(id: "id_example", payloadConfigured: true, localAdminAccountEnabled: true, adminUsername: "adminUsername_example", adminPassword: "adminPassword_example", hiddenAdminAccount: false, localUserManaged: true, userAccountType: "userAccountType_example", versionLock: 123, prefillPrimaryAccountInfoFeatureEnabled: true, prefillType: "prefillType_example", prefillAccountFullName: "prefillAccountFullName_example", prefillAccountUserName: "prefillAccountUserName_example", preventPrefillInfoFromModification: false), recoveryLockPassword: "recoveryLockPassword_example", versionLock: 123) // PutComputerPrestageV3 | Computer Prestage to update

// Update a Computer Prestage 
ComputerPrestagesAPI.v3ComputerPrestagesIdPut(id: id, putComputerPrestageV3: putComputerPrestageV3) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Computer Prestage identifier | 
 **putComputerPrestageV3** | [**PutComputerPrestageV3**](PutComputerPrestageV3.md) | Computer Prestage to update | 

### Return type

[**GetComputerPrestageV3**](GetComputerPrestageV3.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v3ComputerPrestagesPost**
```swift
    open class func v3ComputerPrestagesPost(postComputerPrestageV3: PostComputerPrestageV3, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create a Computer Prestage 

Create a computer prestage

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let postComputerPrestageV3 = PostComputerPrestageV3(displayName: "displayName_example", mandatory: false, mdmRemovable: true, supportPhoneNumber: "supportPhoneNumber_example", supportEmailAddress: "supportEmailAddress_example", department: "department_example", defaultPrestage: false, enrollmentSiteId: "enrollmentSiteId_example", keepExistingSiteMembership: true, keepExistingLocationInformation: true, requireAuthentication: true, authenticationPrompt: "authenticationPrompt_example", preventActivationLock: true, enableDeviceBasedActivationLock: true, deviceEnrollmentProgramInstanceId: "deviceEnrollmentProgramInstanceId_example", skipSetupItems: "TODO", locationInformation: LocationInformationV2(username: "username_example", realname: "realname_example", phone: "phone_example", email: "email_example", room: "room_example", position: "position_example", departmentId: "departmentId_example", buildingId: "buildingId_example", id: "id_example", versionLock: 123), purchasingInformation: PrestagePurchasingInformationV2(id: "id_example", leased: true, purchased: true, appleCareId: "appleCareId_example", poNumber: "poNumber_example", vendor: "vendor_example", purchasePrice: "purchasePrice_example", lifeExpectancy: 123, purchasingAccount: "purchasingAccount_example", purchasingContact: "purchasingContact_example", leaseDate: "leaseDate_example", poDate: "poDate_example", warrantyDate: "warrantyDate_example", versionLock: 123), anchorCertificates: ["anchorCertificates_example"], enrollmentCustomizationId: "enrollmentCustomizationId_example", language: "language_example", region: "region_example", autoAdvanceSetup: true, installProfilesDuringSetup: true, prestageInstalledProfileIds: ["prestageInstalledProfileIds_example"], customPackageIds: ["customPackageIds_example"], customPackageDistributionPointId: "customPackageDistributionPointId_example", enableRecoveryLock: true, recoveryLockPasswordType: "recoveryLockPasswordType_example", rotateRecoveryLockPassword: true, accountSettings: AccountSettingsRequest(id: "id_example", payloadConfigured: true, localAdminAccountEnabled: true, adminUsername: "adminUsername_example", adminPassword: "adminPassword_example", hiddenAdminAccount: false, localUserManaged: true, userAccountType: "userAccountType_example", versionLock: 123, prefillPrimaryAccountInfoFeatureEnabled: true, prefillType: "prefillType_example", prefillAccountFullName: "prefillAccountFullName_example", prefillAccountUserName: "prefillAccountUserName_example", preventPrefillInfoFromModification: false), recoveryLockPassword: "recoveryLockPassword_example") // PostComputerPrestageV3 | Computer Prestage to create. ids defined in this body will be ignored

// Create a Computer Prestage 
ComputerPrestagesAPI.v3ComputerPrestagesPost(postComputerPrestageV3: postComputerPrestageV3) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postComputerPrestageV3** | [**PostComputerPrestageV3**](PostComputerPrestageV3.md) | Computer Prestage to create. ids defined in this body will be ignored | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

