# CloudIdpAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1CloudIdpExportPost**](CloudIdpAPI.md#v1cloudidpexportpost) | **POST** /v1/cloud-idp/export | Export Cloud Identity Providers collection 
[**v1CloudIdpGet**](CloudIdpAPI.md#v1cloudidpget) | **GET** /v1/cloud-idp | Get information about all Cloud Identity Providers configurations.
[**v1CloudIdpIdGet**](CloudIdpAPI.md#v1cloudidpidget) | **GET** /v1/cloud-idp/{id} | Get Cloud Identity Provider configuration with given ID.
[**v1CloudIdpIdHistoryGet**](CloudIdpAPI.md#v1cloudidpidhistoryget) | **GET** /v1/cloud-idp/{id}/history | Get Cloud Identity Provider history
[**v1CloudIdpIdHistoryPost**](CloudIdpAPI.md#v1cloudidpidhistorypost) | **POST** /v1/cloud-idp/{id}/history | Add Cloud Identity Provider history note
[**v1CloudIdpIdTestGroupPost**](CloudIdpAPI.md#v1cloudidpidtestgrouppost) | **POST** /v1/cloud-idp/{id}/test-group | Get group test search
[**v1CloudIdpIdTestUserMembershipPost**](CloudIdpAPI.md#v1cloudidpidtestusermembershippost) | **POST** /v1/cloud-idp/{id}/test-user-membership | Get membership test search
[**v1CloudIdpIdTestUserPost**](CloudIdpAPI.md#v1cloudidpidtestuserpost) | **POST** /v1/cloud-idp/{id}/test-user | Get user test search


# **v1CloudIdpExportPost**
```swift
    open class func v1CloudIdpExportPost(exportFields: [String]? = nil, exportLabels: [String]? = nil, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, exportParameters: ExportParameters? = nil, completion: @escaping (_ data: AnyCodable?, _ error: Error?) -> Void)
```

Export Cloud Identity Providers collection 

Export Cloud Identity Providers collection 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let exportFields = ["inner_example"] // [String] | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields=id,username (optional)
let exportLabels = ["inner_example"] // [String] | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels=identifier,name with matching: export-fields=id,username (optional)
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:desc. Multiple sort criteria are supported and must be seperated with a comma. Example: sort=id:desc,name:asc (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name. This param can be combined with paging and sorting. Example: name==\"*department*\" (optional) (default to "")
let exportParameters = ExportParameters(page: 123, pageSize: 123, sort: ["sort_example"], filter: "filter_example", fields: [ExportField(fieldName: "fieldName_example", fieldLabelOverride: "fieldLabelOverride_example")]) // ExportParameters | Optional. Override query parameters since they can make URI exceed 2,000 character limit. (optional)

// Export Cloud Identity Providers collection 
CloudIdpAPI.v1CloudIdpExportPost(exportFields: exportFields, exportLabels: exportLabels, page: page, pageSize: pageSize, sort: sort, filter: filter, exportParameters: exportParameters) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exportFields** | [**[String]**](String.md) | Export fields parameter, used to change default order or ignore some of the response properties. Default is empty array, which means that all fields of the response entity will be serialized. Example: export-fields&#x3D;id,username | [optional] 
 **exportLabels** | [**[String]**](String.md) | Export labels parameter, used to customize fieldnames/columns in the exported file. Default is empty array, which means that response properties names will be used. Number of the provided labels must match the number of export-fields Example: export-labels&#x3D;identifier,name with matching: export-fields&#x3D;id,username | [optional] 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:desc. Multiple sort criteria are supported and must be seperated with a comma. Example: sort&#x3D;id:desc,name:asc | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, name. This param can be combined with paging and sorting. Example: name&#x3D;&#x3D;\&quot;*department*\&quot; | [optional] [default to &quot;&quot;]
 **exportParameters** | [**ExportParameters**](ExportParameters.md) | Optional. Override query parameters since they can make URI exceed 2,000 character limit. | [optional] 

### Return type

**AnyCodable**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/csv, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpGet**
```swift
    open class func v1CloudIdpGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, completion: @escaping (_ data: ConfigurationSearchResults?, _ error: Error?) -> Void)
```

Get information about all Cloud Identity Providers configurations.

Returns basic informations about all configured Cloud Identity Provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)

// Get information about all Cloud Identity Providers configurations.
CloudIdpAPI.v1CloudIdpGet(page: page, pageSize: pageSize, sort: sort) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 

### Return type

[**ConfigurationSearchResults**](ConfigurationSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpIdGet**
```swift
    open class func v1CloudIdpIdGet(id: String, completion: @escaping (_ data: CloudIdPCommon?, _ error: Error?) -> Void)
```

Get Cloud Identity Provider configuration with given ID.

Get Cloud Identity Provider configuration with given ID.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier

// Get Cloud Identity Provider configuration with given ID.
CloudIdpAPI.v1CloudIdpIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 

### Return type

[**CloudIdPCommon**](CloudIdPCommon.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpIdHistoryGet**
```swift
    open class func v1CloudIdpIdHistoryGet(id: String, page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: HistorySearchResults?, _ error: Error?) -> Void)
```

Get Cloud Identity Provider history

Gets specified Cloud Identity Provider object history

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort=date:desc,name:asc  (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter=username!=admin and details==*disabled* and date<2019-12-15 (optional) (default to "")

// Get Cloud Identity Provider history
CloudIdpAPI.v1CloudIdpIdHistoryGet(id: id, page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is date:desc. Multiple sort criteria are supported and must be separated with a comma. Example: sort&#x3D;date:desc,name:asc  | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter history notes collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: username, date, note, details. This param can be combined with paging and sorting. Example: filter&#x3D;username!&#x3D;admin and details&#x3D;&#x3D;*disabled* and date&lt;2019-12-15 | [optional] [default to &quot;&quot;]

### Return type

[**HistorySearchResults**](HistorySearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpIdHistoryPost**
```swift
    open class func v1CloudIdpIdHistoryPost(id: String, objectHistoryNote: ObjectHistoryNote, completion: @escaping (_ data: ObjectHistory?, _ error: Error?) -> Void)
```

Add Cloud Identity Provider history note

Adds specified Cloud Identity Provider object history notes 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let objectHistoryNote = ObjectHistoryNote(note: "note_example") // ObjectHistoryNote | history notes to create

// Add Cloud Identity Provider history note
CloudIdpAPI.v1CloudIdpIdHistoryPost(id: id, objectHistoryNote: objectHistoryNote) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **objectHistoryNote** | [**ObjectHistoryNote**](ObjectHistoryNote.md) | history notes to create | 

### Return type

[**ObjectHistory**](ObjectHistory.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpIdTestGroupPost**
```swift
    open class func v1CloudIdpIdTestGroupPost(id: String, groupTestSearchRequest: GroupTestSearchRequest, completion: @escaping (_ data: GroupTestSearchResponse?, _ error: Error?) -> Void)
```

Get group test search

Do test search to ensure about configuration and mappings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let groupTestSearchRequest = GroupTestSearchRequest(groupname: "groupname_example") // GroupTestSearchRequest | Search request

// Get group test search
CloudIdpAPI.v1CloudIdpIdTestGroupPost(id: id, groupTestSearchRequest: groupTestSearchRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **groupTestSearchRequest** | [**GroupTestSearchRequest**](GroupTestSearchRequest.md) | Search request | 

### Return type

[**GroupTestSearchResponse**](GroupTestSearchResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpIdTestUserMembershipPost**
```swift
    open class func v1CloudIdpIdTestUserMembershipPost(id: String, membershipTestSearchRequest: MembershipTestSearchRequest, completion: @escaping (_ data: MembershipTestSearchResponse?, _ error: Error?) -> Void)
```

Get membership test search

Do test search to ensure about configuration and mappings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let membershipTestSearchRequest = MembershipTestSearchRequest(username: "username_example", groupname: "groupname_example") // MembershipTestSearchRequest | Search request

// Get membership test search
CloudIdpAPI.v1CloudIdpIdTestUserMembershipPost(id: id, membershipTestSearchRequest: membershipTestSearchRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **membershipTestSearchRequest** | [**MembershipTestSearchRequest**](MembershipTestSearchRequest.md) | Search request | 

### Return type

[**MembershipTestSearchResponse**](MembershipTestSearchResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1CloudIdpIdTestUserPost**
```swift
    open class func v1CloudIdpIdTestUserPost(id: String, userTestSearchRequest: UserTestSearchRequest, completion: @escaping (_ data: UserTestSearchResponse?, _ error: Error?) -> Void)
```

Get user test search

Do test search to ensure about configuration and mappings

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | Cloud Identity Provider identifier
let userTestSearchRequest = UserTestSearchRequest(username: "username_example") // UserTestSearchRequest | Search request

// Get user test search
CloudIdpAPI.v1CloudIdpIdTestUserPost(id: id, userTestSearchRequest: userTestSearchRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | Cloud Identity Provider identifier | 
 **userTestSearchRequest** | [**UserTestSearchRequest**](UserTestSearchRequest.md) | Search request | 

### Return type

[**UserTestSearchResponse**](UserTestSearchResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

