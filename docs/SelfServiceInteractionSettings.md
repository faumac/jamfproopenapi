# SelfServiceInteractionSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notificationsEnabled** | **Bool** | global Self Service setting for if notifications are on or off  | [optional] [default to false]
**alertUserApprovedMdm** | **Bool** | whether users should be notified they need to approve organization&#39;s MDM profile  | [optional] [default to true]
**defaultLandingPage** | **String** | the default landing page in Self Service  | [optional] [default to .home]
**defaultHomeCategoryId** | **Int** | id for the default home category in Self Service  | [optional] [default to -1]
**bookmarksName** | **String** | renamed string for bookmarks if the admin wishes  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


