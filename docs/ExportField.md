# ExportField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fieldName** | **String** | English name of the field to be exported. | [optional] 
**fieldLabelOverride** | **String** | Name which should be used for the label in the response - can be in any language. When null the fieldName itself will be used as the label. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


