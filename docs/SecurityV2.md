# SecurityV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataProtected** | **Bool** |  | [optional] 
**blockLevelEncryptionCapable** | **Bool** |  | [optional] 
**fileLevelEncryptionCapable** | **Bool** |  | [optional] 
**passcodePresent** | **Bool** |  | [optional] 
**passcodeCompliant** | **Bool** |  | [optional] 
**passcodeCompliantWithProfile** | **Bool** |  | [optional] 
**hardwareEncryption** | **Int** |  | [optional] 
**activationLockEnabled** | **Bool** |  | [optional] 
**jailBreakDetected** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


