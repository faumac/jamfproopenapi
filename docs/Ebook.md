# Ebook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**kind** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**free** | **Bool** |  | [optional] 
**version** | **String** |  | [optional] 
**author** | **String** |  | [optional] 
**deployAsManaged** | **Bool** | If true, it will be automatically installed | [optional] 
**installAutomatically** | **Bool** |  | [optional] 
**categoryId** | **String** |  | [optional] 
**siteId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


