# VppTokenSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**enabled** | **Bool** |  | [optional] 
**recipients** | [**Recipients**](Recipients.md) |  | [optional] 
**adminAccounts** | [AdminAccount] |  | [optional] 
**siteID** | **Int** |  | [optional] 
**siteName** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


