# AzureMappings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  | 
**userName** | **String** |  | 
**realName** | **String** |  | 
**email** | **String** |  | 
**department** | **String** |  | 
**building** | **String** |  | 
**room** | **String** |  | 
**phone** | **String** |  | 
**position** | **String** |  | 
**groupId** | **String** |  | 
**groupName** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


