# ComputerGroupsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1ComputerGroupsGet**](ComputerGroupsAPI.md#v1computergroupsget) | **GET** /v1/computer-groups | Returns the list of all computer groups 


# **v1ComputerGroupsGet**
```swift
    open class func v1ComputerGroupsGet(completion: @escaping (_ data: [ComputerGroup]?, _ error: Error?) -> Void)
```

Returns the list of all computer groups 

Use it to get the list of all computer groups. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Returns the list of all computer groups 
ComputerGroupsAPI.v1ComputerGroupsGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[ComputerGroup]**](ComputerGroup.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

