# ApiIntegrationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorizationScopes** | **[String]** | API Role display names.  | 
**displayName** | **String** |  | 
**enabled** | **Bool** |  | [optional] 
**accessTokenLifetimeSeconds** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


