# StartupStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**step** | **String** |  | [optional] 
**stepCode** | **String** |  | [optional] 
**stepParam** | **String** |  | [optional] 
**percentage** | **Int** |  | [optional] 
**warning** | **String** |  | [optional] 
**warningCode** | **String** |  | [optional] 
**warningParam** | **String** |  | [optional] 
**error** | **String** |  | [optional] 
**errorCode** | **String** |  | [optional] 
**setupAssistantNecessary** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


