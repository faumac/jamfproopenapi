# DeviceEnrollmentInstanceSyncStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**syncState** | **String** |  | [optional] 
**instanceId** | **String** |  | [optional] 
**timestamp** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


