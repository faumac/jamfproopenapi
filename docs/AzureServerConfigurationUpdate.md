# AzureServerConfigurationUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**enabled** | **Bool** |  | 
**mappings** | [**AzureMappings**](AzureMappings.md) |  | 
**searchTimeout** | **Int** |  | 
**transitiveMembershipEnabled** | **Bool** | Use this field to enable transitive membership lookup with Single Sign On | 
**transitiveMembershipUserField** | **String** | Use this field to set user field mapping for transitive membership lookup with Single Sign On | 
**transitiveDirectoryMembershipEnabled** | **Bool** | Use this field to enable transitive membership lookup. This setting would not apply to Single Sign On | 
**membershipCalculationOptimizationEnabled** | **Bool** | Use this field to enable membership calculation optimization. This setting would not apply to Single Sign On | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


