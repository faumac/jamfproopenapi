# CloudLdapMappingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userMappings** | [**UserMappings**](UserMappings.md) |  | [optional] 
**groupMappings** | [**GroupMappings**](GroupMappings.md) |  | [optional] 
**membershipMappings** | [**MembershipMappings**](MembershipMappings.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


