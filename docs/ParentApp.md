# ParentApp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timezoneId** | **String** |  | 
**restrictedTimes** | [**ParentAppRestrictedTimes**](ParentAppRestrictedTimes.md) |  | 
**deviceGroupId** | **Int** |  | 
**isEnabled** | **Bool** |  | 
**allowTemplates** | **Bool** |  | [optional] 
**disassociateOnWipeAndReEnroll** | **Bool** |  | [optional] 
**allowClearPasscode** | **Bool** |  | [optional] 
**safelistedApps** | [SafelistedApp] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


