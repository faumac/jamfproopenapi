# LdapConfigurationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cloudIdPCommon** | [**CloudIdPCommonRequest**](CloudIdPCommonRequest.md) |  | 
**server** | [**CloudLdapServerRequest**](CloudLdapServerRequest.md) |  | 
**mappings** | [**CloudLdapMappingsRequest**](CloudLdapMappingsRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


