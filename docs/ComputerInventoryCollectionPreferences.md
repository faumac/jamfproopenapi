# ComputerInventoryCollectionPreferences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**monitorApplicationUsage** | **Bool** |  | [optional] [default to false]
**includeFonts** | **Bool** |  | [optional] [default to false]
**includePlugins** | **Bool** |  | [optional] [default to false]
**includePackages** | **Bool** |  | [optional] [default to false]
**includeSoftwareUpdates** | **Bool** |  | [optional] [default to false]
**includeAccounts** | **Bool** |  | [optional] [default to false]
**calculateSizes** | **Bool** |  | [optional] [default to false]
**includeHiddenAccounts** | **Bool** |  | [optional] [default to false]
**includePrinters** | **Bool** |  | [optional] [default to false]
**includeServices** | **Bool** |  | [optional] [default to false]
**collectSyncedMobileDeviceInfo** | **Bool** |  | [optional] [default to false]
**updateLdapInfoOnComputerInventorySubmissions** | **Bool** |  | [optional] [default to false]
**monitorBeacons** | **Bool** |  | [optional] [default to false]
**allowChangingUserAndLocation** | **Bool** |  | [optional] [default to true]
**useUnixUserPaths** | **Bool** |  | [optional] [default to true]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


