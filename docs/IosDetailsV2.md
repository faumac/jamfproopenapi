# IosDetailsV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **String** |  | [optional] 
**modelIdentifier** | **String** |  | [optional] 
**modelNumber** | **String** |  | [optional] 
**supervised** | **Bool** |  | [optional] 
**batteryLevel** | **Int** |  | [optional] 
**lastBackupTimestamp** | **Date** |  | [optional] 
**capacityMb** | **Int** |  | [optional] 
**availableMb** | **Int** |  | [optional] 
**percentageUsed** | **Int** |  | [optional] 
**shared** | **Bool** |  | [optional] 
**deviceLocatorServiceEnabled** | **Bool** |  | [optional] 
**doNotDisturbEnabled** | **Bool** |  | [optional] 
**cloudBackupEnabled** | **Bool** |  | [optional] 
**lastCloudBackupTimestamp** | **Date** |  | [optional] 
**locationServicesEnabled** | **Bool** |  | [optional] 
**iTunesStoreAccountActive** | **Bool** |  | [optional] 
**bleCapable** | **Bool** |  | [optional] 
**computer** | [**IdAndNameV2**](IdAndNameV2.md) |  | [optional] 
**purchasing** | [**PurchasingV2**](PurchasingV2.md) |  | [optional] 
**security** | [**SecurityV2**](SecurityV2.md) |  | [optional] 
**network** | [**NetworkV2**](NetworkV2.md) |  | [optional] 
**serviceSubscriptions** | [MobileDeviceServiceSubscriptions] |  | [optional] 
**applications** | [MobileDeviceApplication] |  | [optional] 
**certificates** | [MobileDeviceCertificateV2] |  | [optional] 
**ebooks** | [MobileDeviceEbook] |  | [optional] 
**configurationProfiles** | [ConfigurationProfile] |  | [optional] 
**provisioningProfiles** | [MobileDeviceProvisioningProfiles] |  | [optional] 
**attachments** | [MobileDeviceAttachmentV2] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


