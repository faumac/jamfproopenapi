# ApiIntegrationsAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteApiIntegration**](ApiIntegrationsAPI.md#deleteapiintegration) | **DELETE** /v1/api-integrations/{id} | Remove specified API integration
[**getOneApiIntegration**](ApiIntegrationsAPI.md#getoneapiintegration) | **GET** /v1/api-integrations/{id} | Get specified API integration object
[**postCreateApiIntegration**](ApiIntegrationsAPI.md#postcreateapiintegration) | **POST** /v1/api-integrations | Create API integration object
[**postCreateClientCredentials**](ApiIntegrationsAPI.md#postcreateclientcredentials) | **POST** /v1/api-integrations/{id}/client-credentials | Create client credentials for specified API integration
[**putUpdateApiIntegration**](ApiIntegrationsAPI.md#putupdateapiintegration) | **PUT** /v1/api-integrations/{id} | Update specified API integration object
[**v1ApiIntegrationsGet**](ApiIntegrationsAPI.md#v1apiintegrationsget) | **GET** /v1/api-integrations | Get the current API Integrations


# **deleteApiIntegration**
```swift
    open class func deleteApiIntegration(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified API integration

Removes specified API integration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of api integration object

// Remove specified API integration
ApiIntegrationsAPI.deleteApiIntegration(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of api integration object | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneApiIntegration**
```swift
    open class func getOneApiIntegration(id: String, completion: @escaping (_ data: ApiIntegrationResponse?, _ error: Error?) -> Void)
```

Get specified API integration object

Gets specified API integration object

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of api integration object

// Get specified API integration object
ApiIntegrationsAPI.getOneApiIntegration(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of api integration object | 

### Return type

[**ApiIntegrationResponse**](ApiIntegrationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postCreateApiIntegration**
```swift
    open class func postCreateApiIntegration(apiIntegrationRequest: ApiIntegrationRequest, completion: @escaping (_ data: ApiIntegrationResponse?, _ error: Error?) -> Void)
```

Create API integration object

Create API integration object

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let apiIntegrationRequest = ApiIntegrationRequest(authorizationScopes: ["authorizationScopes_example"], displayName: "displayName_example", enabled: true, accessTokenLifetimeSeconds: 123) // ApiIntegrationRequest | api integration object to create

// Create API integration object
ApiIntegrationsAPI.postCreateApiIntegration(apiIntegrationRequest: apiIntegrationRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiIntegrationRequest** | [**ApiIntegrationRequest**](ApiIntegrationRequest.md) | api integration object to create | 

### Return type

[**ApiIntegrationResponse**](ApiIntegrationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postCreateClientCredentials**
```swift
    open class func postCreateClientCredentials(id: String, completion: @escaping (_ data: OAuthClientCredentials?, _ error: Error?) -> Void)
```

Create client credentials for specified API integration

Create client credentials for specified API integration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of api integration object

// Create client credentials for specified API integration
ApiIntegrationsAPI.postCreateClientCredentials(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of api integration object | 

### Return type

[**OAuthClientCredentials**](OAuthClientCredentials.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **putUpdateApiIntegration**
```swift
    open class func putUpdateApiIntegration(id: String, apiIntegrationRequest: ApiIntegrationRequest, completion: @escaping (_ data: ApiIntegrationResponse?, _ error: Error?) -> Void)
```

Update specified API integration object

Update specified API integration object

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of api integration object
let apiIntegrationRequest = ApiIntegrationRequest(authorizationScopes: ["authorizationScopes_example"], displayName: "displayName_example", enabled: true, accessTokenLifetimeSeconds: 123) // ApiIntegrationRequest | api object to update

// Update specified API integration object
ApiIntegrationsAPI.putUpdateApiIntegration(id: id, apiIntegrationRequest: apiIntegrationRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of api integration object | 
 **apiIntegrationRequest** | [**ApiIntegrationRequest**](ApiIntegrationRequest.md) | api object to update | 

### Return type

[**ApiIntegrationResponse**](ApiIntegrationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1ApiIntegrationsGet**
```swift
    open class func v1ApiIntegrationsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: ApiIntegrationSearchResult?, _ error: Error?) -> Void)
```

Get the current API Integrations

Get Jamf|Pro API Integrations with Search Criteria

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Fields allowed in the query: id, displayName. Example: sort=displayName:desc (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter app titles collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, displayName. Example: displayName==\"*IntegrationName*\" (optional) (default to "")

// Get the current API Integrations
ApiIntegrationsAPI.v1ApiIntegrationsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Sorting criteria in the format: property:asc/desc. Default sort is id:asc. Multiple sort criteria are supported and must be separated with a comma. Fields allowed in the query: id, displayName. Example: sort&#x3D;displayName:desc | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter app titles collection. Default filter is empty query - returning all results for the requested page. Fields allowed in the query: id, displayName. Example: displayName&#x3D;&#x3D;\&quot;*IntegrationName*\&quot; | [optional] [default to &quot;&quot;]

### Return type

[**ApiIntegrationSearchResult**](ApiIntegrationSearchResult.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

