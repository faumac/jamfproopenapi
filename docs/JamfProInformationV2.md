# JamfProInformationV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vppTokenEnabled** | **Bool** |  | [optional] 
**depAccountEnabled** | **Bool** |  | [optional] 
**byodEnabled** | **Bool** |  | [optional] 
**userMigrationEnabled** | **Bool** |  | [optional] 
**cloudDeploymentsEnabled** | **Bool** |  | [optional] 
**patchEnabled** | **Bool** |  | [optional] 
**ssoSamlEnabled** | **Bool** |  | [optional] 
**smtpEnabled** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


