# VenafiPkiPayloadRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**urlPath** | **String** |  | [optional] 
**name** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


