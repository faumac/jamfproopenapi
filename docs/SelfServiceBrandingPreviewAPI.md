# SelfServiceBrandingPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**selfServiceBrandingImagesPost**](SelfServiceBrandingPreviewAPI.md#selfservicebrandingimagespost) | **POST** /self-service/branding/images | Upload an image 


# **selfServiceBrandingImagesPost**
```swift
    open class func selfServiceBrandingImagesPost(file: URL, completion: @escaping (_ data: BrandingImageUrl?, _ error: Error?) -> Void)
```

Upload an image 

Uploads an image

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let file = URL(string: "https://example.com")! // URL | The file to upload

// Upload an image 
SelfServiceBrandingPreviewAPI.selfServiceBrandingImagesPost(file: file) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **URL** | The file to upload | 

### Return type

[**BrandingImageUrl**](BrandingImageUrl.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

