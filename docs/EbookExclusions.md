# EbookExclusions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**computerIds** | **[String]** |  | [optional] 
**computerGroupIds** | **[String]** |  | [optional] 
**mobileDeviceIds** | **[String]** |  | [optional] 
**mobileDeviceGroupIds** | **[String]** |  | [optional] 
**buildingIds** | **[String]** |  | [optional] 
**departmentIds** | **[String]** |  | [optional] 
**userIds** | **[String]** |  | [optional] 
**userGroupIds** | **[String]** |  | [optional] 
**limitations** | [**EbookLimitations**](EbookLimitations.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


