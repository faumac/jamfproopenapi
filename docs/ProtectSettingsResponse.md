# ProtectSettingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**apiClientId** | **String** |  | [optional] 
**apiClientName** | **String** | display name used when creating the API Client in the Jamf Protect web console | [optional] 
**registrationId** | **String** | ID used when making requests to identify this particular Protect registration. | [optional] 
**protectUrl** | **String** |  | [optional] 
**lastSyncTime** | **String** |  | [optional] 
**syncStatus** | **String** |  | [optional] 
**autoInstall** | **Bool** | determines whether the Jamf Protect agent will be automatically installed on client computers | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


