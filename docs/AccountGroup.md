# AccountGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessLevel** | **String** |  | [optional] 
**privilegeSet** | **String** |  | [optional] 
**siteId** | **Int** |  | [optional] 
**privileges** | **[String]** |  | [optional] 
**memberUserIds** | **[Int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


