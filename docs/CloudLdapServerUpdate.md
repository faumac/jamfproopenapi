# CloudLdapServerUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serverUrl** | **String** |  | 
**enabled** | **Bool** |  | 
**domainName** | **String** |  | 
**port** | **Int** |  | 
**keystore** | [**CloudLdapKeystoreFile**](CloudLdapKeystoreFile.md) |  | [optional] 
**connectionTimeout** | **Int** |  | 
**searchTimeout** | **Int** |  | 
**useWildcards** | **Bool** |  | 
**connectionType** | **String** |  | 
**membershipCalculationOptimizationEnabled** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


