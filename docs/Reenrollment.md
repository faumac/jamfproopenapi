# Reenrollment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isFlushPolicyHistoryEnabled** | **Bool** |  | [optional] [default to false]
**isFlushLocationInformationEnabled** | **Bool** |  | [optional] [default to false]
**isFlushLocationInformationHistoryEnabled** | **Bool** |  | [optional] [default to false]
**isFlushExtensionAttributesEnabled** | **Bool** |  | [optional] [default to false]
**flushMDMQueue** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


