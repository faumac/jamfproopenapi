# JamfPackageAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1JamfPackageGet**](JamfPackageAPI.md#v1jamfpackageget) | **GET** /v1/jamf-package | Get the packages for a given Jamf application 
[**v2JamfPackageGet**](JamfPackageAPI.md#v2jamfpackageget) | **GET** /v2/jamf-package | Get the packages for a given Jamf application 


# **v1JamfPackageGet**
```swift
    open class func v1JamfPackageGet(application: String, completion: @escaping (_ data: [JamfPackageResponse]?, _ error: Error?) -> Void)
```

Get the packages for a given Jamf application 

Get the packages for a given Jamf application.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let application = "application_example" // String | The Jamf Application key. The only supported values are protect and connect.

// Get the packages for a given Jamf application 
JamfPackageAPI.v1JamfPackageGet(application: application) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **application** | **String** | The Jamf Application key. The only supported values are protect and connect. | 

### Return type

[**[JamfPackageResponse]**](JamfPackageResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2JamfPackageGet**
```swift
    open class func v2JamfPackageGet(application: String, completion: @escaping (_ data: JamfApplicationResponse?, _ error: Error?) -> Void)
```

Get the packages for a given Jamf application 

Get the packages for a given Jamf application.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let application = "application_example" // String | The Jamf Application key. The only supported values are protect and connect.

// Get the packages for a given Jamf application 
JamfPackageAPI.v2JamfPackageGet(application: application) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **application** | **String** | The Jamf Application key. The only supported values are protect and connect. | 

### Return type

[**JamfApplicationResponse**](JamfApplicationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

