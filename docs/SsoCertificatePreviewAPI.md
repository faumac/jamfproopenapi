# SsoCertificatePreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1SsoCertDelete**](SsoCertificatePreviewAPI.md#v1ssocertdelete) | **DELETE** /v1/sso/cert | Delete the currently configured certificate used by SSO 
[**v1SsoCertDownloadGet**](SsoCertificatePreviewAPI.md#v1ssocertdownloadget) | **GET** /v1/sso/cert/download | Download the certificate currently configured for use with Jamf Pro&#39;s SSO configuration 
[**v1SsoCertGet**](SsoCertificatePreviewAPI.md#v1ssocertget) | **GET** /v1/sso/cert | Retrieve the certificate currently configured for use with SSO 
[**v1SsoCertParsePost**](SsoCertificatePreviewAPI.md#v1ssocertparsepost) | **POST** /v1/sso/cert/parse | Parse the certificate to get details about certificate type and keys needed to upload certificate file 
[**v1SsoCertPost**](SsoCertificatePreviewAPI.md#v1ssocertpost) | **POST** /v1/sso/cert | Jamf Pro will generate a new certificate and use it to sign SSO 
[**v1SsoCertPut**](SsoCertificatePreviewAPI.md#v1ssocertput) | **PUT** /v1/sso/cert | Update the certificate used by Jamf Pro to sign SSO requests to the identify provider 


# **v1SsoCertDelete**
```swift
    open class func v1SsoCertDelete(completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete the currently configured certificate used by SSO 

Deletes the currently configured certificate used by SSO.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Delete the currently configured certificate used by SSO 
SsoCertificatePreviewAPI.v1SsoCertDelete() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoCertDownloadGet**
```swift
    open class func v1SsoCertDownloadGet(completion: @escaping (_ data: URL?, _ error: Error?) -> Void)
```

Download the certificate currently configured for use with Jamf Pro's SSO configuration 

Downloads the certificate currently configured for use with Jamf Pro's SSO configuration

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Download the certificate currently configured for use with Jamf Pro's SSO configuration 
SsoCertificatePreviewAPI.v1SsoCertDownloadGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**URL**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoCertGet**
```swift
    open class func v1SsoCertGet(completion: @escaping (_ data: SsoKeystoreWithDetails?, _ error: Error?) -> Void)
```

Retrieve the certificate currently configured for use with SSO 

Retrieves the certificate currently configured for use with SSO.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Retrieve the certificate currently configured for use with SSO 
SsoCertificatePreviewAPI.v1SsoCertGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoKeystoreWithDetails**](SsoKeystoreWithDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoCertParsePost**
```swift
    open class func v1SsoCertParsePost(ssoKeystoreParse: SsoKeystoreParse, completion: @escaping (_ data: SsoKeystoreCertParseResponse?, _ error: Error?) -> Void)
```

Parse the certificate to get details about certificate type and keys needed to upload certificate file 

Parse the certificate to get details about certificate type and keys needed to upload certificate file.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ssoKeystoreParse = SsoKeystoreParse(keystorePassword: "keystorePassword_example", keystoreFile: 123, keystoreFileName: "keystoreFileName_example") // SsoKeystoreParse | 

// Parse the certificate to get details about certificate type and keys needed to upload certificate file 
SsoCertificatePreviewAPI.v1SsoCertParsePost(ssoKeystoreParse: ssoKeystoreParse) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssoKeystoreParse** | [**SsoKeystoreParse**](SsoKeystoreParse.md) |  | 

### Return type

[**SsoKeystoreCertParseResponse**](SsoKeystoreCertParseResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoCertPost**
```swift
    open class func v1SsoCertPost(completion: @escaping (_ data: SsoKeystoreWithDetails?, _ error: Error?) -> Void)
```

Jamf Pro will generate a new certificate and use it to sign SSO 

Jamf Pro will generate a new certificate and use it to sign SSO requests to the identity provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Jamf Pro will generate a new certificate and use it to sign SSO 
SsoCertificatePreviewAPI.v1SsoCertPost() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SsoKeystoreWithDetails**](SsoKeystoreWithDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SsoCertPut**
```swift
    open class func v1SsoCertPut(ssoKeystore: SsoKeystore, completion: @escaping (_ data: SsoKeystoreWithDetails?, _ error: Error?) -> Void)
```

Update the certificate used by Jamf Pro to sign SSO requests to the identify provider 

Update the certificate used by Jamf Pro to sign SSO requests to the identify provider.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let ssoKeystore = SsoKeystore(keys: [CertificateKey(id: "id_example", valid: true)], key: "key_example", password: "password_example", type: "type_example", keystoreSetupType: "keystoreSetupType_example") // SsoKeystore | 

// Update the certificate used by Jamf Pro to sign SSO requests to the identify provider 
SsoCertificatePreviewAPI.v1SsoCertPut(ssoKeystore: ssoKeystore) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ssoKeystore** | [**SsoKeystore**](SsoKeystore.md) |  | 

### Return type

[**SsoKeystoreWithDetails**](SsoKeystoreWithDetails.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

