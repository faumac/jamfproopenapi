# ComputerLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**realname** | **String** | | [optional] 
**real_name** | **String** | | [optional]
**email_address** | **String** | | [optional]
**position** | **String** | | [optional]
**phone** | **String** | | [optional]
**phone_number** | **String** | | [optional]
**department** | **String** | | [optional]
**building** | **String** | | [optional]
**room** | **String** | | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


