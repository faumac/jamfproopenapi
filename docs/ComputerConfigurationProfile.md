# ComputerConfigurationProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**username** | **String** |  | [optional] [readonly] 
**lastInstalled** | **Date** |  | [optional] 
**removable** | **Bool** |  | [optional] [readonly] 
**displayName** | **String** |  | [optional] [readonly] 
**profileIdentifier** | **String** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


