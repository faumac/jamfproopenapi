# TimeZonesPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1TimeZonesGet**](TimeZonesPreviewAPI.md#v1timezonesget) | **GET** /v1/time-zones | Return information about the currently supported Time Zones 


# **v1TimeZonesGet**
```swift
    open class func v1TimeZonesGet(completion: @escaping (_ data: [TimeZone]?, _ error: Error?) -> Void)
```

Return information about the currently supported Time Zones 

Returns information about the currently supported time zones 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Return information about the currently supported Time Zones 
TimeZonesPreviewAPI.v1TimeZonesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[TimeZone]**](TimeZone.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

