# Network

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cellularTechnology** | **String** |  | [optional] 
**isVoiceRoamingEnabled** | **Bool** |  | [optional] 
**imei** | **String** |  | [optional] 
**iccid** | **String** |  | [optional] 
**meid** | **String** |  | [optional] 
**carrierSettingsVersion** | **String** |  | [optional] 
**currentCarrierNetwork** | **String** |  | [optional] 
**currentMobileCountryCode** | **String** |  | [optional] 
**currentMobileNetworkCode** | **String** |  | [optional] 
**homeCarrierNetwork** | **String** |  | [optional] 
**homeMobileCountryCode** | **String** |  | [optional] 
**homeMobileNetworkCode** | **String** |  | [optional] 
**isDataRoamingEnabled** | **Bool** |  | [optional] 
**isRoaming** | **Bool** |  | [optional] 
**isPersonalHotspotEnabled** | **Bool** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


