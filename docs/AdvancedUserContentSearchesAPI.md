# AdvancedUserContentSearchesAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1AdvancedUserContentSearchesGet**](AdvancedUserContentSearchesAPI.md#v1advancedusercontentsearchesget) | **GET** /v1/advanced-user-content-searches | Get All Advanced User Content Search objects 
[**v1AdvancedUserContentSearchesIdDelete**](AdvancedUserContentSearchesAPI.md#v1advancedusercontentsearchesiddelete) | **DELETE** /v1/advanced-user-content-searches/{id} | Remove specified Advanced User Content Search object 
[**v1AdvancedUserContentSearchesIdGet**](AdvancedUserContentSearchesAPI.md#v1advancedusercontentsearchesidget) | **GET** /v1/advanced-user-content-searches/{id} | Get Specified Advanced User Content Search object 
[**v1AdvancedUserContentSearchesIdPut**](AdvancedUserContentSearchesAPI.md#v1advancedusercontentsearchesidput) | **PUT** /v1/advanced-user-content-searches/{id} | Get Specified Advanced User Content Search object 
[**v1AdvancedUserContentSearchesPost**](AdvancedUserContentSearchesAPI.md#v1advancedusercontentsearchespost) | **POST** /v1/advanced-user-content-searches | Create Advanced User Content Search object 


# **v1AdvancedUserContentSearchesGet**
```swift
    open class func v1AdvancedUserContentSearchesGet(completion: @escaping (_ data: AdvancedUserContentSearchSearchResults?, _ error: Error?) -> Void)
```

Get All Advanced User Content Search objects 

Get All Advanced User Content Search Objects 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient


// Get All Advanced User Content Search objects 
AdvancedUserContentSearchesAPI.v1AdvancedUserContentSearchesGet() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AdvancedUserContentSearchSearchResults**](AdvancedUserContentSearchSearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedUserContentSearchesIdDelete**
```swift
    open class func v1AdvancedUserContentSearchesIdDelete(id: String, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Remove specified Advanced User Content Search object 

Removes specified Advanced User Content Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | instance id of Advanced User Content Search record

// Remove specified Advanced User Content Search object 
AdvancedUserContentSearchesAPI.v1AdvancedUserContentSearchesIdDelete(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | instance id of Advanced User Content Search record | 

### Return type

Void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedUserContentSearchesIdGet**
```swift
    open class func v1AdvancedUserContentSearchesIdGet(id: String, completion: @escaping (_ data: AdvancedUserContentSearch?, _ error: Error?) -> Void)
```

Get Specified Advanced User Content Search object 

Gets Specified Advanced User Content Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of target Advanced User Content Search

// Get Specified Advanced User Content Search object 
AdvancedUserContentSearchesAPI.v1AdvancedUserContentSearchesIdGet(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of target Advanced User Content Search | 

### Return type

[**AdvancedUserContentSearch**](AdvancedUserContentSearch.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedUserContentSearchesIdPut**
```swift
    open class func v1AdvancedUserContentSearchesIdPut(id: String, advancedUserContentSearch: AdvancedUserContentSearch, completion: @escaping (_ data: AdvancedUserContentSearch?, _ error: Error?) -> Void)
```

Get Specified Advanced User Content Search object 

Gets Specified Advanced User Content Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = "id_example" // String | id of target Advanced User Content Search
let advancedUserContentSearch = AdvancedUserContentSearch(id: "id_example", name: "name_example", criteria: [SmartSearchCriterion(name: "name_example", priority: 123, andOr: "andOr_example", searchType: "searchType_example", value: "value_example", openingParen: false, closingParen: false)], displayFields: ["displayFields_example"], siteId: "siteId_example") // AdvancedUserContentSearch | 

// Get Specified Advanced User Content Search object 
AdvancedUserContentSearchesAPI.v1AdvancedUserContentSearchesIdPut(id: id, advancedUserContentSearch: advancedUserContentSearch) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String** | id of target Advanced User Content Search | 
 **advancedUserContentSearch** | [**AdvancedUserContentSearch**](AdvancedUserContentSearch.md) |  | 

### Return type

[**AdvancedUserContentSearch**](AdvancedUserContentSearch.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1AdvancedUserContentSearchesPost**
```swift
    open class func v1AdvancedUserContentSearchesPost(advancedUserContentSearch: AdvancedUserContentSearch, completion: @escaping (_ data: HrefResponse?, _ error: Error?) -> Void)
```

Create Advanced User Content Search object 

Creates Advanced User Content Search Object 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let advancedUserContentSearch = AdvancedUserContentSearch(id: "id_example", name: "name_example", criteria: [SmartSearchCriterion(name: "name_example", priority: 123, andOr: "andOr_example", searchType: "searchType_example", value: "value_example", openingParen: false, closingParen: false)], displayFields: ["displayFields_example"], siteId: "siteId_example") // AdvancedUserContentSearch | 

// Create Advanced User Content Search object 
AdvancedUserContentSearchesAPI.v1AdvancedUserContentSearchesPost(advancedUserContentSearch: advancedUserContentSearch) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **advancedUserContentSearch** | [**AdvancedUserContentSearch**](AdvancedUserContentSearch.md) |  | 

### Return type

[**HrefResponse**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

