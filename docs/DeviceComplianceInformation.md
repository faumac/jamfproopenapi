# DeviceComplianceInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | **String** | ID of the device | [optional] 
**applicable** | **Bool** | If device is applicable for compliance calculation | [optional] 
**complianceState** | **String** | Device compliance state. Possible values are: * &#x60;UNKNOWN&#x60; for unknow compliance state, this usually means that the compliance state is being calculated, * &#x60;NON_COMPLIANT&#x60; for non compliant state, * &#x60;COMPLIANT&#x60; for compliant state  | [optional] 
**complianceVendor** | **String** | Name of the compliance vendor | [optional] 
**complianceVendorDeviceInformation** | [**ComplianceVendorDeviceInformation**](ComplianceVendorDeviceInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


