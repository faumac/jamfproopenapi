# PutMobileDevicePrestage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**isMandatory** | **Bool** |  | 
**isMdmRemovable** | **Bool** |  | 
**supportPhoneNumber** | **String** |  | 
**supportEmailAddress** | **String** |  | 
**department** | **String** |  | 
**isDefaultPrestage** | **Bool** |  | 
**enrollmentSiteId** | **Int** |  | 
**isKeepExistingSiteMembership** | **Bool** |  | 
**isKeepExistingLocationInformation** | **Bool** |  | 
**isRequireAuthentication** | **Bool** |  | 
**authenticationPrompt** | **String** |  | 
**isPreventActivationLock** | **Bool** |  | 
**isEnableDeviceBasedActivationLock** | **Bool** |  | 
**deviceEnrollmentProgramInstanceId** | **Int** |  | 
**skipSetupItems** | **[String: Bool]** |  | [optional] 
**locationInformation** | [**LocationInformation**](LocationInformation.md) |  | 
**purchasingInformation** | [**PrestagePurchasingInformation**](PrestagePurchasingInformation.md) |  | 
**anchorCertificates** | **[String]** | The Base64 encoded PEM Certificate | [optional] 
**enrollmentCustomizationId** | **Int** |  | [optional] 
**isAllowPairing** | **Bool** |  | 
**isMultiUser** | **Bool** |  | 
**isSupervised** | **Bool** |  | 
**maximumSharedAccounts** | **Int** |  | 
**isAutoAdvanceSetup** | **Bool** |  | 
**isConfigureDeviceBeforeSetupAssistant** | **Bool** |  | 
**language** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**names** | [**MobileDevicePrestageNames**](MobileDevicePrestageNames.md) |  | [optional] 
**versionLock** | **Int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


