# UpdateTvOs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**airplayPassword** | **String** |  | [optional] 
**purchasing** | [**PurchasingV2**](PurchasingV2.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


