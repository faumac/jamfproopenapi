# SmartMobileDeviceGroupsPreviewAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**v1MobileDevicesIdRecalculateSmartGroupsPost**](SmartMobileDeviceGroupsPreviewAPI.md#v1mobiledevicesidrecalculatesmartgroupspost) | **POST** /v1/mobile-devices/{id}/recalculate-smart-groups | Recalculate all smart groups for the given device id and then return count of smart groups that device fall into 
[**v1SmartMobileDeviceGroupsIdRecalculatePost**](SmartMobileDeviceGroupsPreviewAPI.md#v1smartmobiledevicegroupsidrecalculatepost) | **POST** /v1/smart-mobile-device-groups/{id}/recalculate | Recalculate a smart group for the given id then return the ids for the devices in the smart group 


# **v1MobileDevicesIdRecalculateSmartGroupsPost**
```swift
    open class func v1MobileDevicesIdRecalculateSmartGroupsPost(id: Int, completion: @escaping (_ data: RecalculationResults?, _ error: Error?) -> Void)
```

Recalculate all smart groups for the given device id and then return count of smart groups that device fall into 

Recalculates all smart groups for the given device id and then returns the count of smart groups the device falls into 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | id of mobile device

// Recalculate all smart groups for the given device id and then return count of smart groups that device fall into 
SmartMobileDeviceGroupsPreviewAPI.v1MobileDevicesIdRecalculateSmartGroupsPost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | id of mobile device | 

### Return type

[**RecalculationResults**](RecalculationResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1SmartMobileDeviceGroupsIdRecalculatePost**
```swift
    open class func v1SmartMobileDeviceGroupsIdRecalculatePost(id: Int, completion: @escaping (_ data: RecalculationResults?, _ error: Error?) -> Void)
```

Recalculate a smart group for the given id then return the ids for the devices in the smart group 

recalculates a smart group for the given id and then returns the ids for the devices in the smart group 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | instance id of smart group

// Recalculate a smart group for the given id then return the ids for the devices in the smart group 
SmartMobileDeviceGroupsPreviewAPI.v1SmartMobileDeviceGroupsIdRecalculatePost(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** | instance id of smart group | 

### Return type

[**RecalculationResults**](RecalculationResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

