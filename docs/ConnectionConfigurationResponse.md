# ConnectionConfigurationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | An identifier of connection configuration for Team Viewer Remote Administration | 
**siteId** | **String** | An identifier of a site which Team Viewer Remote Administration is configured on | 
**displayName** | **String** | Name for Team Viewer Connection Configuration | 
**enabled** | **Bool** | Describes if Team Viewer connection is enabled or disabled | 
**sessionTimeout** | **Int** | Number of minutes before the session expires | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


