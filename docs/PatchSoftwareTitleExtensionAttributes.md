# PatchSoftwareTitleExtensionAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eaId** | **String** |  | [optional] 
**accepted** | **Bool** |  | [optional] 
**displayName** | **String** |  | [optional] 
**scriptContents** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


