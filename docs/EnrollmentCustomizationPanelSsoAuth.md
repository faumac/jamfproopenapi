# EnrollmentCustomizationPanelSsoAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **String** |  | 
**rank** | **Int** |  | 
**isUseJamfConnect** | **Bool** |  | 
**longNameAttribute** | **String** |  | 
**shortNameAttribute** | **String** |  | 
**isGroupEnrollmentAccessEnabled** | **Bool** |  | 
**groupEnrollmentAccessName** | **String** |  | [default to ""]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


