# MobileDeviceDetailsV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [readonly] 
**name** | **String** | Mobile device name. | [optional] 
**enforceName** | **Bool** | Enforce the mobile device name. Device must be supervised. If set to true, Jamf Pro will revert the Mobile Device Name to the ‘name’ value each time the device checks in. | [optional] 
**assetTag** | **String** |  | [optional] 
**lastInventoryUpdateTimestamp** | **Date** |  | [optional] 
**osVersion** | **String** |  | [optional] 
**osBuild** | **String** |  | [optional] 
**osSupplementalBuildVersion** | **String** | Collected for iOS 16 and iPadOS 16.1 or later | [optional] 
**osRapidSecurityResponse** | **String** | Collected for iOS 16 and iPadOS 16.1 or later | [optional] 
**softwareUpdateDeviceId** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**udid** | **String** |  | [optional] 
**ipAddress** | **String** |  | [optional] 
**wifiMacAddress** | **String** |  | [optional] 
**bluetoothMacAddress** | **String** |  | [optional] 
**managed** | **Bool** |  | [optional] 
**timeZone** | **String** |  | [optional] 
**initialEntryTimestamp** | **Date** |  | [optional] 
**lastEnrollmentTimestamp** | **Date** |  | [optional] 
**mdmProfileExpirationTimestamp** | **Date** |  | [optional] 
**deviceOwnershipLevel** | **String** |  | [optional] 
**enrollmentMethod** | **String** |  | [optional] 
**enrollmentSessionTokenValid** | **Bool** |  | [optional] 
**declarativeDeviceManagementEnabled** | **Bool** |  | [optional] 
**site** | [**V1Site**](V1Site.md) |  | [optional] 
**extensionAttributes** | [ExtensionAttributeV2] |  | [optional] 
**location** | [**LocationV2**](LocationV2.md) |  | [optional] 
**type** | **String** | Based on the value of this either ios, appleTv, android objects will be populated. | [optional] 
**ios** | [**IosDetailsV2**](IosDetailsV2.md) |  | [optional] 
**tvos** | [**TvOsDetails**](TvOsDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


