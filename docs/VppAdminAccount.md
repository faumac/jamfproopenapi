# VppAdminAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**licenseCount** | **Int** |  | [optional] 
**usedLicenseCount** | **Int** |  | [optional] 
**location** | **String** |  | [optional] 
**expirationDate** | **String** |  | [optional] 
**site** | [**Site**](Site.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


