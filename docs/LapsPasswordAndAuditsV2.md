# LapsPasswordAndAuditsV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** |  | [optional] [readonly] 
**dateLastSeen** | **Date** |  | [optional] 
**expirationTime** | **Date** |  | [optional] 
**audits** | [LapsAuditV2] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


