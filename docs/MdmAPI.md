# MdmAPI

All URIs are relative to */api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**previewMdmCommandsPost**](MdmAPI.md#previewmdmcommandspost) | **POST** /preview/mdm/commands | Post a command for creation and queuing 
[**v1DeployPackagePost**](MdmAPI.md#v1deploypackagepost) | **POST** /v1/deploy-package | Deploy packages using MDM
[**v1MdmCommandsGet**](MdmAPI.md#v1mdmcommandsget) | **GET** /v1/mdm/commands | Get information about mdm commands made by Jamf Pro.
[**v1MdmRenewProfilePost**](MdmAPI.md#v1mdmrenewprofilepost) | **POST** /v1/mdm/renew-profile | Renew MDM Profile 
[**v2MdmCommandsGet**](MdmAPI.md#v2mdmcommandsget) | **GET** /v2/mdm/commands | Get information about mdm commands made by Jamf Pro. 


# **previewMdmCommandsPost**
```swift
    open class func previewMdmCommandsPost(mdmCommandRequest: MdmCommandRequest? = nil, completion: @escaping (_ data: [HrefResponse]?, _ error: Error?) -> Void)
```

Post a command for creation and queuing 

Provided an MDM command type and appropriate information, will create and then queue said command.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let mdmCommandRequest = MdmCommandRequest(clientData: [MdmCommandClientRequest(managementId: "managementId_example")], commandData: MdmCommandRequest_commandData(commandType: MdmCommandType(), lostModeMessage: "lostModeMessage_example", lostModePhone: "lostModePhone_example", lostModeFootnote: "lostModeFootnote_example", preserveDataPlan: true, disallowProximitySetup: true, pin: "pin_example", obliterationBehavior: "obliterationBehavior_example", returnToService: EraseDeviceCommand_returnToService(enabled: true, mdmProfileData: "mdmProfileData_example", wifiProfileData: "wifiProfileData_example"), userName: "userName_example", forceDeletion: true, deleteAllUsers: true, bootstrapTokenAllowed: true, bluetooth: true, appAnalytics: AppAnalyticsSetting(), diagnosticSubmission: DiagnosticSubmissionSetting(), dataRoaming: DataRoamingSetting(), voiceRoaming: VoiceRoamingSetting(), personalHotspot: PersonalHotspotSetting(), maximumResidentUsers: 123, deviceName: "deviceName_example", applicationAttributes: ApplicationAttributes(identifier: "identifier_example", attributes: Attributes(vpnUuid: "vpnUuid_example", associatedDomains: ["associatedDomains_example"], removable: true, enableDirectDownloads: true)), sharedDeviceConfiguration: SharedDeviceConfiguration(quotaSize: 123, residentUsers: 123), applicationConfiguration: ApplicationConfiguration(configuration: "configuration_example", identifier: "identifier_example"), timeZone: "timeZone_example", softwareUpdateSettings: SoftwareUpdateSettings(recommendationCadence: "recommendationCadence_example"), passcodeLockGracePeriod: 123, rebuildKernelCache: true, kextPaths: ["kextPaths_example"], notifyUser: true, newPassword: "newPassword_example", data: "data_example", guid: "guid_example", password: "password_example")) // MdmCommandRequest | The mdm command object to create and queue (optional)

// Post a command for creation and queuing 
MdmAPI.previewMdmCommandsPost(mdmCommandRequest: mdmCommandRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdmCommandRequest** | [**MdmCommandRequest**](MdmCommandRequest.md) | The mdm command object to create and queue | [optional] 

### Return type

[**[HrefResponse]**](HrefResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1DeployPackagePost**
```swift
    open class func v1DeployPackagePost(installPackage: InstallPackage, verbose: Bool? = nil, completion: @escaping (_ data: VerbosePackageDeploymentResponse?, _ error: Error?) -> Void)
```

Deploy packages using MDM

Deploys packages to macOS devices using the InstallEnterpriseApplication MDM command.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let installPackage = InstallPackage(manifest: PackageManifest(url: "url_example", hash: "hash_example", hashType: "hashType_example", displayImageUrl: "displayImageUrl_example", fullSizeImageUrl: "fullSizeImageUrl_example", bundleId: "bundleId_example", bundleVersion: "bundleVersion_example", subtitle: "subtitle_example", title: "title_example", sizeInBytes: 123), installAsManaged: false, devices: [123], groupId: "groupId_example") // InstallPackage | 
let verbose = true // Bool | Enables the 'verbose' response, which includes information about the commands queued as well as information about commands that failed to queue. (optional) (default to false)

// Deploy packages using MDM
MdmAPI.v1DeployPackagePost(installPackage: installPackage, verbose: verbose) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **installPackage** | [**InstallPackage**](InstallPackage.md) |  | 
 **verbose** | **Bool** | Enables the &#39;verbose&#39; response, which includes information about the commands queued as well as information about commands that failed to queue. | [optional] [default to false]

### Return type

[**VerbosePackageDeploymentResponse**](VerbosePackageDeploymentResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MdmCommandsGet**
```swift
    open class func v1MdmCommandsGet(uuids: [String]? = nil, clientManagementId: String? = nil, completion: @escaping (_ data: [MdmCommand]?, _ error: Error?) -> Void)
```

Get information about mdm commands made by Jamf Pro.

Get information about mdm commands made by Jamf Pro.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let uuids = ["inner_example"] // [String] | A list of the UUIDs of the commands being searched for.  Limited to 40 UUIDs in length. Choose one of two parameters, but not both. (optional)
let clientManagementId = "clientManagementId_example" // String | The client management id used to search for a list of commands. Choose one of two parameters, but not both. (optional)

// Get information about mdm commands made by Jamf Pro.
MdmAPI.v1MdmCommandsGet(uuids: uuids, clientManagementId: clientManagementId) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuids** | [**[String]**](String.md) | A list of the UUIDs of the commands being searched for.  Limited to 40 UUIDs in length. Choose one of two parameters, but not both. | [optional] 
 **clientManagementId** | **String** | The client management id used to search for a list of commands. Choose one of two parameters, but not both. | [optional] 

### Return type

[**[MdmCommand]**](MdmCommand.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1MdmRenewProfilePost**
```swift
    open class func v1MdmRenewProfilePost(udids: Udids, completion: @escaping (_ data: RenewMdmProfileResponse?, _ error: Error?) -> Void)
```

Renew MDM Profile 

Renews the device's MDM Profile, including the device identity certificate within the MDM Profile. 

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let udids = Udids(udids: ["udids_example"]) // Udids | List of devices' UDIDs to perform MDM profile renewal

// Renew MDM Profile 
MdmAPI.v1MdmRenewProfilePost(udids: udids) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **udids** | [**Udids**](Udids.md) | List of devices&#39; UDIDs to perform MDM profile renewal | 

### Return type

[**RenewMdmProfileResponse**](RenewMdmProfileResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v2MdmCommandsGet**
```swift
    open class func v2MdmCommandsGet(page: Int? = nil, pageSize: Int? = nil, sort: [String]? = nil, filter: String? = nil, completion: @escaping (_ data: MdmCommandResults?, _ error: Error?) -> Void)
```

Get information about mdm commands made by Jamf Pro. 

Get information about mdm commands made by Jamf Pro.

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let page = 987 // Int |  (optional) (default to 0)
let pageSize = 987 // Int |  (optional) (default to 100)
let sort = ["inner_example"] // [String] | Default sort is dateSent:asc. Multiple sort criteria are supported and must be separated with a comma. (optional)
let filter = "filter_example" // String | Query in the RSQL format, allowing to filter, for a list of commands. All url must contain minimum one filter field. Fields allowed in the query: uuid, clientManagementId, command, status, clientType, dateSent, validAfter, dateCompleted, profileIdentifier, and active. This param can be combined with paging. Please note that any date filters must be used with gt, lt, ge, le Example: clientManagementId==fb511aae-c557-474f-a9c1-5dc845b90d0f;status==Pending;command==INSTALL_PROFILE;uuid==9e18f849-e689-4f2d-b616-a99d3da7db42;clientType==COMPUTER_USER;profileIdentifier==18cc61c2-01fc-11ed-b939-0242ac120002;dateCompleted=ge=2021-08-04T14:25:18.26Z;dateCompleted=le=2021-08-04T14:25:18.26Z;validAfter=ge=2021-08-05T14:25:18.26Z;active==true (optional) (default to "")

// Get information about mdm commands made by Jamf Pro. 
MdmAPI.v2MdmCommandsGet(page: page, pageSize: pageSize, sort: sort, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Int** |  | [optional] [default to 0]
 **pageSize** | **Int** |  | [optional] [default to 100]
 **sort** | [**[String]**](String.md) | Default sort is dateSent:asc. Multiple sort criteria are supported and must be separated with a comma. | [optional] 
 **filter** | **String** | Query in the RSQL format, allowing to filter, for a list of commands. All url must contain minimum one filter field. Fields allowed in the query: uuid, clientManagementId, command, status, clientType, dateSent, validAfter, dateCompleted, profileIdentifier, and active. This param can be combined with paging. Please note that any date filters must be used with gt, lt, ge, le Example: clientManagementId&#x3D;&#x3D;fb511aae-c557-474f-a9c1-5dc845b90d0f;status&#x3D;&#x3D;Pending;command&#x3D;&#x3D;INSTALL_PROFILE;uuid&#x3D;&#x3D;9e18f849-e689-4f2d-b616-a99d3da7db42;clientType&#x3D;&#x3D;COMPUTER_USER;profileIdentifier&#x3D;&#x3D;18cc61c2-01fc-11ed-b939-0242ac120002;dateCompleted&#x3D;ge&#x3D;2021-08-04T14:25:18.26Z;dateCompleted&#x3D;le&#x3D;2021-08-04T14:25:18.26Z;validAfter&#x3D;ge&#x3D;2021-08-05T14:25:18.26Z;active&#x3D;&#x3D;true | [optional] [default to &quot;&quot;]

### Return type

[**MdmCommandResults**](MdmCommandResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

