# ComputerInventoryCollectionSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**computerInventoryCollectionPreferences** | [**ComputerInventoryCollectionPreferences**](ComputerInventoryCollectionPreferences.md) |  | [optional] 
**applicationPaths** | [AppPath] |  | [optional] 
**fontPaths** | [FontPath] |  | [optional] 
**pluginPaths** | [PluginPath] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


