//
// CloudIdPCommonRequest.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** A Cloud Identity Provider information for request */
public struct CloudIdPCommonRequest: Codable, JSONEncodable, Hashable {

    public enum ProviderName: String, Codable, CaseIterable {
        case google = "GOOGLE"
        case azure = "AZURE"
    }
    public var displayName: String
    public var providerName: ProviderName

    public init(displayName: String, providerName: ProviderName) {
        self.displayName = displayName
        self.providerName = providerName
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case displayName
        case providerName
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(displayName, forKey: .displayName)
        try container.encode(providerName, forKey: .providerName)
    }
}

