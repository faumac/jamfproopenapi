//
// MobileDevicePrestage.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDevicePrestage: Codable, JSONEncodable, Hashable {

    public var displayName: String
    public var isMandatory: Bool
    public var isMdmRemovable: Bool
    public var supportPhoneNumber: String
    public var supportEmailAddress: String
    public var department: String
    public var isDefaultPrestage: Bool
    public var enrollmentSiteId: Int
    public var isKeepExistingSiteMembership: Bool
    public var isKeepExistingLocationInformation: Bool
    public var isRequireAuthentication: Bool
    public var authenticationPrompt: String
    public var isPreventActivationLock: Bool
    public var isEnableDeviceBasedActivationLock: Bool
    public var deviceEnrollmentProgramInstanceId: Int
    public var skipSetupItems: [String: Bool]?
    public var locationInformation: LocationInformation
    public var purchasingInformation: PrestagePurchasingInformation
    /** The Base64 encoded PEM Certificate */
    public var anchorCertificates: [String]?
    public var enrollmentCustomizationId: Int?
    public var isAllowPairing: Bool
    public var isMultiUser: Bool
    public var isSupervised: Bool
    public var maximumSharedAccounts: Int
    public var isAutoAdvanceSetup: Bool
    public var isConfigureDeviceBeforeSetupAssistant: Bool
    public var language: String?
    public var region: String?
    public var names: MobileDevicePrestageNames?

    public init(displayName: String, isMandatory: Bool, isMdmRemovable: Bool, supportPhoneNumber: String, supportEmailAddress: String, department: String, isDefaultPrestage: Bool, enrollmentSiteId: Int, isKeepExistingSiteMembership: Bool, isKeepExistingLocationInformation: Bool, isRequireAuthentication: Bool, authenticationPrompt: String, isPreventActivationLock: Bool, isEnableDeviceBasedActivationLock: Bool, deviceEnrollmentProgramInstanceId: Int, skipSetupItems: [String: Bool]? = nil, locationInformation: LocationInformation, purchasingInformation: PrestagePurchasingInformation, anchorCertificates: [String]? = nil, enrollmentCustomizationId: Int? = nil, isAllowPairing: Bool, isMultiUser: Bool, isSupervised: Bool, maximumSharedAccounts: Int, isAutoAdvanceSetup: Bool, isConfigureDeviceBeforeSetupAssistant: Bool, language: String? = nil, region: String? = nil, names: MobileDevicePrestageNames? = nil) {
        self.displayName = displayName
        self.isMandatory = isMandatory
        self.isMdmRemovable = isMdmRemovable
        self.supportPhoneNumber = supportPhoneNumber
        self.supportEmailAddress = supportEmailAddress
        self.department = department
        self.isDefaultPrestage = isDefaultPrestage
        self.enrollmentSiteId = enrollmentSiteId
        self.isKeepExistingSiteMembership = isKeepExistingSiteMembership
        self.isKeepExistingLocationInformation = isKeepExistingLocationInformation
        self.isRequireAuthentication = isRequireAuthentication
        self.authenticationPrompt = authenticationPrompt
        self.isPreventActivationLock = isPreventActivationLock
        self.isEnableDeviceBasedActivationLock = isEnableDeviceBasedActivationLock
        self.deviceEnrollmentProgramInstanceId = deviceEnrollmentProgramInstanceId
        self.skipSetupItems = skipSetupItems
        self.locationInformation = locationInformation
        self.purchasingInformation = purchasingInformation
        self.anchorCertificates = anchorCertificates
        self.enrollmentCustomizationId = enrollmentCustomizationId
        self.isAllowPairing = isAllowPairing
        self.isMultiUser = isMultiUser
        self.isSupervised = isSupervised
        self.maximumSharedAccounts = maximumSharedAccounts
        self.isAutoAdvanceSetup = isAutoAdvanceSetup
        self.isConfigureDeviceBeforeSetupAssistant = isConfigureDeviceBeforeSetupAssistant
        self.language = language
        self.region = region
        self.names = names
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case displayName
        case isMandatory
        case isMdmRemovable
        case supportPhoneNumber
        case supportEmailAddress
        case department
        case isDefaultPrestage
        case enrollmentSiteId
        case isKeepExistingSiteMembership
        case isKeepExistingLocationInformation
        case isRequireAuthentication
        case authenticationPrompt
        case isPreventActivationLock
        case isEnableDeviceBasedActivationLock
        case deviceEnrollmentProgramInstanceId
        case skipSetupItems
        case locationInformation
        case purchasingInformation
        case anchorCertificates
        case enrollmentCustomizationId
        case isAllowPairing
        case isMultiUser
        case isSupervised
        case maximumSharedAccounts
        case isAutoAdvanceSetup
        case isConfigureDeviceBeforeSetupAssistant
        case language
        case region
        case names
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(displayName, forKey: .displayName)
        try container.encode(isMandatory, forKey: .isMandatory)
        try container.encode(isMdmRemovable, forKey: .isMdmRemovable)
        try container.encode(supportPhoneNumber, forKey: .supportPhoneNumber)
        try container.encode(supportEmailAddress, forKey: .supportEmailAddress)
        try container.encode(department, forKey: .department)
        try container.encode(isDefaultPrestage, forKey: .isDefaultPrestage)
        try container.encode(enrollmentSiteId, forKey: .enrollmentSiteId)
        try container.encode(isKeepExistingSiteMembership, forKey: .isKeepExistingSiteMembership)
        try container.encode(isKeepExistingLocationInformation, forKey: .isKeepExistingLocationInformation)
        try container.encode(isRequireAuthentication, forKey: .isRequireAuthentication)
        try container.encode(authenticationPrompt, forKey: .authenticationPrompt)
        try container.encode(isPreventActivationLock, forKey: .isPreventActivationLock)
        try container.encode(isEnableDeviceBasedActivationLock, forKey: .isEnableDeviceBasedActivationLock)
        try container.encode(deviceEnrollmentProgramInstanceId, forKey: .deviceEnrollmentProgramInstanceId)
        try container.encodeIfPresent(skipSetupItems, forKey: .skipSetupItems)
        try container.encode(locationInformation, forKey: .locationInformation)
        try container.encode(purchasingInformation, forKey: .purchasingInformation)
        try container.encodeIfPresent(anchorCertificates, forKey: .anchorCertificates)
        try container.encodeIfPresent(enrollmentCustomizationId, forKey: .enrollmentCustomizationId)
        try container.encode(isAllowPairing, forKey: .isAllowPairing)
        try container.encode(isMultiUser, forKey: .isMultiUser)
        try container.encode(isSupervised, forKey: .isSupervised)
        try container.encode(maximumSharedAccounts, forKey: .maximumSharedAccounts)
        try container.encode(isAutoAdvanceSetup, forKey: .isAutoAdvanceSetup)
        try container.encode(isConfigureDeviceBeforeSetupAssistant, forKey: .isConfigureDeviceBeforeSetupAssistant)
        try container.encodeIfPresent(language, forKey: .language)
        try container.encodeIfPresent(region, forKey: .region)
        try container.encodeIfPresent(names, forKey: .names)
    }
}

