//
// PolicyPropertiesV1.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PolicyPropertiesV1: Codable, JSONEncodable, Hashable {

    /** This field always returns false. */
    public var policiesRequireNetworkStateChange: Bool? = false
    public var allowNetworkStateChangeTriggers: Bool? = true

    public init(policiesRequireNetworkStateChange: Bool? = false, allowNetworkStateChangeTriggers: Bool? = true) {
        self.policiesRequireNetworkStateChange = policiesRequireNetworkStateChange
        self.allowNetworkStateChangeTriggers = allowNetworkStateChangeTriggers
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case policiesRequireNetworkStateChange
        case allowNetworkStateChangeTriggers
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(policiesRequireNetworkStateChange, forKey: .policiesRequireNetworkStateChange)
        try container.encodeIfPresent(allowNetworkStateChangeTriggers, forKey: .allowNetworkStateChangeTriggers)
    }
}

