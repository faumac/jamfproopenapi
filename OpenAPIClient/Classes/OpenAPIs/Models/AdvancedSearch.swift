//
// AdvancedSearch.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct AdvancedSearch: Codable, JSONEncodable, Hashable {

    static let idRule = StringRule(minLength: 1, maxLength: nil, pattern: nil)
    public var id: String?
    public var name: String
    public var criteria: [SmartSearchCriterion]?
    public var displayFields: [String]?
    public var siteId: String?

    public init(id: String? = nil, name: String, criteria: [SmartSearchCriterion]? = nil, displayFields: [String]? = nil, siteId: String? = nil) {
        self.id = id
        self.name = name
        self.criteria = criteria
        self.displayFields = displayFields
        self.siteId = siteId
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case name
        case criteria
        case displayFields
        case siteId
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encodeIfPresent(criteria, forKey: .criteria)
        try container.encodeIfPresent(displayFields, forKey: .displayFields)
        try container.encodeIfPresent(siteId, forKey: .siteId)
    }
}

