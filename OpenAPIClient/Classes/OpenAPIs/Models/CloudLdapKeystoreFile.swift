//
// CloudLdapKeystoreFile.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** Request with the Base64-encoded keystore file */
public struct CloudLdapKeystoreFile: Codable, JSONEncodable, Hashable {

    public var password: String
    public var fileBytes: Data
    public var fileName: String

    public init(password: String, fileBytes: Data, fileName: String) {
        self.password = password
        self.fileBytes = fileBytes
        self.fileName = fileName
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case password
        case fileBytes
        case fileName
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(password, forKey: .password)
        try container.encode(fileBytes, forKey: .fileBytes)
        try container.encode(fileName, forKey: .fileName)
    }
}

