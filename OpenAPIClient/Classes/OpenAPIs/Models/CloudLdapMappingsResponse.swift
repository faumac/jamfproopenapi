//
// CloudLdapMappingsResponse.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** Mappings configuration response for Ldap Cloud Identity Provider configuration */
public struct CloudLdapMappingsResponse: Codable, JSONEncodable, Hashable {

    public var userMappings: UserMappings?
    public var groupMappings: GroupMappings?
    public var membershipMappings: MembershipMappings?

    public init(userMappings: UserMappings? = nil, groupMappings: GroupMappings? = nil, membershipMappings: MembershipMappings? = nil) {
        self.userMappings = userMappings
        self.groupMappings = groupMappings
        self.membershipMappings = membershipMappings
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case userMappings
        case groupMappings
        case membershipMappings
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(userMappings, forKey: .userMappings)
        try container.encodeIfPresent(groupMappings, forKey: .groupMappings)
        try container.encodeIfPresent(membershipMappings, forKey: .membershipMappings)
    }
}

