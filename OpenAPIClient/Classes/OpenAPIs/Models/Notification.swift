//
// Notification.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** Jamf Pro notification used for important alerts. */
public struct Notification: Codable, JSONEncodable, Hashable {

    public var type: String?
    public var id: Int?
    public var params: [String: AnyCodable]?

    public init(type: String? = nil, id: Int? = nil, params: [String: AnyCodable]? = nil) {
        self.type = type
        self.id = id
        self.params = params
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case type
        case id
        case params
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(params, forKey: .params)
    }
}

