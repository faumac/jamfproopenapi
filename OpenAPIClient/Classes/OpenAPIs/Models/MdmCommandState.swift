//
// MdmCommandState.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public enum MdmCommandState: String, Codable, CaseIterable {
    case pending = "PENDING"
    case acknowledged = "ACKNOWLEDGED"
    case notNow = "NOT_NOW"
    case error = "ERROR"
}
