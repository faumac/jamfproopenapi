//
// ComputerContentCachingAlert.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ComputerContentCachingAlert: Codable, JSONEncodable, Hashable {

    public var cacheBytesLimit: Int64?
    public var className: String?
    public var pathPreventingAccess: String?
    public var postDate: String?
    public var reservedVolumeBytes: Int64?
    public var resource: String?

    public init(cacheBytesLimit: Int64? = nil, className: String? = nil, pathPreventingAccess: String? = nil, postDate: String? = nil, reservedVolumeBytes: Int64? = nil, resource: String? = nil) {
        self.cacheBytesLimit = cacheBytesLimit
        self.className = className
        self.pathPreventingAccess = pathPreventingAccess
        self.postDate = postDate
        self.reservedVolumeBytes = reservedVolumeBytes
        self.resource = resource
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case cacheBytesLimit
        case className
        case pathPreventingAccess
        case postDate
        case reservedVolumeBytes
        case resource
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(cacheBytesLimit, forKey: .cacheBytesLimit)
        try container.encodeIfPresent(className, forKey: .className)
        try container.encodeIfPresent(pathPreventingAccess, forKey: .pathPreventingAccess)
        try container.encodeIfPresent(postDate, forKey: .postDate)
        try container.encodeIfPresent(reservedVolumeBytes, forKey: .reservedVolumeBytes)
        try container.encodeIfPresent(resource, forKey: .resource)
    }
}

