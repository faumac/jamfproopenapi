//
// DashboardMetric.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct DashboardMetric: Codable, JSONEncodable, Hashable {

    public enum Tag: String, Codable, CaseIterable {
        case completed = "Completed"
        case remaining = "Remaining"
        case siteLicense = "Site License"
        case noLicenses = "No Licenses"
        case over = "Over"
        case failed = "Failed"
        case retrying = "Retrying"
        case retryingDisabled = "Retrying-disabled"
        case pending = "Pending"
        case expiring = "Expiring"
        case active = "Active"
        case inactive = "Inactive"
        case computers = "Computers"
        case devices = "Devices"
        case users = "Users"
        case inUse = "In Use"
        case nSlashA = "N/A"
        case latestVersion = "Latest Version"
        case otherVersions = "Other Versions"
    }
    /** Usually a number associated with the tag; i.e. 23 Pending Computers */
    public var value: String?
    /** Logical to decide whether metric should be enabled or disabled; i.e. Policy can be at Retrying-Disabled status */
    public var enabled: Bool? = true
    public var tag: Tag?

    public init(value: String? = nil, enabled: Bool? = true, tag: Tag? = nil) {
        self.value = value
        self.enabled = enabled
        self.tag = tag
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case value
        case enabled
        case tag
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(value, forKey: .value)
        try container.encodeIfPresent(enabled, forKey: .enabled)
        try container.encodeIfPresent(tag, forKey: .tag)
    }
}

