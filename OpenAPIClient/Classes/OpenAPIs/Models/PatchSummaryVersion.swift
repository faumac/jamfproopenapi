//
// PatchSummaryVersion.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PatchSummaryVersion: Codable, JSONEncodable, Hashable {

    public var absoluteOrderId: String?
    public var version: String?
    public var onVersion: Int?

    public init(absoluteOrderId: String? = nil, version: String? = nil, onVersion: Int? = nil) {
        self.absoluteOrderId = absoluteOrderId
        self.version = version
        self.onVersion = onVersion
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case absoluteOrderId
        case version
        case onVersion
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(absoluteOrderId, forKey: .absoluteOrderId)
        try container.encodeIfPresent(version, forKey: .version)
        try container.encodeIfPresent(onVersion, forKey: .onVersion)
    }
}

