//
// MobileDeviceUserProfile.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDeviceUserProfile: Codable, JSONEncodable, Hashable {

    public var displayName: String?
    public var version: String?
    public var uuid: String?
    public var identifier: String?
    public var removable: Bool?
    public var lastInstalled: String?
    public var username: String?

    public init(displayName: String? = nil, version: String? = nil, uuid: String? = nil, identifier: String? = nil, removable: Bool? = nil, lastInstalled: String? = nil, username: String? = nil) {
        self.displayName = displayName
        self.version = version
        self.uuid = uuid
        self.identifier = identifier
        self.removable = removable
        self.lastInstalled = lastInstalled
        self.username = username
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case displayName
        case version
        case uuid
        case identifier
        case removable
        case lastInstalled
        case username
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(displayName, forKey: .displayName)
        try container.encodeIfPresent(version, forKey: .version)
        try container.encodeIfPresent(uuid, forKey: .uuid)
        try container.encodeIfPresent(identifier, forKey: .identifier)
        try container.encodeIfPresent(removable, forKey: .removable)
        try container.encodeIfPresent(lastInstalled, forKey: .lastInstalled)
        try container.encodeIfPresent(username, forKey: .username)
    }
}

