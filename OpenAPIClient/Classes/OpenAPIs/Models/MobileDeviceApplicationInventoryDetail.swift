//
// MobileDeviceApplicationInventoryDetail.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDeviceApplicationInventoryDetail: Codable, JSONEncodable, Hashable {

    public var identifier: String?
    public var name: String?
    public var version: String?
    public var shortVersion: String?
    public var managementStatus: String?
    public var validationStatus: Bool?
    public var bundleSize: String?
    public var dynamicSize: String?

    public init(identifier: String? = nil, name: String? = nil, version: String? = nil, shortVersion: String? = nil, managementStatus: String? = nil, validationStatus: Bool? = nil, bundleSize: String? = nil, dynamicSize: String? = nil) {
        self.identifier = identifier
        self.name = name
        self.version = version
        self.shortVersion = shortVersion
        self.managementStatus = managementStatus
        self.validationStatus = validationStatus
        self.bundleSize = bundleSize
        self.dynamicSize = dynamicSize
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case identifier
        case name
        case version
        case shortVersion
        case managementStatus
        case validationStatus
        case bundleSize
        case dynamicSize
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(identifier, forKey: .identifier)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(version, forKey: .version)
        try container.encodeIfPresent(shortVersion, forKey: .shortVersion)
        try container.encodeIfPresent(managementStatus, forKey: .managementStatus)
        try container.encodeIfPresent(validationStatus, forKey: .validationStatus)
        try container.encodeIfPresent(bundleSize, forKey: .bundleSize)
        try container.encodeIfPresent(dynamicSize, forKey: .dynamicSize)
    }
}

