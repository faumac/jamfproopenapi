//
// LapsPasswordAndAuditsV2.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct LapsPasswordAndAuditsV2: Codable, JSONEncodable, Hashable {

    public var password: String?
    public var dateLastSeen: String?
    public var expirationTime: String?
    public var audits: [LapsAuditV2]?

    public init(password: String? = nil, dateLastSeen: String? = nil, expirationTime: String? = nil, audits: [LapsAuditV2]? = nil) {
        self.password = password
        self.dateLastSeen = dateLastSeen
        self.expirationTime = expirationTime
        self.audits = audits
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case password
        case dateLastSeen
        case expirationTime
        case audits
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(password, forKey: .password)
        try container.encodeIfPresent(dateLastSeen, forKey: .dateLastSeen)
        try container.encodeIfPresent(expirationTime, forKey: .expirationTime)
        try container.encodeIfPresent(audits, forKey: .audits)
    }
}

