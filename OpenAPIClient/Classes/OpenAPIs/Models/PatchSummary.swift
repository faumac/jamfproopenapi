//
// PatchSummary.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PatchSummary: Codable, JSONEncodable, Hashable {

    public var softwareTitleId: String?
    public var title: String?
    public var latestVersion: String?
    public var releaseDate: String?
    public var upToDate: Int?
    public var outOfDate: Int?
    public var onDashboard: Bool?
    public var softwareTitleConfigurationId: String?

    public init(softwareTitleId: String? = nil, title: String? = nil, latestVersion: String? = nil, releaseDate: String? = nil, upToDate: Int? = nil, outOfDate: Int? = nil, onDashboard: Bool? = nil, softwareTitleConfigurationId: String? = nil) {
        self.softwareTitleId = softwareTitleId
        self.title = title
        self.latestVersion = latestVersion
        self.releaseDate = releaseDate
        self.upToDate = upToDate
        self.outOfDate = outOfDate
        self.onDashboard = onDashboard
        self.softwareTitleConfigurationId = softwareTitleConfigurationId
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case softwareTitleId
        case title
        case latestVersion
        case releaseDate
        case upToDate
        case outOfDate
        case onDashboard
        case softwareTitleConfigurationId
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(softwareTitleId, forKey: .softwareTitleId)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(latestVersion, forKey: .latestVersion)
        try container.encodeIfPresent(releaseDate, forKey: .releaseDate)
        try container.encodeIfPresent(upToDate, forKey: .upToDate)
        try container.encodeIfPresent(outOfDate, forKey: .outOfDate)
        try container.encodeIfPresent(onDashboard, forKey: .onDashboard)
        try container.encodeIfPresent(softwareTitleConfigurationId, forKey: .softwareTitleConfigurationId)
    }
}

