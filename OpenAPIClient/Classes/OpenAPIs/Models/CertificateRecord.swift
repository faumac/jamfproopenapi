//
// CertificateRecord.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct CertificateRecord: Codable, JSONEncodable, Hashable {

    public var subjectX500Principal: String?
    public var issuerX500Principal: String?
    public var serialNumber: String?
    public var version: Int?
    public var notAfter: Int?
    public var notBefore: Int?
    public var signature: Signature?
    public var keyUsage: [String]?
    public var keyUsageExtended: [String]?
    public var sha1Fingerprint: String?
    public var sha256Fingerprint: String?

    public init(subjectX500Principal: String? = nil, issuerX500Principal: String? = nil, serialNumber: String? = nil, version: Int? = nil, notAfter: Int? = nil, notBefore: Int? = nil, signature: Signature? = nil, keyUsage: [String]? = nil, keyUsageExtended: [String]? = nil, sha1Fingerprint: String? = nil, sha256Fingerprint: String? = nil) {
        self.subjectX500Principal = subjectX500Principal
        self.issuerX500Principal = issuerX500Principal
        self.serialNumber = serialNumber
        self.version = version
        self.notAfter = notAfter
        self.notBefore = notBefore
        self.signature = signature
        self.keyUsage = keyUsage
        self.keyUsageExtended = keyUsageExtended
        self.sha1Fingerprint = sha1Fingerprint
        self.sha256Fingerprint = sha256Fingerprint
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case subjectX500Principal
        case issuerX500Principal
        case serialNumber
        case version
        case notAfter
        case notBefore
        case signature
        case keyUsage
        case keyUsageExtended
        case sha1Fingerprint
        case sha256Fingerprint
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(subjectX500Principal, forKey: .subjectX500Principal)
        try container.encodeIfPresent(issuerX500Principal, forKey: .issuerX500Principal)
        try container.encodeIfPresent(serialNumber, forKey: .serialNumber)
        try container.encodeIfPresent(version, forKey: .version)
        try container.encodeIfPresent(notAfter, forKey: .notAfter)
        try container.encodeIfPresent(notBefore, forKey: .notBefore)
        try container.encodeIfPresent(signature, forKey: .signature)
        try container.encodeIfPresent(keyUsage, forKey: .keyUsage)
        try container.encodeIfPresent(keyUsageExtended, forKey: .keyUsageExtended)
        try container.encodeIfPresent(sha1Fingerprint, forKey: .sha1Fingerprint)
        try container.encodeIfPresent(sha256Fingerprint, forKey: .sha256Fingerprint)
    }
}

