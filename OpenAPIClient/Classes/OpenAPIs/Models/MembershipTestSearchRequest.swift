//
// MembershipTestSearchRequest.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MembershipTestSearchRequest: Codable, JSONEncodable, Hashable {

    public var username: String
    public var groupname: String

    public init(username: String, groupname: String) {
        self.username = username
        self.groupname = groupname
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case username
        case groupname
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(username, forKey: .username)
        try container.encode(groupname, forKey: .groupname)
    }
}

