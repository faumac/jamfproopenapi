//
// LocationInformationV2.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct LocationInformationV2: Codable, JSONEncodable, Hashable {

    public var username: String
    public var realname: String
    public var phone: String
    public var email: String
    public var room: String
    public var position: String
    public var departmentId: String
    public var buildingId: String
    public var id: String
    public var versionLock: Int

    public init(username: String, realname: String, phone: String, email: String, room: String, position: String, departmentId: String, buildingId: String, id: String, versionLock: Int) {
        self.username = username
        self.realname = realname
        self.phone = phone
        self.email = email
        self.room = room
        self.position = position
        self.departmentId = departmentId
        self.buildingId = buildingId
        self.id = id
        self.versionLock = versionLock
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case username
        case realname
        case phone
        case email
        case room
        case position
        case departmentId
        case buildingId
        case id
        case versionLock
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(username, forKey: .username)
        try container.encode(realname, forKey: .realname)
        try container.encode(phone, forKey: .phone)
        try container.encode(email, forKey: .email)
        try container.encode(room, forKey: .room)
        try container.encode(position, forKey: .position)
        try container.encode(departmentId, forKey: .departmentId)
        try container.encode(buildingId, forKey: .buildingId)
        try container.encode(id, forKey: .id)
        try container.encode(versionLock, forKey: .versionLock)
    }
}

