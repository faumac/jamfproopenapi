//
// ApplicationAttributes.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ApplicationAttributes: Codable, JSONEncodable, Hashable {

    public var identifier: String?
    public var attributes: Attributes?

    public init(identifier: String? = nil, attributes: Attributes? = nil) {
        self.identifier = identifier
        self.attributes = attributes
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case identifier
        case attributes
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(identifier, forKey: .identifier)
        try container.encodeIfPresent(attributes, forKey: .attributes)
    }
}

