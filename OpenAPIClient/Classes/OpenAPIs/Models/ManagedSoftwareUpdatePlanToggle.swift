//
// ManagedSoftwareUpdatePlanToggle.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ManagedSoftwareUpdatePlanToggle: Codable, JSONEncodable, Hashable {

    public var toggle: Bool
    public var forceInstallLocalDateEnabled: Bool?
    public var dssEnabled: Bool?

    public init(toggle: Bool, forceInstallLocalDateEnabled: Bool? = nil, dssEnabled: Bool? = nil) {
        self.toggle = toggle
        self.forceInstallLocalDateEnabled = forceInstallLocalDateEnabled
        self.dssEnabled = dssEnabled
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case toggle
        case forceInstallLocalDateEnabled
        case dssEnabled
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(toggle, forKey: .toggle)
        try container.encodeIfPresent(forceInstallLocalDateEnabled, forKey: .forceInstallLocalDateEnabled)
        try container.encodeIfPresent(dssEnabled, forKey: .dssEnabled)
    }
}

