//
// ComputerUserAndLocation.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ComputerUserAndLocation: Codable, JSONEncodable, Hashable {

    public var username: String?
    public var realname: String?
    public var email: String?
    public var position: String?
    public var phone: String?
    public var departmentId: String?
    public var buildingId: String?
    public var room: String?
    public var extensionAttributes: [ComputerExtensionAttribute]?

    public init(username: String? = nil, realname: String? = nil, email: String? = nil, position: String? = nil, phone: String? = nil, departmentId: String? = nil, buildingId: String? = nil, room: String? = nil, extensionAttributes: [ComputerExtensionAttribute]? = nil) {
        self.username = username
        self.realname = realname
        self.email = email
        self.position = position
        self.phone = phone
        self.departmentId = departmentId
        self.buildingId = buildingId
        self.room = room
        self.extensionAttributes = extensionAttributes
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case username
        case realname
        case email
        case position
        case phone
        case departmentId
        case buildingId
        case room
        case extensionAttributes
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(username, forKey: .username)
        try container.encodeIfPresent(realname, forKey: .realname)
        try container.encodeIfPresent(email, forKey: .email)
        try container.encodeIfPresent(position, forKey: .position)
        try container.encodeIfPresent(phone, forKey: .phone)
        try container.encodeIfPresent(departmentId, forKey: .departmentId)
        try container.encodeIfPresent(buildingId, forKey: .buildingId)
        try container.encodeIfPresent(room, forKey: .room)
        try container.encodeIfPresent(extensionAttributes, forKey: .extensionAttributes)
    }
}

