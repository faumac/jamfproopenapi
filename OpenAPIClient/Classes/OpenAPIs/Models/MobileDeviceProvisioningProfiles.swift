//
// MobileDeviceProvisioningProfiles.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDeviceProvisioningProfiles: Codable, JSONEncodable, Hashable {

    public var displayName: String?
    public var uuid: String?
    public var expirationDate: String?

    public init(displayName: String? = nil, uuid: String? = nil, expirationDate: String? = nil) {
        self.displayName = displayName
        self.uuid = uuid
        self.expirationDate = expirationDate
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case displayName
        case uuid
        case expirationDate
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(displayName, forKey: .displayName)
        try container.encodeIfPresent(uuid, forKey: .uuid)
        try container.encodeIfPresent(expirationDate, forKey: .expirationDate)
    }
}

