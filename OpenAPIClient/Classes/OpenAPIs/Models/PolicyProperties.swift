//
// PolicyProperties.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PolicyProperties: Codable, JSONEncodable, Hashable {

    /** This field always returns false. */
    public var isPoliciesRequireNetworkStateChange: Bool? = false
    public var isAllowNetworkStateChangeTriggers: Bool? = true

    public init(isPoliciesRequireNetworkStateChange: Bool? = false, isAllowNetworkStateChangeTriggers: Bool? = true) {
        self.isPoliciesRequireNetworkStateChange = isPoliciesRequireNetworkStateChange
        self.isAllowNetworkStateChangeTriggers = isAllowNetworkStateChangeTriggers
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case isPoliciesRequireNetworkStateChange
        case isAllowNetworkStateChangeTriggers
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(isPoliciesRequireNetworkStateChange, forKey: .isPoliciesRequireNetworkStateChange)
        try container.encodeIfPresent(isAllowNetworkStateChangeTriggers, forKey: .isAllowNetworkStateChangeTriggers)
    }
}

