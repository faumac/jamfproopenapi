//
// MobileDeviceCertificateV2.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDeviceCertificateV2: Codable, JSONEncodable, Hashable {

    public enum CertificateStatus: String, Codable, CaseIterable {
        case expiring = "EXPIRING"
        case expired = "EXPIRED"
        case revoked = "REVOKED"
        case pendingRevoke = "PENDING_REVOKE"
        case issued = "ISSUED"
    }
    public enum LifecycleStatus: String, Codable, CaseIterable {
        case active = "ACTIVE"
        case inactive = "INACTIVE"
    }
    public var commonName: String?
    public var identity: Bool?
    public var expirationDateEpoch: String?
    public var subjectName: String?
    public var serialNumber: String?
    public var sha1Fingerprint: String?
    public var issuedDateEpoch: String?
    public var certificateStatus: CertificateStatus?
    public var lifecycleStatus: LifecycleStatus?

    public init(commonName: String? = nil, identity: Bool? = nil, expirationDateEpoch: String? = nil, subjectName: String? = nil, serialNumber: String? = nil, sha1Fingerprint: String? = nil, issuedDateEpoch: String? = nil, certificateStatus: CertificateStatus? = nil, lifecycleStatus: LifecycleStatus? = nil) {
        self.commonName = commonName
        self.identity = identity
        self.expirationDateEpoch = expirationDateEpoch
        self.subjectName = subjectName
        self.serialNumber = serialNumber
        self.sha1Fingerprint = sha1Fingerprint
        self.issuedDateEpoch = issuedDateEpoch
        self.certificateStatus = certificateStatus
        self.lifecycleStatus = lifecycleStatus
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case commonName
        case identity
        case expirationDateEpoch
        case subjectName
        case serialNumber
        case sha1Fingerprint
        case issuedDateEpoch
        case certificateStatus
        case lifecycleStatus
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(commonName, forKey: .commonName)
        try container.encodeIfPresent(identity, forKey: .identity)
        try container.encodeIfPresent(expirationDateEpoch, forKey: .expirationDateEpoch)
        try container.encodeIfPresent(subjectName, forKey: .subjectName)
        try container.encodeIfPresent(serialNumber, forKey: .serialNumber)
        try container.encodeIfPresent(sha1Fingerprint, forKey: .sha1Fingerprint)
        try container.encodeIfPresent(issuedDateEpoch, forKey: .issuedDateEpoch)
        try container.encodeIfPresent(certificateStatus, forKey: .certificateStatus)
        try container.encodeIfPresent(lifecycleStatus, forKey: .lifecycleStatus)
    }
}

