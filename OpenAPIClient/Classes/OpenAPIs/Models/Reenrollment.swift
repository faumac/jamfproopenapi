//
// Reenrollment.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct Reenrollment: Codable, JSONEncodable, Hashable {

    public enum FlushMDMQueue: String, Codable, CaseIterable {
        case nothing = "DELETE_NOTHING"
        case errors = "DELETE_ERRORS"
        case everythingExceptAcknowledged = "DELETE_EVERYTHING_EXCEPT_ACKNOWLEDGED"
        case everything = "DELETE_EVERYTHING"
    }
    public var isFlushPolicyHistoryEnabled: Bool? = false
    public var isFlushLocationInformationEnabled: Bool? = false
    public var isFlushLocationInformationHistoryEnabled: Bool? = false
    public var isFlushExtensionAttributesEnabled: Bool? = false
    public var flushMDMQueue: FlushMDMQueue

    public init(isFlushPolicyHistoryEnabled: Bool? = false, isFlushLocationInformationEnabled: Bool? = false, isFlushLocationInformationHistoryEnabled: Bool? = false, isFlushExtensionAttributesEnabled: Bool? = false, flushMDMQueue: FlushMDMQueue) {
        self.isFlushPolicyHistoryEnabled = isFlushPolicyHistoryEnabled
        self.isFlushLocationInformationEnabled = isFlushLocationInformationEnabled
        self.isFlushLocationInformationHistoryEnabled = isFlushLocationInformationHistoryEnabled
        self.isFlushExtensionAttributesEnabled = isFlushExtensionAttributesEnabled
        self.flushMDMQueue = flushMDMQueue
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case isFlushPolicyHistoryEnabled
        case isFlushLocationInformationEnabled
        case isFlushLocationInformationHistoryEnabled
        case isFlushExtensionAttributesEnabled
        case flushMDMQueue
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(isFlushPolicyHistoryEnabled, forKey: .isFlushPolicyHistoryEnabled)
        try container.encodeIfPresent(isFlushLocationInformationEnabled, forKey: .isFlushLocationInformationEnabled)
        try container.encodeIfPresent(isFlushLocationInformationHistoryEnabled, forKey: .isFlushLocationInformationHistoryEnabled)
        try container.encodeIfPresent(isFlushExtensionAttributesEnabled, forKey: .isFlushExtensionAttributesEnabled)
        try container.encode(flushMDMQueue, forKey: .flushMDMQueue)
    }
}

