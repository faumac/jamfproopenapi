//
// PatchSoftwareTitleConfigurationDependencies.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PatchSoftwareTitleConfigurationDependencies: Codable, JSONEncodable, Hashable {

    public var totalCount: Int?
    public var results: [PatchSoftwareTitleConfigurationDependency]?

    public init(totalCount: Int? = nil, results: [PatchSoftwareTitleConfigurationDependency]? = nil) {
        self.totalCount = totalCount
        self.results = results
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case totalCount
        case results
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(totalCount, forKey: .totalCount)
        try container.encodeIfPresent(results, forKey: .results)
    }
}

