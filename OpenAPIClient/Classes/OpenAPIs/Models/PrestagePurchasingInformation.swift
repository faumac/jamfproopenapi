//
// PrestagePurchasingInformation.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PrestagePurchasingInformation: Codable, JSONEncodable, Hashable {

    public var id: Int
    public var isLeased: Bool
    public var isPurchased: Bool
    public var appleCareID: String
    public var poNumber: String
    public var vendor: String
    public var purchasePrice: String
    public var lifeExpectancy: Int
    public var purchasingAccount: String
    public var purchasingContact: String
    public var leaseDate: String
    public var poDate: String
    public var warrantyDate: String
    public var versionLock: Int

    public init(id: Int, isLeased: Bool, isPurchased: Bool, appleCareID: String, poNumber: String, vendor: String, purchasePrice: String, lifeExpectancy: Int, purchasingAccount: String, purchasingContact: String, leaseDate: String, poDate: String, warrantyDate: String, versionLock: Int) {
        self.id = id
        self.isLeased = isLeased
        self.isPurchased = isPurchased
        self.appleCareID = appleCareID
        self.poNumber = poNumber
        self.vendor = vendor
        self.purchasePrice = purchasePrice
        self.lifeExpectancy = lifeExpectancy
        self.purchasingAccount = purchasingAccount
        self.purchasingContact = purchasingContact
        self.leaseDate = leaseDate
        self.poDate = poDate
        self.warrantyDate = warrantyDate
        self.versionLock = versionLock
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case isLeased
        case isPurchased
        case appleCareID
        case poNumber
        case vendor
        case purchasePrice
        case lifeExpectancy
        case purchasingAccount
        case purchasingContact
        case leaseDate
        case poDate
        case warrantyDate
        case versionLock
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(isLeased, forKey: .isLeased)
        try container.encode(isPurchased, forKey: .isPurchased)
        try container.encode(appleCareID, forKey: .appleCareID)
        try container.encode(poNumber, forKey: .poNumber)
        try container.encode(vendor, forKey: .vendor)
        try container.encode(purchasePrice, forKey: .purchasePrice)
        try container.encode(lifeExpectancy, forKey: .lifeExpectancy)
        try container.encode(purchasingAccount, forKey: .purchasingAccount)
        try container.encode(purchasingContact, forKey: .purchasingContact)
        try container.encode(leaseDate, forKey: .leaseDate)
        try container.encode(poDate, forKey: .poDate)
        try container.encode(warrantyDate, forKey: .warrantyDate)
        try container.encode(versionLock, forKey: .versionLock)
    }
}

