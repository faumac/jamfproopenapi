//
// LapsPendingRotation.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct LapsPendingRotation: Codable, JSONEncodable, Hashable {

    public var lapsUser: LapsUserV2?
    public var createdDate: String?

    public init(lapsUser: LapsUserV2? = nil, createdDate: String? = nil) {
        self.lapsUser = lapsUser
        self.createdDate = createdDate
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case lapsUser
        case createdDate
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(lapsUser, forKey: .lapsUser)
        try container.encodeIfPresent(createdDate, forKey: .createdDate)
    }
}

