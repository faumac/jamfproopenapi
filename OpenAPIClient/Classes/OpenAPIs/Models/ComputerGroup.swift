//
// ComputerGroup.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ComputerGroup: Codable, JSONEncodable, Hashable {

    static let idRule = StringRule(minLength: 1, maxLength: nil, pattern: nil)
    public var id: String?
    public var name: String?
    public var smartGroup: Bool?

    public init(id: String? = nil, name: String? = nil, smartGroup: Bool? = nil) {
        self.id = id
        self.name = name
        self.smartGroup = smartGroup
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case name
        case smartGroup
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(smartGroup, forKey: .smartGroup)
    }
}

