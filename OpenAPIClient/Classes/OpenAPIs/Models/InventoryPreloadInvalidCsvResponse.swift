//
// InventoryPreloadInvalidCsvResponse.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct InventoryPreloadInvalidCsvResponse: Codable, JSONEncodable, Hashable {

    public var httpsStatus: Int?
    public var errors: [InventoryPreloadCsvError]?

    public init(httpsStatus: Int? = nil, errors: [InventoryPreloadCsvError]? = nil) {
        self.httpsStatus = httpsStatus
        self.errors = errors
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case httpsStatus
        case errors
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(httpsStatus, forKey: .httpsStatus)
        try container.encodeIfPresent(errors, forKey: .errors)
    }
}

