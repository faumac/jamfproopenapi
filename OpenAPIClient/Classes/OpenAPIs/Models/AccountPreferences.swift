//
// AccountPreferences.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct AccountPreferences: Codable, JSONEncodable, Hashable {

    public var language: String?
    public var dateFormat: String?
    public var region: String?
    public var timezone: String?
    public var isDisableRelativeDates: Bool?

    public init(language: String? = nil, dateFormat: String? = nil, region: String? = nil, timezone: String? = nil, isDisableRelativeDates: Bool? = nil) {
        self.language = language
        self.dateFormat = dateFormat
        self.region = region
        self.timezone = timezone
        self.isDisableRelativeDates = isDisableRelativeDates
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case language
        case dateFormat
        case region
        case timezone
        case isDisableRelativeDates
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(language, forKey: .language)
        try container.encodeIfPresent(dateFormat, forKey: .dateFormat)
        try container.encodeIfPresent(region, forKey: .region)
        try container.encodeIfPresent(timezone, forKey: .timezone)
        try container.encodeIfPresent(isDisableRelativeDates, forKey: .isDisableRelativeDates)
    }
}

