//
// MobileDeviceIosGeneral.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDeviceIosGeneral: Codable, JSONEncodable, Hashable {

    public enum DeviceOwnershipType: String, Codable, CaseIterable {
        case institutional = "Institutional"
        case personalDeviceProfile = "PersonalDeviceProfile"
        case userEnrollment = "UserEnrollment"
        case accountDrivenUserEnrollment = "AccountDrivenUserEnrollment"
    }
    public var udid: String?
    public var displayName: String?
    public var assetTag: String?
    public var siteId: String?
    public var lastInventoryUpdateDate: String?
    public var osVersion: String?
    public var osRapidSecurityResponse: String?
    public var osBuild: String?
    public var osSupplementalBuildVersion: String?
    public var softwareUpdateDeviceId: String?
    public var ipAddress: String?
    public var managed: Bool?
    public var supervised: Bool?
    public var deviceOwnershipType: DeviceOwnershipType?
    public var enrollmentMethodPrestage: EnrollmentMethodPrestage?
    public var enrollmentSessionTokenValid: Bool?
    public var lastEnrolledDate: String?
    public var mdmProfileExpirationDate: String?
    /** IANA time zone database name */
    public var timeZone: String?
    public var declarativeDeviceManagementEnabled: Bool?
    public var extensionAttributes: [MobileDeviceExtensionAttribute]?
    public var sharedIpad: Bool?
    public var diagnosticAndUsageReportingEnabled: Bool?
    public var appAnalyticsEnabled: Bool?
    public var residentUsers: Int?
    public var quotaSize: Int?
    public var temporarySessionOnly: Bool?
    public var temporarySessionTimeout: Int?
    public var userSessionTimeout: Int?
    public var syncedToComputer: Int?
    public var maximumSharediPadUsersStored: Int?
    public var lastBackupDate: String?
    public var deviceLocatorServiceEnabled: Bool?
    public var doNotDisturbEnabled: Bool?
    public var cloudBackupEnabled: Bool?
    public var lastCloudBackupDate: String?
    public var locationServicesForSelfServiceMobileEnabled: Bool?
    public var itunesStoreAccountActive: Bool?
    public var exchangeDeviceId: String?
    public var tethered: Bool?

    public init(udid: String? = nil, displayName: String? = nil, assetTag: String? = nil, siteId: String? = nil, lastInventoryUpdateDate: String? = nil, osVersion: String? = nil, osRapidSecurityResponse: String? = nil, osBuild: String? = nil, osSupplementalBuildVersion: String? = nil, softwareUpdateDeviceId: String? = nil, ipAddress: String? = nil, managed: Bool? = nil, supervised: Bool? = nil, deviceOwnershipType: DeviceOwnershipType? = nil, enrollmentMethodPrestage: EnrollmentMethodPrestage? = nil, enrollmentSessionTokenValid: Bool? = nil, lastEnrolledDate: String? = nil, mdmProfileExpirationDate: String? = nil, timeZone: String? = nil, declarativeDeviceManagementEnabled: Bool? = nil, extensionAttributes: [MobileDeviceExtensionAttribute]? = nil, sharedIpad: Bool? = nil, diagnosticAndUsageReportingEnabled: Bool? = nil, appAnalyticsEnabled: Bool? = nil, residentUsers: Int? = nil, quotaSize: Int? = nil, temporarySessionOnly: Bool? = nil, temporarySessionTimeout: Int? = nil, userSessionTimeout: Int? = nil, syncedToComputer: Int? = nil, maximumSharediPadUsersStored: Int? = nil, lastBackupDate: String? = nil, deviceLocatorServiceEnabled: Bool? = nil, doNotDisturbEnabled: Bool? = nil, cloudBackupEnabled: Bool? = nil, lastCloudBackupDate: String? = nil, locationServicesForSelfServiceMobileEnabled: Bool? = nil, itunesStoreAccountActive: Bool? = nil, exchangeDeviceId: String? = nil, tethered: Bool? = nil) {
        self.udid = udid
        self.displayName = displayName
        self.assetTag = assetTag
        self.siteId = siteId
        self.lastInventoryUpdateDate = lastInventoryUpdateDate
        self.osVersion = osVersion
        self.osRapidSecurityResponse = osRapidSecurityResponse
        self.osBuild = osBuild
        self.osSupplementalBuildVersion = osSupplementalBuildVersion
        self.softwareUpdateDeviceId = softwareUpdateDeviceId
        self.ipAddress = ipAddress
        self.managed = managed
        self.supervised = supervised
        self.deviceOwnershipType = deviceOwnershipType
        self.enrollmentMethodPrestage = enrollmentMethodPrestage
        self.enrollmentSessionTokenValid = enrollmentSessionTokenValid
        self.lastEnrolledDate = lastEnrolledDate
        self.mdmProfileExpirationDate = mdmProfileExpirationDate
        self.timeZone = timeZone
        self.declarativeDeviceManagementEnabled = declarativeDeviceManagementEnabled
        self.extensionAttributes = extensionAttributes
        self.sharedIpad = sharedIpad
        self.diagnosticAndUsageReportingEnabled = diagnosticAndUsageReportingEnabled
        self.appAnalyticsEnabled = appAnalyticsEnabled
        self.residentUsers = residentUsers
        self.quotaSize = quotaSize
        self.temporarySessionOnly = temporarySessionOnly
        self.temporarySessionTimeout = temporarySessionTimeout
        self.userSessionTimeout = userSessionTimeout
        self.syncedToComputer = syncedToComputer
        self.maximumSharediPadUsersStored = maximumSharediPadUsersStored
        self.lastBackupDate = lastBackupDate
        self.deviceLocatorServiceEnabled = deviceLocatorServiceEnabled
        self.doNotDisturbEnabled = doNotDisturbEnabled
        self.cloudBackupEnabled = cloudBackupEnabled
        self.lastCloudBackupDate = lastCloudBackupDate
        self.locationServicesForSelfServiceMobileEnabled = locationServicesForSelfServiceMobileEnabled
        self.itunesStoreAccountActive = itunesStoreAccountActive
        self.exchangeDeviceId = exchangeDeviceId
        self.tethered = tethered
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case udid
        case displayName
        case assetTag
        case siteId
        case lastInventoryUpdateDate
        case osVersion
        case osRapidSecurityResponse
        case osBuild
        case osSupplementalBuildVersion
        case softwareUpdateDeviceId
        case ipAddress
        case managed
        case supervised
        case deviceOwnershipType
        case enrollmentMethodPrestage
        case enrollmentSessionTokenValid
        case lastEnrolledDate
        case mdmProfileExpirationDate
        case timeZone
        case declarativeDeviceManagementEnabled
        case extensionAttributes
        case sharedIpad
        case diagnosticAndUsageReportingEnabled
        case appAnalyticsEnabled
        case residentUsers
        case quotaSize
        case temporarySessionOnly
        case temporarySessionTimeout
        case userSessionTimeout
        case syncedToComputer
        case maximumSharediPadUsersStored
        case lastBackupDate
        case deviceLocatorServiceEnabled
        case doNotDisturbEnabled
        case cloudBackupEnabled
        case lastCloudBackupDate
        case locationServicesForSelfServiceMobileEnabled
        case itunesStoreAccountActive
        case exchangeDeviceId
        case tethered
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(udid, forKey: .udid)
        try container.encodeIfPresent(displayName, forKey: .displayName)
        try container.encodeIfPresent(assetTag, forKey: .assetTag)
        try container.encodeIfPresent(siteId, forKey: .siteId)
        try container.encodeIfPresent(lastInventoryUpdateDate, forKey: .lastInventoryUpdateDate)
        try container.encodeIfPresent(osVersion, forKey: .osVersion)
        try container.encodeIfPresent(osRapidSecurityResponse, forKey: .osRapidSecurityResponse)
        try container.encodeIfPresent(osBuild, forKey: .osBuild)
        try container.encodeIfPresent(osSupplementalBuildVersion, forKey: .osSupplementalBuildVersion)
        try container.encodeIfPresent(softwareUpdateDeviceId, forKey: .softwareUpdateDeviceId)
        try container.encodeIfPresent(ipAddress, forKey: .ipAddress)
        try container.encodeIfPresent(managed, forKey: .managed)
        try container.encodeIfPresent(supervised, forKey: .supervised)
        try container.encodeIfPresent(deviceOwnershipType, forKey: .deviceOwnershipType)
        try container.encodeIfPresent(enrollmentMethodPrestage, forKey: .enrollmentMethodPrestage)
        try container.encodeIfPresent(enrollmentSessionTokenValid, forKey: .enrollmentSessionTokenValid)
        try container.encodeIfPresent(lastEnrolledDate, forKey: .lastEnrolledDate)
        try container.encodeIfPresent(mdmProfileExpirationDate, forKey: .mdmProfileExpirationDate)
        try container.encodeIfPresent(timeZone, forKey: .timeZone)
        try container.encodeIfPresent(declarativeDeviceManagementEnabled, forKey: .declarativeDeviceManagementEnabled)
        try container.encodeIfPresent(extensionAttributes, forKey: .extensionAttributes)
        try container.encodeIfPresent(sharedIpad, forKey: .sharedIpad)
        try container.encodeIfPresent(diagnosticAndUsageReportingEnabled, forKey: .diagnosticAndUsageReportingEnabled)
        try container.encodeIfPresent(appAnalyticsEnabled, forKey: .appAnalyticsEnabled)
        try container.encodeIfPresent(residentUsers, forKey: .residentUsers)
        try container.encodeIfPresent(quotaSize, forKey: .quotaSize)
        try container.encodeIfPresent(temporarySessionOnly, forKey: .temporarySessionOnly)
        try container.encodeIfPresent(temporarySessionTimeout, forKey: .temporarySessionTimeout)
        try container.encodeIfPresent(userSessionTimeout, forKey: .userSessionTimeout)
        try container.encodeIfPresent(syncedToComputer, forKey: .syncedToComputer)
        try container.encodeIfPresent(maximumSharediPadUsersStored, forKey: .maximumSharediPadUsersStored)
        try container.encodeIfPresent(lastBackupDate, forKey: .lastBackupDate)
        try container.encodeIfPresent(deviceLocatorServiceEnabled, forKey: .deviceLocatorServiceEnabled)
        try container.encodeIfPresent(doNotDisturbEnabled, forKey: .doNotDisturbEnabled)
        try container.encodeIfPresent(cloudBackupEnabled, forKey: .cloudBackupEnabled)
        try container.encodeIfPresent(lastCloudBackupDate, forKey: .lastCloudBackupDate)
        try container.encodeIfPresent(locationServicesForSelfServiceMobileEnabled, forKey: .locationServicesForSelfServiceMobileEnabled)
        try container.encodeIfPresent(itunesStoreAccountActive, forKey: .itunesStoreAccountActive)
        try container.encodeIfPresent(exchangeDeviceId, forKey: .exchangeDeviceId)
        try container.encodeIfPresent(tethered, forKey: .tethered)
    }
}

