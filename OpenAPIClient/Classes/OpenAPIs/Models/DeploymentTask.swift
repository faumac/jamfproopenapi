//
// DeploymentTask.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct DeploymentTask: Codable, JSONEncodable, Hashable {

    public enum Status: String, Codable, CaseIterable {
        case commandQueued = "COMMAND_QUEUED"
        case noCommand = "NO_COMMAND"
        case pendingManifest = "PENDING_MANIFEST"
        case complete = "COMPLETE"
        case gaveUp = "GAVE_UP"
        case unknown = "UNKNOWN"
    }
    public var id: String?
    public var computerId: String?
    public var computerName: String?
    public var version: String?
    public var updated: String?
    /** Status of this Jamf Connect deployment task. \"Command\" below refers to an `InstallEnterpriseApplication` command. Tasks that are not finished (i.e., `COMPLETE` or `GAVE_UP`) are evaluated once every thirty minutes, so the status value for a device may lag behind a successful Jamf Connect package install up to thirty minutes. * `COMMAND_QUEUED` - command has been queued * `NO_COMMAND` - command has not yet been queued * `PENDING_MANIFEST` - task is waiting to obtain a valid package manifest before a command can be queued * `COMPLETE` - command has been completed successfully * `GAVE_UP` - the command failed with an error or the device did not process it in a reasonable amount of time * `UNKNOWN` - unknown; tasks in this state will be evaluated  */
    public var status: Status?

    public init(id: String? = nil, computerId: String? = nil, computerName: String? = nil, version: String? = nil, updated: String? = nil, status: Status? = nil) {
        self.id = id
        self.computerId = computerId
        self.computerName = computerName
        self.version = version
        self.updated = updated
        self.status = status
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case computerId
        case computerName
        case version
        case updated
        case status
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(computerId, forKey: .computerId)
        try container.encodeIfPresent(computerName, forKey: .computerName)
        try container.encodeIfPresent(version, forKey: .version)
        try container.encodeIfPresent(updated, forKey: .updated)
        try container.encodeIfPresent(status, forKey: .status)
    }
}

