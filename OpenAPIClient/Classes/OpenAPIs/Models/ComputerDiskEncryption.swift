//
// ComputerDiskEncryption.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ComputerDiskEncryption: Codable, JSONEncodable, Hashable {

    public enum IndividualRecoveryKeyValidityStatus: String, Codable, CaseIterable {
        case valid = "VALID"
        case invalid = "INVALID"
        case unknown = "UNKNOWN"
        case notApplicable = "NOT_APPLICABLE"
    }
    public var bootPartitionEncryptionDetails: ComputerPartitionEncryption?
    public var individualRecoveryKeyValidityStatus: IndividualRecoveryKeyValidityStatus?
    public var institutionalRecoveryKeyPresent: Bool?
    public var diskEncryptionConfigurationName: String?
    public var fileVault2EnabledUserNames: [String]?
    public var fileVault2EligibilityMessage: String?

    public init(bootPartitionEncryptionDetails: ComputerPartitionEncryption? = nil, individualRecoveryKeyValidityStatus: IndividualRecoveryKeyValidityStatus? = nil, institutionalRecoveryKeyPresent: Bool? = nil, diskEncryptionConfigurationName: String? = nil, fileVault2EnabledUserNames: [String]? = nil, fileVault2EligibilityMessage: String? = nil) {
        self.bootPartitionEncryptionDetails = bootPartitionEncryptionDetails
        self.individualRecoveryKeyValidityStatus = individualRecoveryKeyValidityStatus
        self.institutionalRecoveryKeyPresent = institutionalRecoveryKeyPresent
        self.diskEncryptionConfigurationName = diskEncryptionConfigurationName
        self.fileVault2EnabledUserNames = fileVault2EnabledUserNames
        self.fileVault2EligibilityMessage = fileVault2EligibilityMessage
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case bootPartitionEncryptionDetails
        case individualRecoveryKeyValidityStatus
        case institutionalRecoveryKeyPresent
        case diskEncryptionConfigurationName
        case fileVault2EnabledUserNames
        case fileVault2EligibilityMessage
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(bootPartitionEncryptionDetails, forKey: .bootPartitionEncryptionDetails)
        try container.encodeIfPresent(individualRecoveryKeyValidityStatus, forKey: .individualRecoveryKeyValidityStatus)
        try container.encodeIfPresent(institutionalRecoveryKeyPresent, forKey: .institutionalRecoveryKeyPresent)
        try container.encodeIfPresent(diskEncryptionConfigurationName, forKey: .diskEncryptionConfigurationName)
        try container.encodeIfPresent(fileVault2EnabledUserNames, forKey: .fileVault2EnabledUserNames)
        try container.encodeIfPresent(fileVault2EligibilityMessage, forKey: .fileVault2EligibilityMessage)
    }
}

