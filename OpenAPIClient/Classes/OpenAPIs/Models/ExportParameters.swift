//
// ExportParameters.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ExportParameters: Codable, JSONEncodable, Hashable {

    public var page: Int? = 0
    public var pageSize: Int? = 100
    /** Sorting criteria in the format: [<property>[:asc/desc]. Default direction when not stated is ascending. */
    public var sort: [String]?
    public var filter: String?
    /** Used to change default order or ignore some of the fields. When null or empty array, all fields will be exported. */
    public var fields: [ExportField]?

    public init(page: Int? = 0, pageSize: Int? = 100, sort: [String]? = nil, filter: String? = nil, fields: [ExportField]? = nil) {
        self.page = page
        self.pageSize = pageSize
        self.sort = sort
        self.filter = filter
        self.fields = fields
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case page
        case pageSize
        case sort
        case filter
        case fields
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(page, forKey: .page)
        try container.encodeIfPresent(pageSize, forKey: .pageSize)
        try container.encodeIfPresent(sort, forKey: .sort)
        try container.encodeIfPresent(filter, forKey: .filter)
        try container.encodeIfPresent(fields, forKey: .fields)
    }
}

