//
// VolumePurchasingLocation.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct VolumePurchasingLocation: Codable, JSONEncodable, Hashable {

    public var name: String?
    public var totalPurchasedLicenses: Int?
    public var totalUsedLicenses: Int?
    public var id: String?
    public var appleId: String?
    public var organizationName: String?
    public var tokenExpiration: String?
    /** The two-letter ISO 3166-1 code that designates the country where the Volume Purchasing account is located. */
    public var countryCode: String?
    public var locationName: String?
    /** If this is \"true\", the clientContext used by this server does not match the clientContext returned by the Volume Purchasing API. */
    public var clientContextMismatch: Bool?
    public var automaticallyPopulatePurchasedContent: Bool?
    public var sendNotificationWhenNoLongerAssigned: Bool?
    public var autoRegisterManagedUsers: Bool?
    public var siteId: String?
    public var lastSyncTime: String?
    public var content: [VolumePurchasingContent]?

    public init(name: String? = nil, totalPurchasedLicenses: Int? = nil, totalUsedLicenses: Int? = nil, id: String? = nil, appleId: String? = nil, organizationName: String? = nil, tokenExpiration: String? = nil, countryCode: String? = nil, locationName: String? = nil, clientContextMismatch: Bool? = nil, automaticallyPopulatePurchasedContent: Bool? = nil, sendNotificationWhenNoLongerAssigned: Bool? = nil, autoRegisterManagedUsers: Bool? = nil, siteId: String? = nil, lastSyncTime: String? = nil, content: [VolumePurchasingContent]? = nil) {
        self.name = name
        self.totalPurchasedLicenses = totalPurchasedLicenses
        self.totalUsedLicenses = totalUsedLicenses
        self.id = id
        self.appleId = appleId
        self.organizationName = organizationName
        self.tokenExpiration = tokenExpiration
        self.countryCode = countryCode
        self.locationName = locationName
        self.clientContextMismatch = clientContextMismatch
        self.automaticallyPopulatePurchasedContent = automaticallyPopulatePurchasedContent
        self.sendNotificationWhenNoLongerAssigned = sendNotificationWhenNoLongerAssigned
        self.autoRegisterManagedUsers = autoRegisterManagedUsers
        self.siteId = siteId
        self.lastSyncTime = lastSyncTime
        self.content = content
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case name
        case totalPurchasedLicenses
        case totalUsedLicenses
        case id
        case appleId
        case organizationName
        case tokenExpiration
        case countryCode
        case locationName
        case clientContextMismatch
        case automaticallyPopulatePurchasedContent
        case sendNotificationWhenNoLongerAssigned
        case autoRegisterManagedUsers
        case siteId
        case lastSyncTime
        case content
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(totalPurchasedLicenses, forKey: .totalPurchasedLicenses)
        try container.encodeIfPresent(totalUsedLicenses, forKey: .totalUsedLicenses)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(appleId, forKey: .appleId)
        try container.encodeIfPresent(organizationName, forKey: .organizationName)
        try container.encodeIfPresent(tokenExpiration, forKey: .tokenExpiration)
        try container.encodeIfPresent(countryCode, forKey: .countryCode)
        try container.encodeIfPresent(locationName, forKey: .locationName)
        try container.encodeIfPresent(clientContextMismatch, forKey: .clientContextMismatch)
        try container.encodeIfPresent(automaticallyPopulatePurchasedContent, forKey: .automaticallyPopulatePurchasedContent)
        try container.encodeIfPresent(sendNotificationWhenNoLongerAssigned, forKey: .sendNotificationWhenNoLongerAssigned)
        try container.encodeIfPresent(autoRegisterManagedUsers, forKey: .autoRegisterManagedUsers)
        try container.encodeIfPresent(siteId, forKey: .siteId)
        try container.encodeIfPresent(lastSyncTime, forKey: .lastSyncTime)
        try container.encodeIfPresent(content, forKey: .content)
    }
}

