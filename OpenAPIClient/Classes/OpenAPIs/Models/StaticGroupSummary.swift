//
// StaticGroupSummary.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct StaticGroupSummary: Codable, JSONEncodable, Hashable {

    static let countRule = NumericRule<Int>(minimum: 0, exclusiveMinimum: false, maximum: nil, exclusiveMaximum: false, multipleOf: nil)
    public var groupId: String?
    public var groupName: String?
    public var siteId: String?
    /** membership count */
    public var count: Int?

    public init(groupId: String? = nil, groupName: String? = nil, siteId: String? = nil, count: Int? = nil) {
        self.groupId = groupId
        self.groupName = groupName
        self.siteId = siteId
        self.count = count
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case groupId
        case groupName
        case siteId
        case count
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(groupId, forKey: .groupId)
        try container.encodeIfPresent(groupName, forKey: .groupName)
        try container.encodeIfPresent(siteId, forKey: .siteId)
        try container.encodeIfPresent(count, forKey: .count)
    }
}

