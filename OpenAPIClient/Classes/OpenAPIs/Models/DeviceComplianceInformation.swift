//
// DeviceComplianceInformation.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** Device compliance information record */
public struct DeviceComplianceInformation: Codable, JSONEncodable, Hashable {

    public enum ComplianceState: String, Codable, CaseIterable {
        case unknown = "UNKNOWN"
        case nonCompliant = "NON_COMPLIANT"
        case compliant = "COMPLIANT"
    }
    /** ID of the device */
    public var deviceId: String?
    /** If device is applicable for compliance calculation */
    public var applicable: Bool?
    /** Device compliance state. Possible values are: * `UNKNOWN` for unknow compliance state, this usually means that the compliance state is being calculated, * `NON_COMPLIANT` for non compliant state, * `COMPLIANT` for compliant state  */
    public var complianceState: ComplianceState?
    /** Name of the compliance vendor */
    public var complianceVendor: String?
    public var complianceVendorDeviceInformation: ComplianceVendorDeviceInformation?

    public init(deviceId: String? = nil, applicable: Bool? = nil, complianceState: ComplianceState? = nil, complianceVendor: String? = nil, complianceVendorDeviceInformation: ComplianceVendorDeviceInformation? = nil) {
        self.deviceId = deviceId
        self.applicable = applicable
        self.complianceState = complianceState
        self.complianceVendor = complianceVendor
        self.complianceVendorDeviceInformation = complianceVendorDeviceInformation
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case deviceId
        case applicable
        case complianceState
        case complianceVendor
        case complianceVendorDeviceInformation
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(deviceId, forKey: .deviceId)
        try container.encodeIfPresent(applicable, forKey: .applicable)
        try container.encodeIfPresent(complianceState, forKey: .complianceState)
        try container.encodeIfPresent(complianceVendor, forKey: .complianceVendor)
        try container.encodeIfPresent(complianceVendorDeviceInformation, forKey: .complianceVendorDeviceInformation)
    }
}

