//
// UpdateAppleTv.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct UpdateAppleTv: Codable, JSONEncodable, Hashable {

    public var airplayPassword: String?
    public var purchasing: Purchasing?

    public init(airplayPassword: String? = nil, purchasing: Purchasing? = nil) {
        self.airplayPassword = airplayPassword
        self.purchasing = purchasing
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case airplayPassword
        case purchasing
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(airplayPassword, forKey: .airplayPassword)
        try container.encodeIfPresent(purchasing, forKey: .purchasing)
    }
}

