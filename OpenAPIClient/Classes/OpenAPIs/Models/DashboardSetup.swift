//
// DashboardSetup.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** Response object that has lists of information for widgets, and what setup tasks to display. */
public struct DashboardSetup: Codable, JSONEncodable, Hashable {

    public var setupTaskOptions: DashboardSetupSetupTaskOptions?
    public var featureOptions: DashboardSetupFeatureOptions?

    public init(setupTaskOptions: DashboardSetupSetupTaskOptions? = nil, featureOptions: DashboardSetupFeatureOptions? = nil) {
        self.setupTaskOptions = setupTaskOptions
        self.featureOptions = featureOptions
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case setupTaskOptions
        case featureOptions
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(setupTaskOptions, forKey: .setupTaskOptions)
        try container.encodeIfPresent(featureOptions, forKey: .featureOptions)
    }
}

