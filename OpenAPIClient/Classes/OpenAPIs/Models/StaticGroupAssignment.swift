//
// StaticGroupAssignment.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct StaticGroupAssignment: Codable, JSONEncodable, Hashable {

    static let groupIdRule = StringRule(minLength: 1, maxLength: nil, pattern: nil)
    public var groupId: String?
    public var groupName: String?
    public var siteId: String?
    public var assignments: [Assignment]?

    public init(groupId: String? = nil, groupName: String? = nil, siteId: String? = nil, assignments: [Assignment]? = nil) {
        self.groupId = groupId
        self.groupName = groupName
        self.siteId = siteId
        self.assignments = assignments
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case groupId
        case groupName
        case siteId
        case assignments
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(groupId, forKey: .groupId)
        try container.encodeIfPresent(groupName, forKey: .groupName)
        try container.encodeIfPresent(siteId, forKey: .siteId)
        try container.encodeIfPresent(assignments, forKey: .assignments)
    }
}

