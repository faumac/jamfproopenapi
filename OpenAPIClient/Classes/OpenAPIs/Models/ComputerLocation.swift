//
// ComputerLocation.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ComputerLocation: Codable, JSONEncodable, Hashable {

    public var username: String?
    public var realname: String?
    public var real_name: String?
    public var email_address: String?
    public var position: String?
    public var phone: String?
    public var phone_number: String?
    public var department: String?
    public var building: String?
    public var room: String?

    public init(username: String? = nil, position: String? = nil, email_address: String? = nil, realname: String? = nil, real_name: String? = nil, phone: String? = nil, phone_number: String? = nil, department: String? = nil, building: String? = nil, room: String? = nil) {
        self.username = username
        self.realname = realname
        self.real_name = real_name
        self.email_address = email_address
        self.position = position
        self.phone = phone
        self.phone_number = phone_number
        self.department = department
        self.building = building
        self.room = room
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case username
        case realname
        case real_name
        case email_address
        case position
        case phone
        case phone_number
        case department
        case building
        case room
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(username, forKey: .username)
        try container.encodeIfPresent(position, forKey: .position)
        try container.encodeIfPresent(realname, forKey: .realname)
        try container.encodeIfPresent(real_name, forKey: .real_name)
        try container.encodeIfPresent(email_address, forKey: .email_address)
        try container.encodeIfPresent(phone, forKey: .phone)
        try container.encodeIfPresent(phone_number, forKey: .phone_number)
        try container.encodeIfPresent(department, forKey: .department)
        try container.encodeIfPresent(building, forKey: .building)
        try container.encodeIfPresent(room, forKey: .room)
    }
}

