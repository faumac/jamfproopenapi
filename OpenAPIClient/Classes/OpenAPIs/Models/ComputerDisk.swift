//
// ComputerDisk.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct ComputerDisk: Codable, JSONEncodable, Hashable {

    public var id: String?
    public var device: String?
    public var model: String?
    public var revision: String?
    public var serialNumber: String?
    /** Disk Size in MB. */
    public var sizeMegabytes: Int64?
    /** S.M.A.R.T Status */
    public var smartStatus: String?
    /** Connection type attribute. */
    public var type: String?
    public var partitions: [ComputerPartition]?

    public init(id: String? = nil, device: String? = nil, model: String? = nil, revision: String? = nil, serialNumber: String? = nil, sizeMegabytes: Int64? = nil, smartStatus: String? = nil, type: String? = nil, partitions: [ComputerPartition]? = nil) {
        self.id = id
        self.device = device
        self.model = model
        self.revision = revision
        self.serialNumber = serialNumber
        self.sizeMegabytes = sizeMegabytes
        self.smartStatus = smartStatus
        self.type = type
        self.partitions = partitions
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case device
        case model
        case revision
        case serialNumber
        case sizeMegabytes
        case smartStatus
        case type
        case partitions
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(device, forKey: .device)
        try container.encodeIfPresent(model, forKey: .model)
        try container.encodeIfPresent(revision, forKey: .revision)
        try container.encodeIfPresent(serialNumber, forKey: .serialNumber)
        try container.encodeIfPresent(sizeMegabytes, forKey: .sizeMegabytes)
        try container.encodeIfPresent(smartStatus, forKey: .smartStatus)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(partitions, forKey: .partitions)
    }
}

