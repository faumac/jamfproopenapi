//
// InventoryPreloadRecord.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct InventoryPreloadRecord: Codable, JSONEncodable, Hashable {

    public enum DeviceType: String, Codable, CaseIterable {
        case computer = "Computer"
        case mobileDevice = "Mobile Device"
        case unknown = "Unknown"
    }
    public var id: Int?
    public var serialNumber: String
    public var deviceType: DeviceType
    public var username: String?
    public var fullName: String?
    public var emailAddress: String?
    public var phoneNumber: String?
    public var position: String?
    public var department: String?
    public var building: String?
    public var room: String?
    public var poNumber: String?
    public var poDate: String?
    public var warrantyExpiration: String?
    public var appleCareId: String?
    public var lifeExpectancy: String?
    public var purchasePrice: String?
    public var purchasingContact: String?
    public var purchasingAccount: String?
    public var leaseExpiration: String?
    public var barCode1: String?
    public var barCode2: String?
    public var assetTag: String?
    public var vendor: String?
    public var extensionAttributes: [InventoryPreloadExtensionAttribute]?

    public init(id: Int? = nil, serialNumber: String, deviceType: DeviceType, username: String? = nil, fullName: String? = nil, emailAddress: String? = nil, phoneNumber: String? = nil, position: String? = nil, department: String? = nil, building: String? = nil, room: String? = nil, poNumber: String? = nil, poDate: String? = nil, warrantyExpiration: String? = nil, appleCareId: String? = nil, lifeExpectancy: String? = nil, purchasePrice: String? = nil, purchasingContact: String? = nil, purchasingAccount: String? = nil, leaseExpiration: String? = nil, barCode1: String? = nil, barCode2: String? = nil, assetTag: String? = nil, vendor: String? = nil, extensionAttributes: [InventoryPreloadExtensionAttribute]? = nil) {
        self.id = id
        self.serialNumber = serialNumber
        self.deviceType = deviceType
        self.username = username
        self.fullName = fullName
        self.emailAddress = emailAddress
        self.phoneNumber = phoneNumber
        self.position = position
        self.department = department
        self.building = building
        self.room = room
        self.poNumber = poNumber
        self.poDate = poDate
        self.warrantyExpiration = warrantyExpiration
        self.appleCareId = appleCareId
        self.lifeExpectancy = lifeExpectancy
        self.purchasePrice = purchasePrice
        self.purchasingContact = purchasingContact
        self.purchasingAccount = purchasingAccount
        self.leaseExpiration = leaseExpiration
        self.barCode1 = barCode1
        self.barCode2 = barCode2
        self.assetTag = assetTag
        self.vendor = vendor
        self.extensionAttributes = extensionAttributes
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case serialNumber
        case deviceType
        case username
        case fullName
        case emailAddress
        case phoneNumber
        case position
        case department
        case building
        case room
        case poNumber
        case poDate
        case warrantyExpiration
        case appleCareId
        case lifeExpectancy
        case purchasePrice
        case purchasingContact
        case purchasingAccount
        case leaseExpiration
        case barCode1
        case barCode2
        case assetTag
        case vendor
        case extensionAttributes
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encode(serialNumber, forKey: .serialNumber)
        try container.encode(deviceType, forKey: .deviceType)
        try container.encodeIfPresent(username, forKey: .username)
        try container.encodeIfPresent(fullName, forKey: .fullName)
        try container.encodeIfPresent(emailAddress, forKey: .emailAddress)
        try container.encodeIfPresent(phoneNumber, forKey: .phoneNumber)
        try container.encodeIfPresent(position, forKey: .position)
        try container.encodeIfPresent(department, forKey: .department)
        try container.encodeIfPresent(building, forKey: .building)
        try container.encodeIfPresent(room, forKey: .room)
        try container.encodeIfPresent(poNumber, forKey: .poNumber)
        try container.encodeIfPresent(poDate, forKey: .poDate)
        try container.encodeIfPresent(warrantyExpiration, forKey: .warrantyExpiration)
        try container.encodeIfPresent(appleCareId, forKey: .appleCareId)
        try container.encodeIfPresent(lifeExpectancy, forKey: .lifeExpectancy)
        try container.encodeIfPresent(purchasePrice, forKey: .purchasePrice)
        try container.encodeIfPresent(purchasingContact, forKey: .purchasingContact)
        try container.encodeIfPresent(purchasingAccount, forKey: .purchasingAccount)
        try container.encodeIfPresent(leaseExpiration, forKey: .leaseExpiration)
        try container.encodeIfPresent(barCode1, forKey: .barCode1)
        try container.encodeIfPresent(barCode2, forKey: .barCode2)
        try container.encodeIfPresent(assetTag, forKey: .assetTag)
        try container.encodeIfPresent(vendor, forKey: .vendor)
        try container.encodeIfPresent(extensionAttributes, forKey: .extensionAttributes)
    }
}

