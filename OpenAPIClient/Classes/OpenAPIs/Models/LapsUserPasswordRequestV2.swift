//
// LapsUserPasswordRequestV2.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct LapsUserPasswordRequestV2: Codable, JSONEncodable, Hashable {

    public var lapsUserPasswordList: [LapsUserPasswordV2]?

    public init(lapsUserPasswordList: [LapsUserPasswordV2]? = nil) {
        self.lapsUserPasswordList = lapsUserPasswordList
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case lapsUserPasswordList
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(lapsUserPasswordList, forKey: .lapsUserPasswordList)
    }
}

