//
// VolumePurchasingSubscription.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct VolumePurchasingSubscription: Codable, JSONEncodable, Hashable {

    public enum Triggers: String, Codable, CaseIterable {
        case noMoreLicenses = "NO_MORE_LICENSES"
        case removedFromAppStore = "REMOVED_FROM_APP_STORE"
    }
    static let nameRule = StringRule(minLength: 1, maxLength: nil, pattern: nil)
    public var name: String
    public var enabled: Bool? = true
    public var triggers: [Triggers]?
    public var locationIds: [String]?
    public var internalRecipients: [InternalRecipient]?
    public var externalRecipients: [ExternalRecipient]?
    public var siteId: String? = "-1"
    public var id: String?

    public init(name: String, enabled: Bool? = true, triggers: [Triggers]? = nil, locationIds: [String]? = nil, internalRecipients: [InternalRecipient]? = nil, externalRecipients: [ExternalRecipient]? = nil, siteId: String? = "-1", id: String? = nil) {
        self.name = name
        self.enabled = enabled
        self.triggers = triggers
        self.locationIds = locationIds
        self.internalRecipients = internalRecipients
        self.externalRecipients = externalRecipients
        self.siteId = siteId
        self.id = id
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case name
        case enabled
        case triggers
        case locationIds
        case internalRecipients
        case externalRecipients
        case siteId
        case id
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encodeIfPresent(enabled, forKey: .enabled)
        try container.encodeIfPresent(triggers, forKey: .triggers)
        try container.encodeIfPresent(locationIds, forKey: .locationIds)
        try container.encodeIfPresent(internalRecipients, forKey: .internalRecipients)
        try container.encodeIfPresent(externalRecipients, forKey: .externalRecipients)
        try container.encodeIfPresent(siteId, forKey: .siteId)
        try container.encodeIfPresent(id, forKey: .id)
    }
}

