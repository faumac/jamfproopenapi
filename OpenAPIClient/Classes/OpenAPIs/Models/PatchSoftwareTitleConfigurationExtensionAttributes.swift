//
// PatchSoftwareTitleConfigurationExtensionAttributes.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PatchSoftwareTitleConfigurationExtensionAttributes: Codable, JSONEncodable, Hashable {

    /** Once an extension attribute is accepted, it cannot be reverted. */
    public var accepted: Bool? = false
    public var eaId: String?

    public init(accepted: Bool? = false, eaId: String? = nil) {
        self.accepted = accepted
        self.eaId = eaId
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case accepted
        case eaId
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(accepted, forKey: .accepted)
        try container.encodeIfPresent(eaId, forKey: .eaId)
    }
}

