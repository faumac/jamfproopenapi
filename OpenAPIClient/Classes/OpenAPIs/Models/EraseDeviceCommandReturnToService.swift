//
// EraseDeviceCommandReturnToService.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

/** The configuration settings for Return to Service. */
public struct EraseDeviceCommandReturnToService: Codable, JSONEncodable, Hashable {

    public var enabled: Bool?
    /** Base64 encoded mdm profile */
    public var mdmProfileData: String?
    /** Base64 encoded wifi profile */
    public var wifiProfileData: String?

    public init(enabled: Bool? = nil, mdmProfileData: String? = nil, wifiProfileData: String? = nil) {
        self.enabled = enabled
        self.mdmProfileData = mdmProfileData
        self.wifiProfileData = wifiProfileData
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case enabled
        case mdmProfileData
        case wifiProfileData
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(enabled, forKey: .enabled)
        try container.encodeIfPresent(mdmProfileData, forKey: .mdmProfileData)
        try container.encodeIfPresent(wifiProfileData, forKey: .wifiProfileData)
    }
}

