//
// MdmCommand.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MdmCommand: Codable, JSONEncodable, Hashable {

    public var uuid: String?
    public var dateSent: String?
    public var client: MdmCommandClient?
    public var commandState: MdmCommandState?
    public var commandType: MdmCommandType?

    public init(uuid: String? = nil, dateSent: String? = nil, client: MdmCommandClient? = nil, commandState: MdmCommandState? = nil, commandType: MdmCommandType? = nil) {
        self.uuid = uuid
        self.dateSent = dateSent
        self.client = client
        self.commandState = commandState
        self.commandType = commandType
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case uuid
        case dateSent
        case client
        case commandState
        case commandType
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(uuid, forKey: .uuid)
        try container.encodeIfPresent(dateSent, forKey: .dateSent)
        try container.encodeIfPresent(client, forKey: .client)
        try container.encodeIfPresent(commandState, forKey: .commandState)
        try container.encodeIfPresent(commandType, forKey: .commandType)
    }
}

