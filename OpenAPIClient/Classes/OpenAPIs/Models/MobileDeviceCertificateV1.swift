//
// MobileDeviceCertificateV1.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MobileDeviceCertificateV1: Codable, JSONEncodable, Hashable {

    public var commonName: String?
    public var isIdentity: Bool?

    public init(commonName: String? = nil, isIdentity: Bool? = nil) {
        self.commonName = commonName
        self.isIdentity = isIdentity
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case commonName
        case isIdentity
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(commonName, forKey: .commonName)
        try container.encodeIfPresent(isIdentity, forKey: .isIdentity)
    }
}

