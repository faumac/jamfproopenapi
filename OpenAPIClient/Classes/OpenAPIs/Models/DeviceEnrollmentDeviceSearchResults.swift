//
// DeviceEnrollmentDeviceSearchResults.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct DeviceEnrollmentDeviceSearchResults: Codable, JSONEncodable, Hashable {

    static let totalCountRule = NumericRule<Int>(minimum: 0, exclusiveMinimum: false, maximum: nil, exclusiveMaximum: false, multipleOf: nil)
    public var totalCount: Int?
    public var results: [DeviceEnrollmentDevice]?

    public init(totalCount: Int? = nil, results: [DeviceEnrollmentDevice]? = nil) {
        self.totalCount = totalCount
        self.results = results
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case totalCount
        case results
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(totalCount, forKey: .totalCount)
        try container.encodeIfPresent(results, forKey: .results)
    }
}

