//
// StartupStatus.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct StartupStatus: Codable, JSONEncodable, Hashable {

    public enum StepCode: String, Codable, CaseIterable {
        case start = "SERVER_INIT_START"
        case analyzingWebapp = "SERVER_INIT_ANALYZING_WEBAPP"
        case populatingNavigation = "SERVER_INIT_POPULATING_NAVIGATION"
        case populatingObjects = "SERVER_INIT_POPULATING_OBJECTS"
        case initializingObj = "SERVER_INIT_INITIALIZING_OBJ"
        case verifyingCache = "SERVER_INIT_VERIFYING_CACHE"
        case initializingChangeManagement = "SERVER_INIT_INITIALIZING_CHANGE_MANAGEMENT"
        case initializingCommunicationSystem = "SERVER_INIT_INITIALIZING_COMMUNICATION_SYSTEM"
        case initializingMdmQueueMonitor = "SERVER_INIT_INITIALIZING_MDM_QUEUE_MONITOR"
        case calculatingSmartGroups = "SERVER_INIT_CALCULATING_SMART_GROUPS"
        case dbSchemaCompare = "SERVER_INIT_DB_SCHEMA_COMPARE"
        case dbTableCheckForRename = "SERVER_INIT_DB_TABLE_CHECK_FOR_RENAME"
        case dbTableAlter = "SERVER_INIT_DB_TABLE_ALTER"
        case dbTableAnalyzing = "SERVER_INIT_DB_TABLE_ANALYZING"
        case dbTableCreate = "SERVER_INIT_DB_TABLE_CREATE"
        case dbTableDrop = "SERVER_INIT_DB_TABLE_DROP"
        case dbTableRename = "SERVER_INIT_DB_TABLE_RENAME"
        case dbColumnRename = "SERVER_INIT_DB_COLUMN_RENAME"
        case dbColumnEncodingChangeStep1 = "SERVER_INIT_DB_COLUMN_ENCODING_CHANGE_STEP_1"
        case dbColumnEncodingChangeStep2 = "SERVER_INIT_DB_COLUMN_ENCODING_CHANGE_STEP_2"
        case dbColumnEncodingChangeStep3 = "SERVER_INIT_DB_COLUMN_ENCODING_CHANGE_STEP_3"
        case dbUpgradeCheck = "SERVER_INIT_DB_UPGRADE_CHECK"
        case dbUpgradeComplete = "SERVER_INIT_DB_UPGRADE_COMPLETE"
        case ssGenerateNotifications = "SERVER_INIT_SS_GENERATE_NOTIFICATIONS"
        case ssGenerateNotificationsStatus = "SERVER_INIT_SS_GENERATE_NOTIFICATIONS_STATUS"
        case ssGenerateNotificationsFinalize = "SERVER_INIT_SS_GENERATE_NOTIFICATIONS_FINALIZE"
        case pkiMigrationDone = "SERVER_INIT_PKI_MIGRATION_DONE"
        case pkiMigrationStatus = "SERVER_INIT_PKI_MIGRATION_STATUS"
        case memcachedEndpointsCheck = "SERVER_INIT_MEMCACHED_ENDPOINTS_CHECK"
        case cacheFlushing = "SERVER_INIT_CACHE_FLUSHING"
        case complete = "SERVER_INIT_COMPLETE"
    }
    public enum WarningCode: String, Codable, CaseIterable {
        case serverInitWarningDbTableEncoding = "SERVER_INIT_WARNING_DB_TABLE_ENCODING"
    }
    public enum ErrorCode: String, Codable, CaseIterable {
        case cacheConfigurationError = "CACHE_CONFIGURATION_ERROR"
        case secondaryNodeStartupError = "SECONDARY_NODE_STARTUP_ERROR"
        case moreThanOneClusterSettingsError = "MORE_THAN_ONE_CLUSTER_SETTINGS_ERROR"
        case primaryNodeNotSetError = "PRIMARY_NODE_NOT_SET_ERROR"
        case databaseError = "DATABASE_ERROR"
        case databasePasswordMissing = "DATABASE_PASSWORD_MISSING"
        case ehcacheError = "EHCACHE_ERROR"
        case flagInitializationFailed = "FLAG_INITIALIZATION_FAILED"
        case memcachedError = "MEMCACHED_ERROR"
        case databaseMyisamError = "DATABASE_MYISAM_ERROR"
        case oldVersionError = "OLD_VERSION_ERROR"
    }
    public var step: String?
    public var stepCode: StepCode?
    public var stepParam: String?
    public var percentage: Int?
    public var warning: String?
    public var warningCode: WarningCode?
    public var warningParam: String?
    public var error: String?
    public var errorCode: ErrorCode?
    public var setupAssistantNecessary: Bool?

    public init(step: String? = nil, stepCode: StepCode? = nil, stepParam: String? = nil, percentage: Int? = nil, warning: String? = nil, warningCode: WarningCode? = nil, warningParam: String? = nil, error: String? = nil, errorCode: ErrorCode? = nil, setupAssistantNecessary: Bool? = nil) {
        self.step = step
        self.stepCode = stepCode
        self.stepParam = stepParam
        self.percentage = percentage
        self.warning = warning
        self.warningCode = warningCode
        self.warningParam = warningParam
        self.error = error
        self.errorCode = errorCode
        self.setupAssistantNecessary = setupAssistantNecessary
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case step
        case stepCode
        case stepParam
        case percentage
        case warning
        case warningCode
        case warningParam
        case error
        case errorCode
        case setupAssistantNecessary
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(step, forKey: .step)
        try container.encodeIfPresent(stepCode, forKey: .stepCode)
        try container.encodeIfPresent(stepParam, forKey: .stepParam)
        try container.encodeIfPresent(percentage, forKey: .percentage)
        try container.encodeIfPresent(warning, forKey: .warning)
        try container.encodeIfPresent(warningCode, forKey: .warningCode)
        try container.encodeIfPresent(warningParam, forKey: .warningParam)
        try container.encodeIfPresent(error, forKey: .error)
        try container.encodeIfPresent(errorCode, forKey: .errorCode)
        try container.encodeIfPresent(setupAssistantNecessary, forKey: .setupAssistantNecessary)
    }
}

