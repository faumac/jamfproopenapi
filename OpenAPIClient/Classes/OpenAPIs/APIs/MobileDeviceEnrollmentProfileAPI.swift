//
// MobileDeviceEnrollmentProfileAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

open class MobileDeviceEnrollmentProfileAPI {

    /**
     Retrieve the MDM Enrollment Profile 
     
     - parameter id: (path) MDM Enrollment Profile identifier 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1MobileDeviceEnrollmentProfileIdDownloadProfileGet(id: String, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: URL?, _ error: Error?) -> Void)) -> RequestTask {
        return v1MobileDeviceEnrollmentProfileIdDownloadProfileGetWithRequestBuilder(id: id).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Retrieve the MDM Enrollment Profile 
     - GET /v1/mobile-device-enrollment-profile/{id}/download-profile
     - Retrieve the MDM Enrollment Profile
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter id: (path) MDM Enrollment Profile identifier 
     - returns: RequestBuilder<URL> 
     */
    open class func v1MobileDeviceEnrollmentProfileIdDownloadProfileGetWithRequestBuilder(id: String) -> RequestBuilder<URL> {
        var localVariablePath = "/v1/mobile-device-enrollment-profile/{id}/download-profile"
        let idPreEscape = "\(APIHelper.mapValueToPathItem(id))"
        let idPostEscape = idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{id}", with: idPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<URL>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }
}
