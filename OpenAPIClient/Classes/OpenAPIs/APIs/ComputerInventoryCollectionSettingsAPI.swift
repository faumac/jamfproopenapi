//
// ComputerInventoryCollectionSettingsAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

open class ComputerInventoryCollectionSettingsAPI {

    /**
     Delete Custom Path from Computer Inventory Collection Settings
     
     - parameter id: (path) id of Custom Path 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1ComputerInventoryCollectionSettingsCustomPathIdDelete(id: String, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Void?, _ error: Error?) -> Void)) -> RequestTask {
        return v1ComputerInventoryCollectionSettingsCustomPathIdDeleteWithRequestBuilder(id: id).execute(apiResponseQueue) { result in
            switch result {
            case .success:
                completion((), nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Delete Custom Path from Computer Inventory Collection Settings
     - DELETE /v1/computer-inventory-collection-settings/custom-path/{id}
     - Delete Custom Path from Computer Inventory Collection Settings
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter id: (path) id of Custom Path 
     - returns: RequestBuilder<Void> 
     */
    open class func v1ComputerInventoryCollectionSettingsCustomPathIdDeleteWithRequestBuilder(id: String) -> RequestBuilder<Void> {
        var localVariablePath = "/v1/computer-inventory-collection-settings/custom-path/{id}"
        let idPreEscape = "\(APIHelper.mapValueToPathItem(id))"
        let idPostEscape = idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{id}", with: idPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Void>.Type = OpenAPIClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return localVariableRequestBuilder.init(method: "DELETE", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Create Computer Inventory Collection Settings Custom Path
     
     - parameter createPath: (body) Computer inventory settings to update 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1ComputerInventoryCollectionSettingsCustomPathPost(createPath: CreatePath, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: HrefResponse?, _ error: Error?) -> Void)) -> RequestTask {
        return v1ComputerInventoryCollectionSettingsCustomPathPostWithRequestBuilder(createPath: createPath).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Create Computer Inventory Collection Settings Custom Path
     - POST /v1/computer-inventory-collection-settings/custom-path
     - Creates a custom search path to use when collecting applications, fonts, and plug-ins.
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter createPath: (body) Computer inventory settings to update 
     - returns: RequestBuilder<HrefResponse> 
     */
    open class func v1ComputerInventoryCollectionSettingsCustomPathPostWithRequestBuilder(createPath: CreatePath) -> RequestBuilder<HrefResponse> {
        let localVariablePath = "/v1/computer-inventory-collection-settings/custom-path"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters = JSONEncodingHelper.encodingParameters(forEncodableObject: createPath)

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<HrefResponse>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "POST", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Returns computer inventory settings
     
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1ComputerInventoryCollectionSettingsGet(apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: ComputerInventoryCollectionSettings?, _ error: Error?) -> Void)) -> RequestTask {
        return v1ComputerInventoryCollectionSettingsGetWithRequestBuilder().execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Returns computer inventory settings
     - GET /v1/computer-inventory-collection-settings
     - Returns computer inventory settings
     - Bearer Token:
       - type: http
       - name: Bearer
     - returns: RequestBuilder<ComputerInventoryCollectionSettings> 
     */
    open class func v1ComputerInventoryCollectionSettingsGetWithRequestBuilder() -> RequestBuilder<ComputerInventoryCollectionSettings> {
        let localVariablePath = "/v1/computer-inventory-collection-settings"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<ComputerInventoryCollectionSettings>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Update computer inventory settings
     
     - parameter computerInventoryCollectionSettings: (body) Computer inventory settings to update 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1ComputerInventoryCollectionSettingsPatch(computerInventoryCollectionSettings: ComputerInventoryCollectionSettings, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: ComputerInventoryCollectionSettings?, _ error: Error?) -> Void)) -> RequestTask {
        return v1ComputerInventoryCollectionSettingsPatchWithRequestBuilder(computerInventoryCollectionSettings: computerInventoryCollectionSettings).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Update computer inventory settings
     - PATCH /v1/computer-inventory-collection-settings
     - Update computer inventory settings
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter computerInventoryCollectionSettings: (body) Computer inventory settings to update 
     - returns: RequestBuilder<ComputerInventoryCollectionSettings> 
     */
    open class func v1ComputerInventoryCollectionSettingsPatchWithRequestBuilder(computerInventoryCollectionSettings: ComputerInventoryCollectionSettings) -> RequestBuilder<ComputerInventoryCollectionSettings> {
        let localVariablePath = "/v1/computer-inventory-collection-settings"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters = JSONEncodingHelper.encodingParameters(forEncodableObject: computerInventoryCollectionSettings)

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<ComputerInventoryCollectionSettings>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "PATCH", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }
}
