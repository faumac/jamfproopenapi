//
// RemoteAdministrationAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

open class RemoteAdministrationAPI {

    /**
     Get information about all remote administration configurations.
     
     - parameter page: (query)  (optional, default to 0)
     - parameter pageSize: (query)  (optional, default to 100)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func previewRemoteAdministrationConfigurationsGet(page: Int? = nil, pageSize: Int? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: RemoteAdministrationSearchResults?, _ error: Error?) -> Void)) -> RequestTask {
        return previewRemoteAdministrationConfigurationsGetWithRequestBuilder(page: page, pageSize: pageSize).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Get information about all remote administration configurations.
     - GET /preview/remote-administration-configurations
     - Remote administration feature creates a secure screen-sharing experience between Jamf Pro administrators and their end-users.
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter page: (query)  (optional, default to 0)
     - parameter pageSize: (query)  (optional, default to 100)
     - returns: RequestBuilder<RemoteAdministrationSearchResults> 
     */
    open class func previewRemoteAdministrationConfigurationsGetWithRequestBuilder(page: Int? = nil, pageSize: Int? = nil) -> RequestBuilder<RemoteAdministrationSearchResults> {
        let localVariablePath = "/preview/remote-administration-configurations"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        var localVariableUrlComponents = URLComponents(string: localVariableURLString)
        localVariableUrlComponents?.queryItems = APIHelper.mapValuesToQueryItems([
            "page": (wrappedValue: page?.encodeToJSON(), isExplode: true),
            "page-size": (wrappedValue: pageSize?.encodeToJSON(), isExplode: true),
        ])

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<RemoteAdministrationSearchResults>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }
}
