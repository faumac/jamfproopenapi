//
// JamfProUserAccountSettingsAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

open class JamfProUserAccountSettingsAPI {

    /**
     Remove specified setting for authenticated user 
     
     - parameter keyId: (path) unique key of user setting to be persisted 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1UserPreferencesKeyIdDelete(keyId: String, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Void?, _ error: Error?) -> Void)) -> RequestTask {
        return v1UserPreferencesKeyIdDeleteWithRequestBuilder(keyId: keyId).execute(apiResponseQueue) { result in
            switch result {
            case .success:
                completion((), nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Remove specified setting for authenticated user 
     - DELETE /v1/user/preferences/{keyId}
     - Remove specified setting for authenticated user 
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter keyId: (path) unique key of user setting to be persisted 
     - returns: RequestBuilder<Void> 
     */
    open class func v1UserPreferencesKeyIdDeleteWithRequestBuilder(keyId: String) -> RequestBuilder<Void> {
        var localVariablePath = "/v1/user/preferences/{keyId}"
        let keyIdPreEscape = "\(APIHelper.mapValueToPathItem(keyId))"
        let keyIdPostEscape = keyIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{keyId}", with: keyIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Void>.Type = OpenAPIClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return localVariableRequestBuilder.init(method: "DELETE", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Get the user setting for the authenticated user and key 
     
     - parameter keyId: (path) user setting to be retrieved 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1UserPreferencesKeyIdGet(keyId: String, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: AnyCodable?, _ error: Error?) -> Void)) -> RequestTask {
        return v1UserPreferencesKeyIdGetWithRequestBuilder(keyId: keyId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Get the user setting for the authenticated user and key 
     - GET /v1/user/preferences/{keyId}
     - Gets the user setting for the authenticated user and key. 
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter keyId: (path) user setting to be retrieved 
     - returns: RequestBuilder<AnyCodable> 
     */
    open class func v1UserPreferencesKeyIdGetWithRequestBuilder(keyId: String) -> RequestBuilder<AnyCodable> {
        var localVariablePath = "/v1/user/preferences/{keyId}"
        let keyIdPreEscape = "\(APIHelper.mapValueToPathItem(keyId))"
        let keyIdPostEscape = keyIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{keyId}", with: keyIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<AnyCodable>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Persist the user setting 
     
     - parameter keyId: (path) unique key of user setting to be persisted 
     - parameter body: (body) user setting value to be persisted (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1UserPreferencesKeyIdPut(keyId: String, body: AnyCodable? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: AnyCodable?, _ error: Error?) -> Void)) -> RequestTask {
        return v1UserPreferencesKeyIdPutWithRequestBuilder(keyId: keyId, body: body).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Persist the user setting 
     - PUT /v1/user/preferences/{keyId}
     - Persists the user setting 
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter keyId: (path) unique key of user setting to be persisted 
     - parameter body: (body) user setting value to be persisted (optional)
     - returns: RequestBuilder<AnyCodable> 
     */
    open class func v1UserPreferencesKeyIdPutWithRequestBuilder(keyId: String, body: AnyCodable? = nil) -> RequestBuilder<AnyCodable> {
        var localVariablePath = "/v1/user/preferences/{keyId}"
        let keyIdPreEscape = "\(APIHelper.mapValueToPathItem(keyId))"
        let keyIdPostEscape = keyIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{keyId}", with: keyIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters = JSONEncodingHelper.encodingParameters(forEncodableObject: body)

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<AnyCodable>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "PUT", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Get the user preferences for the authenticated user and key. 
     
     - parameter keyId: (path) user setting to be retrieved 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1UserPreferencesSettingsKeyIdGet(keyId: String, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: UserPreferencesSettings?, _ error: Error?) -> Void)) -> RequestTask {
        return v1UserPreferencesSettingsKeyIdGetWithRequestBuilder(keyId: keyId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Get the user preferences for the authenticated user and key. 
     - GET /v1/user/preferences/settings/{keyId}
     - Gets the user preferences for the authenticated user and key. 
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter keyId: (path) user setting to be retrieved 
     - returns: RequestBuilder<UserPreferencesSettings> 
     */
    open class func v1UserPreferencesSettingsKeyIdGetWithRequestBuilder(keyId: String) -> RequestBuilder<UserPreferencesSettings> {
        var localVariablePath = "/v1/user/preferences/settings/{keyId}"
        let keyIdPreEscape = "\(APIHelper.mapValueToPathItem(keyId))"
        let keyIdPostEscape = keyIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{keyId}", with: keyIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<UserPreferencesSettings>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }
}
