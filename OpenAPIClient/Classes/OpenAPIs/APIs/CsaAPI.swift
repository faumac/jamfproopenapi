//
// CsaAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

open class CsaAPI {

    /**
     Delete the CSA token exchange - This will disable Jamf Pro's ability to authenticate with cloud-hosted services 
     
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1CsaTokenDelete(apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Void?, _ error: Error?) -> Void)) -> RequestTask {
        return v1CsaTokenDeleteWithRequestBuilder().execute(apiResponseQueue) { result in
            switch result {
            case .success:
                completion((), nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Delete the CSA token exchange - This will disable Jamf Pro's ability to authenticate with cloud-hosted services 
     - DELETE /v1/csa/token
     - Delete the CSA token exchange - This will disable Jamf Pro's ability to authenticate with cloud-hosted services 
     - Bearer Token:
       - type: http
       - name: Bearer
     - returns: RequestBuilder<Void> 
     */
    open class func v1CsaTokenDeleteWithRequestBuilder() -> RequestBuilder<Void> {
        let localVariablePath = "/v1/csa/token"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Void>.Type = OpenAPIClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return localVariableRequestBuilder.init(method: "DELETE", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Get details regarding the CSA token exchange 
     
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1CsaTokenGet(apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: CsaToken?, _ error: Error?) -> Void)) -> RequestTask {
        return v1CsaTokenGetWithRequestBuilder().execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Get details regarding the CSA token exchange 
     - GET /v1/csa/token
     - Get details regarding the CSA token exchange 
     - Bearer Token:
       - type: http
       - name: Bearer
     - returns: RequestBuilder<CsaToken> 
     */
    open class func v1CsaTokenGetWithRequestBuilder() -> RequestBuilder<CsaToken> {
        let localVariablePath = "/v1/csa/token"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<CsaToken>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Initialize the CSA token exchange 
     
     - parameter jamfNationCredentials: (body) Jamf Nation username and password  (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1CsaTokenPost(jamfNationCredentials: JamfNationCredentials? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: CsaToken?, _ error: Error?) -> Void)) -> RequestTask {
        return v1CsaTokenPostWithRequestBuilder(jamfNationCredentials: jamfNationCredentials).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Initialize the CSA token exchange 
     - POST /v1/csa/token
     - Initializes the CSA token exchange - This will allow Jamf Pro to authenticate with cloud-hosted services 
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter jamfNationCredentials: (body) Jamf Nation username and password  (optional)
     - returns: RequestBuilder<CsaToken> 
     */
    open class func v1CsaTokenPostWithRequestBuilder(jamfNationCredentials: JamfNationCredentials? = nil) -> RequestBuilder<CsaToken> {
        let localVariablePath = "/v1/csa/token"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters = JSONEncodingHelper.encodingParameters(forEncodableObject: jamfNationCredentials)

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<CsaToken>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "POST", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }

    /**
     Re-initialize the CSA token exchange with new credentials 
     
     - parameter jamfNationCredentials: (body) Jamf Nation username and password  (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func v1CsaTokenPut(jamfNationCredentials: JamfNationCredentials? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: CsaToken?, _ error: Error?) -> Void)) -> RequestTask {
        return v1CsaTokenPutWithRequestBuilder(jamfNationCredentials: jamfNationCredentials).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Re-initialize the CSA token exchange with new credentials 
     - PUT /v1/csa/token
     - Re-initialize the CSA token exchange with new credentials 
     - Bearer Token:
       - type: http
       - name: Bearer
     - parameter jamfNationCredentials: (body) Jamf Nation username and password  (optional)
     - returns: RequestBuilder<CsaToken> 
     */
    open class func v1CsaTokenPutWithRequestBuilder(jamfNationCredentials: JamfNationCredentials? = nil) -> RequestBuilder<CsaToken> {
        let localVariablePath = "/v1/csa/token"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters = JSONEncodingHelper.encodingParameters(forEncodableObject: jamfNationCredentials)

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<CsaToken>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "PUT", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: true)
    }
}
